﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationMonobehaviour : MonoBehaviour {

    public static void HandheldVibrate()
    {
        Handheld.Vibrate();
    }

    public static void PeekVibration()
    {
#if UNITY_EDITOR

#elif UNITY_IOS
        if (Vibration.HasVibrator())
            Vibration.VibratePeek();;
#elif UNITY_ANDROID
        if (Vibration.HasVibrator())
            Vibration.Vibrate(50);
#endif
    }

    public static void PopVibration()
    {
#if UNITY_EDITOR

#elif UNITY_IOS
        if (Vibration.HasVibrator())
            Vibration.VibratePop();
#elif UNITY_ANDROID
        if (Vibration.HasVibrator())
            Vibration.Vibrate(200);
#endif
    }

    public static void NopeVibration()
    {
#if UNITY_EDITOR

#elif UNITY_IOS
        if (Vibration.HasVibrator())
            Vibration.VibrateNope();
#endif
    }

    public static void LoadCatScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

}
