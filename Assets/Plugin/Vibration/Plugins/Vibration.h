//
//  Vibration.h
//  https://videogamecreation.fr
//
//  Created by Beno�t Freslon on 23/03/2017.
//  Copyright � 2018 Beno�t Freslon. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Vibration : NSObject

//////////////////////////////////////////

#pragma mark - Vibrate

+ (BOOL)hasVibrator;
+(void)vibrate;
+(void)vibratePeek;
+(void)vibratePop;
+(void)vibrateNope;

//////////////////////////////////////////


@end

