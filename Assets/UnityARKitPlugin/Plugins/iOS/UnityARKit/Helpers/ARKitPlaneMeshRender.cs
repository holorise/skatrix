﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class ARKitPlaneMeshRender : MonoBehaviour {

    [SerializeField] private Material _debugMaterial;

    [SerializeField]
	private MeshFilter meshFilter;
	[SerializeField]
	private LineRenderer lineRenderer;
    private MeshRenderer _renderer;
	private Mesh planeMesh;
    private List<GameObject> _cubesList = new List<GameObject>();
    private ARPlaneAnchor _anchor;

	public void InitiliazeMesh(ARPlaneAnchor arPlaneAnchor)
	{
		planeMesh = new Mesh ();
		UpdateMesh (arPlaneAnchor);
		meshFilter.mesh = planeMesh;
        _renderer = GetComponent<MeshRenderer>();
    }

	public void UpdateMesh(ARPlaneAnchor arPlaneAnchor)
	{
        if (UnityARSessionNativeInterface.IsARKit_1_5_Supported()) //otherwise we cannot access planeGeometry
        {
	        if (arPlaneAnchor.planeGeometry.vertices.Length != planeMesh.vertices.Length || 
	            arPlaneAnchor.planeGeometry.textureCoordinates.Length != planeMesh.uv.Length ||
	            arPlaneAnchor.planeGeometry.triangleIndices.Length != planeMesh.triangles.Length)
	        {
		        planeMesh.Clear();
	        }

            for (int i = 0; i < _cubesList.Count; i++)
            {
                Destroy(_cubesList[i]);
            }
            _cubesList = new List<GameObject>();
            _anchor = arPlaneAnchor;
            planeMesh.vertices = arPlaneAnchor.planeGeometry.vertices;
            planeMesh.uv = arPlaneAnchor.planeGeometry.textureCoordinates;
            planeMesh.triangles = arPlaneAnchor.planeGeometry.triangleIndices;

            lineRenderer.positionCount = arPlaneAnchor.planeGeometry.boundaryVertexCount;
            lineRenderer.SetPositions(arPlaneAnchor.planeGeometry.boundaryVertices);

            // Assign the mesh object and update it.
            planeMesh.RecalculateBounds();
            planeMesh.RecalculateNormals();
        }

	}

    public void SetQuadFromBoundaries()
    {
        var boundaries = _anchor.planeGeometry.boundaryVertices;
        var minX = float.MaxValue;
        var minZ = float.MaxValue;
        var maxX = float.MinValue;
        var maxZ = float.MinValue;
        var posY = boundaries[0].y;
        var length = boundaries.Length;

        for (int i = 0; i < length; i++)
        {
            var boundary = boundaries[i];
            var currX = boundary.x;
            var currZ = boundary.z;

            if(currX > maxX)
            {
                maxX = currX; 
            }
            if (currX < minX)
            {
                minX = currX;
            }

            if (currZ > maxZ)
            {
                maxZ = currZ;
            }
            if (currZ < minZ)
            {
                minZ = currZ;
            }

        }
       
        var quadBoundaries = new Vector3[4];
        quadBoundaries[0] = new Vector3(minX, posY, maxZ);
        quadBoundaries[1] = new Vector3(maxX, posY, maxZ);
        quadBoundaries[2] = new Vector3(maxX, posY, minZ);
        quadBoundaries[3] = new Vector3(minX, posY, minZ);
        lineRenderer.positionCount = 4;
        lineRenderer.SetPositions(quadBoundaries);
        //var uvs = new Vector2[4];

        //uvs[0] = new Vector2(0, 1);
        //uvs[0] = new Vector2(1, 1);
        //uvs[0] = new Vector2(0, 0);
        //uvs[0] = new Vector2(1, 0);

        //var tris = new int[6];
        //tris[0] = 0;
        //tris[1] = 1;
        //tris[2] = 2;
        //tris[3] = 2;
        //tris[4] = 1;
        //tris[5] = 3;
        //var mesh = new Mesh();
        //_renderer.material = _debugMaterial;
        //mesh.vertices = quadBoundaries;
        //mesh.uv = uvs;
        //mesh.triangles = tris;
        //meshFilter.mesh = mesh;

        var centerPose = new Vector3((maxX - minX) / 2 + minX, posY, (maxZ - minZ) / 2 + minZ);
        transform.GetChild(0).localPosition = centerPose;
        //length = _anchor.planeGeometry.vertices.Length + 4;

        //var updatedVerticies = new Vector3 [length];
        //_anchor.planeGeometry.vertices.CopyTo(updatedVerticies,0);
        //quadBoundaries.CopyTo(updatedVerticies, 0);

        //planeMesh.vertices = quadBoundaries;
        //planeMesh.RecalculateBounds();
        //planeMesh.RecalculateNormals();

    }

    private void CreateDebugCube(Vector3 position, Color color)
    {
        var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        go.transform.position = position;
        go.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        _cubesList.Add(go);
        //var material = new Material(_debugMaterial);
        //material.color = color;
        //go.GetComponent<MeshRenderer>().material = material;
    }

    void PrintOutMesh()
	{
		string outputMessage = "\n";
		outputMessage += "Vertices = " + planeMesh.vertices.GetLength (0);
		outputMessage += "\nVertices = [";
		foreach (Vector3 v in planeMesh.vertices) {
			outputMessage += v.ToString ();
			outputMessage += ",";
		}
		outputMessage += "]\n Triangles = " + planeMesh.triangles.GetLength (0);
		outputMessage += "\n Triangles = [";
		foreach (int i in planeMesh.triangles) {
			outputMessage += i;
			outputMessage += ",";
		}
		outputMessage += "]\n";
		Debug.Log (outputMessage);

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
