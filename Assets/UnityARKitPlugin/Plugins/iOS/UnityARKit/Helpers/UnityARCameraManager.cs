

#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using SKTRX.Managers;
using SKTRX.AR;

public class UnityARCameraManager : MonoBehaviour {

    public Camera m_camera;
    private UnityARSessionNativeInterface m_session;
    private Material savedClearMaterial;

    [Header("AR Config Options")]
    public UnityARAlignment startAlignment = UnityARAlignment.UnityARAlignmentGravity;
    public UnityARPlaneDetection planeDetection = UnityARPlaneDetection.Horizontal;
    public bool getPointCloud = true;
    public bool enableLightEstimation = true;
    public bool enableAutoFocus = true;
	public UnityAREnvironmentTexturing environmentTexturing = UnityAREnvironmentTexturing.UnityAREnvironmentTexturingNone;

    [Header("Image Tracking")]
    public ARReferenceImagesSet detectionImages = null;
    public int maximumNumberOfTrackedImages = 0;

    [Header("Object Tracking")]
    public ARReferenceObjectsSetAsset detectionObjects = null;
    private bool sessionStarted = false;

    private WorldAnchorMapManager _worldMapManager;

    public ARKitWorldTrackingSessionConfiguration sessionConfiguration
    {
        get
        {
            ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
            config.planeDetection = planeDetection;
            config.alignment = startAlignment;
            config.getPointCloudData = getPointCloud;
            config.enableLightEstimation = enableLightEstimation;
            config.enableAutoFocus = enableAutoFocus;
            config.maximumNumberOfTrackedImages = maximumNumberOfTrackedImages;
            config.environmentTexturing = environmentTexturing;
            if (detectionImages != null)
                config.referenceImagesGroupName = detectionImages.resourceGroupName;

			if (detectionObjects != null) 
			{
				config.referenceObjectsGroupName = "";  //lets not read from XCode asset catalog right now
                config.dynamicReferenceObjectsPtr = m_session.CreateNativeReferenceObjectsSet(detectionObjects.LoadReferenceObjectsInSet());
			}

            return config;
        }
    }

    void CreatedScannedObjects()
    {

    }

    // Use this for initialization
    public void Start () {
        Debug.unityLogger.logEnabled = false;
        m_session = UnityARSessionNativeInterface.GetARSessionNativeInterface();
        Application.targetFrameRate = 60;
        _worldMapManager = GetComponent<WorldAnchorMapManager>();
        var config = sessionConfiguration;

        if (_worldMapManager != null)
        {
            _worldMapManager.Load();

            if(_worldMapManager.WorldMap != null)
            {
                Debug.Log("@@@ World map loaded");
                config.worldMap = _worldMapManager.WorldMap;
            }
        }

        if (config.IsSupported) {

            if(_worldMapManager != null && _worldMapManager.WorldMap != null)
            {
                m_session.RunWithConfigAndOptions(config, _worldMapManager.mapRunOptions);
            }
            else
            {
                m_session.RunWithConfig(config);
            }

            UnityARSessionNativeInterface.ARFrameUpdatedEvent += FirstFrameUpdate;
        }

        if (m_camera == null) {
            m_camera = Camera.main;
        }
    }

    public void TurnOffPlaneDetection()
    {
        planeDetection = UnityARPlaneDetection.None;
        getPointCloud = false;
        ARKitWorldTrackingSessionConfiguration config = sessionConfiguration;

        if (_worldMapManager != null && _worldMapManager.WorldMap != null)
        {
            Debug.Log("@@@ Start session with a world map");

            config.worldMap = _worldMapManager.WorldMap;
            m_session.RunWithConfigAndOptions(config, _worldMapManager.mapRunOptions);
        }
        else
        {
            m_session.RunWithConfig(config);
        }
    }

    public void TurnOnPlaneDetection()
    {
        planeDetection = UnityARPlaneDetection.Horizontal;
        getPointCloud = true;
        ARKitWorldTrackingSessionConfiguration config = sessionConfiguration;

        if (_worldMapManager != null && _worldMapManager.WorldMap != null)
        {
            Debug.Log("@@@ Start session with a world map");

            config.worldMap = _worldMapManager.WorldMap;
            m_session.RunWithConfigAndOptions(config, _worldMapManager.mapRunOptions);
        }
        else
        {
            m_session.RunWithConfig(config);
        }
    }

    public void TurnOnObjScaning()
    {
        ScannedObjectSerializationManager.instance.boxScannerObject.SetActive(true);
        ScannedObjectSerializationManager.instance.pointParticleSystem.SetActive(true);
    }

    public void TurnOffTracking()
    {
        //TurnOffPlaneDetection();
        ScannedObjectSerializationManager.instance.boxScannerObject.SetActive(false);
        ScannedObjectSerializationManager.instance.pointParticleSystem.SetActive(false);
    }

    public void TurnOnObjTracking()
    {
        Debug.Log("@@@ TurnOnObjTracking");
        planeDetection = UnityARPlaneDetection.None;
        ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
        config.planeDetection = planeDetection;
        config.alignment = startAlignment;
        config.getPointCloudData = true;
        config.enableLightEstimation = true;
        config.enableAutoFocus = true;
        config.maximumNumberOfTrackedImages = maximumNumberOfTrackedImages;
        config.environmentTexturing = environmentTexturing;
        if (detectionImages != null)
            config.referenceImagesGroupName = detectionImages.resourceGroupName;
        List<ARReferenceObject> arRefObjList = ScannedObjectSerializationManager.instance.ARReferenceObjects;
        Debug.Log("@@@@ Start detection session ArRefObjCount:" + arRefObjList.Count);

        DetectionObjectManager.instance.CreateScannedObjectAnchor(arRefObjList);

        if (detectionObjects != null)
        {
            arRefObjList.AddRange(detectionObjects.LoadReferenceObjectsInSet());
        }

        Debug.Log("@@@@ Start detection session ArRefObjCount:" + arRefObjList.Count);
        config.referenceObjectsGroupName = "";  //lets not read from XCode asset catalog right now
        config.dynamicReferenceObjectsPtr = m_session.CreateNativeReferenceObjectsSet(arRefObjList);

        if (_worldMapManager != null && _worldMapManager.WorldMap != null)
        {
            config.worldMap = _worldMapManager.WorldMap;
            m_session.RunWithConfigAndOptions(config, _worldMapManager.mapRunOptions);
        }
        else
        {
            m_session.RunWithConfig(config);
        }

        m_session.RunWithConfig(config);
    }

    void OnDestroy()
    {
        m_session.Pause();
    }

    void FirstFrameUpdate(UnityARCamera cam)
    {
        sessionStarted = true;
        UnityARSessionNativeInterface.ARFrameUpdatedEvent -= FirstFrameUpdate;
    }

    public void SetWorldOrigin(Transform originTransform)
    {
        Debug.Log(string.Format("@@@ SetWorldOrigin position: {0} rotation {1}", originTransform.position.ToString(), originTransform.rotation.eulerAngles.ToString()));

        UnityARSessionNativeInterface.GetARSessionNativeInterface().SetWorldOrigin(originTransform);
    }

    public void SetCamera(Camera newCamera)
    {
        if (m_camera != null) {
            UnityARVideo oldARVideo = m_camera.gameObject.GetComponent<UnityARVideo> ();
            if (oldARVideo != null) {
                savedClearMaterial = oldARVideo.m_ClearMaterial;
                Destroy (oldARVideo);
            }
        }
        SetupNewCamera (newCamera);
    }

    private void SetupNewCamera(Camera newCamera)
    {
        m_camera = newCamera;

        if (m_camera != null) {
            UnityARVideo unityARVideo = m_camera.gameObject.GetComponent<UnityARVideo> ();
            if (unityARVideo != null) {
                savedClearMaterial = unityARVideo.m_ClearMaterial;
                Destroy (unityARVideo);
            }
            unityARVideo = m_camera.gameObject.AddComponent<UnityARVideo> ();
            unityARVideo.m_ClearMaterial = savedClearMaterial;
        }
    }

    // Update is called once per frame

    void Update () {
        
        if (m_camera != null && sessionStarted)
        {
            // JUST WORKS!
            Matrix4x4 matrix = m_session.GetCameraPose();
            m_camera.transform.localPosition = UnityARMatrixOps.GetPosition(matrix);
            m_camera.transform.localRotation = UnityARMatrixOps.GetRotation (matrix);

            m_camera.projectionMatrix = m_session.GetCameraProjection ();
        }

    }

}
