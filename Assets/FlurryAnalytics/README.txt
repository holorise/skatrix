-------------------------------------------------------------------------------
Flurry Analytics Plugin
version 1.6.0
-------------------------------------------------------------------------------
Current Android Flurry SDK version 11.0.0
Current iOS Flurry SDK version 8.6.1

For technical support, send your questions to:
kelevra39@gmail.com
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Flurry Analytics Documentation And Site
-------------------------------------------------------------------------------
http://flurry.com
https://developer.yahoo.com/flurry/docs/analytics/

-------------------------------------------------------------------------------
System Requirements
-------------------------------------------------------------------------------
-- Xcode 8 and iOS 8 and higher.
-- Android API level 16 and higher.

-------------------------------------------------------------------------------
UPGRADE STEPS from 1.5.1:
-------------------------------------------------------------------------------

Please remove all modifications from AndroidManifest.xml that you have made for Flurry Analytics plugin.

-------------------------------------------------------------------------------
Requirements for Android:
-------------------------------------------------------------------------------

You need to resolve Google Play Service and Flurry Analtyics dependencies.

Go to the Menu -> Assets -> Play Service Resolver -> Adnroid Resolver and run Resolve.
After that GPGS and FlurryAnalytics library will be added to your Plugins/Android folder.

-------------------------------------------------------------------------------
Usage:
-------------------------------------------------------------------------------
First you need obtain API Key for you application. If you don't have
account yet please register at https://dev.flurry.com/secure/signup.do
Create application and get your API Key.

Integration:

First you need to start session.
No coding is required.

In your first scene, create empty GameObject and add FlurryAnalyticsHelper script.
Fill API keys and thats all, SDK will do the rest.

You can start session directly from code. Add this code at start of your application
KHD.FlurryAnalytics.Instance.StartSession(IOS_API_KEY, ANDROID_API_KEY, true /* set false if you don't want send crash reports to flurry */);

The Flurry SDK automatically transfers the data captured during the session once the SDK determines the session completed.
In case the device is not connected, the data is saved on the device and transferred once the device is connected again.
The SDK manages the entire process. Currently, there is no way for the app to schedule the data transfer.

Important
A unique API Key must be used for each distinct app. Using the same API Key across distinct apps is not supported
and will result in erroneous data being displayed in Flurry Analytics

Custom Events:

To track custom events, simple add this code to your script:
KHD.FlurryAnalytics.Instance.LogEvent("YourEventName");

You can track up to 300 unique Events names for each app. There is no limit on the number of times any event
can be triggered across time. Once you have added Events to your app, Flurry will automatically build
User Paths based on this data, so you can see how a user navigates through your app.
You can also use Events to build conversion funnels and user segments.

You can capture Event parameters (which include the Event itself) with 1 line of code:
KHD.FlurryAnalytics.Instance.LogEventWithParameters("YourEventName",
    new Dictionary<string, string>() {
      { "Param1", "Value1" }
    });

You can also add the dimension of time to any Event that you track.
Flurry will automatically record the duration of the Event and provide you metrics
for the average Event length overall, by session and by user.

// Start timed event.
KHD.FlurryAnalytics.Instance.LogEvent("YourTimedEventName", true);
// End timed event.
KHD.FlurryAnalytics.Instance.EndTimedEvent("YourTimedEventName");

Replicate data to Unity Analytics (Unity 5.2 or newer):

To setup Unity Analytics please follow this guide http://docs.unity3d.com/Manual/UnityAnalyticsOverview.html.
After setup all you need to do is set ReplicateDataToUnityAnalytics in FlurryAnalyticsHelper to true.
Or use this line of code:
KHD.FlurryAnalytics.Instance.replicateDataToUnityAnalytics = true;


-------------------------------------------------------------------------------
Revenue API:
-------------------------------------------------------------------------------
For some reasons API is different for platforms.
iOS:
Just call FlurryAnalyticsIOS.SetIAPReportingEnabled(true) and all your
purchases will be tracked automaticly.
This method should be called after StartSession!
Android:
You need manually track each purchase with FlurryAnalyticsAndroid.LogPayment.

-------------------------------------------------------------------------------
Other:
-------------------------------------------------------------------------------

You can find all unified methods and their description in FlurryAnalytics/Scripts/FlurryAnalytics.cs.
Some methods should be called before StartSession, please read methods summary.

-------------------------------------------------------------------------------
Playmaker support:
-------------------------------------------------------------------------------

To enable/disable PlayMaker support for FlurryAnalytics go to
Window -> Flurry -> Playmaker -> Switch

-------------------------------------------------------------------------------
Plugin structure:
-------------------------------------------------------------------------------
FlurryAnalytics/Scripts
FlurryAnalytics/Example - contains example scene to test plugin.


-------------------------------------------------------------------------------
Android only:
-------------------------------------------------------------------------------
FlurryAnalytics/Scripts/FlurryAnalyticsAndroid.cs

Initialization example:
KHD.FlurryAnalyticsAndroid.Init(ANDROID_API_KEY, true);
KHD.FlurryAnalyticsAndroid.OnStartSession();

-------------------------------------------------------------------------------
iOS only:
-------------------------------------------------------------------------------
FlurryAnalytics/Scripts/FlurryAnalyticsIOS.cs

Initialization example:
FlurryAnalyticsIOS.StartSession(IOS_API_KEY, true);




