///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2016-2018 Aleksei Kuzin
///----------------------------------------------

#if false

using System.Collections.Generic;
using HutongGames.PlayMaker;

namespace KHD {
namespace PlayMaker {

    public static class Utils {

        /// <summary>
        /// PlayMaker action category name.
        /// </summary>
        public const string kActionCategory = "FlurryAnalytics";

        public static Dictionary<string, string> CreateEventParameters(FsmString[] parameters, FsmString[] values) {
            if (parameters == null ||
                values == null ||
                parameters.Length == 0 ||
                values.Length == 0 ||
                parameters.Length != values.Length) {
                return null;
            }
            var dict = new Dictionary<string, string>();
            for (var i = 0; i < parameters.Length; ++i) {
                if (parameters[i].IsNone || values[i].Value == null) {
                    continue;
                }
                dict[parameters[i].Value] = values[i].Value;
            }
            return dict;
        }

        public static void CheckEventParametersCount(
            FsmStateAction action, FsmString[] parameters, FsmString[] values) {

            if (action != null && parameters != null && values != null &&
                (parameters.Length > 10 || values.Length > 10)) {
                action.LogError(string.Format(
                    "Maximum 10 parameters are allowed (parameters.Length={0}). " +
                    "Please reduce the number of parameters.", parameters.Length));
            }
        }

        public static bool IsValidFsmString(FsmString str) {
            return str != null && !str.IsNone && !string.IsNullOrEmpty(str.Value);
        }
    }
}
}

#endif