///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2016-2018 Aleksei Kuzin
///----------------------------------------------

#if false

using UnityEngine;

namespace HutongGames.PlayMaker.Actions {

    [ActionCategory(KHD.PlayMaker.Utils.kActionCategory)]
    [Tooltip("Start Flurry session.\nMust be called before using other FlurryAnalytics actions.")]
    public class FlurryStartSessionAction : FsmStateAction {

#if UNITY_IOS
        [RequiredField]
#endif
        [Tooltip("iOS API key")]
        public FsmString iOSApiKey;

#if UNITY_ANDROID
        [RequiredField]
#endif
        [Tooltip("Android API key")]
        public FsmString androidApiKey;

        [Tooltip("Enable/Disable Flurry SDK debug logs")]
        public FsmBool enableDebugLog;

        [Tooltip("Send errors to Flurry")]
        public FsmBool sendCrashReports;

#if (UNITY_5_2 || UNITY_5_3_OR_NEWER)
        [Tooltip("Enable/disable replication events to UnityAnalytics.")]
        public FsmBool replicateDataToUnityAnalytics;
#endif
        [Tooltip("Enables implicit recording of Apple Store transactions.")]
        public FsmBool iOSIAPReportingEnabled;

        public override void Reset() {
            iOSApiKey = null;
            androidApiKey = null;
            enableDebugLog = false;
            sendCrashReports = true;

#if (UNITY_5_2 || UNITY_5_3_OR_NEWER)
            replicateDataToUnityAnalytics = false;
#endif

            iOSIAPReportingEnabled = false;
        }

        public override void OnEnter() {
            KHD.FlurryAnalytics.Instance.SetDebugLogEnabled(enableDebugLog.Value);

#if (UNITY_5_2 || UNITY_5_3_OR_NEWER)
            KHD.FlurryAnalytics.Instance.replicateDataToUnityAnalytics =
                replicateDataToUnityAnalytics.Value;
#endif

            KHD.FlurryAnalytics.Instance.StartSession(
                iOSApiKey != null ? iOSApiKey.Value : "",
                androidApiKey != null ? androidApiKey.Value : "",
                sendCrashReports.Value);

#if UNITY_IOS
            KHD.FlurryAnalyticsIOS.SetIAPReportingEnabled(iOSIAPReportingEnabled.Value);
#endif
            Finish();
        }
    }
}

#endif
