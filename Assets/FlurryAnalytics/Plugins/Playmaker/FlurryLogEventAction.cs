///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2016-2018 Aleksei Kuzin
///----------------------------------------------

#if false

using UnityEngine;
using KHD.PlayMaker;

namespace HutongGames.PlayMaker.Actions {

    [ActionCategory(KHD.PlayMaker.Utils.kActionCategory)]
    [Tooltip("Records an event specified by eventName")]
    public class FlurryLogEventAction : FsmStateAction {

        [RequiredField]
        [Tooltip("The event name")]
        public FsmString eventName;

        [Tooltip("Is it timed event or not. If it's timed, you must run EndTimedEventAction " +
                 "with the same eventName")]
        public FsmBool isTimed;

        public override void Reset() {
            eventName = "";
            isTimed = false;
        }

        public override void OnEnter() {
            if (Utils.IsValidFsmString(eventName)) {
                KHD.FlurryAnalytics.Instance.LogEvent(eventName.Value, isTimed.Value);
            }

            Finish();
        }
    }
}

#endif
