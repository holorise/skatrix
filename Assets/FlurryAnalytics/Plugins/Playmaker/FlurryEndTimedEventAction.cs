///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2016-2018 Aleksei Kuzin
///----------------------------------------------

#if false

using UnityEngine;
using System.Collections.Generic;
using KHD.PlayMaker;

namespace HutongGames.PlayMaker.Actions {

    [ActionCategory(KHD.PlayMaker.Utils.kActionCategory)]
    [Tooltip("Ends a timed event specified by eventName and optionally updates parameters with parameters.\n" +
             "A maximum of 10 parameter names may be associated with any event.")]
    public class FlurryEndTimedEventAction : FsmStateAction {
        [RequiredField]
        [Tooltip("The event name")]
        public FsmString eventName;

        [CompoundArray("Parameters", "Parameter", "Value")]
        public FsmString[] parameters;

        public FsmString[] values;

        public override void Reset() {
            eventName = "";
            parameters = null;
            values = null;
        }

        public override void OnEnter() {
            if (Utils.IsValidFsmString(eventName)) {
                Utils.CheckEventParametersCount(this, parameters, values);

                // Create parameters.
                var dict = Utils.CreateEventParameters(parameters, values);
                KHD.FlurryAnalytics.Instance.EndTimedEvent(eventName.Value, dict);
            }

            Finish();
        }
    }
}

#endif
