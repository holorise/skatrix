﻿///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2016-2018 Aleksei Kuzin
///----------------------------------------------

using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;

namespace KHD {
namespace PlayMaker {

    public class PlayMakerAssetPostprocessor : UnityEditor.AssetPostprocessor {

        private static string kPlayMakerTypeCheck =
            "HutongGames.PlayMaker.Actions.ActivateGameObject, " +
            "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";
        private static string kPlayMakerFlurryTypeCheck =
            "HutongGames.PlayMaker.Actions.FlurryLogEventAction, " +
            "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null";

        private static string kIgnorePlayMakerKey = "KHD.IgnorePlayMaker";
        private static string kPlayMakerEnabledKey = "KHD.PlayMakerEnabled";

        /// <summary>
        /// Enabled/disabled playmaker.
        /// </summary>
        [MenuItem("Window/FlurryAnalytics/PlayMaker/Switch", false)]
        private static void SwitchPlayMaker() {
            var enabled = false;
            var fail = false;

            var files = new string[] {
                "/FlurryAnalytics/Plugins/Playmaker/FlurryAndroidLogPaymentAction.cs",
                "/FlurryAnalytics/Plugins/Playmaker/FlurryLogEventAction.cs",
                "/FlurryAnalytics/Plugins/Playmaker/FlurryLogEventWithParametersAction.cs",
                "/FlurryAnalytics/Plugins/Playmaker/FlurryStartSessionAction.cs",
                "/FlurryAnalytics/Plugins/Playmaker/Utils.cs",
                "/FlurryAnalytics/Plugins/Playmaker/FlurryEndTimedEventAction.cs",
                "/FlurryAnalytics/Example/FlurryAnalyticsPlayMakerTest.cs"
            };

            var disabledText = "#if false";
            var enabledText = "#if true";

            foreach (var file in files) {
                try {
                    enabled = EnableFile(Application.dataPath + file, disabledText, enabledText);
                } catch {
                    Debug.Log("Failed to enabled/disable:" + file);
                    fail = true;
                }
            }

            if (fail) {
                ResetPrefs();
                Debug.Log("[FlurryAnalyticsPlugin]: Failed to enabled/disable PlayMaker actions.");
            } else if (enabled) {
                EditorPrefs.SetBool(GetPlayMakerEnabledKey(), true);
                Debug.Log("[FlurryAnalyticsPlugin]: enabled PlayMaker actions.");
            } else {
                ResetPrefs();
                Debug.Log("[FlurryAnalyticsPlugin]: disabled PlayMaker actions.");
            }

            AssetDatabase.Refresh();
        }

        /// <summary>
        /// Function will replace #if in the file and returns true if the file was enabled.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="disabledText"></param>
        /// <param name="enabledText"></param>
        /// <returns></returns>
        private static bool EnableFile(string filePath, string disabledText, string enabledText) {
            var enabled = false;

            // Read the whole file.
            var reader = new System.IO.StreamReader(filePath);
            var content = reader.ReadToEnd();
            reader.Close();

            if (content.Contains(disabledText)) {
                enabled = true;
                content = Regex.Replace(content, disabledText, enabledText);
            } else {
                enabled = false;
                content = Regex.Replace(content, enabledText, disabledText);
            }

            // Write the whole file.
            var writer = new System.IO.StreamWriter(filePath);
            writer.Write(content);
            writer.Close();

            return enabled;
        }

        private static string GetIgnorePlayMakerKey() {
            return kIgnorePlayMakerKey + "-" + Application.dataPath;
        }

        private static string GetPlayMakerEnabledKey() {
            return kPlayMakerEnabledKey + "-" + Application.dataPath;
        }

        private static void ResetPrefs() {
            EditorPrefs.DeleteKey(GetIgnorePlayMakerKey());
            EditorPrefs.DeleteKey(GetPlayMakerEnabledKey());
        }

        private static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            var playmakerDetected = System.Type.GetType(kPlayMakerTypeCheck) != null;
            var flurryEnabled =  System.Type.GetType(kPlayMakerFlurryTypeCheck) != null;

            if (!playmakerDetected || flurryEnabled) {
                return;
            }

            if (EditorPrefs.GetBool(GetIgnorePlayMakerKey())) {
                return;
            }

            if (EditorPrefs.GetBool(GetPlayMakerEnabledKey())) {
                return;
            }

            if (EditorUtility.DisplayDialog(
                "FlurryAnalytics: PlayMaker Detected",
                "Do you want to enable PlayMaker Actions for FlurryAnalytics?", "Yes", "No")) {
                EditorPrefs.SetBool(GetPlayMakerEnabledKey(), true);
                SwitchPlayMaker();
            } else {
                EditorPrefs.SetBool(GetIgnorePlayMakerKey(), true);
                Debug.Log("[FlurryAnalyticsPlugin]: To enable PlayMaker support for FlurryAnalytics " +
                            "manualy, simply go to the menu: 'Window/FlurryAnalytics/PlayMaker/Switch'");
            }
        }
    }
}
}
