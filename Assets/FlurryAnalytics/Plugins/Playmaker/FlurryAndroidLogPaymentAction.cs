///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2016-2018 Aleksei Kuzin
///----------------------------------------------

#if false

using UnityEngine;
using System.Collections.Generic;
using KHD.PlayMaker;

namespace HutongGames.PlayMaker.Actions {

    [ActionCategory(KHD.PlayMaker.Utils.kActionCategory)]
    [Tooltip("Records a payment event for Android")]
    public class FlurryAndroidLogPaymentAction : FsmStateAction {

        [RequiredField]
        [Tooltip("The name of the product purchased.")]
        public FsmString productName;

        [RequiredField]
        [Tooltip("The id of the product purchased.")]
        public FsmString productId;

        [RequiredField]
        [Tooltip("The number of products purchased.")]
        public FsmInt quantity;

        [RequiredField]
        [Tooltip("The price of the the products purchased in the given currency.")]
        public FsmFloat price;

        [RequiredField]
        [Tooltip("The currency for the price argument.")]
        public FsmString currency;

        [RequiredField]
        [Tooltip("A unique identifier for the transaction used to make the purchase.")]
        public FsmString transactionId;

        [CompoundArray("Parameters", "Parameter", "Value")]
        public FsmString[] parameters;

        public FsmString[] values;

        public override void Reset() {
            productName = "";
            productId = "";
            quantity = 1;
            price = 0.0f;
            currency = "";
            transactionId = "";
            parameters = null;
            values = null;
        }

        public override void OnEnter() {
#if UNITY_ANDROID
            if (Utils.IsValidFsmString(productName) &&
                Utils.IsValidFsmString(productId) &&
                Utils.IsValidFsmString(currency) &&
                Utils.IsValidFsmString(transactionId) &&
                quantity != null && !quantity.IsNone &&
                price != null && !price.IsNone) {

                KHD.PlayMaker.Utils.CheckEventParametersCount(this, parameters, values);

                // Create parameters.
                var dict = KHD.PlayMaker.Utils.CreateEventParameters(parameters, values);

                KHD.FlurryAnalyticsAndroid.LogPayment(
                    productName.Value,
                    productId.Value,
                    quantity.Value,
                    price.Value,
                    currency.Value,
                    transactionId.Value,
                    dict);
            }
#endif

            Finish();
        }
    }
}

#endif
