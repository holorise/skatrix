///----------------------------------------------
/// Flurry Analytics Plugin
/// Copyright © 2017 Aleksei Kuzin
///----------------------------------------------

#if false

using UnityEngine;
using HutongGames.PlayMaker;

namespace KHD {

    public class FlurryAnalyticsPlayMakerTest : MonoBehaviour {

        public static string kEventLogEvent = "Event_LogEvent";
        public static string kEventLogEventWithParameters = "Event_LogEventWithParameters";
        public static string kEventLogTimedEvent = "Event_LogTimedEvent";
        public static string kEventEndTimedEvent = "Event_EndTimedEvent";
        public static string kEventAndroidPaymentEvent = "Event_AndroidPaymentEvent";

        public PlayMakerFSM fsm;

        private void OnGUI() {
            var index = 0;
            if (Button("Log Event", index++)) {
                fsm.SendEvent(kEventLogEvent);
            }
            if (Button("Log Event Wit Parameters", index++)) {
                fsm.SendEvent(kEventLogEventWithParameters);
            }
            if (Button("Log Timed Event", index++)) {
                fsm.SendEvent(kEventLogTimedEvent);
            }
            if (Button("End Timed Event", index++)) {
                fsm.SendEvent(kEventEndTimedEvent);
            }
#if UNITY_ANDROID
            if (Button("Log Payment", index++)) {
                fsm.SendEvent(kEventAndroidPaymentEvent);
            }
#endif
        }

        private bool Button(string label, int index) {
            var width = Screen.width * 0.8f;
            var height = Screen.height * 0.07f;

            var rect = new Rect((Screen.width - width) * 0.5f,
                                index * height + height * 0.5f,
                                width, height);
            return GUI.Button(rect, label);
        }
    }
}

#endif
