﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.Extensions
{
    public static class Extensions
    {
        public static string FormatCSVString<T>(this T obj) where T : struct
        {
            var str = obj.ToString();
            str = str.Replace(',', '.');
            return str; 
        }
    }
}
