﻿
#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.AR
{
    public class TouchColliderInteraction : MonoBehaviour
    {
        public delegate void StartTrackingObject();
        public StartTrackingObject startTracking;
        public delegate void StopTrackingObject();
        public StopTrackingObject stopTracking;

        public float defaultAlpha = 0.2f;
        public float highlightAlpha = 1.0f;
        public float fadeTime = 1.0f;
        public float delayTime = 0.5f;
        private Renderer _objRenderer;
        // Start is called before the first frame update
        void Start()
        {
#if SKTRX_DEBUG
            defaultAlpha = 0.6f;
            highlightAlpha = 1.0f;
#else
            defaultAlpha = 0.0f;
            highlightAlpha = 0.3f;
#endif

            _objRenderer = GetComponent<Renderer>();
            ResetMaterial();
            Blink();
        }

#if SKTRX_DEBUG
        // Update is called once per frame
        void Update()
        {

            if (touched)
            {
                var touch = GetTouch();

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        DoBegan(touch);
                        break;
                    case TouchPhase.Moved:
                        DoMoved(touch);
                        break;
                }
            }

        }
#endif

        void DoBegan(Touch trouch)
        {
            if (Input.touchCount > 1)
                return;

            if(isRaycastObject)
                TrackingBlink();
        }

        void DoMoved(Touch trouch)
        {
            if (Input.touchCount > 1)
                return;
        }

        public void Blink()
        {
            StartCoroutine(Blink(defaultAlpha, highlightAlpha, fadeTime, delayTime));
        }

        public void TrackingBlink()
        {
            StartCoroutine(TrackingBlink(defaultAlpha, highlightAlpha, fadeTime, delayTime));
        }

        bool touched
        {
            get
            {
                return Input.GetMouseButton(0) || (Input.touchCount > 0);
            }
        }

#if UNITY_EDITOR
        Vector3 m_LastTouchPosition;
#endif

        Touch GetTouch()
        {
#if UNITY_EDITOR
            var touch = new Touch();
            if (Input.GetMouseButtonDown(0))
                touch.phase = TouchPhase.Began;
            else if (Input.GetMouseButton(0) && m_LastTouchPosition != Input.mousePosition)
                touch.phase = TouchPhase.Moved;
            else
                touch.phase = TouchPhase.Stationary;

            touch.position = Input.mousePosition;
            m_LastTouchPosition = touch.position;
            return touch;
#else
            return Input.GetTouch(0);
#endif
        }

        bool isRaycastObject
        {
            get
            {
                Ray raycast = Camera.main.ScreenPointToRay(GetTouch().position);
                RaycastHit raycastHit;

                if (Physics.Raycast(raycast, out raycastHit))
                {
                    if (raycastHit.collider.gameObject == gameObject)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        bool isExistRenderer
        {
            get
            {
                if (_objRenderer == null)
                {
                    Debug.Log("@@@ GameObject " + gameObject.name + " rendered is NULL");

                    return false;
                }

                return true;
            }
        }

        void ResetMaterial()
        {
            if (!isExistRenderer) return;

            Color objColor = _objRenderer.material.color;
            Color newColor = new Color(objColor.r, objColor.g, objColor.b, 0.0f);
            _objRenderer.material.color = newColor;
        }


        private IEnumerator Blink(float minAlpha, float maxAlpha, float fade, float delay)
        {
            yield return StartCoroutine(FadeTo(maxAlpha, fade));
            yield return new WaitForSeconds(delay);
            yield return StartCoroutine(FadeTo(minAlpha, fade));
        }

        private IEnumerator TrackingBlink(float minAlpha, float maxAlpha, float fade, float delay)
        {
            if (startTracking != null)
                startTracking.Invoke();

            yield return StartCoroutine(FadeTo(maxAlpha, fade));
            yield return new WaitForSeconds(delay);
            yield return StartCoroutine(FadeTo(minAlpha, fade));

            if (stopTracking != null)
                stopTracking.Invoke();
        }

        IEnumerator FadeTo(float aValue, float aTime)
        {
            Color objColor = _objRenderer.material.color;
            float alpha = objColor.a;

            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                Color newColor = new Color(objColor.r, objColor.g, objColor.b, Mathf.Lerp(alpha, aValue, t));
                _objRenderer.material.color = newColor;
                yield return null;
            }
        }
    }
}

