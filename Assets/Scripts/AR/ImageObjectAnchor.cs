﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using SKTRX.Managers;


namespace SKTRX.AR
{
    public class ImageObjectAnchor : MonoBehaviour
    {
        public delegate void DidUpdateColliderPosition(Vector3 colliderTransfor);
        public DidUpdateColliderPosition updateWorldPlane;

        [SerializeField]
        private ARReferenceImage _referenceImage;

        [SerializeField]
        private GameObject _prefabToGenerate;

        [SerializeField]
        private Vector3 _size = Vector3.zero;

        [SerializeField]
        private Vector3 _anchor = new Vector3(0.5f, 1.0f, 0.5f);

        [SerializeField]
        private bool _isNeedUpdate = true;

        [SerializeField]
        private bool _xRotation = false;

        [SerializeField]
        private bool _yRotation = true;

        [SerializeField]
        private bool _zRotation = false;

        [SerializeField]
        private bool _touchEnabled = true;


        private GameObject _createdObject;



        // Use this for initialization
        void Start()
        {
            /*
            if(_touchEnabled)
            {
                _isNeedUpdate = false;
            }
            else
            {
                _isNeedUpdate = true;
            }
            */

            UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;

            
        }

        public bool IsNeedUpdate
        {
            get { return _isNeedUpdate; }
            set
            {
                _isNeedUpdate = value;
            }
        }

        PredefinedCollidersSize predefinedCollidersSize
        {
            get
            {
                if(_createdObject == null)
                {
                    Debug.Log("@@@createdObject is null with Name:" + gameObject.name);

                    return null;
                }
                else
                {
                    PredefinedCollidersSize componentScript = _createdObject.GetComponent<PredefinedCollidersSize>();

                    if(componentScript == null)
                    {
                        Debug.Log("@@@predefinedCollidersSize is null with Name:" + gameObject.name);

                        return null;
                    }

                    return componentScript;
                }
            }
        }

        void CreateObjectCollider(ARImageAnchor arImageAnchor)
        {
            Vector3 position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

            if(updateWorldPlane != null)
            {
                updateWorldPlane.Invoke(position);
            }

            rotation.eulerAngles = CheckRotation(rotation.eulerAngles, Vector3.zero);

            _createdObject = Instantiate<GameObject>(_prefabToGenerate, position, rotation);
            Transform planeAnchor = PlaneDetectionManager.Instance.GetParentTransform();

            if (planeAnchor != null)
            {
                _createdObject.transform.SetParent(planeAnchor);
            }

           //IOSARManager.Instance.SetWorldOrigin(_createdObject.transform);

            PredefinedCollidersSize sizeScript = predefinedCollidersSize;

            if (sizeScript != null)
            {
                sizeScript.Anchor = _anchor;
                sizeScript.Size = _size;
                TouchColliderInteraction touchCollider = sizeScript.GetTouchColliderInteraction();

                if(touchCollider != null && _touchEnabled)
                {
                    touchCollider.stopTracking = StopTracking;
                    touchCollider.startTracking = StartTracking;
                }
                else if(touchCollider != null && !_touchEnabled)
                {
                    //touchCollider.enabled = false;
                }
            }
        }

        private Vector3 CheckRotation(Vector3 objEuler, Vector3 planeEuler)
        {
            if(!_xRotation)
            {
                objEuler.x = planeEuler.x;
            }

            if (!_yRotation)
            {
                objEuler.y = planeEuler.y;
            }

            if (!_zRotation)
            {
                objEuler.z = planeEuler.z;
            }

            return objEuler;
        }

        void UpdateObjectCollider(ARImageAnchor arImageAnchor)
        {
            if (!_createdObject.activeSelf)
            {
                _createdObject.SetActive(true);
            }

            Vector3 targetPosition = UnityARMatrixOps.GetPosition(arImageAnchor.transform);

            if (updateWorldPlane != null)
            {
                updateWorldPlane.Invoke(_createdObject.transform.position);
            }

            Quaternion rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);
            rotation.eulerAngles = CheckRotation(rotation.eulerAngles, Vector3.zero);
            _createdObject.transform.rotation = rotation;
            //IOSARManager.Instance.SetWorldOrigin(_createdObject.transform);

            PredefinedCollidersSize sizeScript = predefinedCollidersSize;

            if (sizeScript != null)
            {
                sizeScript.Anchor = _anchor;
                sizeScript.Size = _size;
                sizeScript.DidUpdatePosition();
            }

            StopCoroutine(SmoothUpdatePosition(_createdObject.transform, targetPosition, 0.5f));
            StartCoroutine(SmoothUpdatePosition(_createdObject.transform, targetPosition, 0.5f));
        }

        void AddImageAnchor(ARImageAnchor arImageAnchor)
        {
            if (arImageAnchor.referenceImageName == _referenceImage.imageName && _createdObject == null)
            {
                CreateObjectCollider(arImageAnchor);
            }
        }

        void UpdateImageAnchor(ARImageAnchor arImageAnchor)
        {
            if (arImageAnchor.referenceImageName == _referenceImage.imageName)
            {
                if (_createdObject != null && arImageAnchor.isTracked && _isNeedUpdate) 
                {
                    UpdateObjectCollider(arImageAnchor);
                }
                else if(arImageAnchor.isTracked && _createdObject == null)
                {
                    CreateObjectCollider(arImageAnchor);
                }
            }

        }

        void RemoveImageAnchor(ARImageAnchor arImageAnchor)
        {
            if (_createdObject)
            {
                GameObject.Destroy(_createdObject);
            }

        }

        IEnumerator SmoothUpdatePosition(Transform objTransform, Vector3 targetPosition, float aTime)
        {
            Vector3 pos  = objTransform.position;

            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
            {
                Vector3 newPosition = Vector3.Lerp(pos, targetPosition, t);
                objTransform.position = newPosition;
                yield return null;
            }
        }

        void OnDestroy()
        {

            if(_createdObject != null)
            {
                Destroy(_createdObject);
                _createdObject = null;
            }

            UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;

        }

        // Update is called once per frame
        void Update()
        {

        }

        void StartTracking()
        {
            _isNeedUpdate = true;
        }

        void StopTracking()
        {
            _isNeedUpdate = true;
        }
    }
}


