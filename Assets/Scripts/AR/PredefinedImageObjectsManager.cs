﻿

#pragma warning disable 649
using System.Collections.Generic;
using UnityEngine;
using SKTRX.AR;

namespace SKTRX.Managers
{
    public class PredefinedImageObjectsManager : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> _predefinedObjectsPrefabsList;

        [SerializeField]
        private List<GameObject> _prefefinedRefObjPrefabsList;

        [SerializeField]
        private bool _isCreateOnAwake = false;


        private List<GameObject> _predefinedImgObjList;
        private List<GameObject> _predefinedRefObjList;

        private void Start()
        {
            if(_isCreateOnAwake)
            {
                CreateAllObjects();
            }
        }

        public void DestroyAllObjects()
        {
            DestroyAllImageObjects();
            DestroyAllRefObjects();
        }

        public void DestroyAllImageObjects()
        {
            if (_predefinedImgObjList == null) return;

            foreach (GameObject obj in _predefinedImgObjList)
            {
                Destroy(obj);
            }
        }


        public void DestroyAllRefObjects()
        {
            if (_predefinedRefObjList == null) return;

            foreach (GameObject obj in _predefinedRefObjList)
            {
                Destroy(obj);
            }
        }

        public void CreateAllObjects()
        {
            CreateAllImageObjects();
            StartTrackingAllPredefinedObjects();
            CreateAllRefObjects();
        }

        public void CreateAllImageObjects()
        {
            if (_predefinedImgObjList == null)
            {
                _predefinedImgObjList = new List<GameObject>(_predefinedObjectsPrefabsList.Count);
            }
            else if (_predefinedImgObjList != null && _predefinedImgObjList.Count > 0)
            {
                DestroyAllObjects();
            }

            foreach (GameObject objPrefab in _predefinedObjectsPrefabsList)
            {
                GameObject obj = Instantiate<GameObject>(objPrefab, Vector3.zero, Quaternion.identity);
                obj.transform.parent = gameObject.transform;
                _predefinedImgObjList.Add(obj);
            }
        }

        public void CreateAllRefObjects()
        {
            if (_predefinedRefObjList == null)
            {
                _predefinedRefObjList = new List<GameObject>(_prefefinedRefObjPrefabsList.Count);
            }
            else if (_predefinedRefObjList != null && _predefinedRefObjList.Count > 0)
            {
                DestroyAllRefObjects();
            }

            foreach (GameObject objPrefab in _prefefinedRefObjPrefabsList)
            {
                GameObject obj = Instantiate<GameObject>(objPrefab, Vector3.zero, Quaternion.identity);
                obj.transform.parent = gameObject.transform;
                _predefinedRefObjList.Add(obj);
            }
        }

        public void StopTrackingAllPredefinedObjects()
        {
            /*foreach (GameObject obj in _predefinedImgObjList)
            {
                ImageObjectAnchor imgAnchor = obj.GetComponent<ImageObjectAnchor>();

                if(imgAnchor != null)
                {
                    imgAnchor.IsNeedUpdate = false;
                }

            }*/
        }

        public void StartTrackingAllPredefinedObjects()
        {
            /*foreach (GameObject obj in _predefinedImgObjList)
            {
                ImageObjectAnchor imgAnchor = obj.GetComponent<ImageObjectAnchor>();

                if (imgAnchor != null)
                {
                    imgAnchor.IsNeedUpdate = true;
                }

            }*/
        }

        public void SetEnabledAllObjects(bool isEnabled)
        {
            foreach(GameObject obj in _predefinedImgObjList)
            {
                obj.SetActive(isEnabled);
            }
        }
    }
}

