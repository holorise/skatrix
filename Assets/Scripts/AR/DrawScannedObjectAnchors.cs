﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace SKTRX.AR
{
    public class DrawScannedObjectAnchors : MonoBehaviour
    {
        public GameObject dotPrefab;
        public GameObject centerPrefab;
        private ARReferenceObject _arRefObj;
        private List<GameObject> _dotsList;

        public ARReferenceObject ARReferenceObject
        {
            get { return _arRefObj; }
            set {
                Debug.Log("@@@Set ArRefObj");
            _arRefObj = value;
                InitDots();
            }
        }
        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("@@@DrawScannedObjectAnchors Start");
           // RemoveDots();
           // InitDots();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void InitDots()
        {
            if(_dotsList == null) _dotsList = new List<GameObject>();

            Debug.Log("@@@ InitDots0");

            GameObject center = Instantiate(centerPrefab, Vector3.zero, Quaternion.identity);
            center.transform.parent = gameObject.transform;
            center.transform.localPosition = Vector3.zero;
            _dotsList.Add(center);

            ARPointCloud pointCloud = _arRefObj.pointCloud;

            Debug.Log("@@@ InitDots1");

            if (pointCloud != null)
            {
                Vector3[] points = pointCloud.Points;

                Debug.Log("@@@ Point count:" + points.Length);

                for(int i = 0; i < points.Length; i++)
                {
                    GameObject dot = Instantiate(dotPrefab, Vector3.zero, Quaternion.identity);
                    dot.transform.parent = gameObject.transform;
                    dot.transform.localPosition = UnityARMatrixOps.GetPosition(points[i]) * 1.0f / gameObject.transform.localScale.x;
                    _dotsList.Add(dot);
                }

                Debug.Log("@@@ Dots count:" + _dotsList.Count);
            }
            else
            {
                Debug.Log("@@@ ARPointCloud is null");
            }
        }

        private void RemoveDots()
        {
            if (_dotsList == null) return;

            foreach(GameObject obj in _dotsList)
            {
                Destroy(obj);
            }

            _dotsList.Clear();
        }
    }
}

