﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.Obstacle;


namespace SKTRX.AR
{
    public class PredefinedCollidersSize : MonoBehaviour
    {
        [SerializeField]
        private Vector3 _anchor = new Vector3(0.5f, 1.0f, 0.5f);
        [SerializeField]
        private Vector3 _size = Vector3.zero;
        [SerializeField]
        private GameObject _objCollider;

        public Vector3 Anchor
        {
            get
            {
                return _anchor;
            }

            set
            {
                _anchor = value;
                SetUpAnchor();
            }
        }

        public Vector3 Size
        {
            get
            {
                return _size;
            }

            set
            {
                _size = value;
                SetUpSize();
            }
        }

        public TouchColliderInteraction GetTouchColliderInteraction()
        {
            return _objCollider.GetComponent<TouchColliderInteraction>();
        }

        // Start is called before the first frame update
        void Start()
        {
            SetUpSize();
            SetUpAnchor();
        }

        void SetUpSize()
        {
            if (!isExistObjCollider) return;

            _objCollider.transform.localScale = _size;
        }

        void SetUpAnchor()
        {
            if (!isExistObjCollider) return;

            _objCollider.transform.localPosition = new Vector3(_size.x * (-_anchor.x + 0.5f), _size.y * (-_anchor.y + 0.5f), _size.z * (-_anchor.z + 0.5f));
        }

        bool isExistObjCollider
        {
            get
            {
                if (_objCollider == null)
                {
                    Debug.Log("@@@ GameObject " + gameObject.name + " collider is NULL");

                    return false;
                }

                return true;
            }
        }

        public void DidUpdatePosition()
        {
            if(_objCollider == null)
            {
                Debug.Log("@@@ Object collider is null");
                return;
            }

            RealObstacle realObstacleCopm = _objCollider.GetComponent<RealObstacle>();

            if(realObstacleCopm != null)
            {
                realObstacleCopm.DidUpdatePosition();
            }
        }
    }
}

