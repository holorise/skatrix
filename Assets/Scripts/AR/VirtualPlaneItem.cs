﻿

#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using SKTRX.Statics;

namespace SKTRX.AR
{
    public class VirtualPlaneItem : MonoBehaviour
    {
        //TODO Refactor functions
        private const float SIZE_Z = 2f;
        private const float SIZE_Y = 2f;//50f;
        //For the scale 0.05 for both objects
        private const float COLLISION_DELTA = 0f;
        private const float CORNER_PIPE_SIZE = 0.125f;
        private const float PIPE_SIZE = 0.075f;
        private const float PIPE_SCALE_DELTA = 0.25f;
        private const float PIPE_SCALE_KOEF = 1.82f;
        private const float DIFF_PIPES_OFFSET = 0.0365f;

        [SerializeField] private GameObject _generalRamp;
        [SerializeField] private GameObject _cornerRamp;
        [SerializeField] private Material _debugMaterial;
        [SerializeField] private BoxCollider _planeCollider;

        private List<Material> _materials;
        [SerializeField] private BoxCollider _rightCollider;
        [SerializeField] private BoxCollider _leftCollider;
        [SerializeField] private BoxCollider _topCollider;
        [SerializeField] private BoxCollider _botCollider;
        private bool _isFaded = true;

        private void SetColliders()
        {

        }

        private void Awake()
        {
            _materials = new List<Material>();

            //SetScale(new Vector3(1, 0, 2));
            //ResizeColliders();

        }

        public void FadeInBoundaries()
        {
            if (!_isFaded)
                return;

            var length = _materials.Count;
            for (int i = 0; i < length; i++)
            {
                var material = _materials[i];
                material.DOFade(1, 0.5f);
            }
            _isFaded = false;
        }

        public void FadeOutBoundaries()
        {
            if (_isFaded)
                return;

            var length = _materials.Count;
            for (int i = 0; i < length; i++)
            {
                var material = _materials[i];
                material.DOFade(0, 0);
            }

            _isFaded = true;
        }

        private void Update()
        {
            if(Input.GetKey(KeyCode.A))
            {
                FadeOutBoundaries(); 
            }
            if (Input.GetKey(KeyCode.D))
            {
                FadeInBoundaries();
            }
        }

        private  void SetCornerQuarterPipe(Vector3 position, Vector3 angles)
        {
            var cornerGo = Instantiate(_cornerRamp, transform);

            var materials = cornerGo.GetComponent<MeshRenderer>().sharedMaterials.ToList();
            _materials.AddRange(materials);
            cornerGo.transform.localPosition = position;
            cornerGo.transform.eulerAngles = angles;
        }

        private void SetCommonQuarterPipe(Vector3 position, Vector3 angles, float scaleZ)
        {
            var commonGo = Instantiate(_generalRamp, transform);

            var materials = commonGo.GetComponent<MeshRenderer>().sharedMaterials.ToList();
            _materials.AddRange(materials);
            commonGo.transform.localPosition = position;
            commonGo.transform.eulerAngles = angles;
            var scaleGo = commonGo.transform.localScale;
            scaleGo.z = scaleZ;
            commonGo.transform.localScale = scaleGo;
        }

        [ContextMenu ("Update boundarys")]
        private void SetBordersQuarterpipes()
        {
            var scale = _planeCollider.transform.localScale;
            var angles = transform.eulerAngles;
              
            var pos = new Vector3(scale.x / 2 + CORNER_PIPE_SIZE / 2 - DIFF_PIPES_OFFSET, scale.y/2,scale.z/2 + CORNER_PIPE_SIZE / 2 - DIFF_PIPES_OFFSET);
            var angle = new Vector3(angles.x, -90 + angles.y, angles.z);
            SetCornerQuarterPipe(pos, angle);

            pos = new Vector3(-(scale.x / 2) - CORNER_PIPE_SIZE / 2 + DIFF_PIPES_OFFSET, scale.y / 2, scale.z / 2 + CORNER_PIPE_SIZE / 2 - DIFF_PIPES_OFFSET);
            angle = new Vector3(angles.x, 180 + angles.y, angles.z);
            SetCornerQuarterPipe(pos, angle);

            pos =  new Vector3(scale.x / 2 + CORNER_PIPE_SIZE / 2 - DIFF_PIPES_OFFSET , scale.y / 2, -(scale.z / 2) - CORNER_PIPE_SIZE / 2 + DIFF_PIPES_OFFSET);
            angle = new Vector3(angles.x, 0 + angles.y, angles.z);
            SetCornerQuarterPipe(pos, angle);

            pos = new Vector3(-(scale.x / 2) - CORNER_PIPE_SIZE / 2 + DIFF_PIPES_OFFSET, scale.y / 2, -(scale.z / 2) - CORNER_PIPE_SIZE / 2 + DIFF_PIPES_OFFSET);
            angle = new Vector3(angles.x, 90 + angles.y, angles.z);
            SetCornerQuarterPipe(pos, angle);

            pos = new Vector3(0, scale.y / 2, scale.z / 2 + PIPE_SIZE);
            angle = new Vector3(angles.x, -90 + angles.y, angles.z);
            var scaleZ = scale.x / PIPE_SCALE_KOEF;// - PIPE_SCALE_DELTA / PIPE_SCALE_KOEF;
            SetCommonQuarterPipe(pos, angle, scaleZ);

            pos = new Vector3(0, scale.y / 2, -(scale.z / 2) - PIPE_SIZE);
            angle = new Vector3(angles.x, 90 + angles.y, angles.z);
            scaleZ = scale.x / PIPE_SCALE_KOEF;// - PIPE_SCALE_DELTA / PIPE_SCALE_KOEF;
            SetCommonQuarterPipe(pos, angle, scaleZ);

            pos = new Vector3(-(scale.x / 2) - PIPE_SIZE, scale.y / 2, 0);
            angle = new Vector3(angles.x, 180 + angles.y, angles.z);
            scaleZ = scale.z / PIPE_SCALE_KOEF;// - PIPE_SCALE_DELTA / PIPE_SCALE_KOEF;
            SetCommonQuarterPipe(pos, angle, scaleZ);

            pos = new Vector3(scale.x / 2 + PIPE_SIZE, scale.y / 2, 0);
            angle = new Vector3(angles.x, 0 + angles.y, angles.z);
            scaleZ = scale.z / PIPE_SCALE_KOEF; //- PIPE_SCALE_DELTA / PIPE_SCALE_KOEF;
            SetCommonQuarterPipe(pos, angle, scaleZ);

            //var length = _materials.Count;
            //for (int i = 0; i < length; i++)
            //{
            //    var material = _materials[i];
            //    material.DOFade(0, 0);
            //}
        }

        public void SetScale(Vector3 scale)
        {
           _planeCollider.transform.localScale = scale;
            //SetBordersQuarterpipes();
            //FadeInBoundaries();
           //DOVirtual.DelayedCall(1, FadeOutBoundaries);
        }

        public void ResizeColliders()
        {
            SetColliders();

            var scale = _planeCollider.transform.localScale;

            var size = _rightCollider.size;
            _rightCollider.transform.localPosition = new Vector3(0, 0, scale.z / 2 + SIZE_Z/2 - COLLISION_DELTA);
            size.z = SIZE_Z;
            size.y = SIZE_Y;
            size.x = scale.x;
            _rightCollider.transform.localScale = size;

            size = _leftCollider.size;
            _leftCollider.transform.localPosition = new Vector3(0, 0, scale.z / -2 - SIZE_Z / 2 + COLLISION_DELTA);
            size.z = SIZE_Z;
            size.y = SIZE_Y;
            size.x = scale.x;
            _leftCollider.transform.localScale = size;

            size = _topCollider.size;
            _topCollider.transform.localPosition = new Vector3(scale.x / 2 + SIZE_Z / 2 - COLLISION_DELTA, 0, 0);
            size.x = SIZE_Z;
            size.y = SIZE_Y;
            size.z = scale.z;
            _topCollider.transform.localScale = size;

            size = _botCollider.size;
            _botCollider.transform.localPosition = new Vector3(scale.x / -2 - SIZE_Z / 2 + COLLISION_DELTA, 0, 0);
            size.x = SIZE_Z;
            size.y = SIZE_Y;
            size.z = scale.z;
            _botCollider.transform.localScale = size;

        }
    }
}
