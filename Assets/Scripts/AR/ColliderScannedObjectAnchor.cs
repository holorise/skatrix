﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace SKTRX.AR
{
    public class ColliderScannedObjectAnchor : MonoBehaviour
    {

        //[SerializeField]
        public string scannedObjectName;

        [SerializeField]
        private GameObject prefabToGenerate;

        [SerializeField]
        private ARReferenceObjectAsset _objReference;

        [SerializeField]
        private bool _xRotation = false;

        [SerializeField]
        private bool _yRotation = true;

        [SerializeField]
        private bool _zRotation = false;

        [SerializeField]
        private bool _drawAnchors = false;

        [SerializeField]
        private Vector3 _size = new Vector3(0.2f, 0.08f, 0.225f);

        [SerializeField]
        private Vector3 _anchor = new Vector3(0.5f, 0.0f, 0.5f);

        [SerializeField]
        private bool _isNeedUpdate = true;

        [SerializeField]
        private bool _touchEnabled = true;

        private ARReferenceObject _arRefObj;

        private GameObject _createdObject;



        // Use this for initialization
        void Start()
        {
            if(_objReference != null && _arRefObj == null)
            {
                _arRefObj = _objReference.LoadReferenceObject();
            }

            UnityARSessionNativeInterface.ARObjectAnchorAddedEvent += AddObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorUpdatedEvent += UpdateObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorRemovedEvent += RemoveObjectAnchor;

        }

        private Vector3 CheckRotation(Vector3 objEuler, Vector3 planeEuler)
        {
            if (!_xRotation)
            {
                objEuler.x = planeEuler.x;
            }

            if (!_yRotation)
            {
                objEuler.y = planeEuler.y;
            }

            if (!_zRotation)
            {
                objEuler.z = planeEuler.z;
            }

            return objEuler;
        }

        private void CreateObject(ARObjectAnchor arObjectAnchor)
        {
            Vector3 position = UnityARMatrixOps.GetPosition(arObjectAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arObjectAnchor.transform);
            rotation.eulerAngles = CheckRotation(rotation.eulerAngles, Vector3.zero);

            _createdObject = Instantiate<GameObject>(prefabToGenerate, position, rotation);

            ColliderEditor editComponent = _createdObject.GetComponent<ColliderEditor>();

            if (_arRefObj != null && editComponent != null)
            {
                editComponent.nameScannedObject = scannedObjectName;
                editComponent.ARReferenceObject = _arRefObj;
            }
            else
            {
                Debug.Log("@@@ ColliderEditor is null");
            }

            PredefinedCollidersSize sizeComponent = _createdObject.GetComponent<PredefinedCollidersSize>();

            if (sizeComponent != null)
            {
                sizeComponent.Anchor = _anchor;
                sizeComponent.Size = _size;
            }

            DrawScannedObjectAnchors drawAnchorsComponent = _createdObject.GetComponent<DrawScannedObjectAnchors>();

            if (_arRefObj != null && drawAnchorsComponent != null && _drawAnchors)
            {
                drawAnchorsComponent.ARReferenceObject = _arRefObj;
            }
            else
            {
                Debug.Log("@@@ ARReferenceObject or DrawScannedObjectAnchors is null");
            }
        }

        private void UpdateObject(ARObjectAnchor arObjectAnchor)
        { 
            Vector3 position = UnityARMatrixOps.GetPosition(arObjectAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arObjectAnchor.transform);
            rotation.eulerAngles = CheckRotation(rotation.eulerAngles, Vector3.zero);
        }

        void AddObjectAnchor(ARObjectAnchor arObjectAnchor)
        {
            if (arObjectAnchor.referenceObjectName == scannedObjectName)
            {
                CreateObject(arObjectAnchor);
            }
        }

        void UpdateObjectAnchor(ARObjectAnchor arObjectAnchor)
        {
            if (arObjectAnchor.referenceObjectName == scannedObjectName)
            {
                if(_createdObject == null)
                {
                    CreateObject(arObjectAnchor);
                }
                else
                {
                    UpdateObject(arObjectAnchor);
                }
            }

        }

        void RemoveObjectAnchor(ARObjectAnchor arObjectAnchor)
        {
            if ((arObjectAnchor.referenceObjectName == scannedObjectName) && (_createdObject != null))
            {
                GameObject.Destroy(_createdObject);
            }
        }

        void OnDestroy()
        {
            if(_createdObject != null)
            {
                Destroy(_createdObject);

            }

            UnityARSessionNativeInterface.ARObjectAnchorAddedEvent -= AddObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorUpdatedEvent -= UpdateObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorRemovedEvent -= RemoveObjectAnchor;

        }

        public GameObject CreatedObject
        {
            get { return _createdObject; }
            set { _createdObject = value; }
        }

        public ARReferenceObject ARReferenceObject
        {
            get { return _arRefObj; }
            set { _arRefObj = value; }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

