﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using System.IO;
using SKTRX.AR.SerializableObjects;
using SKTRX.Managers;

public class ARRefObjectsSetAsset : ScriptableObject {

	public string resourceGroupName;
    public bool isEditCollider = false;

	public List<ARReferenceObject> LoadReferenceObjectsInSet()
	{
		List<ARReferenceObject> listRefObjects = new List<ARReferenceObject> ();

		if (UnityARSessionNativeInterface.IsARKit_2_0_Supported() == false)
		{
			return listRefObjects;
		}

        SerializableScanObject objData = ScannedObjectSerializationManager.instance.EditScannedObject();

        if(objData != null)
        {
            string filePath = objData.filePath;

            if(File.Exists(filePath))
            {
                ARReferenceObject arro = ARReferenceObject.Load(filePath);
                arro.name = objData.name;
                listRefObjects.Add(arro);
            }
        }
        else
        {
            Debug.Log("@@@ SKTRXScanObject is null");
        }

        return listRefObjects;
	}

}