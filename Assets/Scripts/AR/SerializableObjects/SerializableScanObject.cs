﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.AR.SerializableObjects
{
    [System.Serializable]
    public class SerializableScanObject
    {
        public enum ScanType
        {
            predefined = 0,
            scanned = 1
        }

        public enum ColliderShape
        {
            Cube = 0,
            Sphere = 1,
            Cylinder = 2
        }

        public int index;
        public string name;
        public ScanType scanType = ScanType.scanned;
        public string filePath;
        public ColliderShape shapeType = ColliderShape.Cube;
        public SerializableVector3 scale;
        public SerializableVector3 position;
        public SerializableVector3 rotation;

        public SerializableScanObject()
        {
            shapeType = ColliderShape.Cube;
            scanType = ScanType.scanned;
            scale = new SerializableVector3(1, 1, 1);
            position = new SerializableVector3(0, 0, 0);
            rotation = new SerializableVector3(0, 0, 0);

        }

        public static SerializableScanObject CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<SerializableScanObject>(jsonString);
        }

        public string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }

}
