﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.AR.SerializableObjects
{
    [System.Serializable]
    public class SerializableVector3
    {
        public float x = 0.0f;
        public float y = 0.0f;
        public float z = 0.0f;

        public SerializableVector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public SerializableVector3(float x1, float y1, float z1)
        {
            x = x1;
            y = y1;
            z = z1;
        }

        public static SerializableVector3 CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<SerializableVector3>(jsonString);
        }

        public string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
