﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.AR.SerializableObjects
{
    [System.Serializable]
    public class SerializableScanObjectList
    {
        public List<SerializableScanObject> objList;

        public SerializableScanObjectList()
        {
            objList = new List<SerializableScanObject>();
        }

        public static SerializableScanObjectList CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<SerializableScanObjectList>(jsonString);
        }

        public string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
