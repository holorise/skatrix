﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.AR.SerializableObjects;
using SKTRX.AR;

namespace SKTRX.Managers
{
    public class ScannedObjectDetectionManager : MonoBehaviour
    {
        public static ScannedObjectDetectionManager instance = null;

        public GameObject prefabScannedObj;
        private List<GameObject> _scannedObjList;
        private SerializableScanObject _arScanObjData;

        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                Destroy(gameObject);

            //Call the Init function 
            Init();
        }

        //Initializes the class.
        void Init()
        {
            _scannedObjList = new List<GameObject>();

            CreateScannedObjectAnchor();
        }

        void CreateScannedObjectAnchor()
        {
            List<SerializableScanObject> scannedObjDataList = ScannedObjectSerializationManager.instance.GetScannedObjectsList();

            foreach(SerializableScanObject objData in scannedObjDataList)
            {
                GameObject scannedObj = Instantiate(prefabScannedObj, Vector3.zero, Quaternion.identity);

                if (scannedObj != null)
                {
                    scannedObj.GetComponent<GenerateScannedObjectListAnchor>().scannedObjectName = _arScanObjData.name;
                    Debug.Log("@@@Create Scanned Obj:" + scannedObj.GetComponent<GenerateScannedObjectListAnchor>().scannedObjectName);

                    _scannedObjList.Add(scannedObj);
                }
            }


        }
    }

}
