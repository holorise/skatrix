﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.Managers;
using SKTRX.AR.SerializableObjects;
using UnityEngine.XR.iOS;

namespace SKTRX.AR
{
    public class ColliderEditor : MonoBehaviour
    {
        public string nameScannedObject;
        public GameObject cylinderPrefab;
        public GameObject spherePrefab;
        public GameObject cubePrefab;

        private GameObject _collider;
        private SerializableScanObject _scanObjData;
        private ObjCollider _objCollider;
        private ARReferenceObject _arRefObj;


        public ARReferenceObject ARReferenceObject
        {
            get { return _arRefObj; }
            set
            {
                Debug.Log("@@@Set ArRefObj");
                _arRefObj = value;

            }
        }
        // Start is called before the first frame update
        void Start()
        {
            //LoadCollider();
            /*
                        switch (_scanObjData.shapeType)
                        {
                            case SerializableScanObject.ColliderShape.Sphere:
                                CreateSphere();
                                break;

                            case SerializableScanObject.ColliderShape.Cylinder:
                                CreateCylinder();
                                break;

                            default:
                                CreateCube();
                                break;
                        }
                        */

            CreateCube();
        }

        // Update is called once per frame
        void Update()
        {
            if(_arRefObj != null && _objCollider != null)
            {
                _objCollider.scaleX = _arRefObj.extent.x * 1.0f / gameObject.transform.localScale.x * 2.0f;
                _objCollider.scaleY = _arRefObj.extent.y * 1.0f / gameObject.transform.localScale.x * 2.0f;
                _objCollider.scaleZ = _arRefObj.extent.z * 1.0f / gameObject.transform.localScale.x * 2.0f;
            }

            if (_objCollider != null)
            {
                _objCollider.scaleX = _scanObjData.scale.x;
                _objCollider.scaleY = _scanObjData.scale.y;
                _objCollider.scaleZ = _scanObjData.scale.z;

                _objCollider.rotationX = _scanObjData.rotation.x;
                _objCollider.rotationY = _scanObjData.rotation.y;
                _objCollider.rotationZ = _scanObjData.rotation.z;

                _objCollider.positionX = _scanObjData.position.x;
                _objCollider.positionY = _scanObjData.position.y;
                _objCollider.positionZ = _scanObjData.position.z;
            }
        }

        public void SetScaleX(float scaleX)
        {
            _scanObjData.scale.x = scaleX;
        }

        public void SetScaleY(float scaleY)
        {
            _scanObjData.scale.y = scaleY;
        }

        public void SetScaleZ(float scaleZ)
        {
            _scanObjData.scale.z = scaleZ;
        }

        public void SetRotationX(float rotX)
        {
            _scanObjData.rotation.x = rotX;
        }

        public void SetRotationY(float rotY)
        {
            _scanObjData.rotation.y = rotY;
        }

        public void SetRotationZ(float rotZ)
        {
            _scanObjData.rotation.z = rotZ;
        }

        public void SetPositionX(float posX)
        {
            _scanObjData.position.x = posX;
        }

        public void SetPositionY(float posY)
        {
            _scanObjData.position.y = posY;
        }

        public void SetPositionZ(float posZ)
        {
            _scanObjData.position.z = posZ;
        }

        public void CreateCylinder()
        {
            return;
            //if (shapeType == ColliderShape.Cylinder) return;
            Debug.Log("CreateCylinder");
            if (_collider != null) Destroy(_collider);

            _collider = Instantiate(cylinderPrefab, Vector3.zero, Quaternion.identity);
            _collider.transform.parent = gameObject.transform;
            _scanObjData.shapeType = SerializableScanObject.ColliderShape.Cylinder;
            _objCollider = _collider.GetComponent<ObjCollider>();
        }

        public void CreateSphere()
        {
            return;
            //if (shapeType == ColliderShape.Sphere) return;

            if (_collider != null) Destroy(_collider);

            _collider = Instantiate(spherePrefab, Vector3.zero, Quaternion.identity);
            _collider.transform.parent = gameObject.transform;
            _scanObjData.shapeType = SerializableScanObject.ColliderShape.Sphere;
            _objCollider = _collider.GetComponent<ObjCollider>();
        }

        public void CreateCube()
        {
            if (_collider != null) Destroy(_collider);

            _collider = Instantiate(cubePrefab, Vector3.zero, Quaternion.identity);
            _collider.transform.parent = gameObject.transform;
            _objCollider = _collider.GetComponent<ObjCollider>();

        }

        public void SaveCollider()
        {
            ScannedObjectSerializationManager.instance.SetEditScannedObject(_scanObjData);
        }

        public void LoadCollider()
        {
            _scanObjData = ScannedObjectSerializationManager.instance.EditScannedObject();
        }
    }
}

