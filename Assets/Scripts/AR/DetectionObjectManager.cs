﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using SKTRX.AR.SerializableObjects;
using SKTRX.AR;

namespace SKTRX.Managers
{
    public class DetectionObjectManager : MonoBehaviour
    {
        public static DetectionObjectManager instance = null;
        public GameObject prefabScannedObj;
        private List<GameObject> _scannedObjList;

        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                Destroy(gameObject);

            //Call the Init function 
            Init();
        }

        //Initializes the class.
        void Init()
        {
            _scannedObjList = new List<GameObject>();
        }

        public List<GameObject> ScannedObjList
        {
            get { return _scannedObjList; }
        }

        public GameObject GetLastObject()
        {
            if (_scannedObjList.Count > 0)
                return _scannedObjList[_scannedObjList.Count - 1];

            return null;
        }

        public void CreateScannedObjectAnchor(List<ARReferenceObject> arRefObjList)
        {
            if(_scannedObjList.Count > 0)
                foreach(GameObject obj in _scannedObjList)
                {
                    Destroy(obj);
                }

            _scannedObjList.Clear();

            foreach(ARReferenceObject objRef in arRefObjList)
            {
                if(objRef == null)
                {
                    Debug.Log("@@@ objRef is null");
                    continue;
                }
                Debug.Log("@@@ CreateScannedObjectAnchor with index: " + arRefObjList.IndexOf(objRef));

                GameObject scannedObj = Instantiate(prefabScannedObj, Vector3.zero, Quaternion.identity);

                if (scannedObj != null)
                {
                    Debug.Log("@@@ Set up scannedObj" + objRef.name);

                    if (scannedObj.GetComponent<ColliderScannedObjectAnchor>() == null)
                        Debug.Log("@@@ ColliderScannedObjectAnchor is null");
                    else
                    {
                        scannedObj.GetComponent<ColliderScannedObjectAnchor>().enabled = true;
                        scannedObj.GetComponent<ColliderScannedObjectAnchor>().scannedObjectName = objRef.name;
                        scannedObj.GetComponent<ColliderScannedObjectAnchor>().ARReferenceObject = objRef;
                    }

                    Debug.Log("@@@ Create Scanned Obj:" + objRef.name);
                    _scannedObjList.Add(scannedObj);
                }
            }
        }


        public void CreateScannedObjectAnchor(ARReferenceObject arRefObjList)
        {
            if (arRefObjList == null) return;


            GameObject scannedObj = Instantiate(prefabScannedObj, Vector3.zero, Quaternion.identity);

            if (scannedObj != null)
            {
                scannedObj.GetComponent<ColliderScannedObjectAnchor>().scannedObjectName = arRefObjList.name;
                scannedObj.GetComponent<ColliderScannedObjectAnchor>().ARReferenceObject = arRefObjList;
                Debug.Log("@@@ Create Scanned Obj:" + arRefObjList.name);
                _scannedObjList.Add(scannedObj);
            }
        }

        public void CreateScannedObjectAnchor(ARReferenceObject arRefObj, SerializableScanObject arScanObjData)
        {
            Debug.Log("@@@ CreateScannedObjectAnchor 1");

            GameObject scannedObj = Instantiate(prefabScannedObj, Vector3.zero, Quaternion.identity);

            if (scannedObj != null)
            {
                scannedObj.GetComponent<ColliderScannedObjectAnchor>().scannedObjectName = arScanObjData.name;
                Debug.Log("@@@ Create Scanned Obj:" + scannedObj.GetComponent<ColliderScannedObjectAnchor>().scannedObjectName);

                if(arRefObj != null)
                {
                    scannedObj.GetComponent<ColliderScannedObjectAnchor>().ARReferenceObject = arRefObj;
                }

                _scannedObjList.Add(scannedObj);
            }
        }
    }

}
