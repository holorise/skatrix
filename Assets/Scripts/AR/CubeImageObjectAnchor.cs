﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;


namespace SKTRX.AR
{
    public class CubeImageObjectAnchor : MonoBehaviour
    {
        [SerializeField]
        private List<ARReferenceImage> _referenceImagesList;

        [SerializeField]
        private GameObject _prefabToGenerate;

        [SerializeField]
        private Vector3 _size = Vector3.zero;

        [SerializeField]
        private List<Vector3> _anchorsList;

        private GameObject _createdObject;
        private List<ARImageAnchor> _objectTrackingAnchors;


        // Use this for initialization
        void Start()
        {
            _objectTrackingAnchors = new List<ARImageAnchor>(_referenceImagesList.Count);
            UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;

        }

        PredefinedCollidersSize predefinedCollidersSize
        {
            get
            {
                if(_createdObject == null)
                {
                    Debug.Log("@@@createdObject is null with Name:" + gameObject.name);

                    return null;
                }
                else
                {
                    PredefinedCollidersSize componentScript = _createdObject.GetComponent<PredefinedCollidersSize>();

                    if(componentScript == null)
                    {
                        Debug.Log("@@@predefinedCollidersSize is null with Name:" + gameObject.name);

                        return null;
                    }

                    return componentScript;
                }
            }
        }

        bool isExistAnchor
        {
            get
            {
                bool result = false;

                foreach(ARImageAnchor anchor in _objectTrackingAnchors)
                {
                    if(anchor != null)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }

        int IndexOfReferenceImage(ARImageAnchor arImageAnchor)
        {
            int result = -1;

            for(int i = 0; i < _referenceImagesList.Count; i ++)
            {
                if(arImageAnchor.referenceImageName == _referenceImagesList[i].imageName)
                {
                    result = i;

                    break;
                }
            }

            return result;
        }

        void UpdateObject(ARImageAnchor arImageAnchor, Vector3 anchor)
        {
            if (arImageAnchor.isTracked)
            {
                if (!_createdObject.activeSelf)
                {
                    _createdObject.SetActive(true);
                }
                _createdObject.transform.position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
                _createdObject.transform.rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

                PredefinedCollidersSize sizeScript = predefinedCollidersSize;

                if (sizeScript != null)
                {
                    sizeScript.Anchor = anchor;
                    sizeScript.Size = _size;
                }
            }
        }

        void CreateObject(ARImageAnchor arImageAnchor, Vector3 anchor)
        {
            Vector3 position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

            if (_createdObject == null)
            {
                _createdObject = Instantiate<GameObject>(_prefabToGenerate, position, rotation);
            }

            PredefinedCollidersSize sizeScript = predefinedCollidersSize;

            if (sizeScript != null)
            {
                sizeScript.Anchor = anchor;
                sizeScript.Size = _size;
            }
        }

        void AddImageAnchor(ARImageAnchor arImageAnchor)
        {
            Debug.LogFormat("@@@image anchor added[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

            if(_referenceImagesList == null || _referenceImagesList.Count == 0)
            {
                Debug.LogFormat("@@@ referenceImagesList is null or empty");
            }

            int index = IndexOfReferenceImage(arImageAnchor);

            if(index >= 0 && _anchorsList.Count > index)
            {
                CreateObject(arImageAnchor, _anchorsList[index]);
                _objectTrackingAnchors[index] = arImageAnchor;
            }
        }

        void UpdateImageAnchor(ARImageAnchor arImageAnchor)
        {
            Debug.LogFormat("@@@image anchor updated[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

            if (_referenceImagesList == null || _referenceImagesList.Count == 0)
            {
                Debug.LogFormat("@@@ referenceImagesList is null or empty");
            }

            int index = IndexOfReferenceImage(arImageAnchor);

            if (index >= 0 && _anchorsList.Count > index)
            {
                UpdateObject(arImageAnchor, _anchorsList[index]);
                _objectTrackingAnchors[index] = arImageAnchor;
            }
        }

        void RemoveImageAnchor(ARImageAnchor arImageAnchor)
        {
            Debug.LogFormat("@@@image anchor removed[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

            if (_referenceImagesList == null || _referenceImagesList.Count == 0)
            {
                Debug.LogFormat("@@@ referenceImagesList is null or empty");
            }

            int index = IndexOfReferenceImage(arImageAnchor);

            if(index >= 0 && _objectTrackingAnchors.Count > index)
            {
                _objectTrackingAnchors[index] = null;
            }

            if (_createdObject && !isExistAnchor)
            {
                Destroy(_createdObject);
            }

        }

        void OnDestroy()
        {
            UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}


