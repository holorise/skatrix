﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.AR
{
    [ExecuteInEditMode]
    public class ObjCollider : MonoBehaviour
    {
        [Range(0.1f, 2.0f)]
        public float scaleX = 1.0f;
        [Range(0.1f, 2.0f)]
        public float scaleY = 1.0f;
        [Range(0.1f, 2.0f)]
        public float scaleZ = 1.0f;
        [Range(0, 360)]
        public float rotationX = 0;
        [Range(0, 360)]
        public float rotationY = 0;
        [Range(0, 360)]
        public float rotationZ = 0;
        [Range(-1, 1)]
        public float positionX = 0;
        [Range(-1, 1)]
        public float positionY = 0;
        [Range(-1, 1)]
        public float positionZ = 0;

        private Vector3 rotation = Vector3.zero;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            rotation.y = rotationY;
            rotation.x = rotationX;
            rotation.z = rotationZ;
            gameObject.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
            gameObject.transform.localRotation = Quaternion.Euler(rotation);
            gameObject.transform.localPosition = new Vector3(positionX, positionY, positionZ);
        }
    }

}
