﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace SKTRX.AR
{
    public class WorldAnchorMapManager : MonoBehaviour
    {
        UnityARCameraManager _arCameraManager;
        ARWorldMap _loadedMap;
        serializableARWorldMap _serializedWorldMap;

        // Start is called before the first frame update
        void Start()
        {
            _arCameraManager = GetComponent<UnityARCameraManager>();
            UnityARSessionNativeInterface.ARFrameUpdatedEvent += OnFrameUpdate;
        }

        ARTrackingStateReason m_LastReason;

        void OnFrameUpdate(UnityARCamera arCamera)
        {
            if (arCamera.trackingReason != m_LastReason)
            {
                Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
                Debug.LogFormat("@@@worldTransform: {0}", arCamera.worldTransform.column3);
                Debug.LogFormat("@@@trackingState: {0} {1}", arCamera.trackingState, arCamera.trackingReason);
                m_LastReason = arCamera.trackingReason;
            }
        }

        void OnWorldMap(ARWorldMap worldMap)
        {
            if (worldMap != null)
            {
                worldMap.Save(path);
                Debug.LogFormat("@@@ARWorldMap saved to {0}", path);
            }
        }

        static UnityARSessionNativeInterface session
        {
            get { return UnityARSessionNativeInterface.GetARSessionNativeInterface(); }
        }

        public void Save()
        {
            session.GetCurrentWorldMapAsync(OnWorldMap);
        }

        public void Load()
        {
            Debug.LogFormat("@@@Loading ARWorldMap {0}", path);

            if(!File.Exists(path))
            {
                Debug.Log("@@@ the World map file doesn't exist");
                _loadedMap = null;
                return;
            }

            var worldMap = ARWorldMap.Load(path);
            if (worldMap != null)
            {
                _loadedMap = worldMap;
                Debug.LogFormat("@@@Map loaded. Center: {0} Extent: {1}", worldMap.center, worldMap.extent);

                UnityARSessionNativeInterface.ARSessionShouldAttemptRelocalization = true;
            }
        }

        public ARWorldMap WorldMap
        {
            get
            {
                return _loadedMap;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        static string path
        {
            get { return Path.Combine(Application.persistentDataPath, "sktrx-map.worldmap"); }
        }

        void OnWorldMapSerialized(ARWorldMap worldMap)
        {
            if (worldMap != null)
            {
                //we have an operator that converts a ARWorldMap to a serializableARWorldMap
                _serializedWorldMap = worldMap;
                Debug.Log("@@@ARWorldMap serialized to serializableARWorldMap");
            }
        }

        public void SaveSerialized()
        {
            session.GetCurrentWorldMapAsync(OnWorldMapSerialized);
        }

        public void LoadSerialized()
        {
            Debug.Log("@@@Loading ARWorldMap from serialized data");
            //we have an operator that converts a serializableARWorldMap to a ARWorldMap
            ARWorldMap worldMap = _serializedWorldMap;
            if (worldMap != null)
            {
                _loadedMap = worldMap;
                Debug.LogFormat("@@@ Map loaded. Center: {0} Extent: {1}", worldMap.center, worldMap.extent);
            }
        }


        public UnityARSessionRunOption mapRunOptions
        {
            get
            {
                return UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking;
            }
        }
    }
}


