﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace SKTRX.AR
{
    public class GenerateScannedObjectListAnchor : MonoBehaviour
    {

        //[SerializeField]
        public string scannedObjectName;

        [SerializeField]
        private GameObject prefabToGenerate;

        private GameObject objectAnchorGO;

        // Use this for initialization
        void Start()
        {
            UnityARSessionNativeInterface.ARObjectAnchorAddedEvent += AddObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorUpdatedEvent += UpdateObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorRemovedEvent += RemoveObjectAnchor;

        }

        public GameObject GetObjectAnchorGO()
        {
            return objectAnchorGO;
        }


        void AddObjectAnchor(ARObjectAnchor arObjectAnchor)
        {
            Debug.Log("object anchor added");
            if (arObjectAnchor.referenceObjectName == scannedObjectName)
            {
                Vector3 position = UnityARMatrixOps.GetPosition(arObjectAnchor.transform);
                Quaternion rotation = UnityARMatrixOps.GetRotation(arObjectAnchor.transform);

                objectAnchorGO = Instantiate<GameObject>(prefabToGenerate, position, rotation);
                objectAnchorGO.GetComponent<ColliderEditor>().nameScannedObject = scannedObjectName;
            }
        }

        void UpdateObjectAnchor(ARObjectAnchor arObjectAnchor)
        {
            Debug.Log("object anchor updated");
            if (arObjectAnchor.referenceObjectName == scannedObjectName)
            {
                objectAnchorGO.transform.position = UnityARMatrixOps.GetPosition(arObjectAnchor.transform);
                objectAnchorGO.transform.rotation = UnityARMatrixOps.GetRotation(arObjectAnchor.transform);
            }

        }

        void RemoveObjectAnchor(ARObjectAnchor arObjectAnchor)
        {
            Debug.Log("object anchor removed");
            if ((arObjectAnchor.referenceObjectName == scannedObjectName) && (objectAnchorGO != null))
            {
                GameObject.Destroy(objectAnchorGO);
            }
        }

        void OnDestroy()
        {
            UnityARSessionNativeInterface.ARObjectAnchorAddedEvent -= AddObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorUpdatedEvent -= UpdateObjectAnchor;
            UnityARSessionNativeInterface.ARObjectAnchorRemovedEvent -= RemoveObjectAnchor;

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
