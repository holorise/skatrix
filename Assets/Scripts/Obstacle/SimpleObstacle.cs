﻿

#pragma warning disable 649
using UnityEngine;

namespace SKTRX.Obstacle
{
    public class SimpleObstacle : BaseObstacle
    {
        private float _rotate = 0;
        protected SimpleObstacle()
        {}

        public override void ChangeRotation(float value)
        {
            _rotate = value;
            Quaternion rotation = Quaternion.AngleAxis(value, new Vector3(0, 1, 0));
            transform.rotation = rotation;
            base.OnStateChange();
        }

        public override void ChangeScale(float value)
        {
            transform.localScale = Vector3.one * value;
            base.OnStateChange();
        }
       
        public override float GetRotate()
        {
            return _rotate;
        }

        public override float GetScale()
        {
           return transform.localScale.x;
        }

    }
}
