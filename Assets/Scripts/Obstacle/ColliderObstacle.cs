﻿
#pragma warning disable 649
using UnityEngine;

namespace SKTRX.Obstacle
{
    public class ColliderObstacle : MonoBehaviour
    {
        [SerializeField] private BaseObstacle _baseObstacle;

        public BaseObstacle BaseObstacle { get => _baseObstacle;}

        public void SetParentObstacle(BaseObstacle parentObstacle) 
        {
            _baseObstacle = parentObstacle;
        }

        public void SetLayer(string tagName)
        {
            transform.tag = tagName;
        }
       
        public void GenerateMeshCollider(PhysicMaterial material)
        {
            var meshFilter = gameObject.GetComponent<MeshFilter>();
            if(meshFilter != null)
            {
                var collider = gameObject.AddComponent<MeshCollider>();
                collider.material = material;
            }
        }

        [ContextMenu("Get Parent")]
        private void GetParentObstacle()
        {
            var baseObstacle = transform.GetComponentInParent<BaseObstacle>();
            if (baseObstacle != null)
                SetParentObstacle(baseObstacle);
        }
    }
}