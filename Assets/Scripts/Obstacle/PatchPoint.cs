﻿
#pragma warning disable 649
using UnityEngine;
using System.Collections;

public class PatchPoint : MonoBehaviour
{
    [SerializeField] private float _pointSize = 0.004f;
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, _pointSize);
    }

}
