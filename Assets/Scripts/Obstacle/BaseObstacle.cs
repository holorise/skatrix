﻿

#pragma warning disable 649
//#define Debug
using UnityEngine;
using System.Collections.Generic;
using SKTRX.Enums;
using SKTRX.DTOModel;
using SKTRX.Tools;
using System.Linq;
using SKTRX.PlayerBehaviour;
using System;
using SKTRX.Obstacle.Rail;
using SKTRX.Statics;

namespace SKTRX.Obstacle
{
    public abstract class BaseObstacle : MonoBehaviour
    {
        [SerializeField] private ObstacleDTO _obstacleDTO;
        [SerializeField] private PhysicMaterial _physicMaterial;
        [SerializeField] private RailObstacle[] _railObstacles;
        [SerializeField] private TrickObstacle[] _trickObstacles;
        [SerializeField] private bool _allosSetObstacleUnder = false;
        [SerializeField] private bool _allosSetRailUnder = true;
        [SerializeField] private bool _allosDestroy = true;
        [SerializeField] private bool _allowMoveUnder = true;
        [SerializeField] private List<RailPoint> _railPoints = new List<RailPoint>();

        private Action _onStateChange;
        private Vector3 _positionStart;
        public Transform ParentForRail { get => transform; }

        public ObstacleDTO ObstacleDTO { get => _obstacleDTO; protected set => _obstacleDTO = value; }
        public RailObstacle[] RailObstacles { get => _railObstacles; protected set => _railObstacles = value; }
        public bool AllosSetObstacleUnder { get => _allosSetObstacleUnder; protected set => _allosSetObstacleUnder = value; }
        public bool AllosSetRailpUnder { get => _allosSetRailUnder; protected set => _allosSetRailUnder = value; }
        public bool AllosDestroy { get => _allosDestroy; protected set => _allosDestroy = value; }
        public bool AllowMoveUnder { get => _allowMoveUnder;}

        public virtual void Destroy()
        {
            for (int i = 0; i < _railPoints.Count; i++)
            {
                if (_railPoints[i] != null)
                {
                    _railPoints[i].DestroyObstacle();
                }
            }
            if(gameObject != null)
                Destroy(gameObject);
        }

        public abstract void ChangeRotation(float value);

        public abstract void ChangeScale(float value);
        public abstract float GetScale();
        public abstract float GetRotate();

       
        /// <summary>
        /// Get path on rail
        /// </summary>
        /// <param name="position">Position intersect with rail collider OBSTACLE!</param>
        /// <param name="forward">Player forward</param>
        /// <param name="railPoints">ref return rail path</param>
        /// <returns>true if can build rail path.</returns>
        public virtual bool GetRailPath(Vector3 position, PlayerDTO playerDTO, Vector3 forward, ref List<PathPointDTO> railPoints)
        {
            Dictionary<Vector3, List<Vector3>> newPath =  new Dictionary<Vector3, List<Vector3>>();
            _positionStart = position;
            position = position - forward * 0.1f;//minus 3 percent behind !!! very important
            var t = new GameObject();
            t.transform.position = position;
            t.transform.rotation = Quaternion.identity;
            for (int j = 0; j < RailObstacles.Length; j++)
            {
                RailObstacle railObstacle = RailObstacles[j];
                if (railObstacle.PatchPoints.Length > 1)
                {
                    Vector3 pointCross;
                    Vector3 endPoint = position + forward * playerDTO.skateboardSize * 20;
                    for (int i = 1; i < railObstacle.PatchPoints.Length; i++)
                    {
                        Vector3 pointA = railObstacle.PatchPoints[i - 1].position;
                        Vector3 pointB = railObstacle.PatchPoints[i].position;
                        Vector3 pointResult2;
                        bool segment = false;
                        bool intersect = false;
                        position.y = pointB.y;
                        endPoint.y = pointB.y;
                        pointA.y = pointB.y;

                        MathTool.LineSegmentsIntersection3D(pointA, pointB, position, endPoint, out pointCross, out pointResult2, out segment, out intersect);

                        float angleFix = (Vector3.Angle(forward, (pointA - pointB).normalized));

                        if (segment && angleFix > 10f)
                        {
                            Vector3 firstDirection = (pointA - pointCross).normalized;
                            Vector3 secondDirection = (pointB - pointCross).normalized;
                            Vector3 mainDirection = (position - pointCross).normalized;
                            float angle = Vector3.Angle(firstDirection, mainDirection);
                            float angle2 = Vector3.Angle(secondDirection, mainDirection);
#if Debug
                            Debug.Log("Rail angle " + angle);
                            Debug.Log("Rail angle2 " + angle2);
                            Debug.Log("Rail angleFix " + angleFix);
#endif

                            if (angle > 2 && angle < railObstacle.AngleForRail)
                            {
                                if (angle < 5)
                                    pointCross = _positionStart;
                                if (!newPath.ContainsKey(pointCross))
                                    newPath.Add(pointCross, new List<Vector3>());

                                newPath[pointCross].Add(pointCross);

                                newPath[pointCross].AddRange(GetRestPoints(railObstacle, i));

                                PlayerAnimation.UpdateAngle(angle);
                            }
                            else if (angle2 > 2 && angle2 < railObstacle.AngleForRail)
                            {
                                if (angle2 < 5)
                                    pointCross = _positionStart;
                                if (!newPath.ContainsKey(pointCross))
                                    newPath.Add(pointCross, new List<Vector3>());

                                newPath[pointCross].Add(pointCross);
                                if (i > 1)
                                    newPath[pointCross].AddRange(GetRestPointsMinus(railObstacle, i - 2));
                                else
                                {
                                    newPath[pointCross].Add(railObstacle.PatchPoints[0].position);
                                }
                                PlayerAnimation.UpdateAngle(angle2);
                            }
                            else if (angle < 2 || angle2 < 2)
                            {
                                pointCross = _positionStart;

                                float firstPoint = Vector3.Distance(playerDTO.position, pointA);
                                float secondPoint = Vector3.Distance(playerDTO.position, pointB);
#if Debug
                                Debug.LogError("Fix error 3 angleFix: " + angleFix);
                                Debug.LogError("Fix error 3: " + firstPoint);
                                Debug.LogError("Fix error 3 : " + secondPoint);
#endif
                                pointCross.y = pointA.y;
                                if (!newPath.ContainsKey(pointCross))
                                    newPath.Add(pointCross, new List<Vector3>());

                                newPath[pointCross].Add(pointCross);
                                if (firstPoint < secondPoint)
                                    newPath[pointCross].Add(railObstacle.PatchPoints[railObstacle.PatchPoints.Length - 1].position);
                                else
                                    newPath[pointCross].Add(railObstacle.PatchPoints[0].position);

                                PlayerAnimation.UpdateAngle(angleFix);
                            }

                        }
                        else if (angleFix < 10f )
                        {

                            pointCross = _positionStart;

                            float firstPoint = Vector3.Distance(playerDTO.position, pointA);
                            float secondPoint = Vector3.Distance(playerDTO.position, pointB);
#if Debug
                            Debug.LogError("Fix error 1 angleFix: " + angleFix);
                            Debug.LogError("Fix error 1 : " + firstPoint);
                            Debug.LogError("Fix error 1: " + secondPoint);
#endif
                            pointCross.y = pointA.y;
                            if (!newPath.ContainsKey(pointCross))
                                newPath.Add(pointCross, new List<Vector3>());

                            newPath[pointCross].Add(pointCross);
                            if (firstPoint < secondPoint)
                                newPath[pointCross].Add(railObstacle.PatchPoints[railObstacle.PatchPoints.Length-1].position);
                            else
                                newPath[pointCross].Add(railObstacle.PatchPoints[0].position);
                          
                            PlayerAnimation.UpdateAngle(angleFix);
                        }

                    }
                }
            }
            if (newPath.Count == 0)
                return false;
            else if (newPath.Count == 1)
            {
                railPoints.AddRange(UpdateJumpRailPoints(new List<Vector3>(newPath.First().Value)));
                return true;
            }
            else
            {
                float d = 10;
                Vector3 key = Vector3.zero;
                foreach (var point in newPath)
                {
                    if (Vector3.Distance(point.Key, position) < d)
                    {
                        key = point.Key;
                        d = Vector3.Distance(point.Key, position);
                    }
                }
                if (key == Vector3.zero)
                    return false;
                else
                {
                    railPoints.AddRange(UpdateJumpRailPoints((new List<Vector3>(newPath[key]))));
                    return true;
                }
            }
        }

        private List<PathPointDTO> UpdateJumpRailPoints(List<Vector3> points)
        {
            List<PathPointDTO> pathPoints = new List<PathPointDTO>();
            if (points.Count > 1)
            {
                for (int i = 0; i < points.Count; i++)
                    pathPoints.Add(new PathPointDTO(points[i], PathPointType.Rail));

                pathPoints[0] = new PathPointDTO(pathPoints[0].Point, pathPoints[0].PathPointType, PathPointJumpType.Start);

                pathPoints[pathPoints.Count - 1] = new PathPointDTO(pathPoints[pathPoints.Count - 1].Point, pathPoints[pathPoints.Count - 1].PathPointType, PathPointJumpType.End);
            }
            return pathPoints;
        }

        private List<Vector3> GetRestPoints(RailObstacle railObstacle, int index)
        {
            List<Vector3> points = new List<Vector3>();
            for (int i = index; i < railObstacle.PatchPoints.Length; i++)
                points.Add(railObstacle.PatchPoints[i].position);
            return points;
        }

        private List<Vector3> GetRestPointsMinus(RailObstacle railObstacle, int index)
        {
            List<Vector3> points = new List<Vector3>();
            for (int i = index; i >= 0; i--)
                points.Add(railObstacle.PatchPoints[i].position);
            return points;
        }

#if UNITY_EDITOR

        [ContextMenu("Set Name")]
        private void SetName()
        {
            gameObject.name = _obstacleDTO.Type.ToString();
        }

        [ContextMenu("UpdateObstacle")]
        private void UpdateObstacle()
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                if(transform.GetChild(i).GetComponent<ColliderObstacle>() == null)
                {
                    var colliderObstacle = transform.GetChild(i).gameObject.AddComponent<ColliderObstacle>();
                    colliderObstacle.SetParentObstacle(this);
                    colliderObstacle.GenerateMeshCollider(_physicMaterial);
                    colliderObstacle.SetLayer(TagManager.OBSTACLE_TAG);
                    colliderObstacle.SetLayer(TagManager.OBSTACLE_TAG);
                    gameObject.name = _obstacleDTO.Type.ToString();
                }
            }
        }

        [ContextMenu("Reset Transform values")]
        private void ResetPrefabTransform()
        {
            transform.position = Vector3.zero;
            transform.localScale = Vector3.one;
            transform.rotation = Quaternion.identity;
        }
    //    List<Vector3> _points = new List<Vector3>();

        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.green;
        //    for (int i = 0; i < _points.Count; i += 2)
        //    {
        //        Gizmos.DrawLine(_points[i], _points[i + 1]);
        //        Gizmos.color = Color.blue;
        //    }
        //    Gizmos.color = Color.red;
        //    for (int i = 0; i < _points.Count; i ++)
        //    {
        //        Gizmos.DrawSphere(_points[i], 0.013f);
        //    }

        //}
#endif

        public void AddPoint(RailPoint railPoint)
        {
            _railPoints.Add(railPoint);
        }

        public virtual void OnStateChange()
        {
            for (int i = 0; i < _railPoints.Count; i++)
            {
                if (_railPoints[i] != null)
                {
                    _railPoints[i].UpdateScale();
                    _railPoints[i].UdpateState();
                }
            }
        }
    }
}
