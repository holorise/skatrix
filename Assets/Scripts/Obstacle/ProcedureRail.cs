﻿

#pragma warning disable 649
using SKTRX.Managers;
using SKTRX.Obstacle.Rail;
using SKTRX.Statics;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.Obstacle
{
    public class ProcedureRail : BaseObstacle
    {
        private float _rotate = 0;
        protected ProcedureRail()
        {
            base.ObstacleDTO = new DTOModel.ObstacleDTO();
        }

        public override void Destroy()
        {
            if (_firstRailPoint != null)
                Destroy(_firstRailPoint.gameObject);
            base.Destroy();
        }

        public override void ChangeRotation(float value)
        {
            _rotate = value;
        }

        public override void ChangeScale(float value)
        {
            //transform.localScale = Vector3.one * value;
        }

        public override float GetRotate()
        {
            return _rotate;
        }

        public override float GetScale()
        {
            return transform.localScale.x;
        }

        private RailPoint _firstRailPoint;
        private RailPoint _endRailPoint;
        private List<RailLine> _railLines = new List<RailLine>();

        public void InitializeRail(RailPoint firstRailPoint, RailPoint endRailPoint)
        {
            _firstRailPoint = firstRailPoint;
            _endRailPoint = endRailPoint;
            BuildRailBetweenPoints();
            transform.tag = TagManager.OBSTACLE_TAG;
        }

        public void SubscribeOnUpdateStartPoint()
        {
            Debug.Log("SubscribeOnUpdateStartPoint");
            BuildRailBetweenPoints();
            transform.tag = TagManager.OBSTACLE_TAG;
        }

        private RailObstacle GetRailObstacle()
        {
            if(RailObstacles == null)
            {
                var go = new GameObject();
                go.transform.parent = transform;
                go.transform.localPosition = Vector3.zero;
                go.transform.rotation = Quaternion.identity;
                RailObstacle railObstacle = go.AddComponent<RailObstacle>();
                RailObstacles = new RailObstacle[1] { railObstacle };
            }
            return RailObstacles[0];
        }

        private void BuildRailBetweenPoints()
        {
            if (_firstRailPoint != null && _endRailPoint != null)
            {
                try
                {
                    _railLines.ForEach(p => Destroy(p.gameObject));
                    _railLines.Clear();
                    Vector3 direction = (_endRailPoint.PointOfRail - _firstRailPoint.PointOfRail).normalized;
                    RailLine railLine = Instantiate(ObstacleManager.Instance.RailLinePrefab, transform);
                    railLine.InstantiateLine(_firstRailPoint.PointOfRail, direction);
                    float lenthOfRail = Vector3.Distance(railLine.EndPoint, railLine.transform.position);
                    float distance = Vector3.Distance(_firstRailPoint.PointOfRail, _endRailPoint.PointOfRail);
                    _railLines.Add(railLine);
                    while (lenthOfRail < distance)
                    {
                        railLine = Instantiate(ObstacleManager.Instance.RailLinePrefab, transform);
                        railLine.InstantiateLine(_railLines[_railLines.Count - 1].EndPoint, direction);
                        _railLines.Add(railLine);
                        distance = Vector3.Distance(railLine.EndPoint, _endRailPoint.PointOfRail);
                    }
                    direction = (_firstRailPoint.PointOfRail - _endRailPoint.PointOfRail).normalized;
                    railLine = Instantiate(ObstacleManager.Instance.RailLinePrefab, transform);
                    railLine.InstantiateLine(_endRailPoint.PointOfRail, direction);
                    _railLines.Add(railLine);
                    GenerateNewRailObstacles(GetRailObstacle());
                }
                catch { }

            }
        }

        private void GenerateNewRailObstacles(RailObstacle railObstacle)
        {
            for (int i = 0; i < _railLines.Count; i++)
                _railLines[i].SetObstacle(this);
            base.AllosSetRailpUnder = false;
            base.AllosSetObstacleUnder = false;
            List<Transform> transform = new List<Transform>();
            List<Collider> colliders = new List<Collider>();
            transform.Add(_railLines[0].PatchPoints[0]);
            transform.Add(_railLines[0].PatchPoints[1]);
            colliders.Add(_railLines[0].Colliders[0]);
            for (int i = 1; i < _railLines.Count - 1; i++)
            {
                transform.Add(_railLines[i].PatchPoints[1]);
                colliders.Add(_railLines[i].Colliders[0]);
            }
            transform.Add(_railLines[_railLines.Count - 1].PatchPoints[0]);
            colliders.Add(_railLines[_railLines.Count - 1].Colliders[0]);
            railObstacle.GenerateRail(transform.ToArray(), colliders.ToArray());
        }
    }
}
