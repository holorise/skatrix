﻿

#pragma warning disable 649
using UnityEngine;

namespace SKTRX.Obstacle
{

    public class RailObstacle : MonoBehaviour
    {
        [SerializeField] private Transform[] _patchPoints;
        [SerializeField] private Collider[] _colliders;
        [SerializeField] private float _angleForRail = 80;
        public Transform[] PatchPoints { get => _patchPoints;}
        public float AngleForRail { get => _angleForRail;}

        public void GenerateRail(Transform[] patchPoints, Collider[] colliders)
        {
            _patchPoints = patchPoints;
            _colliders = colliders;

        }
    }
}