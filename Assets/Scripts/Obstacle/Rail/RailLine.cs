﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKTRX.Statics;

namespace SKTRX.Obstacle.Rail
{
    public class RailLine : MonoBehaviour
    {
        [SerializeField] private Transform _endPoint;
        [SerializeField] private ColliderObstacle _colliderObstacle;
        [SerializeField] private Transform[] _patchPoints;
        [SerializeField] private Collider[] _colliders;
        public float AngleForRail = 70;
        public Transform[] PatchPoints { get => _patchPoints; }
        public Vector3 EndPoint { get => _endPoint.position; }
        public Collider[] Colliders { get => _colliders; }

        public void InstantiateLine(Vector3 position, Vector3 direction)
        {
            transform.position = position;
            transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
        }

        public void SetObstacle(BaseObstacle obstacle)
        {
            _colliderObstacle.SetParentObstacle(obstacle);
            _colliderObstacle.SetLayer(TagManager.OBSTACLE_TAG);
        }
    }
}
