﻿
#pragma warning disable 649
using UnityEngine;

namespace SKTRX.Obstacle.Rail
{
    public class RailPoint : MonoBehaviour
    {
        [SerializeField] private Transform _pointOfRail;
        [SerializeField] private ProcedureRail _procedureRail;

        public Vector3 PointOfRail { get => _pointOfRail.position; }

        public void InstantiatePoint(ProcedureRail procedurteRail, Vector3 position)
        {
            _procedureRail = procedurteRail;
            transform.position = position;
        }

        private void OnEnable()
        {
            UpdateScale();
        }

        [ContextMenu("Update Scale")]
        public void UpdateScale()
        {
            transform.localScale = Vector3.one;
            float scaleX = 1 / transform.lossyScale.x;
            float scaleY = 1 / transform.lossyScale.y;
            float scaleZ = 1 / transform.lossyScale.z;
            if (transform.localScale.x != scaleX || transform.localScale.y != scaleY || transform.localScale.z != scaleZ )
                transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
        }

        public void DestroyObstacle()
        {
            if (_procedureRail != null)
                _procedureRail.Destroy();
        }

        public void UdpateState()
        {
            if (_procedureRail != null)
                _procedureRail.SubscribeOnUpdateStartPoint();

        }
    }
}
