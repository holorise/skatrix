﻿

#pragma warning disable 649
using UnityEngine;
using SKTRX.Statics;

namespace SKTRX.Obstacle
{
    public class RealObstacle : BaseObstacle
    {
        public RealObstacle()
        {
           
        }

        private void OnEnable()
        {
            transform.tag = TagManager.OBSTACLE_TAG;
            AllosSetObstacleUnder = true;
            base.AllosDestroy = false;
        }

        public override void ChangeRotation(float value)
        {
        }

        public override void ChangeScale(float value)
        {
        }

        public override float GetRotate()
        {
            return 1;
        }

        public override float GetScale()
        {
            return 1;
        }

        [ContextMenu("DidUpdatePosition")]
        public void DidUpdatePosition()
        {
            base.OnStateChange();
        }

        public void OnDestroy()
        {
            base.Destroy();
        }
    }
}