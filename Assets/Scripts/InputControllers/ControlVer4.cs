﻿
#pragma warning disable 649
using UnityEngine;
using System;
using System.Collections.Generic;
using SKTRX.DTOModel;
using SKTRX.Enums;

namespace SKTRX.InputControllers
{
    public class ControlVer4 : BaseControler
    {

        //1 variant
        //1 Тап на игрока появляется стрелка указывающая куда смотри игрок и джойстик с помощью котрого можно вращать игрока и менять его направление движения
        //2 После выбора направления и отжатия кнопки джойстика и гроку придается фиксированный импульс
        //3 Появляется справа область на экране для свайпа если игрок свайпнет в этой области то он придаст персонажу еще импульс подобный первому

        [SerializeField] private JoystikInput _joystikInput; //get direction 
        [SerializeField] private SwipePanel _swipePanel; //get direction 

        public ControlVer4()
        {
            ControllType = ControlType.Control_v4;
        }

        public override void StartListeningForInstantiate()
        {
            Show();
            instantiatePlayer = true;
            TriggerMessage("Spawn avatar");
        }

        public override void InstantiatePlayer(Pose pose)
        {
            TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        public override void StartControl()
        {
            TriggerMessage("Tap the joistik for set direction and release for start move.");
            StartLisenTapOnPLayer();
        }

        private void StartLisenTapOnPLayer()
        {
            TriggerMessage("Tap the avatar for change direction. Swipe up for speed up and swipe down for speed down. Tap for jump.");
            tapOnPlayerAction = ShowJoystik;
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            if (playerState.Equals(PlayerState.Moving))
            {
                _swipePanel.Show();
                _swipePanel.SubscrideOnSwipeComand(SwipeComand, PointerUp);
                StartLisenTapOnPLayer();
            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }

        private void PointerUp()
        {
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.Jump
            });
        }

        public void SwipeComand(List<Direction> swipeComandsAction)
        {
            if (swipeComandsAction.Contains(Direction.up))
            {
                TriggerInput(new ControlDTO()
                {
                    inputType = InputType.SpeedUp
                });
            }
            if (swipeComandsAction.Contains(Direction.down))
            {
                TriggerInput(new ControlDTO()
                {
                    inputType = InputType.SpeedDown
                });
            }
        }

        private void ShowJoystik()
        {
            _joystikInput.Show();
            _joystikInput.Subscribe(PointerDownOnJoystik, PointerUpOnJoystik, PointerChangeDirection);
            //tapOnPlayerAction = null;
        }

        private void PointerUpOnJoystik()
        {
            Debug.Log("PointerUpOnJoystik");
            _joystikInput.Hide();
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.StartMove
            });
        }

        private void PointerDownOnJoystik()
        {
            Debug.Log("PointerDownOnJoystik");
        }

        private void PointerChangeDirection(Vector2 direction)
        {
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.SetDirection,
                direction = direction
            });
        }

        public override void StopControl()
        {
            
        }
    }
}