﻿using UnityEngine;
using System.Collections.Generic;
using SKTRX.DTOModel;
using SKTRX.Enums;

namespace SKTRX.InputControllers
{
    public class ControlVer5 : BaseControler
    {
        public ControlVer5() { ControllType = ControlType.Control_v5; }

        [SerializeField] private SwipePanel _swipePanel; //get direction 
        [SerializeField] private SliderPanel _slidePanel;
      
        private List<Vector3> _playerTargets;
        private float _holdTime = .3f;

        /// <summary>
        /// Start listen controll from here
        /// </summary>
        public override void StartListeningForInstantiate()
        {
            instantiatePlayer = true;
            Show();
            TriggerMessage("Spawn avatar");
        }

        public override void InstantiatePlayer(Pose pose)
        {
            TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        /// <summary>
        /// Call after player get this control
        /// </summary>
        public override void StartControl()
        {
            //PLAYER INSTANTIATED SUCCEDD 
            TriggerMessage("Tap the avatar(cube) to initialize a sequence.");
            _playerTargets = null;
            tapOnPlayerAction = PlayerTapAction;
            _swipePanel.Hide();
        }

        public override void StopControl()
        {
            Debug.LogError("StopControl");
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            Debug.LogWarning(string.Format("PlayerStateChange: {0}", playerState));

            if (playerState.Equals(PlayerState.Moving))
            {

            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }

        private void PlayerTapAction()
        {
            tapOnPlayerAction = null;
            TriggerMessage("Tap a location that you`d like it to move towards");
            _playerTargets = new List<Vector3>();
            playerTargetAction = OnPlaneTap;
        }

        private void OnPlaneTap(Vector3 position)
        {
            TriggerMessage("Tap the avatar(cube) again to begin movement.");
            _playerTargets.Add(position);
            tapOnPlayerAction = CompletePointCheck;
        }

        private void CompletePointCheck()
        {
            playerTargetAction = null;
            tapOnPlayerAction = null;
            TriggerMessage("Tap the avatar(cube) again to begin movement.");
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.MoveTowards,
                pointToMove = _playerTargets.ToArray()
            });
            _swipePanel.SubscrideOnHold(HoldCallback);
            _swipePanel.Show();
        }

        private void HoldCallback(float timeHold)
        {
            Debug.Log(string.Format("HoldCallback: {0}", timeHold));
            if (timeHold < _holdTime)
            {
                _slidePanel.Hide();
                TriggerInput(new ControlDTO()
                {
                    inputType = InputType.Jump
                });
            }
            else
            {
                _slidePanel.Show(delegate (float speedNormalized) 
                {
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.SpeedUp,
                        speedNormalized = speedNormalized
                    });
                });
            }
        }
    }
}