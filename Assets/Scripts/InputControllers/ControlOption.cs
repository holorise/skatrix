﻿using System;
using UnityEngine;
using SKTRX.Enums;

namespace SKTRX.InputControllers
{
    [Serializable]
    public class ControlOption
    {
        [SerializeField] private string _title;
        [SerializeField] private string _descriprion;

        public string Title { get => _title; }
        public string Descriprion { get => _descriprion; }
        public ControlType ControllType { get; set; } = ControlType.None;
    }
}
