﻿
#pragma warning disable 649
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using SKTRX.Statics;

namespace SKTRX.InputControllers
{
    public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        private Action _onClickPointDownAction;
        private Action<float> _onClickPointUpAction;
        private Action<Vector3> _onClickPointDragAction;
        private Vector3 _startPosition;
        private Vector3 _endPosition;
        private bool _allowPlayerDrag = false;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void ShowSubscribeDrag(Action onPointDown, Action<float> onPointUp, Action<Vector3> onPointDrag)
        {
            _onClickPointDownAction = onPointDown;
            _onClickPointUpAction = onPointUp;
            _onClickPointDragAction = onPointDrag;
            Show();
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            Ray raycast = CameraObject.Camera.ScreenPointToRay(eventData.position);
            RaycastHit[] raycastHit = Physics.RaycastAll(raycast);
            List<RaycastHit> hits = raycastHit.ToList<RaycastHit>();
            RaycastHit plane = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLANE_TAG));
            RaycastHit planeDrag = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLANE_DRAG_TAG));
            if (_allowPlayerDrag)
            {
                //if (plane.collider != null)
                //{
                //    _endPosition = plane.point;
                //    _onClickPointDragAction?.Invoke(GetDirection());
                //}
                //else 
                if (planeDrag.collider != null)
                {
                    _endPosition = planeDrag.point;
                    _onClickPointDragAction?.Invoke(GetDirection());
                }
            }
        }
        
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            _allowPlayerDrag = false;
            Ray raycast = CameraObject.Camera.ScreenPointToRay(eventData.position);
            RaycastHit[] raycastHit = Physics.RaycastAll(raycast);
            List<RaycastHit> hits = raycastHit.ToList<RaycastHit>();
            RaycastHit player = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLAYER_TAG));
            if (player.collider != null)
            {
                _startPosition = player.point;
                Debug.Log("Player clicked player");
                _onClickPointDownAction?.Invoke();
                _allowPlayerDrag = true;
            }
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if(_allowPlayerDrag)
                _onClickPointUpAction?.Invoke(Vector3.Distance(_startPosition, _endPosition));
            _allowPlayerDrag = false;
        }

        private Vector3 GetDirection()
        {
            _startPosition.y = 0;
            _endPosition.y = 0;
            Vector3 direction = (_startPosition - _endPosition).normalized;
            return direction;
        }
    }
}