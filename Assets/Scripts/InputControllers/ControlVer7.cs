﻿
#pragma warning disable 649
using UnityEngine;
using System.Collections.Generic;
using SKTRX.Enums;
using SKTRX.DTOModel;
using SKTRX.Tools;
using System.Text;

namespace SKTRX.InputControllers
{
    public class ControlVer7 : BaseControler
    {

        [SerializeField] private DragPanel _dragPanel;
        [SerializeField] private SwipePanel _swipePanel;
        [SerializeField] private ArrowControl _arrowControl;

        private bool _pointerDown = false;
        private bool _left = false;
        private bool _right = false;

        public ControlVer7() { ControllType = ControlType.Control_v7; }

        /// <summary>
        /// Start listen control from here
        /// </summary>
        public override void StartListeningForInstantiate()
        {
            instantiatePlayer = true;
            Show();
            TriggerMessage("Spawn avatar");
        }

        public override void InstantiatePlayer(Pose pose)
        {
            //TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        public override void StopControl()
        {
            Debug.LogError("StopControl");
            StartControl();
        }

        /// <summary>
        /// Call after player get this control
        /// </summary>
        public override void StartControl()
        {
            //PLAYER INSTANTIATED SUCCEDED 
            TriggerMessage(" ");//Drag the avatar in the opposite direction that you want it to go or tap go
            _dragPanel.ShowSubscribeDrag(PlayerClickDownAction, PlayerClickUpAction, PlayerClickDragAction);
            _swipePanel.Hide();
            _arrowControl.Subscribe(UpPointClick, DownPointClick, JumpClick);
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            Debug.LogWarning(string.Format("PlayerStateChange: {0}", playerState));

            if (playerState.Equals(PlayerState.Moving))
            {

            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }

        public void OnLeftClickUp()
        {
            _left = false;
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Forward
            });
            VibrationTool.PopVibration();
        }

        public void OnLeftClickDown()
        {
            _left = true;
            VibrationTool.PopVibration();
        }

        public void OnRightClickUp()
        {
            _right = false;
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Forward
            });
            VibrationTool.PopVibration();
        }

        public void OnRightClickDown()
        {
            _right = true;
            VibrationTool.PopVibration();
        }

        public void OnJumpClick()
        {
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.Jump
            });
            VibrationTool.PopVibration();
        }

        protected override void Destroy()
        {
            TriggerMessage("");
            base.Destroy();
        }

        private void UpPointClick()
        {
            //TriggerMessage("Swiping on the screen makes the tricks");
            _swipePanel.Show();
            _swipePanel.SubscrideOnSwipeComand(SwipeComand);
            _dragPanel.Hide();
            tapOnPlayerAction = StopPlayer;
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.SpeedUp
            });
            VibrationTool.PopVibration();
        }
        
        private void DownPointClick()
        {
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.SpeedDown
            });
            VibrationTool.PopVibration();
        }

        private void JumpClick()
        {
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Jump
            });
            VibrationTool.PopVibration();
        }

        public new void   Update()
        {
            base.Update();
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.W))
            {
                base.TriggerInput(new ControlDTO()
                {
                    inputType = InputType.SpeedUp
                });

            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                base.TriggerInput(new ControlDTO()
                {
                    inputType = InputType.Left,
                    isStaticTurn = true
                });

            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                base.TriggerInput(new ControlDTO()
                {
                    inputType = InputType.Right,
                    isStaticTurn = true
                });
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("jump trick number 0");
                TriggerInput(new ControlDTO()
                {
                    inputType = InputType.Trick,
                    trickType = TrickType.JumpType,
                    trickNumber = 0
                });
            }
#endif
        }

        private void PlayerClickDownAction()
        {
            TriggerMessage("Release the avatar to \"slingshot\" it across the play space");
            _swipePanel.Hide();
            _pointerDown = true;
        }

        private void PlayerClickUpAction(float distance)
        {
            if (_pointerDown)
            {
                _pointerDown = false;
                TriggerMessage("");
                _swipePanel.Show();
                _swipePanel.SubscrideOnSwipeComand(SwipeComand);
                distance = Mathf.Clamp(distance / 10f, 0, 1);
                if (distance > 0.001f)
                {
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.MoveWithSpeed,
                        velocity = distance
                    });
                    _dragPanel.Hide();
                    tapOnPlayerAction = StopPlayer;
                }
            }
        }

        private void StopPlayer()
        {
            //tapOnPlayerAction = null;
            //TriggerInput(new ControlDTO()
            //{
            //    inputType = InputType.Stop
            //});
        }

        private void PlayerClickDragAction(Vector3 direction)
        {
            TriggerMessage("");
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.SetOppositDirection,
                direction3 = direction
            });
        }

        public void SwipeComand(List<Direction> swipeComandsAction)
        {
            StringBuilder comands = new StringBuilder();
           //comands.AppendLine("swipe input:");
            foreach (var t in swipeComandsAction)
                comands.AppendLine(t.ToString());
          //  TriggerMessage(comands.ToString());
            TriggerMessage("");


            if (swipeComandsAction.Count == 0)
            {
             
            }
            else if (swipeComandsAction.Count == 2)
            {
                if (swipeComandsAction[0].Equals(Direction.up) && swipeComandsAction[1].Equals(Direction.down))
                {
                    Debug.Log("jump trick number 2 hiil flip");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.JumpType,
                        trickNumber = 2
                    });
                    VibrationTool.PopVibration();
                }
                else if (swipeComandsAction[0].Equals(Direction.up) && swipeComandsAction[1].Equals(Direction.left))
                {
                    Debug.Log("jump trick number Noseslide_BS");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.Rail,
                        trickNumber = 4
                    });
                    VibrationTool.PopVibration();
                }
                else if (swipeComandsAction[0].Equals(Direction.up) && swipeComandsAction[1].Equals(Direction.right))
                {
                    Debug.Log("jump trick number Dark Slide");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.Rail,
                        trickNumber = 2
                    });
                    VibrationTool.PopVibration();
                }
            }
            else if (swipeComandsAction.Count == 1)
            {
                if (swipeComandsAction.Contains(Direction.up))
                {
                    Debug.Log("jump trick number 0");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.JumpType,
                        trickNumber = 0
                    });
                    VibrationTool.PopVibration();
                }
                else if (swipeComandsAction.Contains(Direction.left))
                {
                    Debug.Log("jump trick number 3 360 hard Flip");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.JumpType,
                        trickNumber = 3
                    });
                    VibrationTool.PopVibration();
                }
                else if (swipeComandsAction.Contains(Direction.right))
                {
                    Debug.Log("jump trick number 1 kickflip");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.JumpType,
                        trickNumber = 1
                    });
                    VibrationTool.PopVibration();
                }
                else if (swipeComandsAction.Contains(Direction.down))
                {
                    Debug.Log("jump trick number 1 manual");
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Trick,
                        trickType = TrickType.Manual,
                        trickNumber = 1
                    });
                    VibrationTool.PopVibration();
                }
            }
        }

        private void FixedUpdate()
        {
            if (_left)
                LeftTurn();
            if (_right)
                RightTurn();
        }
 
        private void LeftTurn()
        {
            TriggerMessage("");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Left,
                isStaticTurn = true
            });
            VibrationTool.PopVibration();
        }

        private void RightTurn()
        {
            TriggerMessage("");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Right,
                isStaticTurn = true
            });
            VibrationTool.PopVibration();
        }
    }
}