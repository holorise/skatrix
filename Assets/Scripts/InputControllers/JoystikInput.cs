﻿
#pragma warning disable 649
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

namespace SKTRX.InputControllers
{
    [RequireComponent(typeof(AspectRatioFitter))]
    public class JoystikInput : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {

        [SerializeField] private RectTransform _background;
        [SerializeField] private RectTransform _knob;
        [SerializeField] private float _offset;

        private Vector2 _pointPosition;
        private Action<Vector2> _onDirectionUpdateAction;
        private Action _onPointerDown;
        private Action _onPointerUp;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Subscribe(Action pointerDown, Action pointerUp, Action<Vector2> onDirectionUpdate)
        {
            _onPointerDown = pointerDown;
            _onPointerUp = pointerUp;
            Subscribe(onDirectionUpdate);
        }

        public void Subscribe(Action<Vector2> onDirectionUpdate)
        {
            _onDirectionUpdateAction = onDirectionUpdate;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnDrag(eventData);
            _onPointerDown?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnEndDrag(eventData);
            _onPointerUp?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _pointPosition = new Vector2((eventData.position.x - _background.position.x) / ((_background.rect.size.x - _knob.rect.size.x) / 2), (eventData.position.y - _background.position.y) / ((_background.rect.size.y - _knob.rect.size.y) / 2));
            _pointPosition = (_pointPosition.magnitude > 1.0f) ? _pointPosition.normalized : _pointPosition;
            _knob.transform.position = new Vector2((_pointPosition.x * ((_background.rect.size.x - _knob.rect.size.x) / 2) * _offset) + _background.position.x, (_pointPosition.y * ((_background.rect.size.y - _knob.rect.size.y) / 2) * _offset) + _background.position.y);
            InvokeChange();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _pointPosition = new Vector2(0f, 0f);
            _knob.transform.position = _background.position;
        }

        private void InvokeChange()
        {
            _onDirectionUpdateAction?.Invoke(_pointPosition);
        }
    }
}