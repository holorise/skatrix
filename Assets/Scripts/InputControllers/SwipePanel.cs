﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using SKTRX.Enums;

namespace SKTRX.InputControllers
{
    public class SwipePanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        [SerializeField] private List<Direction> _directionCommands = new List<Direction>();

        private Action<float> _onPointerHoldAction;
        private Action<List<Direction>> _swipeComandsAction;
        private Action _onPointerDownAction;
        private Direction _currentDirection = Direction.none;
        private Vector2 _directionVector = Vector2.zero;
        private float _timeSwipe;
        private float _directionKoef = .3f;//0.5
        private Vector2 _lastPosition = Vector2.zero;
        private float _koefUpDownLefRight = 0.3f;
        private float _timeSwipeExucute = 1f;
        private bool _pointerDrag = false;
        private Vector2 _startPoint = Vector2.zero;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SubscrideOnHold(Action<float> onPointerHold)
        {
            _onPointerHoldAction = onPointerHold;
        }

        public void SubscrideOnSwipeComand(Action<List<Direction>> swipeComandsAction, Action onPointerDown)
        {
            SubscrideOnSwipeComand(swipeComandsAction);
            _onPointerDownAction = onPointerDown;
        }

        public void SubscrideOnSwipeComand(Action<List<Direction>> swipeComandsAction)
        {
            _swipeComandsAction = swipeComandsAction;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _pointerDrag = false;
            _timeSwipe = Time.realtimeSinceStartup - _timeSwipe;
            _currentDirection = Direction.none;
            if (_directionCommands.Count == 0)
            {
                _onPointerDownAction?.Invoke();
                _onPointerHoldAction?.Invoke(_timeSwipe);
            }

            if (_timeSwipe < _timeSwipeExucute)
                _swipeComandsAction?.Invoke(_directionCommands);
            _directionCommands.Clear();
            _startPoint = Vector3.zero;
            _timeSwipe = float.MinValue;
            _lastPosition = Vector2.zero;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _pointerDrag = true;
            _startPoint = eventData.position;
            _directionCommands.Clear();
            _timeSwipe = Time.realtimeSinceStartup;
            _currentDirection = Direction.none;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_pointerDrag)
            {
                _directionVector = (eventData.position - _startPoint).normalized;
                var ang = Mathf.Asin(_directionVector.x) * Mathf.Rad2Deg;
                if (Mathf.Abs(ang) > 40 && Mathf.Abs(ang) < 80)
                {
                    if (_lastPosition != Vector2.zero && (_currentDirection == Direction.right || _currentDirection == Direction.left || _currentDirection == Direction.up || _currentDirection == Direction.down))
                    {
                        _startPoint = _lastPosition;
                        _lastPosition = Vector2.zero;
                    }
                    else
                    {
                        SideDirection(eventData.position);
                    }
                }
                else
                {
                    if (_lastPosition != Vector2.zero && (_currentDirection == Direction.rightDown || _currentDirection == Direction.rightUp || _currentDirection == Direction.leftDown || _currentDirection == Direction.leftUp))
                    {
                        _startPoint = _lastPosition;
                        _lastPosition = Vector2.zero;
                    }
                    else
                    {
                        UpDownDirection(eventData.position);
                    }
                }
            }
        }

        private void SideDirection(Vector2 position)
        {
            if (_directionVector.x > _directionKoef && _directionVector.y > _directionKoef)
            {
                SetDirection(Direction.rightUp, position);
            }
            else if (_directionVector.x < -_directionKoef && _directionVector.y > _directionKoef)
            {
                SetDirection(Direction.leftUp, position);
            }
            if (_directionVector.x > _directionKoef && _directionVector.y < -_directionKoef)
            {
                SetDirection(Direction.rightDown, position);
            }
            else if (_directionVector.x < -_directionKoef && _directionVector.y < -_directionKoef)
            {
                SetDirection(Direction.leftDown, position);
            }
        }

        private void UpDownDirection(Vector2 position)
        {
            if (Mathf.Abs(_directionVector.x) > Mathf.Abs(_directionVector.y))
            {
                if (_directionVector.x >= _koefUpDownLefRight)
                {
                    SetDirection(Direction.right, position);
                }
                else if (_directionVector.x <= -_koefUpDownLefRight)
                {
                    SetDirection(Direction.left, position);
                }
            }
            else
            {
                if (_directionVector.y >= _koefUpDownLefRight)
                {
                    SetDirection(Direction.up, position);
                }
                else if (_directionVector.y <= -_koefUpDownLefRight)
                {
                    SetDirection(Direction.down, position);
                }
            }
        }

        private void SetDirection(Direction direction, Vector2 position)
        {
            if (_currentDirection.Equals(direction))
            {
                _lastPosition = position;
            }
            else
            {
                _directionCommands.Add(direction);
                _currentDirection = direction;
            }
        }
    }
}