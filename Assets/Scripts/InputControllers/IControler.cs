﻿
using System;
using SKTRX.DTOModel;
using SKTRX.Enums;
using UnityEngine;

public interface IControler
{
    void SubscribeToInstantiate(Action<Pose> onControlDtoAction);
    void Subscribe(Action<ControlDTO> onControlDtoAction);
    void SubscribeMessage(Action<string> onMessageSendAction);
    void StartControl();
    void StopControl();
    void ChangePlayerState(PlayerState playerState);
    void StartListeningForInstantiate();
    void DestroyControl();
}