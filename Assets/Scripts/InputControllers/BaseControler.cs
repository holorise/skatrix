﻿using System;
using UnityEngine;
using SKTRX.DTOModel;
using SKTRX.Enums;
//using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using System.Collections.Generic;
using SKTRX.AR;
using UnityEngine.EventSystems;
using SKTRX.Statics;
using System.Linq;

namespace SKTRX.InputControllers
{
    public abstract class BaseControler : MonoBehaviour, IControler
    {
        
        protected Action<Pose> onPlayerInstantiate;
        protected Action<Vector3> playerTargetAction;
        protected Action tapOnPlayerAction;
        protected Action<string> messageSubscribeAction;
        protected bool instantiatePlayer = false;

        private Action<ControlDTO> controlDtoAction;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private ControlOption _controlOptionUI;

        public ControlType ControllType { get; protected set; }

        public abstract void InstantiatePlayer(Pose pose);
        public abstract void StartListeningForInstantiate();
        public abstract void StartControl();
        public abstract void StopControl();

        public abstract void ChangePlayerState(PlayerState playerState);

        public ControlOption GetControllOption()
        {
            _controlOptionUI.ControllType = ControllType;
            return _controlOptionUI;
        }

        public void Subscribe(Action<ControlDTO> onControlDtoAction)
        {
            controlDtoAction = onControlDtoAction;
        }

        public virtual void SetParent(Transform parent)
        {
            transform.SetParent(parent);
            ResetPosition();
        }

        public virtual void Show()
        {
            Show(true);
        }

        public virtual void Hide()
        {
            Show(false);
        }

        protected void TriggerInput(ControlDTO controllDto)
        {
            controlDtoAction?.Invoke(controllDto);
        }

        protected virtual void Destroy()
        {
            Destroy(gameObject);
        }

        protected virtual void TriggerMessage(string text)
        {
            messageSubscribeAction?.Invoke(text);
        }

        protected virtual void Update()
        {
            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
            {
                Ray raycast = CameraObject.Camera.ScreenPointToRay(GetPointPosition());
                List<RaycastResult> uiTouches = new List<RaycastResult>();
                PointerEventData pointerData = new PointerEventData(EventSystem.current);
                pointerData.position = GetPointPosition();
                EventSystem.current.RaycastAll(pointerData, uiTouches);
                uiTouches.RemoveAll(p => p.gameObject.tag.Equals(TagManager.DRAG_PANEL_TAG));

                RaycastHit[] raycastHit = Physics.RaycastAll(raycast);
                List<RaycastHit> hits = raycastHit.ToList<RaycastHit>();
                RaycastHit obstacle = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.OBSTACLE_TAG));
                RaycastHit plane = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLANE_TAG));
                RaycastHit player = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLAYER_TAG));

                if (uiTouches.Count == 0)
                {
                    if (player.collider != null)
                    {
                        Debug.Log("Player clicked player");
                        tapOnPlayerAction?.Invoke();
                    }
                    else if (obstacle.collider != null)
                    {
                        if (instantiatePlayer)
                        {
                            instantiatePlayer = false;
                            Debug.Log("Player clicked obstacle");
                            InstantiatePlayer(new Pose(obstacle.point, Quaternion.identity));
                        }
                        else
                        {
                            playerTargetAction?.Invoke(obstacle.point);
                        }
                    }
                    else if (plane.collider != null)
                    {
                        if (instantiatePlayer)
                        {
                            instantiatePlayer = false;
                            Debug.Log("Player clicked plane");
                            InstantiatePlayer(new Pose(plane.point, Quaternion.identity));
                        }
                        else
                        {
                            playerTargetAction?.Invoke(plane.point);
                        }
                    }
                } 
            }
        }

        private Vector3 GetPointPosition()
        {
#if UNITY_EDITOR
            Vector3 position = Input.mousePosition;
#else
            Vector3 position = Input.GetTouch(0).position;
#endif
            return position;
        }

        private void ResetPosition()
        {
            _rectTransform.localScale = Vector3.one;
            _rectTransform.anchorMin = Vector3.zero;
            _rectTransform.anchorMax = Vector3.one;
            _rectTransform.sizeDelta = Vector3.zero;
            _rectTransform.anchoredPosition = Vector3.zero;
        }

        private void Show(bool active)
        {
            gameObject.SetActive(active);
        }

        void IControler.SubscribeToInstantiate(Action<Pose> onPlayerInstantiateAction)
        {
            onPlayerInstantiate = onPlayerInstantiateAction;
        }

        void IControler.SubscribeMessage(Action<string> onMessageSendAction)
        {
            messageSubscribeAction = onMessageSendAction;
        }

        void IControler.DestroyControl()
        {
            Destroy();
        }
    }
}