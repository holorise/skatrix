﻿
using System;
using SKTRX.DTOModel;
using SKTRX.Enums;
using UnityEngine;

namespace SKTRX.InputControllers
{
    public class ControlVer2 : BaseControler
    {
        public ControlVer2(){ControllType = ControlType.Control_v2;}

        private bool _left = false;
        private bool _right = false;

        public override void Show()
        {
            base.Show();
            _left = false;
            _right = false;
        }
        public override void Hide()
        {
            base.Hide();
            _left = false;
            _right = false;
        }

        /// <summary>
        /// Start listen controll from here
        /// </summary>
        public override void StartListeningForInstantiate()
        {
            instantiatePlayer = true;
            Show();
            TriggerMessage("Spawn avatar");
        }

        public override void InstantiatePlayer(Pose pose)
        {
            TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        /// <summary>
        /// Call after player get this control
        /// </summary>
        public override void StartControl()
        {
            //PLAYER INSTANTIATED SUCCEDD 
            TriggerMessage("Tap to player for start move");
            tapOnPlayerAction = StartMove;
        }

        private void StartMove()
        {
            tapOnPlayerAction = null;
            TriggerMessage("");

           TriggerInput(new ControlDTO()
            {
                inputType = InputType.StartMove
            });
        }

        private void FixedUpdate()
        {
            if (_left)
                LeftTurn();
            if (_right)
                RightTurn();
        }

        public void OnLeftClickUp()
        {
            _left = false;
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Forward
            });
        }

        public void OnLeftClickDown()
        {
            _left = true;
        }

        public void OnRightClickUp()
        {
            _right = false;
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Forward                
            });
        }

        public void OnRightClickDown()
        {
            _right = true;
        }

        private void LeftTurn()
        {
            Debug.Log("LeftTurn");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Left,
				isStaticTurn = true
            });
        }

        private void RightTurn()
        {
            Debug.Log("RightTurn");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Right,
				isStaticTurn = true
            });
        }

        public void OnJumpClick()
        {
            Debug.Log("OnJumpClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Jump
            });
        }

        public void OnFootClick()
        {
            Debug.Log("OnFootClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Foot
            });
        }

        public void OnHandClick()
        {
            Debug.Log("OnHandClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Hand
            });
        }
        public void OnRampClick()
        {
            Debug.Log("OnRampClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Rail
            });
        }

        public override void StopControl()
        {
           
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            Debug.LogWarning(string.Format("PlayerStateChange: {0}", playerState));

            if (playerState.Equals(PlayerState.Moving))
            {

            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }
    }
}