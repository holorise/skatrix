﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

namespace SKTRX.InputControllers
{
    public class SliderPanel : MonoBehaviour, IPointerUpHandler
    {
        [SerializeField] private Slider _slider;
        private Action<float> _onSliderPanelAction;

        public  void Show()
        {
            Activate(true);
        }

        public void Hide()
        {
            _onSliderPanelAction = null;
            Activate(false);
        }

        public void Show(Action<float> sliderValueChangeAction)
        {
            Activate(true);
            _onSliderPanelAction = sliderValueChangeAction;
            _slider.onValueChanged.AddListener(SlideChange);
        }

        private void SlideChange(float value)
        {
            _onSliderPanelAction?.Invoke(value);
        }

        private void Activate(bool active)
        {
            gameObject.SetActive(active);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            Activate(false);
        }
    }
}
