﻿using UnityEngine;
using System.Collections.Generic;
using SKTRX.Enums;
using System;
using SKTRX.DTOModel;

namespace SKTRX.InputControllers
{
    public class ControlVer3 : BaseControler
    {
        private const float INTENSITY = 20f;


        [SerializeField] private SwipePanel _swipePanel;
        public ControlVer3() { ControllType = ControlType.Control_v3; }

        private PlayerState _playerState = PlayerState.Standing;

        /// <summary>
        /// Start listen controll from here
        /// </summary>
        public override void StartListeningForInstantiate()
        {
            instantiatePlayer = true;
            Show();
            _swipePanel.Hide();
            TriggerMessage("Spawn avatar");
        }

        public override void InstantiatePlayer(Pose pose)
        {
            TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        /// <summary>
        /// Call after player get this control
        /// </summary>
        public override void StartControl()
        {
            //PLAYER INSTANTIATED SUCCEDD 
            TriggerMessage("Swipe up for start move or speed up, tap for jump, left right for turn.");
            _swipePanel.Show();
            _swipePanel.SubscrideOnSwipeComand(SwipeProcess);
        }

        public override void StopControl()
        {
        
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            Debug.LogWarning(string.Format("PlayerStateChange: {0}", playerState));
            _playerState = playerState;
            if (playerState.Equals(PlayerState.Moving))
            {

            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }

        private void SwipeProcess(List<Direction> commands)
        {
            if (commands.Count == 0)
            {
                if (_playerState.Equals(PlayerState.Moving))
                    base.TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Jump
                    });
            }
            if (commands.Count == 1)
            {
                if (commands.Contains(Direction.up))
                {
                    if (_playerState.Equals(PlayerState.Standing))
                    {
                        base.TriggerInput(new ControlDTO()
                        {
                            inputType = InputType.StartMove
                        });
                    }
                    else if (_playerState.Equals(PlayerState.Moving))
                    {
                        base.TriggerInput(new ControlDTO()
                        {
                            inputType = InputType.SpeedUp
                        });
                    }
                }
                else if (commands.Contains(Direction.down))
                {
                    if (_playerState.Equals(PlayerState.Standing))
                    {
                        base.TriggerInput(new ControlDTO()
                        {
                            inputType = InputType.StartMove
                        });
                    }
                    else if (_playerState.Equals(PlayerState.Moving))
                    {
                        base.TriggerInput(new ControlDTO()
                        {
                            inputType = InputType.SpeedDown
                        });
                    }
                }
                else if (commands.Contains(Direction.left))
                {
                    base.TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Left,
                        isStaticTurn = false
                    });
                }
                else if (commands.Contains(Direction.right))
                {
                    base.TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Right,
                        isStaticTurn = false
                    });
                }
            }
            else
            {
                //todo execute some tricks
            }
        }
    }
}