﻿
using System;
using SKTRX.DTOModel;
using SKTRX.Enums;
using UnityEngine;

namespace SKTRX.InputControllers
{

    public class ControlVer1 : BaseControler
    {
        public ControlVer1 () { ControllType = ControlType.Control_v1; }

        [SerializeField] private JoystikInput _joystikInput; //get direction 

        /// <summary>
        /// Start listen controll from here
        /// </summary>
        public override void StartListeningForInstantiate()
        {
            instantiatePlayer = true;
            Show();
            TriggerMessage("Spawn avatar");
        }

        public override void InstantiatePlayer(Pose pose)
        {
            TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        /// <summary>
        /// Call after player get this control
        /// </summary>
        public override void StartControl()
        {
            //PLAYER INSTANTIATED SUCCEDD 
            TriggerMessage("Tap the player to start moving");
            _joystikInput.Subscribe(ChangeDirection);
            tapOnPlayerAction = StartMove;
        }

        private void StartMove()
        {
            TriggerMessage("");
            tapOnPlayerAction = null;
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.StartMove
            });
        }

        private void ChangeDirection(Vector2 direction)
        {
            Debug.Log(string.Format("ChangeDirection: {0}",direction));
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.SetDirection,
                direction = direction
            });
        }

        public void OnJumpClick()
        {
            Debug.Log("OnJumpClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Jump
            });
        }

        public void OnFootClick()
        {
            Debug.Log("OnFootClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Foot
            });
        }

        public void OnHandClick()
        {
            Debug.Log("OnHandClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Hand
            });
        }

        public void OnRampClick()
        {
            Debug.Log("OnRampClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Rail
            });
        }

        public void OnMoveClick()
        {
            Debug.Log("OnMoveClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.StartMove
            });
        }

        public void OnTrickClick()
        {
            Debug.Log("OnTrickClick");
            base.TriggerInput(new ControlDTO()
            {
                inputType = InputType.Trick,
            });
        }

        public override void StopControl()
        {
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            Debug.LogWarning(string.Format("PlayerStateChange: {0}", playerState));

            if (playerState.Equals(PlayerState.Moving))
            {

            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }
    }
}