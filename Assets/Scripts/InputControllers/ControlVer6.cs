﻿using UnityEngine;
using System.Collections.Generic;
using SKTRX.Enums;
using SKTRX.DTOModel;

namespace SKTRX.InputControllers
{
    public class ControlVer6 : BaseControler
    {

        [SerializeField] private DragPanel _dragPanel;
        [SerializeField] private SwipePanel _swipePanel;
        private bool _pointerDown = false;

        public ControlVer6() { ControllType = ControlType.Control_v6; }

        /// <summary>
        /// Start listen controll from here
        /// </summary>
        public override void StartListeningForInstantiate()
        {
            instantiatePlayer = true;
            Show();
            TriggerMessage("Spawn avatar");
            //_dragPanel.Hide();
        }

        public override void InstantiatePlayer(Pose pose)
        {
            TriggerMessage("Player Instantiated");
            onPlayerInstantiate?.Invoke(pose);
        }

        /// <summary>
        /// Call after player get this control
        /// </summary>
        public override void StartControl()
        {
            //PLAYER INSTANTIATED SUCCEDD 
            TriggerMessage("");//Drag the avatar in the opposite direction that you want it to go
            _dragPanel.ShowSubscribeDrag(PlayerClickDownAction, PlayerClickUpAction, PlayerClickDragAction);
            _swipePanel.Hide();
        }

        public override void StopControl()
        {
            Debug.LogError("StopControl");
            StartControl();
        }

        public override void ChangePlayerState(PlayerState playerState)
        {
            Debug.LogWarning(string.Format("PlayerStateChange: {0}", playerState));

            if (playerState.Equals(PlayerState.Moving))
            {

            }
            else if (playerState.Equals(PlayerState.Stopping))
            {
                StartControl();
            }
        }
     
        private void PlayerClickDownAction()
        {
            TriggerMessage("Release the avatar to \"slingshot\" it across the play space");
            _swipePanel.Hide();
            _pointerDown = true;
        }

        private void PlayerClickUpAction(float distance)
        {
            if (_pointerDown)
            {
                _pointerDown = false;
                TriggerMessage("Swipe up to make a cude jump");
                _swipePanel.Show();
                _swipePanel.SubscrideOnSwipeComand(SwipeComand);
                distance = Mathf.Clamp(distance / 10f, 0, 1);
                Debug.Log("PlayerClickUpAction " + distance);
                if (distance > 0.001f)
                {
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.MoveWithSpeed,
                        velocity = distance
                    });
                    _dragPanel.Hide();
                    tapOnPlayerAction = StopPlayer;
                }
            }
        }

        private void StopPlayer()
        {
            tapOnPlayerAction = null;
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.Stop
            });
        }

        private void PlayerClickDragAction(Vector3 direction)
        {
            Debug.Log("PlayerClickDragAction");
            TriggerInput(new ControlDTO()
            {
                inputType = InputType.SetOppositDirection,
                direction3 = direction
            });
        }

        public void SwipeComand(List<Direction> swipeComandsAction)
        {
            if (swipeComandsAction.Count == 0)
            {
                base.TriggerInput(new ControlDTO()
                {
                    inputType = InputType.Forward,
                });
            }
            else
            {
                if (swipeComandsAction.Contains(Direction.up))
                {
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Jump
                    });
                }
                else if (swipeComandsAction.Contains(Direction.left))
                {
                    base.TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Left,
                        isStaticTurn = false
                    });
                }
                else if (swipeComandsAction.Contains(Direction.right))
                {
                    base.TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.Right,
                        isStaticTurn = false
                    });
                }
                else if (swipeComandsAction.Contains(Direction.down))
                {
                    TriggerInput(new ControlDTO()
                    {
                        inputType = InputType.SpeedUp
                    });
                }
            }
        }
    }
}