﻿using UnityEngine;
using System;

namespace SKTRX.InputControllers
{
    public class ArrowControl : MonoBehaviour
    {
        private Action _upPointerClick;
        private Action _downPointerClick;
        private Action _jumpClick;

        public void Subscribe(Action upPointClick, Action downPointClick, Action jump)
        {
            _upPointerClick = upPointClick;
            _downPointerClick = downPointClick;
            _jumpClick = jump;
        }

        public void OnClickUp()
        {
            _upPointerClick?.Invoke();
        }

        public void OnClickDown()
        {
            _downPointerClick?.Invoke();
        }

        public void OnClickJump()
        {
            _jumpClick?.Invoke();
        }
    }
}