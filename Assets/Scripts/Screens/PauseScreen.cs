﻿
#pragma warning disable 649
using UnityEngine;
using System;
using UnityEngine.UI;
using SKTRX.Managers;
using SKTRX.Tools;

namespace SKTRX.Screens
{
    public class PauseScreen : BaseScreen
    {
        
        public override void Show(Action onScreenComplete)
        {
            base.Show(onScreenComplete);
            UpdateScreen();
        }

        public void OnRestartClick()
        {
            GameManager.Instance.RestartPlayer();
            Hide();

            //OnClickClose();
        }

        public void OnRestartObstacleClick()
        {
            GameManager.Instance.RestartObstaclePlayer();
            Hide();

            //OnClickClose();
        }

        public void OnRestartScanClick()
        {
            GameManager.Instance.RestartRealObjectTracking();
            Hide();

            //OnClickClose();
        }

        public void OnRescanClick()
        {
#if UNITY_IOS && !UNITY_EDITOR
            IOSARManager.Instance.DestroyDetectedObjects();
#endif
            GameManager.Instance.StartScenario();
            ObstacleManager.Instance.DeleteAll();
            Hide();
            //OnClickClose();
        }

        public void OnRestartControlClick()
        {
            GameManager.Instance.RestartControl();
            Hide();

            //OnClickClose();
        }

        public void OnUpScalePlayer()
        {
            PlayersManager.Instance.UpScaleClick();
        }

        public void OnDownScalePlayer()
        {
            PlayersManager.Instance.DownScaleClick();
        }

        public void OnClickClose()
        {
            Hide();
            base.CompleteScreen();
        }

        private void UpdateScreen()
        {
            //todo update screen

        }

        [SerializeField] private Toggle _vibrationTogle;
        private void OnEnbale()
        {
            _vibrationTogle.isOn = VibrationTool.VibrationAllow;
        }

        public void OnVibrationChange()
        {
            VibrationTool.VibrationAllow = _vibrationTogle.isOn;
        }
    }
}