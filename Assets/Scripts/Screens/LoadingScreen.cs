﻿using UnityEngine;
using UnityEngine.UI;

namespace SKTRX.Screens
{
    public class LoadingScreen : BaseScreen
    {
        [SerializeField] private Text _messageText;

        public LoadingScreen()
        {
            ScreenType = Enums.ScreenType.Loading;
        }
    }
}