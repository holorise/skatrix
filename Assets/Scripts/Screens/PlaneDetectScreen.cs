﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SKTRX.Screens
{
    public class PlaneDetectScreen : BaseScreen
    {
        public Action NextScreenClicked = delegate { };
        [SerializeField] private Text _messageText;
        [SerializeField] private Button _button;

        public PlaneDetectScreen()
        {
            ScreenType = Enums.ScreenType.PlaneDetect;
        }

        public void ShowMessage(string text)
        {
            _messageText.text = text;
        }

        public void SetActiveButton(bool isActive)
        {
            _button.gameObject.SetActive(isActive);
        }

        public Action GetSelectButtonDelegate()
        {
            return () =>
            {
                base.CompleteScreen();
                Hide();
            };
        }

        public void OnOkClick()
        {
            NextScreenClicked.Invoke();
        }
    }
}