﻿

#pragma warning disable 649
using UnityEngine;
using UnityEngine.UI;
using SKTRX.Enums;

namespace SKTRX.Screens
{
    public class ObjectPlaceScreen : BaseScreen
    {
        [SerializeField] private ObstacleMenu _obstacleMenu;
        [SerializeField] private Image _holdImage;
        [SerializeField] private Text _messageText;

        public ObjectPlaceScreen()
        {
            ScreenType = ScreenType.ObjectPlace;
        }

        public void ShowObstacleMenu(bool show)
        {
            _obstacleMenu.ShowMenu(show);
        }

        public void ShowMessage(string text)
        {
            _messageText.text = text;
        }

        public void ShowHoldIndicator(Vector2 position, bool show = true, float fill = 0f)
        {
            _holdImage.transform.position = position;
            _holdImage.enabled = show && fill < 1;
            _holdImage.fillAmount = fill;
        }

        public void OnDoneClick()
        {
            base.CompleteScreen();
            base.Hide();
        }

        public override void Show()
        {
            ShowObstacleMenu(false);
            base.Show();
        }
        public override void Hide()
        {
            ShowObstacleMenu(false);
            base.Hide();
        }
    }
}