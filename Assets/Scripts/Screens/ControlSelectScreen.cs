﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using SKTRX.Enums;
using SKTRX.DTOModel;
using SKTRX.Managers;
using SKTRX.UI;

namespace SKTRX.Screens
{
    public class ControlSelectScreen : BaseScreen
    {

        [SerializeField] private ControlOptionUI _controlOptionUIPrefab;
        [SerializeField] private Transform _parentVariant;
        [SerializeField] private Button _startGameButton;

        private List<ControlOptionUI> _options;
        private ControlOptionUI _currentControlOptionUI;

        public ControlSelectScreen()
        {
            base.ScreenType = ScreenType.ControlVariantSelect;
        }

        public override void Show<T>(Action<T> onScreenDTOComplete)
        {
            base.Show(onScreenDTOComplete);
            UpdateVariants();
            HoverCurrentVariant();
            SetGameStartInteractable(_currentControlOptionUI);
            if(ControlManager.Instance.Controllers.Length == 1)
            {
                ScreenDTO screenDTO = new ScreenDTO();
                screenDTO.ControlType = ControlManager.Instance.Controllers[0].ControllType;
                base.CompleteScreen(screenDTO);
            }
        }

        public void OnClickPlay()
        {
            base.CompleteScreen(GetScreenOptions());
        }

        private void UpdateVariants()
        {
            if (_options == null)
            {
                _options = new List<ControlOptionUI>(ControlManager.Instance.Controllers.Length);
                foreach (var control in ControlManager.Instance.Controllers)
                {
                    ControlOptionUI option = Instantiate(_controlOptionUIPrefab, _parentVariant);
                    option.UpdateObject(control.GetControllOption(), SelectOption);
                    _options.Add(option);
                }
            }
        }

        private void SelectOption(ControlOptionUI controlOptionUI)
        {
            _options.ForEach(p => p.Deselect());
            _currentControlOptionUI = controlOptionUI;
            SetGameStartInteractable(_currentControlOptionUI);
            HoverCurrentVariant();
        }

        private void HoverCurrentVariant()
        {
            if(_currentControlOptionUI)
                _currentControlOptionUI.Select();
        }

        private ScreenDTO GetScreenOptions()
        {
            ScreenDTO screenDTO = new ScreenDTO();
            screenDTO.ControlType = _currentControlOptionUI.ControlType;
            return screenDTO;
        }

        private void SetGameStartInteractable(bool interactable)
        {
            _startGameButton.interactable = interactable;
        }
    }
}