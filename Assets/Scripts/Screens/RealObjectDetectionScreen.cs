﻿
#pragma warning disable 649
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.Enums;
using UnityEngine.UI;
using SKTRX.Managers;

namespace SKTRX.Screens
{
    public class RealObjectDetectionScreen : BaseScreen
    {
        [SerializeField] private Text _messageText;

        private void Start()
        {


        }

        private void OnEnable()
        {

        }

        public RealObjectDetectionScreen()
        {
            base.ScreenType = ScreenType.RealObjectDetection;
        }

        public void ShowMessage(string text)
        {
            _messageText.text = text;
        }

        public override void Show<T>(Action<T> onScreenDTOComplete)
        {
            base.Show(onScreenDTOComplete);
        }

        public override void Show(Action onScreenComplete)
        {
            base.Show(onScreenComplete);

#if UNITY_IOS && !UNITY_EDITOR
            IOSARManager.Instance.SetWorldOrigin(PlaneDetectionManager.Instance.GetPlaneTransform());
            IOSARManager.Instance.CreatePredefinedObjects();
            ShowMessage("Scan objects");
#endif
        }

        public void OnDoneClick()
        {
#if UNITY_IOS && !UNITY_EDITOR

#endif
            base.CompleteScreen();
            base.Hide();
        }
            public void OnSelectClick()
        {
            ScannedObjectSerializationManager.instance.TurnOffPoints();
            ScannedObjectSerializationManager.instance.CreateRefObject(delegate ()
            {
                /*
                if (_arManager == null)
                    Debug.Log("@@@ _arManager is null");
                else
                {
                    _arManager.SetObjScaningActive(false);
                    _arManager.SetObjTrackingActive(true);
                }
                */
            });

        }
    }
}
