﻿

#pragma warning disable 649
using UnityEngine;
using System;
using SKTRX.Enums;
using SKTRX.DTOModel;

namespace SKTRX.Screens
{
    public abstract class BaseScreen : MonoBehaviour
    {
        public ScreenType ScreenType { get; protected set; } = ScreenType.None;

        private Action _onScreenCompleteAction;
        private Action<ScreenDTO> _onScreenCompleteDTOAction;

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual bool IsShow()
        {
           return gameObject.activeSelf;
        }

        public virtual void Show(Action onScreenComplete)
        {
            Show();
            _onScreenCompleteAction = onScreenComplete;
        }

        public virtual void Show<T>(Action<T> onScreenDTOComplete) where T : ScreenDTO
        {
            Show();
            _onScreenCompleteDTOAction = (Action<ScreenDTO>)onScreenDTOComplete;
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        protected virtual void CompleteScreen()
        {
            _onScreenCompleteAction?.Invoke();
        }

        protected virtual void CompleteScreen(ScreenDTO screenDTO)
        {
            _onScreenCompleteDTOAction?.Invoke(screenDTO);
        }
    }
}