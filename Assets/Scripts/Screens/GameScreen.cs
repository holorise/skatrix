﻿
#pragma warning disable 649
using System;
using UnityEngine;
using UnityEngine.UI;
using SKTRX.Enums;
using SKTRX.InputControllers;
using SKTRX.Managers;

namespace SKTRX.Screens
{
    public class GameScreen : BaseScreen
    {
        [SerializeField] private Transform _controlParent;
        [SerializeField] private Text _messageText;
        [SerializeField] private PauseScreen _pauseScreen;
        [SerializeField] private Button _pauseButton;
        [SerializeField] private Button _respawnButton;
        [SerializeField] private Toggle _showForward;
        private BaseControler _currentController;
        public static bool ShowForward = false;
        public GameScreen()
        {
            ScreenType = Enums.ScreenType.GameScreen;
        }

        public void ShowUIStep(GameScreenStep screenStep)
        {
            HideAll();
            if (screenStep.Equals(GameScreenStep.Control))
                _currentController.Show();
        }

        public override void Show(Action onScreenComplete)
        {
            _pauseButton.gameObject.SetActive(true);
            base.Show(onScreenComplete);
            _pauseScreen.Hide();
        }

        public void InstantiateController(Func<BaseControler> control)
        {
            _currentController = control?.Invoke();
            _currentController.SetParent(_controlParent);
        }

        public void ShowForwardToggle()
        {
            ShowForward = _showForward.isOn;
        }

        public void ShowMessage(string message)
        {
            _messageText.text = message;
        }

        public void SetRespawnButtonActive(bool isActive)
        {
            _respawnButton.gameObject.SetActive(isActive); 
        }

        public void OnClickBack()
        {
            base.CompleteScreen();
        }

        private void HideAll()
        {
            _currentController.Hide();
        }

        public void OnRaspawnClick()
        {
            PlayersManager.Instance.RespawnPlayer();
        }

        public void OnPauseClick()
        {
            PlayersManager.Instance.PausePlayer();
            _pauseScreen.Show(()=> ResumeGame());
            if(_currentController != null) 
                _currentController.Hide();
            _pauseButton.gameObject.SetActive(false);
        }

        private void ResumeGame()
        {
            PlayersManager.Instance.ResumePlayer();
            if (_currentController != null)
                _currentController.Show();
            _pauseButton.gameObject.SetActive(true);
        }
    }
}