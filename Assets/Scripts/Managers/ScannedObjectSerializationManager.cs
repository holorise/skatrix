﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using UnityEngine;
using SKTRX.AR.SerializableObjects;
using UnityEngine.XR.iOS;
using SKTRX.Enums;
using SKTRX.Screens;
using SKTRX.AR;

namespace SKTRX.Managers
{
    public class ScannedObjectSerializationManager : MonoBehaviour
    {
        public static ScannedObjectSerializationManager instance = null;
        public GameObject boxScannerObject;
        public GameObject pointParticleSystem;

        private SerializableScanObjectList _scanObjList;
        private List<SerializableScanObject> _editObjList;
        private List<ARReferenceObject> _arRefObjList;
        private ScannerPickBoundingBox _pickBoundingBox;
        private RealObjectDetectionScreen _detectScreen;
        private AnchorPointCloudParticleExample _pointCloudParticle;
        private Action _createArRefObjCompleteAction;

        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                Destroy(gameObject);

            //Sets this to not be destroyed when reloading scene
            //DontDestroyOnLoad(gameObject);

            //Call the Init function 
            Init();
        }

        //Initializes the class.
        void Init()
        {
            _pointCloudParticle = pointParticleSystem.GetComponent<AnchorPointCloudParticleExample>();
            _pickBoundingBox = boxScannerObject.GetComponent<ScannerPickBoundingBox>();
           /// _detectScreen = ScreenManager.Instance.Get<RealObjectDetectionScreen>(ScreenType.RealObjectDetection);

            if (_pickBoundingBox == null)
            {
                Debug.Log("@@@Pick Bounding Box is null");
            }

            _editObjList = new List<SerializableScanObject>();
            _arRefObjList = new List<ARReferenceObject>();
            _scanObjList = new SerializableScanObjectList();
            TurnOnPoints();
            TurnOnBoundigBox();
            // LoadScanObjData();
            // LoadARReferenceObject();
        }

        public void TurnOnPoints()
        {
            if (_pointCloudParticle == null)
            {
                Debug.Log("@@@ AnchorPointCloudParticleExample is null");
                return;
            }

            _pointCloudParticle.TurnOn();
        }

        public void TurnOffPoints()
        {
            if(_pointCloudParticle == null)
            {
                Debug.Log("@@@ AnchorPointCloudParticleExample is null");
                return;
            }

            _pointCloudParticle.TurnOff();
        }

        public void TurnOnBoundigBox()
        {
            if (_pickBoundingBox == null)
            {
                Debug.Log("@@@ ScannerPickBoundingBox is null");
                return;
            }

            Debug.Log("@@@ Turn on bounding box");

            _pickBoundingBox.gameObject.SetActive(true);
        }

        public void TurnOffBoundingBox()
        {
            if (_pickBoundingBox == null)
            {
                Debug.Log("@@@ ScannerPickBoundingBox is null");
                return;
            }

            Debug.Log("@@@ Turn off bounding box");

            _pickBoundingBox.gameObject.SetActive(false);
        }

        public void ShowHintMessage(string message)
        {
            if (_detectScreen == null) return;

            _detectScreen.ShowMessage(message);
        }

        private void LoadScanObjData()
        {
            string filePath = Path.Combine(Application.streamingAssetsPath, "scannedObjList.json");

            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                SerializableScanObjectList loadedData = JsonUtility.FromJson<SerializableScanObjectList>(dataAsJson);

                if (loadedData != null)
                    _scanObjList = loadedData;
            }
            else
            {
                Debug.Log("@@@Cannot load scanned obj data!");
                _scanObjList = new SerializableScanObjectList();
            }
        }

        private void LoadARReferenceObject()
        {
            if (UnityARSessionNativeInterface.IsARKit_2_0_Supported() == false)
            {
                return;
            }

            foreach(SerializableScanObject objData in _scanObjList.objList)
            {
                if (objData != null)
                {
                    string filePath = objData.filePath;

                    if (File.Exists(filePath))
                    {
                        ARReferenceObject arro = ARReferenceObject.Load(filePath);
                        arro.name = objData.name;
                        _arRefObjList.Add(arro);
                    }
                }
                else
                {
                    Debug.Log("@@@ SKTRXScanObject is null");
                }
            }
        }

        private void SaveScanObjData()
        {
            string dataAsJson = JsonUtility.ToJson(_scanObjList);
            string pathToSaveTo = Path.Combine(Application.persistentDataPath, "SKTRXARReferenceObjects");

            if (!Directory.Exists(pathToSaveTo))
            {
                Directory.CreateDirectory(pathToSaveTo);
            }

            string fullPath = Path.Combine(pathToSaveTo, "scannedObjList.json");
            File.WriteAllText(fullPath, dataAsJson);
        }

        private void SaveARScanObjList()
        {
            if (_arRefObjList.Count == 0)
                return;

            string pathToSaveTo = Path.Combine(Application.persistentDataPath, "SKTRXARReferenceObjects");

            if (!Directory.Exists(pathToSaveTo))
            {
                Directory.CreateDirectory(pathToSaveTo);
            }

            foreach (ARReferenceObject arro in _arRefObjList)
            {
                string fullPath = Path.Combine(pathToSaveTo, arro.name + ".arobject");

                Debug.Log("@@@ Did Save object on path:" + fullPath);

                arro.Save(fullPath);
            }
        }

        public List<ARReferenceObject> ARReferenceObjects
        {
            set { _arRefObjList = value; }
            get { return _arRefObjList; }
        }

        public SerializableScanObject EditScannedObject()
        {
            if (_scanObjList.objList.Count > 0)
                return _scanObjList.objList[_scanObjList.objList.Count - 1];

            Debug.Log("@@@_scanObjList.Count == 0");
            return null;
        }

        public ARReferenceObject EditARReferenceObject()
        {
            if (_arRefObjList.Count > 0)
                return _arRefObjList[_arRefObjList.Count - 1];

            Debug.Log("@@@_arRefObjList.Count == 0");
            return null;
        }

        public void SetEditScannedObject(SerializableScanObject objData)
        {
            if (_scanObjList.objList.Count > 0)
                 _scanObjList.objList[_scanObjList.objList.Count - 1] = objData;
        }

        public void CreateRefObject(Action completeAction)
        {
            if(_pickBoundingBox == null)
            {
                Debug.Log("@@@ Pick Bounding Box is null");
            }

            //this script should be placed on the bounding volume GameObject
            CreateReferenceObject(_pickBoundingBox.transform, _pickBoundingBox.bounds.center - _pickBoundingBox.transform.position, _pickBoundingBox.bounds.size, completeAction);
        }

        private void CreateReferenceObject(Transform objectTransform, Vector3 center, Vector3 extent, Action completeAction)
        {
            Debug.Log("@@@CreateReferenceObject");

            UnityARSessionNativeInterface.GetARSessionNativeInterface().ExtractReferenceObjectAsync(objectTransform, center, extent, (ARReferenceObject referenceObject) => {
                if (referenceObject != null)
                {
                    Debug.LogFormat("@@@ARReferenceObject created: center {0} extent {1}", referenceObject.center, referenceObject.extent);
                    referenceObject.name = "objScan_" + DateTime.Now.ToFileTime();
                    Debug.LogFormat("@@@ARReferenceObject has name {0}", referenceObject.name);
                    AddARReferenceObject(referenceObject);
                    completeAction?.Invoke();
                }
                else
                {
                    Debug.Log("@@@Failed to create ARReferenceObject.");
                    ShowHintMessage("Scanner can't create an object reference. No enough anchors points in the scanner box! Please scan the object againe!");
                }
            });
        }

        private void AddARReferenceObject(ARReferenceObject referenceObject)
        {
            if (referenceObject == null)
            {
                Debug.LogError("@@@AddARReferenceObject == null");
                return;
            }

            string pathToSaveTo = Path.Combine(Application.persistentDataPath, "SKTRXARReferenceObjects");
            string fullPath = Path.Combine(pathToSaveTo, referenceObject.name + ".arobject");

            int maxAverageRate = 1;

            //if(_scanObjList.objList.Count > 0)
            //    maxAverageRate = _scanObjList.objList.Max(r => r.index) + 1;
            SerializableScanObject scanObj = new SerializableScanObject();
            scanObj.name = referenceObject.name;
            scanObj.index = maxAverageRate;
            scanObj.filePath = fullPath;

            _arRefObjList.Add(referenceObject);
            _scanObjList.objList.Add(scanObj);

            Debug.Log("@@@ AddARReferenceObject did end arRefObjList.Count: " + _arRefObjList.Count);

        }

        public List<SerializableScanObject> GetScannedObjectsList()
        {
            return _scanObjList.objList;
        }

        public void SaveData()
        {
            Debug.Log("@@@ Save Data");

            SaveARScanObjList();
            SaveScanObjData();
        }
    }
}
