﻿using UnityEngine;
using Tayx.Graphy.Utils;
using SKTRX.UI;
using System.Collections.Generic;
using System;
using System.Linq;
using Tayx.Graphy;

namespace SKTRX.Managers.PerformanceTracking
{
    public class PerformanceItemData
    {
        public string LabelName;
        public float Value; 
    }

    public class PerformanceTrackingManager : G_Singleton<PerformanceTrackingManager>
    {

        public event Action<List<string>> ItemsAdded = delegate { };
        private const string TRIS_LABEL = "Tris count";
        private const string BRIGHTNESS_LABEL = "Average Brightnes";
        private const float SPACING = 5f;

        [SerializeField] private PerformanceItemView _itemView;
        [SerializeField] private Transform _graphyParent;
        [SerializeField] private Transform _meshesParent;
        private Dictionary<string, PerformanceItemView> _itemsDict;
        private List<PerformanceItemData> _performanceItems;
      
        private void Awake()
        {
            _itemsDict = new Dictionary<string, PerformanceItemView>();
            _performanceItems = new List<PerformanceItemData>();
        }

        private void OnEnable()
        {
            //ARSubsystemManager.cameraFrameReceived += OnCameraFrameReceived;
        }

        private void OnDisable()
        {
            //ARSubsystemManager.cameraFrameReceived -= OnCameraFrameReceived;
        }

        /*
        void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            if (eventArgs.lightEstimation.averageBrightness.HasValue)
            {
                var value = eventArgs.lightEstimation.averageBrightness.Value;
                var item = _performanceItems.Find(x => x.LabelName == BRIGHTNESS_LABEL);

                if (item == null)
                {
                    item = new PerformanceItemData();
                    item.LabelName = BRIGHTNESS_LABEL;
                }

                item.Value = value;
                UpdateItemView(BRIGHTNESS_LABEL, value.ToString());

            }
        }
        */
    
        public void AddItems()
        {
            var item = new PerformanceItemData();
            item.LabelName = TRIS_LABEL;
            item.Value = 0;
            AddPerformanceItem(item);
            UpdateTrisCountInScene();

            item = new PerformanceItemData();
            item.LabelName = BRIGHTNESS_LABEL;
            item.Value = 0;
            AddPerformanceItem(item);

            var keys = _itemsDict.Keys.ToList();
            ItemsAdded(keys);

            GraphyManager.Instance.Disable();
        }

        public List<PerformanceItemData> GetPerformanceValues()
        {
            return _performanceItems;
        }

        public void SetActiveItems(bool isActive)
        {
            foreach (var item in _itemsDict)
            {
                item.Value.gameObject.SetActive(isActive);
            }
        }

        public void UpdateTrisCountInScene()
        {
            var item = _performanceItems.Find(x => x.LabelName == TRIS_LABEL);
            if (item == null)
            {
                item = new PerformanceItemData();
                item.LabelName = TRIS_LABEL;
            }
            var value = item.Value;

            _meshesParent = _meshesParent == null ? transform.root : _meshesParent;
            var meshes = _meshesParent.GetComponentsInChildren<MeshFilter>();
            var length = meshes.Length;
            for (int i = 0; i < length; i++)
            {
                var meshFilter = meshes[i];
                value += meshFilter.sharedMesh.triangles.Length / 3;
            }
         

            item.Value = value;
            UpdateItemView(TRIS_LABEL, value.ToString());
        }

        private void UpdateItemView(string labelName, string value)
        {
            if(_itemsDict.ContainsKey(labelName))
            {
                _itemsDict[labelName].UpdateValue(value);
            }
        }

        private void AddPerformanceItem(PerformanceItemData item, string valueUnit = "")
        {
            _performanceItems.Add(item);
            var view = Instantiate(_itemView, _graphyParent);
            var rect = view.GetComponent<RectTransform>();
            var label = item.LabelName;
            var value = item.Value.ToString();
            rect.anchoredPosition = GetNextItemLocalPosition(rect.sizeDelta.y);
            view.SetValues(label, value, valueUnit);
            _itemsDict[label] = view;
        }

        private Vector3 GetNextItemLocalPosition(float ySize)
        {
            var pos = new Vector3();
            var childsCount = _graphyParent.childCount;
            var lastActiveIndex = 0;
            for (int i = childsCount - 2; i > 0; i--)
            {
                var go = _graphyParent.GetChild(i).gameObject;
                if(go.activeSelf)
                {
                    lastActiveIndex = i;
                    break;
                }
            }

            var lastChild = _graphyParent.GetChild(lastActiveIndex);
            //Debug.LogError(lastChild.name);
            var rect = lastChild.GetComponent<RectTransform>();
            pos.x = rect.anchoredPosition.x;
            pos.y = rect.anchoredPosition.y - rect.sizeDelta.y/2 - ySize/2 - SPACING;

            return pos;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {

                var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                go.transform.SetParent(_meshesParent);
                UpdateTrisCountInScene();
            }
        }
#endif

    }
}
