﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tayx.Graphy;
using UnityEngine;
using SKTRX.Extensions;

namespace SKTRX.Managers.PerformanceTracking
{
    public class PerformanceDataSaverManager : MonoBehaviour
    {
        private const char KOMA = ',';
        private const string EXTENSION_CSV = ".csv";
        private const string NAME_PREF_KEY = "LastName";
        private const string FILE_NAME = "PerformanceData_";
        private const string DATE = "Date,";
        private const string FPS_AVG = "FPS average,";
        private const string FPS_CURR = "FPS current,";
        private const string FPS_MIN = "FPS min,";
        private const string FPS_MAX = "FPS max,";
        private const string RAM_RESERVED = "RAM reserved,";
        private const string RAM_ALLOCATED = "RAM allocated,";
        private const string RAM_MONO = "RAM mono,";

        [SerializeField] private float _savePeriodTime = 5f;

        [SerializeField] private PerformanceTrackingManager _performanceManager;
        [SerializeField] private GraphyManager _graphyManager;
        private StringBuilder _stringBuilder;

        private void OnEnable()
        {
            _performanceManager.ItemsAdded += AddInitialStrings;
        }

        private void OnDisable()
        {
            _performanceManager.ItemsAdded -= AddInitialStrings;
        }
       

        private void AddInitialStrings(List<string> additionalKeys)
        {
            _stringBuilder = new StringBuilder();
            _stringBuilder.Append(DATE);
            _stringBuilder.Append(FPS_AVG);
            _stringBuilder.Append(FPS_CURR);
            _stringBuilder.Append(FPS_MIN);
            _stringBuilder.Append(FPS_MAX);
            _stringBuilder.Append(RAM_RESERVED);
            _stringBuilder.Append(RAM_ALLOCATED);
            _stringBuilder.Append(RAM_MONO);

            var length = additionalKeys.Count;

            for (int i = 0; i < length; i++)
            {
                var str = additionalKeys[i];
                str += KOMA;
                _stringBuilder.Append(str);
            }
           
            StartCoroutine(WaitAndDo(SaveData));

        }

        private void SaveData()
        {
            _stringBuilder.AppendLine();
            AddCSVValue(DateTime.Now);
            AddCSVValue(_graphyManager.AverageFPS.FormatCSVString());
            AddCSVValue(_graphyManager.CurrentFPS.FormatCSVString());
            AddCSVValue(_graphyManager.MinFPS.FormatCSVString());
            AddCSVValue(_graphyManager.MaxFPS.FormatCSVString());
            AddCSVValue(_graphyManager.ReservedRam.FormatCSVString());
            AddCSVValue(_graphyManager.AllocatedRam.FormatCSVString());
            AddCSVValue(_graphyManager.MonoRam.FormatCSVString());

            var extraValues = _performanceManager.GetPerformanceValues();
            var length = extraValues.Count;

            for (int i = 0; i < length; i++)
            {
                AddCSVValue(extraValues[i].Value);
            }
        }


        private void AddCSVValue<T>(T value) 
        {
            _stringBuilder.Append(value);
            _stringBuilder.Append(KOMA);
        }

        private IEnumerator WaitAndDo(Action action)
        {
            yield return new WaitForSeconds(_savePeriodTime);
            action?.Invoke();
            StartCoroutine(WaitAndDo(action));
        }

        private string GetNextFileName()
        {
            var lastVersion = PlayerPrefs.GetInt(NAME_PREF_KEY);
            var nextVersion = lastVersion + 1;
            var fileName = FILE_NAME + nextVersion.ToString() + "_" + EXTENSION_CSV;
            PlayerPrefs.SetInt(NAME_PREF_KEY, nextVersion);
            return fileName;
        }

        private void SaveToFile()
        {
            var path = Application.persistentDataPath + "/" + GetNextFileName();
            //Debug.LogError(path);
            if (_stringBuilder != null)
            {
                //File.Create(path).Close();
                File.WriteAllText(path, _stringBuilder.ToString());
            }
        }

        private void OnApplicationPause(bool pause)
        {
            SaveToFile();
        }

        private void OnApplicationQuit()
        {
            SaveToFile();
        }

    }
}
