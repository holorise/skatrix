﻿

#pragma warning disable 649
using UnityEngine;
using System;
using System.Collections.Generic;
using SKTRX.Obstacle;
using Tools;
using SKTRX.Statics;
using SKTRX.Managers.PerformanceTracking;
using UnityEngine.EventSystems;
using System.Linq;
using System.Collections;
using SKTRX.Obstacle.Rail;

namespace SKTRX.Managers
{
    public class ObstacleManager : Singelton<ObstacleManager>
    {
        [SerializeField] private List<BaseObstacle> _obstacles;
        [SerializeField] private Transform _obstacleParent;
        [SerializeField] private List<BaseObstacle> _instantiatedObstacle = new List<BaseObstacle>();

        [Header("Rail parts")]
        [SerializeField] private RailPoint _railPointPrefab;
        [SerializeField] private RailLine _railLinePrefab;
        [Header (" ")]

        private bool _menuShowed = false;
        private Dictionary<int, BaseObstacle> _obstacleDictionary;
        private float _realTime = 0;
        private Action _inputCommand;
        private BaseObstacle _holdObstacle = null;
        private float _timeEndHold = .7f;
        private float _thresholdClick = .3f;
        private bool _menuShow = false;
        private Action<Vector3> _onPlaneTapAction;
        private Action<BaseObstacle, Vector3> _onObstacleTapAction;

        private Dictionary<int, BaseObstacle> ObstacleDictionary
        {
            get
            {
                if (_obstacleDictionary == null)
                {
                    _obstacleDictionary = new Dictionary<int, BaseObstacle>();
                    for (int i = 0; i < _obstacles.Count; i++)
                    {
                        if (_obstacleDictionary.ContainsKey(_obstacles[i].ObstacleDTO.GetHashCode()))
                            throw new Exception(string.Format("[ObstacleManager] key exist for {0} {1}", _obstacles[i].ObstacleDTO.Title, _obstacles[i].ObstacleDTO.Description));
                        else
                            _obstacleDictionary.Add(_obstacles[i].ObstacleDTO.GetHashCode(), _obstacles[i]);
                    }
                }
                return _obstacleDictionary;
            }
        }

        private ScreenManager _screenManager { get => ScreenManager.Instance; }
       

        public void SubscribeOnTaps(Action<Vector3> onPlaneTapAction, Action<BaseObstacle, Vector3> onObstacleTapAction)
        {
            _onPlaneTapAction = onPlaneTapAction;
            _onObstacleTapAction = onObstacleTapAction;
        }

        public void UnSubscribe()
        {
            _onPlaneTapAction = null;
            _onObstacleTapAction = null;
        }
        public List<BaseObstacle> Obstacles { get => _obstacles; }
        public RailPoint RailPointPrefab { get => _railPointPrefab; }
        public RailLine RailLinePrefab { get => _railLinePrefab; }

        public BaseObstacle GetObstacle(int obstacleDTOHash)
        {
            if(ObstacleDictionary.ContainsKey(obstacleDTOHash))
            {
                var newObstacle = Instantiate(ObstacleDictionary[obstacleDTOHash], GetObstacleParent());
#if PERF_TRACK
               PerformanceTrackingManager.Instance.UpdateTrisCountInScene();
#endif
                _instantiatedObstacle.Add(newObstacle);
                return newObstacle;
            }
            else
            {
                throw new Exception(string.Format("[ObstacleManager] obstacle not found hash: {0}", obstacleDTOHash));
            }
        }

        public void DeleteAll()
        {
            if(_instantiatedObstacle != null)
            {
                for(int i=0;i< _instantiatedObstacle.Count; i++)
                {
                    _instantiatedObstacle[i].Destroy();
                }
                _instantiatedObstacle.Clear();
            }
        }

        private Transform GetObstacleParent()
        {
            /*if (PlaneDetectionManager.Instance.GetPlaneTransform() != null)
                return PlaneDetectionManager.Instance.GetPlaneTransform();*/
            return _obstacleParent;
        }

        private void Update()
        {

            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
            {
                _holdObstacle = null;
                Ray raycast = CameraObject.Camera.ScreenPointToRay(GetPointPosition());

                List<RaycastResult> results = new List<RaycastResult>();

                PointerEventData pointerData = new PointerEventData(EventSystem.current);
                pointerData.position = GetPointPosition();
                EventSystem.current.RaycastAll(pointerData, results);
                results.RemoveAll(p => p.gameObject.tag.Equals("DragPanel"));


                RaycastHit[] raycastHit = Physics.RaycastAll(raycast);
                List<RaycastHit> hits = raycastHit.ToList<RaycastHit>();
                RaycastHit obstacle = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.OBSTACLE_TAG));
                RaycastHit plane = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLANE_TAG));
                List<RaycastResult> uiTouches = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, uiTouches);
                uiTouches.RemoveAll(p => p.gameObject.tag.Equals(TagManager.DRAG_PANEL_TAG));


                if (obstacle.collider != null)
                {
                    if (_holdObstacle == null)
                    {
                        BaseObstacle targetObstacle;
                        if (GetObstacleByCollider(obstacle.collider, out targetObstacle))
                        {
                            _holdObstacle = targetObstacle;
                        }
                    }
                    _inputCommand = delegate
                    {
                        if (uiTouches.Count == 0)
                            TapOnObstacle(GetPointPosition(), obstacle.collider, obstacle.point);
                    };
                    _realTime = Time.realtimeSinceStartup;
                }
                else if (plane.collider != null)
                {
                    if (results.Count == 0)
                    {
                        _inputCommand = delegate
                        {
                            if (uiTouches.Count == 0)
                                TapOnPlane(); _onPlaneTapAction?.Invoke(plane.point);
                        };
                        _realTime = Time.realtimeSinceStartup;
                    }
                }
            }
            else if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary) || Input.GetMouseButton(0))
            {
                if ((Time.realtimeSinceStartup - _realTime) > _thresholdClick)
                {
                    _inputCommand = null;

                    if (_holdObstacle != null && !_menuShow)
                    {
                        HideMenu();
                        float current = Time.realtimeSinceStartup - _realTime;
                        float target = _timeEndHold + _thresholdClick;
                        ShowHoldIndicator(GetPointPosition(), true, current / target);
                        if (current > target)
                        {
                            MoveObstacle();
                        }
                    }
                }
            }
            else if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
            {
                if ((Time.realtimeSinceStartup - _realTime) < 0.3f)
                {
                    _inputCommand?.Invoke();
                    _inputCommand = null;
                    _holdObstacle = null;
                }
                HideHoldIndicator();
            }
        }

        private void MoveObstacle()
        {
            Ray raycast = CameraObject.Camera.ScreenPointToRay(GetPointPosition());
            RaycastHit[] raycastHit = Physics.RaycastAll(raycast);
            List<RaycastHit> hits = raycastHit.ToList<RaycastHit>();
            RaycastHit[] obstacle = hits.FindAll(p => p.collider.gameObject.tag.Equals(TagManager.OBSTACLE_TAG)).ToArray();
            RaycastHit plane = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLANE_TAG));

            if (obstacle.Length > 0 && obstacle[0].collider != null)
            {
                ColliderObstacle colliderObstacle = obstacle[0].transform.GetComponent<ColliderObstacle>();
                if (colliderObstacle != null && colliderObstacle.BaseObstacle != null)
                {
                    if (colliderObstacle.BaseObstacle.AllosSetObstacleUnder)
                    {
                        _holdObstacle.transform.position = obstacle[0].point;
                        _holdObstacle.OnStateChange();
                    }
                    else if (_holdObstacle != null && colliderObstacle.BaseObstacle.Equals(_holdObstacle))
                    {
                        if (plane.collider != null)
                        {
                            _holdObstacle.transform.position = plane.point;
                            _holdObstacle.OnStateChange();
                        }
                    }
                }
            }
            else if (plane.collider != null)
            {
                _holdObstacle.transform.position = plane.point;
                _holdObstacle.OnStateChange();
            }
        }

        private void TapOnObstacle(Vector3 menuPosition, Collider collider, Vector3 raycastPoint )
        {
            BaseObstacle targetObstacle;
            if (GetObstacleByCollider(collider, out targetObstacle))
            {
                ShowMenu(targetObstacle, menuPosition, true);
                _onObstacleTapAction?.Invoke(targetObstacle, raycastPoint);
            }
            else
            {
                Debug.LogError("TapOnObstacle false can`t find obstacle");
            }
        }

        private Vector3 GetPointPosition()
        {
#if UNITY_EDITOR
            Vector3 position = Input.mousePosition;
#else
            Vector3 position = Input.GetTouch(0).position;
#endif
            return position;
        }

        private bool GetObstacleByCollider(Collider collider, out BaseObstacle targetObstacle)
        {
            var colliderObstacle = collider.GetComponent<ColliderObstacle>();
            targetObstacle = null;
            if (colliderObstacle)
            {
                targetObstacle = colliderObstacle.BaseObstacle;
                return true;
            }
            else
            {
                var realObstacle = collider.GetComponent<RealObstacle>();

                if (realObstacle)
                {
                    targetObstacle = realObstacle;
                    return true;
                }
            }
            return false;
        }

        private void TapOnPlane()
        {
            HideMenu();
        }

        private void ShowMenu(BaseObstacle baseObstacle, Vector2 pointPosition, bool show)
        {
            _menuShow = _screenManager.ShowContentObstacleMenu(baseObstacle, pointPosition, DeleteObstacle);
        }

        private void DeleteObstacle(BaseObstacle baseObstacle)
        {
            _instantiatedObstacle.Remove(baseObstacle);
            _menuShow = false;
            StartCoroutine(WaitAndRecalculateTris());
        }

        private IEnumerator WaitAndRecalculateTris()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
#if PERF_TRACK
            PerformanceTrackingManager.Instance.UpdateTrisCountInScene();
#endif
        }

        private void HideMenu()
        {
            _menuShow = _screenManager.HideContentObstacleMenu();
        }

        private void ShowHoldIndicator(Vector2 position, bool show = true, float fill = 0f)
        {
            _screenManager.ShowHoldIndicator(position, true, fill);
        }

        private void HideHoldIndicator()
        {
            _screenManager.ShowHoldIndicator(Vector2.zero, false);
        }
    }
}