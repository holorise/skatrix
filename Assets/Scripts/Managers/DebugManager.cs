﻿using System.Collections;
using System.Collections.Generic;
using SKTRX.Enums;
using SKTRX.Interfaces;
using UnityEngine;

public class DebugManager : MonoBehaviour, ICustomDebuggable
{
    public DebugType DebugType { get; set; }
    public bool CanDebug { get; set; }

    void Start()
    {
        DebugType = DebugType.General;
        CustomDebug.Subscribe(this);
    }
#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CustomDebug.Log("Log works for: " + DebugType);
        }

    }
#endif
}