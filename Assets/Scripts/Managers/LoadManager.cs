﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using SKTRX.AR.SerializableObjects;

namespace SKTRX.Managers
{
    //Load all managers on scene
    public class LoadManager : MonoBehaviour
    {
        public GameObject scanObjManager;

        void Awake()
        {
            if (ScannedObjectSerializationManager.instance == null)

                //Instantiate ScannedObjectSerializationManager prefab
                Instantiate(scanObjManager);
        }


    }
}
