﻿

#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.Managers
{
    public class ARPlatformManager : MonoBehaviour
    {
        private const string CAMERA = "MainCamera";
        [SerializeField] private GameObject _androidPrefab;
        [SerializeField] private IOSARManager _iosPrefab;
        [SerializeField] private PlaneDetectionManager _planeManager;
        [SerializeField] private GameObject _editorPrefab;
        private void Awake()
        {
            InstantiatePlatformPrefabs();
        }

        private void InstantiatePlatformPrefabs()
        {
#if UNITY_EDITOR
        _editorPrefab.tag = CAMERA;
        _editorPrefab.SetActive(true);
        CameraObject.Camera = _editorPrefab.GetComponent<Camera>();
#endif
#if UNITY_IOS && !UNITY_EDITOR
            var manager = Instantiate(_iosPrefab);
            _planeManager.Init(manager);
#elif UNITY_ANDROID && !UNITY_EDITOR
            var go = Instantiate(_androidPrefab);
#endif
        }


    }
}
