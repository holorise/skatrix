﻿

#pragma warning disable 649
using UnityEngine;
using Tools;
using SKTRX.PlayerBehaviour;
using System;
using System.Collections;
#if PERF_TRACK
using SKTRX.Managers.PerformanceTracking;
#endif
using SKTRX.Screens;

namespace SKTRX.Managers
{
    public class PlayersManager : Singelton<PlayersManager>
    {
        public Action PlayerInstantantiateEvent;
        public Action<Vector3> PlayerScaleChangeEvent;

        [SerializeField] private PlayerControllerBase _playerToPlace = null;
		[SerializeField] private Transform _playerParent = null;
        private Pose _placementPose;
        private PlayerControllerBase _currentPlayer;
        private bool _allowUsePlayer = false;

        public void Start()
        {
            _currentPlayer = Instantiate<PlayerControllerBase>(_playerToPlace);
            StartCoroutine(WaitAndHide());
            _allowUsePlayer = false;
        }

        private IEnumerator WaitAndHide()
        {
            yield return new WaitForSeconds(1f);
            _currentPlayer.gameObject.SetActive(false);
        }

        public void SetControlScheme(IControler controler)
        {
            controler.SubscribeToInstantiate(delegate (Pose placementPose) 
            {
                _placementPose = placementPose;
                if(_currentPlayer == null)
                    _currentPlayer = Instantiate<PlayerControllerBase>(_playerToPlace, placementPose.position, placementPose.rotation);

                _currentPlayer.transform.position = placementPose.position;
                _currentPlayer.transform.rotation = placementPose.rotation;
                var screen = ScreenManager.Instance.Get<GameScreen>(Enums.ScreenType.GameScreen);
                screen.SetRespawnButtonActive(true);
                _currentPlayer.gameObject.SetActive(true);
#if PERF_TRACK
               PerformanceTrackingManager.Instance.UpdateTrisCountInScene();
#endif
                _currentPlayer.transform.SetParent(_playerParent);              
				_currentPlayer.SetControler(controler);
                _allowUsePlayer = true;
            });
        }

        public void RespawnPlayer()
        {
            if(_currentPlayer)
            {
                _currentPlayer.Stop();

                _currentPlayer.transform.position = _placementPose.position;
                _currentPlayer.transform.rotation = _placementPose.rotation;
                _currentPlayer.ResetAll();
                _currentPlayer.gameObject.SetActive(true);
            }
        }

        public void StopPlayer()
        {
            if (_currentPlayer) _currentPlayer.Stop();
        }

        public void DestroyPlayer()
        {
            if (_currentPlayer)
            {
                _currentPlayer.gameObject.SetActive(false);
                _allowUsePlayer = false;
#if PERF_TRACK
               PerformanceTrackingManager.Instance.UpdateTrisCountInScene();
#endif
            }
        }

        public void UpScaleClick()
        {
            if (_currentPlayer != null && _allowUsePlayer)
            {
                _currentPlayer.ScaleUp();
                PlayerScaleChangeEvent?.Invoke(_currentPlayer.GetCurrentScale());
            }
        }

        public void DownScaleClick()
        {
            if (_currentPlayer != null && _allowUsePlayer)
            {
                _currentPlayer.ScaleDown();
                PlayerScaleChangeEvent?.Invoke(_currentPlayer.GetCurrentScale());
            }
        }

        public void PausePlayer()
        {
            if (_currentPlayer != null && _allowUsePlayer)
            {
                _currentPlayer.Stop();
            }
        }

        public void ResumePlayer()
        {
            if (_currentPlayer != null && _allowUsePlayer)
            {
                _currentPlayer.StartMove();
            }
        }
    }
}