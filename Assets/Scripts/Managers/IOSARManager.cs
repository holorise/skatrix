﻿

#pragma warning disable 649
using Collections.Hybrid.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using Tools;
using SKTRX.AR;

namespace SKTRX.Managers
{
    public class IOSARManager : Singelton<IOSARManager>
    {
        [SerializeField] private UnityARGeneratePlane _generatePlaneManager;
        [SerializeField] private UnityARCameraManager _arCameraManager;
        [SerializeField] private WorldAnchorMapManager _worldMapManager;
        [SerializeField] private PredefinedImageObjectsManager _predefinedImageObjectsManager;

        public LinkedListDictionary<string, ARPlaneAnchorGameObject> GetPlaneAnchorMap()
        {
            return _generatePlaneManager?.GetPlaneAnchorMap();
        }

        public void SetPlaneDetectionActive(bool isActive)
        {
            if (isActive)
            {
                _arCameraManager.TurnOnPlaneDetection();
            }
            else
            {
                _arCameraManager.TurnOffPlaneDetection();
            }
        }

        public void SetObjScaningActive(bool isActive)
        {
            if (isActive)
            {
                _arCameraManager.TurnOnObjScaning();
            }
            else
            {
                _arCameraManager.TurnOffTracking();
            }
        }

        public void SetObjTrackingActive(bool isActive)
        {
            if (isActive)
            {
                _arCameraManager.TurnOnObjTracking();
            }
            else
            {
                _arCameraManager.TurnOffTracking();
            }
        }

        public void CreatePredefinedObjects()
        {
            if(_predefinedImageObjectsManager == null)
            {
                Debug.Log("@@@ PredefinedImageObjectsManager is null");
                return;
            }

            _predefinedImageObjectsManager.CreateAllObjects();
        }

        public void StopTrackingPredefinedImageObjects()
        {
            if (_predefinedImageObjectsManager == null)
            {
                Debug.Log("@@@ PredefinedImageObjectsManager is null");
                return;
            }

            _predefinedImageObjectsManager.StopTrackingAllPredefinedObjects();

        }

        public void StartTrackingPredefinedImageObjects()
        {
            if (_predefinedImageObjectsManager == null)
            {
                Debug.Log("@@@ PredefinedImageObjectsManager is null");
                return;
            }

            _predefinedImageObjectsManager.StartTrackingAllPredefinedObjects();

        }

        public void DestroyDetectedObjects()
        {
            if (_predefinedImageObjectsManager == null)
            {
                Debug.Log("@@@ PredefinedImageObjectsManager is null");
                return;
            }

            _predefinedImageObjectsManager.DestroyAllObjects();
        }

        public void SaveWorldMap()
        {
            _worldMapManager.Save();
        }

        public void LoadWorldMap()
        {
            _worldMapManager.Load();
        }

        public void SetWorldOrigin(Transform originTransform)
        {
            if(_arCameraManager == null)
            {
                Debug.Log("@@@ AR Camera Origin is null");
                return;
            }

            _arCameraManager.SetWorldOrigin(originTransform);
        }

    }
}
