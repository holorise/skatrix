using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace SKTRX.Managers
{
    public class ScannerARCameraManager : MonoBehaviour
    {
        public Camera m_camera;
        private UnityARSessionNativeInterface m_session;
        private Material savedClearMaterial;

        [Header("AR Config Options")]
        public UnityARAlignment startAlignment = UnityARAlignment.UnityARAlignmentGravity;
        public UnityARPlaneDetection planeDetection = UnityARPlaneDetection.Horizontal;
        public bool getPointCloud = true;
        public bool enableLightEstimation = true;
        public bool enableAutoFocus = true;
        public UnityAREnvironmentTexturing environmentTexturing = UnityAREnvironmentTexturing.UnityAREnvironmentTexturingNone;

        [Header("Image Tracking")]
        public ARReferenceImagesSet detectionImages = null;
        public int maximumNumberOfTrackedImages = 0;

        [Header("Object Tracking")]
        public ARReferenceObjectsSetAsset detectionObjects = null;
        private bool sessionStarted = false;

        public ARKitWorldTrackingSessionConfiguration sessionTrackingConfiguration
        {
            get
            {
                Debug.Log("@@@ TurnOnObjTracking");
                planeDetection = UnityARPlaneDetection.None;
                ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
                config.planeDetection = planeDetection;
                config.alignment = startAlignment;
                config.getPointCloudData = getPointCloud;
                config.enableLightEstimation = enableLightEstimation;
                config.enableAutoFocus = enableAutoFocus;
                config.maximumNumberOfTrackedImages = maximumNumberOfTrackedImages;
                config.environmentTexturing = environmentTexturing;
                if (detectionImages != null)
                    config.referenceImagesGroupName = detectionImages.resourceGroupName;
                List<ARReferenceObject> arRefObjList = ScannedObjectSerializationManager.instance.ARReferenceObjects;
                Debug.Log("@@@@ Start detection session ArRefObjCount:" + arRefObjList.Count);

                DetectionObjectManager.instance.CreateScannedObjectAnchor(arRefObjList);

                if (detectionObjects != null)
                {
                    arRefObjList.AddRange(detectionObjects.LoadReferenceObjectsInSet());
                }

                Debug.Log("@@@@ Start detection session ArRefObjCount:" + arRefObjList.Count);
                config.referenceObjectsGroupName = "";  //lets not read from XCode asset catalog right now
                config.dynamicReferenceObjectsPtr = m_session.CreateNativeReferenceObjectsSet(arRefObjList);
                
                return config;
            }
        }

        public ARKitObjectScanningSessionConfiguration sessionScannerConfiguration
        {
            get
            {
                ARKitObjectScanningSessionConfiguration config = new ARKitObjectScanningSessionConfiguration();
                config.planeDetection = planeDetection;
                config.alignment = startAlignment;
                config.getPointCloudData = getPointCloud;
                config.enableLightEstimation = enableLightEstimation;
                config.enableAutoFocus = enableAutoFocus;

                return config;
            }
        }


        public void TurnOnScannerSession()
        {
            Debug.Log("@@@ TurnOnScannerSession");

            var config = sessionScannerConfiguration;

            if (config.IsSupported)
            {
                m_session.RunWithConfig(config);
            }
        }

        public void TurnOnTrackingSession()
        {
            Debug.Log("@@@ TurnOnTrackingSession");

            var config = sessionTrackingConfiguration;

            if (config.IsSupported)
            {
                m_session.RunWithConfig(config);
            }
        }

        // Use this for initialization
        void Start()
        {

            m_session = UnityARSessionNativeInterface.GetARSessionNativeInterface();

            Application.targetFrameRate = 60;

            var config = sessionScannerConfiguration;
            if (config.IsSupported)
            {
                m_session.RunWithConfig(config);
                UnityARSessionNativeInterface.ARFrameUpdatedEvent += FirstFrameUpdate;
            }

            if (m_camera == null)
            {
                m_camera = Camera.main;
            }
        }

        void OnDestroy()
        {
            m_session.Pause();
        }

        void FirstFrameUpdate(UnityARCamera cam)
        {
            sessionStarted = true;
            UnityARSessionNativeInterface.ARFrameUpdatedEvent -= FirstFrameUpdate;
        }

        public void SetCamera(Camera newCamera)
        {
            if (m_camera != null)
            {
                UnityARVideo oldARVideo = m_camera.gameObject.GetComponent<UnityARVideo>();
                if (oldARVideo != null)
                {
                    savedClearMaterial = oldARVideo.m_ClearMaterial;
                    Destroy(oldARVideo);
                }
            }
            SetupNewCamera(newCamera);
        }

        private void SetupNewCamera(Camera newCamera)
        {
            m_camera = newCamera;

            if (m_camera != null)
            {
                UnityARVideo unityARVideo = m_camera.gameObject.GetComponent<UnityARVideo>();
                if (unityARVideo != null)
                {
                    savedClearMaterial = unityARVideo.m_ClearMaterial;
                    Destroy(unityARVideo);
                }
                unityARVideo = m_camera.gameObject.AddComponent<UnityARVideo>();
                unityARVideo.m_ClearMaterial = savedClearMaterial;
            }
        }

        // Update is called once per frame

        void Update()
        {

            if (m_camera != null && sessionStarted)
            {
                // JUST WORKS!
                Matrix4x4 matrix = m_session.GetCameraPose();
                m_camera.transform.localPosition = UnityARMatrixOps.GetPosition(matrix);
                m_camera.transform.localRotation = UnityARMatrixOps.GetRotation(matrix);

                m_camera.projectionMatrix = m_session.GetCameraProjection();
            }

        }

    }
}

