﻿

#pragma warning disable 649
using UnityEngine;
using System;
using System.Collections.Generic;
using Tools;
using SKTRX.Screens;
using SKTRX.Enums;
using SKTRX.DTOModel;
using SKTRX.Obstacle;
using SKTRX.UI;

namespace SKTRX.Managers
{
    public class ScreenManager : Singelton<ScreenManager>
    {
        [SerializeField] private BaseScreen[] _baseScreens;
        [SerializeField] private ContentObstacleMenu _contentObstacleMenu;

        private Dictionary<ScreenType, BaseScreen> _screens;
        
        public Dictionary<ScreenType, BaseScreen> Screens
        {
            get
            {
                if (_screens == null)
                {
                    _screens = new Dictionary<ScreenType, BaseScreen>();
                    foreach(var screen in _baseScreens)
                        if(!_screens.ContainsKey(screen.ScreenType))
                            _screens.Add(screen.ScreenType, screen);
                }
                return _screens;
            }
        }

        public T Get<T>(ScreenType screenType) where T : BaseScreen
        {
            return (T)GetScreen(screenType);
        }

        public void Show(ScreenType screenType)
        {
            GetScreen(screenType).Show();
        }
        public void Show(ScreenType screenType, Action onCompleteScreen)
        {
            GetScreen(screenType).Show(onCompleteScreen);
        }

        public void Show<T>(ScreenType screenType, Action<T> onCompleteScreen) where T : ScreenDTO
        {
            GetScreen(screenType).Show<T>(onCompleteScreen);
        }

        public void Hide(ScreenType screenType)
        {
            GetScreen(screenType).Hide();
        }

        public bool ShowContentObstacleMenu(BaseObstacle targetObstacle, Vector2 positionTap, Action<BaseObstacle> onDeleteAction)
        {
            ObjectPlaceScreen objectPlaceScreen = (ObjectPlaceScreen)GetScreen(ScreenType.ObjectPlace);
            if (objectPlaceScreen.IsShow())
            {
                if (_contentObstacleMenu.IsBuildRail())
                {
                    return false;
                }
                else
                {
                    _contentObstacleMenu.Show(targetObstacle, positionTap, onDeleteAction);
                    objectPlaceScreen.ShowObstacleMenu(false);
                    return true;
                }
            }
            return false;
        }

        public bool HideContentObstacleMenu()
        {
            return _contentObstacleMenu.Hide();
        }

        public void ShowHoldIndicator(Vector2 position, bool show = true, float fill = 0f)
        {
            ((ObjectPlaceScreen)GetScreen(ScreenType.ObjectPlace)).ShowHoldIndicator(position, show, fill);
        }

        private BaseScreen GetScreen(ScreenType screenType)
        {
            if (Screens.ContainsKey(screenType))
                return Screens[screenType];
            else
                throw new Exception(string.Format("[ScreenManager] GetScreen missing type in dictionary {0}", screenType));
        }
 
        [ContextMenu ("GetScreens")]
        private void GetAllScreens()
        {
            _baseScreens = transform.GetComponentsInChildren<BaseScreen>(true);
        }
    }
}