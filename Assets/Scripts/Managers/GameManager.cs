﻿

#pragma warning disable 649
using UnityEngine;
using System;
using System.Collections;
using Tools;
using SKTRX.Enums;
using SKTRX.DTOModel;
using SKTRX.Screens;
using SKTRX.AR;

namespace SKTRX.Managers
{
    public class GameManager : Singelton<GameManager>
    {
        private const string DETECT_PLANE_MSG = "Detect a plane first";
        private const string REAL_OBJECT_PLACE_MSG = "Scan objects";
        private const string OBSTACLE_SCREEN_MSG = "Set obstacles";

        private ScreenManager _screenManager { get { return ScreenManager.Instance; } }
        private PlaneDetectionManager _planeManager { get { return PlaneDetectionManager.Instance; } }
        private PlayersManager _playerManager { get { return PlayersManager.Instance; } }
        private ControlManager _controlManager { get { return ControlManager.Instance; } }
        private ScreenDTO _currentScreenDTO;
     
        /// <summary>
        /// Start this scene from here
        /// </summary>
        /// <returns></returns>
        private IEnumerator Start()
        {
            _screenManager.Show(ScreenType.Loading);
            yield return new WaitForSeconds(1f);
            _screenManager.Hide(ScreenType.Loading);
            StartScenario();
        }

        private void ControlSelected(ScreenDTO screenDTO)
        {
            InstantiatePlayer(screenDTO);
            CheckPlaneDection(delegate ()
            {
                StartPlayerListingForInstantiate();
            });
        }

        private void OnRestartAnyScreen()
        {
           // _playerManager.StopPlayer();
            _playerManager.DestroyPlayer();
            _controlManager.DestroyControl();
            HideRespawnButton();
        }

        public void StartScenario()
        {
#if UNITY_IOS && !UNITY_EDITOR
            IOSARManager.Instance.DestroyDetectedObjects();
#endif
            ObstacleManager.Instance.DeleteAll();
            OnRestartAnyScreen();
            RescanPlaneDection(delegate ()
            {
                StartRealObjectTrackingScreen();
            });
            _planeManager.Restart();
        }


        private void StartRealObjectTrackingScreen()
        {
            _screenManager.Hide(ScreenType.PlaneDetect);
            var screen = _screenManager.Get<RealObjectDetectionScreen>(ScreenType.RealObjectDetection);
            screen.ShowMessage(REAL_OBJECT_PLACE_MSG);
            screen.Show(delegate ()
            {
                ShowObstacleScreen();
            });
        }

        public void RestartRealObjectTracking()
        {
            _screenManager.Hide(ScreenType.GameScreen);
            OnRestartAnyScreen();
            var screen = _screenManager.Get<RealObjectDetectionScreen>(ScreenType.RealObjectDetection);
            screen.ShowMessage(REAL_OBJECT_PLACE_MSG);
            screen.Show(delegate ()
            {
                ShowObstacleScreen();
            });
        }

        private void ShowObstacleScreen()
        {
            ObjectPlaceScreen screen = _screenManager.Get<ObjectPlaceScreen>(ScreenType.ObjectPlace);
            screen.ShowMessage(OBSTACLE_SCREEN_MSG);
            screen.Show(delegate ()
            {
                _screenManager.Show<ScreenDTO>(ScreenType.ControlVariantSelect, ControlSelected);
            });
            screen.ShowObstacleMenu(true);
        }


        public void RestartObstaclePlayer()
        {
            _screenManager.Hide(ScreenType.GameScreen);
            OnRestartAnyScreen();
            ObjectPlaceScreen screen = _screenManager.Get<ObjectPlaceScreen>(ScreenType.ObjectPlace);
            screen.ShowMessage(OBSTACLE_SCREEN_MSG);
            screen.Show(delegate ()
            {
                RestartPlayer();
            });
            screen.ShowObstacleMenu(true);
        }
              

        public void RestartPlayer()
        {
            OnRestartAnyScreen();
            InstantiatePlayer(_currentScreenDTO);
            StartPlayerListingForInstantiate();
        }
         
        public void RestartControl()
        {
            _playerManager.StopPlayer();
            _playerManager.DestroyPlayer();
            _controlManager.DestroyControl();
            _screenManager.Hide(ScreenType.GameScreen);
            _screenManager.Show<ScreenDTO>(ScreenType.ControlVariantSelect, ControlSelected);
        }
              

        private void InstantiatePlayer(ScreenDTO screenDTO)
        {
            _currentScreenDTO = screenDTO;
            _screenManager.Hide(ScreenType.ControlVariantSelect);
            GameScreen screen = _screenManager.Get<GameScreen>(ScreenType.GameScreen);
            screen.InstantiateController(() => _controlManager.GetControlPrefab(screenDTO.ControlType));
            _playerManager.SetControlScheme(_controlManager.GetCurrentController());
            _screenManager.Show(ScreenType.GameScreen, GameScreenBack);
            _controlManager.GetCurrentController().SubscribeMessage(screen.ShowMessage);
        }

        private void StartPlayerListingForInstantiate()
        {
            _controlManager.GetCurrentController().StartListeningForInstantiate();
        }

        private void CheckPlaneDection(Action onPlaneDetecAction)
        {
            onPlaneDetecAction?.Invoke();
        }

        private void RescanPlaneDection(Action onPlaneDetecAction)
        {
#if UNITY_EDITOR
            onPlaneDetecAction?.Invoke();
#else
            PlaneDetectionManager.Instance.RescanPlane(onPlaneDetecAction);
#endif
        }

        private void HideRespawnButton()
        {
            GameScreen screen = _screenManager.Get<GameScreen>(ScreenType.GameScreen);
            screen.SetRespawnButtonActive(false);
        }

        private void GameScreenBack()
        {
            _screenManager.Hide(ScreenType.GameScreen);
            _playerManager.StopPlayer();
            _playerManager.DestroyPlayer();
            _controlManager.DestroyControl();
            StartCoroutine(Start());
        }
    }
}