﻿

#pragma warning disable 649
#define DEBUG_MODE

using System;
using System.Collections.Generic;
using System.Linq;
using Collections.Hybrid.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using SKTRX.AR;
using SKTRX.Enums;
using SKTRX.Screens;
using Tools;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.iOS;

namespace SKTRX.Managers
{
    public class PlaneDetectionManager : Singelton<PlaneDetectionManager>
    {
        private const float MIN_X = 0.5f;
        private const float MIN_Z = 0.5f;
        private const float PLANE_ALPHA = 0.3f;
        private const string FIND_AREA_MSG = "Scan the environment";
        private const string TAP_ON_AREA_MSG = "Select gameplay area";
        private const string EXPAND_AREA_MSG = "Please expand the gameplay area";
        private const string SELECT_PLANE_MSG = "Please select the plane";
        private const string PRESS_BUTTON_MSG = "You selected the plane, press Select button to move further";
        private const string PLANE_TAG = "Plane";

        public GameObject DetectedPlanePrefab;

#if UNITY_ANDROID
        private List<DetectedPlane> m_NewPlanes = new List<DetectedPlane>();
        private DetectedPlane _currentPlane;

#elif UNITY_IOS
        private IOSARManager _iosManager;
        private LinkedListDictionary<string, ARPlaneAnchorGameObject> _planesDict;
        private List<ARPlaneAnchor> m_NewPlanes = new List<ARPlaneAnchor>();
        private ARPlaneAnchorGameObject _currentPlane;
#endif

        [SerializeField] private Material _debugMaterial;
        [SerializeField] private Transform _parent;
        [SerializeField] private VirtualPlaneItem _planeItemPrefab;

        private VirtualPlaneItem _virtualPlane;
        private PlaneDetectScreen _detectScreen;
        private List<GameObject> _planesGOsList = new List<GameObject>();
        private Vector3 _posToRotate;

        private bool _isSelected;
        private bool _isTracking = true;

#if UNITY_IOS

        public void Init(IOSARManager manager)
        {
            _iosManager = manager;
        }

        private void OnEnable()
        {
            UnityARSessionNativeInterface.ARAnchorAddedEvent += AddAnchor;
            UnityARSessionNativeInterface.ARAnchorUpdatedEvent += UpdateAnchor;
        }

        private void OnDisable()
        {
            UnityARSessionNativeInterface.ARAnchorAddedEvent -= AddAnchor;
            UnityARSessionNativeInterface.ARAnchorUpdatedEvent -= UpdateAnchor;
        }

        public void AddAnchor(ARPlaneAnchor arPlaneAnchor)
        {

            m_NewPlanes.Add(arPlaneAnchor);
        }

        public void UpdateAnchor(ARPlaneAnchor arPlaneAnchor)
        {
            if (!_isSelected)
            {
                var size = arPlaneAnchor.extent;
               //_currentPlane = arPlaneAnchor;
                //UpdateMaterial(plane);
                UdateText(size);
            }
        }

        public ARPlaneAnchorGameObject GetCurrentPlaneAnchor()
        {
            return _currentPlane;
        }
#endif

        public Transform GetPlaneTransform()
        { 
            if(_virtualPlane != null)
            {
                return _virtualPlane.transform;
            }
            else
            {
                return null; 
            }
        }

        public Transform GetParentTransform()
        {
            if (_parent != null)
            {
                return _parent;
            }
            else
            {
                return null;
            }
        }


        public void Restart()
        {
#if !UNITY_EDITOR
            if (_virtualPlane != null)
            {
                //Destroy(_virtualPlane.gameObject);
                _virtualPlane.gameObject.SetActive(false);
            }
            _isSelected = false;
            _isTracking = true;
            _currentPlane = null;
            _detectScreen.SetActiveButton(false);
#if UNITY_ANDROID
            for (int i = 0; i < _planesGOsList.Count; i++)
            {
                _planesGOsList[i].gameObject.SetActive(true);
            }
#elif UNITY_IOS
            _planesDict = _iosManager.GetPlaneAnchorMap();

            foreach (var value in _planesDict.Values)
            {
                value.gameObject.GetComponent<MeshRenderer>().enabled = true;
                value.gameObject.GetComponent<LineRenderer>().enabled = true;

            }
            _iosManager.SetPlaneDetectionActive(true);
            UnityARAnchorManager.CanTrack = true;
#endif

            var text = FIND_AREA_MSG;
            _detectScreen.SetActiveButton(false);
            _detectScreen.ShowMessage(text);
#endif

        }
#if UNITY_ANDROID
        private void OnPlaneAdded(DetectedPlaneVisualizer plane)
        {
            _planesGOsList.Add(plane.gameObject);
        }

        private void OnPlaneUpdated(DetectedPlaneVisualizer plane)
        {
            if (!_isSelected)
            {
                var size = plane.GetSize();
                _currentPlane = plane.GetPlane();
                //UpdateMaterial(plane);
                UdateText(size);
            }
            else
            {
                Debug.LogError("IsSelected = true"); 
            }
        }


        //private void UpdateMaterial(DetectedPlaneVisualizer plane)
        //{
        //    if (CanSelectSurface(plane.GetSize()))
        //    {
        //        var mesh = plane.GetComponent<MeshRenderer>();
        //        var color = Color.green;
        //        color.a = PLANE_ALPHA;
        //        mesh.material.color = color;
        //    }
        //}
#endif


        private void UdateText(Vector3 size)
        {
            var text = "";

            if (CanSelectSurface(size))
            {
                text = TAP_ON_AREA_MSG;
            }
            else
            {
                text = EXPAND_AREA_MSG;
            }
            _detectScreen.ShowMessage(text);
          
        }

        public void SetActiveBoundaries(bool isActive)
        {
            if(isActive)
            {
                _virtualPlane.FadeInBoundaries(); 
            }
            else
            {
                _virtualPlane.FadeOutBoundaries();
            }     
        }
#if UNITY_IOS
        private bool CanSelectSurface(Vector3 size)
        {
            return size.x >= MIN_X && size.z >= MIN_Z;
        }
#elif UNITY_ANDROID
        private bool CanSelectSurface(Vector2 size)
        {
            return size.x >= MIN_X && size.y >= MIN_Z;
        }
#endif

        public void PlacePlane()
        {
            if(_currentPlane!=null)
            {
#if UNITY_ANDROID
                var size = new Vector3(_currentPlane.ExtentX,_currentPlane.ExtentZ,0);
#elif UNITY_IOS
                var size = _currentPlane.planeAnchor.extent;
#endif
                if (CanSelectSurface(size))
                {
                    //var item = Instantiate(_planeItemPrefab);
                    //_virtualPlane = item;
#if UNITY_IOS
                                   
                    var arMesh = _currentPlane.gameObject.GetComponent<ARKitPlaneMeshRender>();
                    arMesh.SetQuadFromBoundaries();
                    var item = _currentPlane.gameObject.transform.GetComponentInChildren<VirtualPlaneItem>(true);
                    _virtualPlane = item;
                    item.gameObject.SetActive(true);
                    _parent.SetParent(_currentPlane.gameObject.transform);

#elif UNITY_ANDROID

                    var position = _currentPlane.CenterPose.position;
                    var anchor = _currentPlane.CreateAnchor(_currentPlane.CenterPose);
                   
                    _parent.SetParent(anchor.transform);
                    _parent.localPosition = new Vector3();
                
                    item.transform.SetParent(anchor.transform);
        
                    item.transform.position = position;
                    size = new Vector3(_currentPlane.ExtentX,0, _currentPlane.ExtentZ);


#endif

                    item.SetScale(size);
                    item.ResizeColliders();
                                     
#if UNITY_ANDROID
                    for (int i = 0; i < _planesGOsList.Count; i++)
                    {
                        if(_planesGOsList[i]!=null)
                        {
                            _planesGOsList[i].SetActive(false);
                        }
                        else
                        {
                            Debug.LogError("plane == null"); 
                        }
                    }
#elif UNITY_IOS
                    foreach (var value in _planesDict.Values)
                    {
                        value.gameObject.GetComponent<MeshRenderer>().enabled = false;
                        value.gameObject.GetComponent<LineRenderer>().enabled = false;
                    }
                   
                    //UnityARAnchorManager.CanTrack = false;
                    _iosManager.SetPlaneDetectionActive(false);
#endif
                   
                    _detectScreen.GetSelectButtonDelegate().Invoke();
                   
                    _isTracking = false;
                   

                }
                else
                {
                    _detectScreen.SetActiveButton(false);
                }

            }


        }



        public void Update()
        {
#if UNITY_ANDROID
            if (Session.Status != SessionStatus.Tracking || !_isTracking)
            {
                return;
            }

            Session.GetTrackables<DetectedPlane>(m_NewPlanes, TrackableQueryFilter.New);
            for (int i = 0; i < m_NewPlanes.Count; i++)
            {

                GameObject planeObject =
                    Instantiate(DetectedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
                var plane = planeObject.GetComponent<DetectedPlaneVisualizer>();
                plane.PlaneUpdated = OnPlaneUpdated;
                plane.PlaneAdded = OnPlaneAdded;
                plane.Initialize(m_NewPlanes[i]);
            }
#elif UNITY_IOS
            CheckClick();
#endif
        }


        public void RescanPlane(Action onCompleteRescane)
        {
            ScreenManager.Instance.Hide(ScreenType.GameScreen);
            _detectScreen = ScreenManager.Instance.Get<PlaneDetectScreen>(ScreenType.PlaneDetect);
            _detectScreen.Show(onCompleteRescane);
            _detectScreen.ShowMessage("Detecting plane");
            _detectScreen.NextScreenClicked = () => PlacePlane();

        }

        private void CheckClick()
        {

            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }

            if (EventSystem.current.IsPointerOverGameObject(0))
            {
                //Debug.LogError("Clicked on UI buttont");
                return;
            }

            var text = "";

#if UNITY_ANDROID

            TrackableHit hit;
            var raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

            if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
            {
                _currentPlane = hit.Trackable as DetectedPlane;
                if (_currentPlane != null)
                {
                    if (CanSelectSurface(new Vector2(_currentPlane.ExtentX,_currentPlane.ExtentZ)))
                    {
                        _isSelected = true;
                        text = PRESS_BUTTON_MSG;
                    }
                    else
                    {
                        text = EXPAND_AREA_MSG;
                    }
                }

            _detectScreen.SetActiveButton(true);

            }
            else
            {
                _isSelected = false;
                if (IsAnySurfaceReady())
                {
                    text = SELECT_PLANE_MSG;
                }
                else
                {
                    text = EXPAND_AREA_MSG;
                }
                _detectScreen.SetActiveButton(false);

            }
            _detectScreen.ShowMessage(text);
#endif

#if UNITY_IOS

            Vector3 screenPosition = CameraObject.Camera.ScreenToViewportPoint(touch.position);
            ARPoint point = new ARPoint
            {
                x = screenPosition.x,
                y = screenPosition.y
            };
            List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(point, ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent);
            if (hitResults.Count > 0)
            {
                foreach (ARHitTestResult hitResult in hitResults)
                {
                    Vector3 hitPosition = UnityARMatrixOps.GetPosition(hitResult.worldTransform);
                 
                    var length = _planesDict.Count;
   
                    var id = hitResult.anchorIdentifier;
                    if(id!=null)
                    {
                        if (_planesDict.ContainsKey(id))
                        {
                            _isSelected = true;
                            _currentPlane = _planesDict[id];

                            if (CanSelectSurface(_currentPlane.planeAnchor.extent))
                            {
                                text = PRESS_BUTTON_MSG;
                                _detectScreen.SetActiveButton(true);
                            }
                            else
                            {
                                text = EXPAND_AREA_MSG;
                                _detectScreen.SetActiveButton(false);
                            }

                        }

                    }
                }
              
            }
      
#endif
        }

        private bool IsAnySurfaceReady()
        {
            var isReady = false;
            var list = new List<DetectedPlane>();
            Session.GetTrackables<DetectedPlane>(list, TrackableQueryFilter.New);
            var length = list.Count;
            for (int i = 0; i < length; i++)
            {
                var plane = list[i];
                var size = new Vector2(plane.ExtentX, plane.ExtentZ);
                if (CanSelectSurface(size))
                {
                    isReady = true;
                    break;
                }
            }
            return isReady;
        }

    }
}
