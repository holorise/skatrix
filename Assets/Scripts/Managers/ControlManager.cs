﻿
using UnityEngine;
using System;
using System.Collections.Generic;
using SKTRX.InputControllers;
using Tools;
using SKTRX.Enums;

namespace SKTRX.Managers
{
    public class ControlManager : Singelton<ControlManager>
    {
        [SerializeField] private BaseControler[] _controllers;
        [SerializeField] private BaseControler _currentController;

        private Dictionary<ControlType, BaseControler> _controllersObject;

        public Dictionary<ControlType, BaseControler> ControllersObject
        {
            get
            {
                if(_controllersObject == null)
                {
                    _controllersObject = new Dictionary<ControlType, BaseControler>(_controllers.Length);
                    foreach(BaseControler control in _controllers)
                        if (!_controllersObject.ContainsKey(control.ControllType))
                            _controllersObject.Add(control.ControllType, control);
                }
                return _controllersObject;
            }
        }

        public BaseControler[] Controllers { get => _controllers;}

        public IControler GetCurrentController()
        {
            return _currentController;
        }
        public void DestroyControl()
        {
            if (_currentController) ((IControler)(_currentController)).DestroyControl();
        }

        public BaseControler GetControlPrefab(ControlType controlType)
        {
            _currentController = Instantiate(GetControl(controlType));
            return _currentController;
        }

        private BaseControler GetControl(ControlType controlType)
        {
            if (ControllersObject.ContainsKey(controlType))
                return ControllersObject[controlType];
            else
                throw new Exception(string.Format("[ControlManager] GetControl not contains {0}", controlType));
        }
    }
}