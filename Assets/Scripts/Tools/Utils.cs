using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;
using System;
using System.Text.RegularExpressions;

namespace Tools
{
    /// <summary>
    /// Ortho side.
    /// </summary>
    public enum OrthoSide
    {
        Top,
        Bottom,
        Left,
        Right,
        Center,
    }

    /// <summary>
    /// All sides.
    /// </summary>
    public enum AllSides
    {
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
    }

    /// <summary>
    /// Utils.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Sets teh color's transparancy
        /// </summary>
        /// <returns>The transparent.</returns>
        /// <param name="color">Color.</param>
        /// <param name="transparent">If set to <c>true</c> transparent.</param>
        /// <param name="coloredAlpha">Colored alpha.</param>
        public static Color ToTransparent(this Color color, bool transparent = true, float coloredAlpha = 1)
        {
            return new Color(color.r, color.g, color.b, transparent ? coloredAlpha : transparent.ToInt());
        }

        /// <summary>
        /// Cast bool to int (true : 1   false : 0)
        /// </summary>
        /// <returns>The int.</returns>
        /// <param name="val">If set to <c>true</c> value.</param>
        public static int ToInt(this bool val)
        {
            return val ? 1 : 0;
        }

        /// <summary>
        /// Cast bool to sign (true : 1   false : -1)
        /// </summary>
        /// <returns>The sign.</returns>
        /// <param name="val">If set to <c>true</c> value.</param>
        public static int ToSign(this bool val)
        {
            return val ? 1 : -1;
        }

        /// <summary>
        /// Cast int to bool (1 : true   0 : false)
        /// </summary>
        /// <returns><c>true</c>, if bool was toed, <c>false</c> otherwise.</returns>
        /// <param name="val">Value.</param>
        public static bool ToBool(this int val)
        {
            return Math.Abs(val) > 0 ? true : false;
        }

        /// <summary>
        /// Kills the and clear the tween.
        /// </summary>
        /// <param name="tween">Tween.</param>
        /// <param name="complete">If set to <c>true</c> complete.</param>
        public static void KillAndClear(this Tween tween, bool complete = false)
        {
            if (tween != null && tween.IsActive())
            {
                tween.Kill(complete);
                tween = null;
            }
        }

        /// <summary>
        /// Lerp the specified a, b by x.
        /// </summary>
        /// <returns>The lerp.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="x">The x coordinate.</param>
        public static double Lerp(double a, double b, double x)
        {
            return a + x * (b - a);
        }

        /// <summary>
        /// Shuffle the specified list.
        /// </summary>
        /// <returns>The shuffle.</returns>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        static public List<T> Shuffle<T>(this List<T> list)
        {
            UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
            int n = list.Count;
            List<T> new_list = list.CopyList();
            while (n > 0)
            {
                int k = UnityEngine.Random.Range(0, n);
                T item = new_list[n - 1];
                new_list[n - 1] = new_list[k];
                new_list[k] = item;
                n--;
            }
            return new_list;
        }


        static public List<T> ClearFrom<T>(this List<T> list, int index)
        {
            List<T> res = new List<T>();
            if (index < 0 || index > list.Count - 1)
            {
                throw new IndexOutOfRangeException();
            }

            for (int i = 0; i <= index; i++)
            {
                res.Add(list[i]);
            }
            return res;
        }

        /// <summary>
        /// Shuffle the specified array.
        /// </summary>
        /// <returns>The shuffle.</returns>
        /// <param name="array">Array.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        static public T[] Shuffle<T>(this T[] array)
        {
            int n = array.Length;
            T[] new_array = array.CopyArray();
            while (n > 0)
            {
                int k = UnityEngine.Random.Range(0, n);
                T item = new_array[n - 1];
                new_array[n - 1] = new_array[k];
                new_array[k] = item;
                n--;
            }
            return new_array;
        }

        /// <summary>
        /// Sets the z value for specified Vector3
        /// </summary>
        /// <returns>The z.</returns>
        /// <param name="vector">Vector.</param>
        /// <param name="newZ">New z.</param>
        public static Vector3 SetZ(this Vector3 vector, float newZ)
        {
            return new Vector3(vector.x, vector.y, newZ);
        }

        /// <summary>
        /// Sets the global scale.
        /// </summary>
        /// <param name="transform">Transform.</param>
        /// <param name="globalScale">Global scale.</param>
        public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
        {
            transform.localScale = Vector3.one;
            transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
        }

        /// <summary>
        /// Gets the intersection angle.
        /// </summary>
        /// <returns>The intersection angle.</returns>
        /// <param name="intersectionPoint">Intersection point.</param>
        /// <param name="direction">Direction.</param>
        /// <param name="collider">Collider.</param>
        public static float GetIntersectionAngle(Vector3 intersectionPoint, Vector3 direction, Collider collider)
        {
            Vector3 hitLocalPosition = collider.transform.InverseTransformPoint(intersectionPoint);
            float Xsize = Remap(Math.Abs(hitLocalPosition.x), 0, collider.bounds.size.x, 0, 1);
            float Zsize = Remap(Math.Abs(hitLocalPosition.z), 0, collider.bounds.size.z, 0, 1);

            Vector3 targetAxis;

            if (Xsize > Zsize)
                targetAxis = collider.transform.forward;
            else
                targetAxis = Quaternion.AngleAxis(90, Vector3.up) * collider.transform.forward;

            return Vector3.Angle(direction, targetAxis);
        }

        /// <summary>
        /// Adds the rotation.
        /// </summary>
        /// <param name="tr">Tr.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        public static void AddRotation(this UnityEngine.Transform tr, float x, float y, float z)
        {
            tr.eulerAngles = new Vector3(tr.eulerAngles.x + x, tr.eulerAngles.y + y, tr.eulerAngles.z + z);
        }

        /// <summary>
        /// Adds the rotation.
        /// </summary>
        /// <param name="tr">Tr.</param>
        /// <param name="rotaion">Rotaion.</param>
        public static void AddRotation(this UnityEngine.Transform tr, Vector3 rotaion)
        {
            tr.eulerAngles = new Vector3(tr.eulerAngles.x + rotaion.x, tr.eulerAngles.y + rotaion.y, tr.eulerAngles.z + rotaion.z);
        }

        /// <summary>
        /// Gets the last element.
        /// </summary>
        /// <returns>The last.</returns>
        /// <param name="array">Array.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T GetLast<T>(this T[] array)
        {
            return array[array.Length - 1];
        }

        /// <summary>
        /// Gets the last element.
        /// </summary>
        /// <returns>The last.</returns>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T GetLast<T>(this List<T> list)
        {
            return list[list.Count - 1];
        }

        /// <summary>
        /// Copies the list.
        /// </summary>
        /// <returns>The list.</returns>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static List<T> CopyList<T>(this List<T> list)
        {
            int n = list.Count;
            List<T> new_list = new List<T>();
            for (int i = 0; i < n; i++)
            {
                new_list.Add(list[i]);
            }
            return new_list;
        }

        /// <summary>
        /// Copies the array.
        /// </summary>
        /// <returns>The array.</returns>
        /// <param name="array">Array.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T[] CopyArray<T>(this T[] array)
        {
            int n = array.Length;
            T[] new_array = new T[n];

            for (int i = 0; i < n; i++)
            {
                new_array[i] = array[i];
            }
            return new_array;
        }

        /// <summary>
        /// Merges the array with other one.
        /// </summary>
        /// <returns>The array.</returns>
        /// <param name="array">Array.</param>
        /// <param name="other">Other.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T[] MergeArray<T>(this T[] array, T[] other)
        {
            List<T> res = new List<T>();
            res.AddRange(array);
            res.AddRange(other);

            return res.ToArray();
        }

        /// <summary>
        /// Pushes the specified array with other one.
        /// </summary>
        /// <returns>The push.</returns>
        /// <param name="array">Array.</param>
        /// <param name="other">Other.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T[] Push<T>(this T[] array, T other)
        {
            List<T> res = new List<T>();
            res.AddRange(array);
            res.Add(other);

            return res.ToArray();
        }

        /// <summary>
        /// Gets the random element.
        /// </summary>
        /// <returns>The random element.</returns>
        /// <param name="list">List.</param>
        /// <param name="except">Except.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T GetRandomElement<T>(this List<T> list, params T[] except)
        {
            //TODO refactor for List
            return list.ToArray().GetRandomElement(except);
        }

        /// <summary>
        /// Randomises the sign.
        /// </summary>
        /// <returns>The sign.</returns>
        public static int RandomSign()
        {
            return UnityEngine.Random.value > 0.5f ? 1 : -1;
        }

        /// <summary>
        /// Parses the camel-type word to words.
        /// </summary>
        /// <returns>The camel to words.</returns>
        /// <param name="en">En.</param>
        public static string ParseCamelToWords(this Enum en)
        {
            return Regex.Replace(en.ToString(), "(\\B[A-Z]+?(?=[A-Z][^A-Z])|\\B[A-Z]+?(?=[^A-Z]))", " $1");
        }


        public static Vector3 MiddlePoint(Vector3 point1, Vector3 point2)
        {
            return (point1 + point2) * .5f;
        }
        /// <summary>
        /// Gets the random element.
        /// </summary>
        /// <returns>The random element.</returns>
        /// <param name="array">Array.</param>
        /// <param name="except">Except.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static T GetRandomElement<T>(this T[] array, params T[] except)
        {
            UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
            if (except == null || except.Length == 0)
            {
                return array[UnityEngine.Random.Range(0, array.Length)];
            }
            T res = default(T);
            bool hasElementToReturn = false;
            foreach (T element in array)
            {
                if (!except.Contains(element))
                {
                    hasElementToReturn = true;
                    break;
                }
            }
            if (!hasElementToReturn)
            {
                throw new System.NullReferenceException("Collection of " + typeof(T).ToString() + " has no element to return");
            }
            res = array[UnityEngine.Random.Range(0, array.Length)];
            while (except.Contains(res))
            {
                res = array[UnityEngine.Random.Range(0, array.Length)];
            }
            return res;
        }

        /// <summary>
        /// Is the specified rect collides with other one.
        /// </summary>
        /// <returns>The collides.</returns>
        /// <param name="rect">Rect.</param>
        /// <param name="other">Other.</param>
        public static bool Collides(this RectTransform rect, RectTransform other)
        {
            {
                Rect rect1 = new Rect(rect.localPosition.x, rect.localPosition.y, rect.rect.width, rect.rect.height);
                Rect rect2 = new Rect(other.localPosition.x, other.localPosition.y, other.rect.width, other.rect.height);

                return rect1.Overlaps(rect2);
            }
        }

        /// <summary>
        /// Remap the specified value to different scale.
        /// </summary>
        /// <returns>The remap.</returns>
        /// <param name="value">Value.</param>
        /// <param name="from1">From1.</param>
        /// <param name="to1">To1.</param>
        /// <param name="from2">From2.</param>
        /// <param name="to2">To2.</param>
        public static float Remap(float value, float from1, float to1, float from2, float to2)
        {
            if (value > to1)
            {
                return to2;
            }
            if (value <= from1)
            {
                return from2;
            }
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}
