﻿using UnityEngine;

namespace Assets.Scripts.Tools
{
    public class EditorObject : MonoBehaviour
    {
        public void OnEnable()
        {
#if UNITY_EDITOR
            gameObject.SetActive(true);
#else
            gameObject.SetActive(false);
#endif
        }
    }
}
