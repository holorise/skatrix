﻿/*
    Copyright (c) 2017 Vasyl Romanets
    romanets.vasyl@gmail.com
    
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgement in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using SKTRX.Enums;
using SKTRX.Interfaces;
using UnityEngine;
using UnityEngine.Internal;

/// <summary>
/// Class containing methods to ease debugging while developing a game.
/// Methods are called only if "LOGGING" define is added to Scripting Define Symbols in Player Settings.
/// </summary>
public sealed class CustomDebug
{
    private const string LOGGING = "LOGGING";
    private const string UNITY_ASSERTIONS = "UNITY_ASSERTIONS";

#region CATEGORY_DEBUG
    private static List<ICustomDebuggable> _debuggablesList = new List<ICustomDebuggable>();
    private static Dictionary<DebugType, bool> _debuggableDict = new Dictionary<DebugType, bool>();

    public static void Subscribe(ICustomDebuggable debuggable)
    {
        if (!_debuggablesList.Contains(debuggable))
        {
            _debuggablesList.Add(debuggable);
        }
        if(!_debuggableDict.ContainsKey(debuggable.DebugType))
        {
            _debuggableDict.Add(debuggable.DebugType, false);
            _debuggableDict[debuggable.DebugType] = true;
        }
    }

    public static Dictionary<DebugType, bool> GetDebuggables()
    {
        return _debuggableDict; 
    }

    public static void Unsusbscribe(ICustomDebuggable debuggable)
    {
        if (_debuggablesList.Contains(debuggable))
        {
            _debuggablesList.Remove(debuggable);
        }
        if (_debuggableDict.ContainsKey(debuggable.DebugType))
        {
            _debuggableDict.Remove(debuggable.DebugType);
        }
    }

    private static bool CanDebugCategory(DebugType debugType)
    {
        if(_debuggableDict.ContainsKey(debugType))
        {
            return _debuggableDict[debugType]; 
        } 
        else
        {
            return false; 
        }
    }

    private static bool CanDebug(string pathName)
    {
        var canDebug = false;
        var str = pathName.Remove(0, pathName.LastIndexOf('/'));
        str = str.Remove(0, 1);
        var name = str.Replace(".cs", "");
        var length = _debuggablesList.Count;
        for (int i = 0; i < length; i++)
        {
            var currDebuggable = _debuggablesList[i];
            var currName = currDebuggable.ToString();
            currName = currName.Remove(currName.IndexOf('('));
            currName = currName.Replace("(","");
            currName = currName.Replace(")", "");
            currName = currName.Replace(" ", "");

            if (currName == name)
            {
                canDebug = CanDebugCategory(currDebuggable.DebugType); 
            }
        }
        return canDebug; 
    }
    #endregion

    /// <summary>
    /// Reports whether the development console is visible.
    /// The development console cannot be made to appear using:
    /// </summary>
    public static bool developerConsoleVisible
    {
        get { return UnityEngine.Debug.developerConsoleVisible; }
        set { UnityEngine.Debug.developerConsoleVisible = value; }
    }

    /// <summary>
    /// In the Build Settings dialog there is a check box called "Development Build".
    /// </summary>
    public static bool isDebugBuild
    {
        get { return UnityEngine.Debug.isDebugBuild; }
    }

    /// <summary>
    /// Get default debug logger.
    /// </summary>
    public static ILogger logger
    {
        get { return UnityEngine.Debug.unityLogger; }
    }

    /// <summary>
    /// Assert a condition and logs an error message to the Unity console on failure.
    /// </summary>
    /// <param name="condition">Condition you expect to be true.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void Assert(bool condition)
    {
        UnityEngine.Debug.Assert(condition);
    }

    /// <summary>
    /// Assert a condition and logs an error message to the Unity console on failure.
    /// </summary>
    /// <param name="condition">Condition you expect to be true.</param>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void Assert(bool condition, string message, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Assert(condition, message);
        }
    }

    /// <summary>
    /// Assert a condition and logs an error message to the Unity console on failure.
    /// </summary>
    /// <param name="condition">Condition you expect to be true.</param>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void Assert(bool condition, object message, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Assert(condition, message);
        }
    }

    /// <summary>
    /// Assert a condition and logs an error message to the Unity console on failure.
    /// </summary>
    /// <param name="condition">Condition you expect to be true.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void Assert(bool condition, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Assert(condition, context);
        }
    }

    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void Assert(bool condition, string message, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Assert(condition, message, context);
        }
    }

    /// <summary>
    /// Assert a condition and logs an error message to the Unity console on failure.
    /// </summary>
    /// <param name="condition">Condition you expect to be true.</param>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void Assert(bool condition, object message, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Assert(condition, message, context);
        }
    }

    /// <summary>
    /// Pauses the editor.
    /// </summary>
    [Conditional(LOGGING)]
    public static void Break()
    {
        UnityEngine.Debug.Break();
    }

    /// <summary>
    /// Clears errors from the developer console.
    /// </summary>
    [Conditional(LOGGING)]
    public static void ClearDeveloperConsole()
    {
        UnityEngine.Debug.ClearDeveloperConsole();
    }

    [Conditional(LOGGING)]
    public static void DebugBreak()
    {
        UnityEngine.Debug.DebugBreak();
    }

    /// <summary>
    /// Draws a line between specified start and end points.
    /// </summary>
    /// <param name="start">Point in world space where the line should start.</param>
    /// <param name="end">Point in world space where the line should end.</param>
    [ExcludeFromDocs]
    [Conditional(LOGGING)]
    public static void DrawLine(Vector3 start, Vector3 end, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawLine(start, end);
        }
    }

    /// <summary>
    /// Draws a line between specified start and end points.
    /// </summary>
    /// <param name="start">Point in world space where the line should start.</param>
    /// <param name="end">Point in world space where the line should end.</param>
    /// <param name="color">Color of the line.</param>
    [ExcludeFromDocs]
    [Conditional(LOGGING)]
    public static void DrawLine(Vector3 start, Vector3 end, Color color, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawLine(start, end, color);
        }
    }

    /// <summary>
    /// Draws a line between specified start and end points.
    /// </summary>
    /// <param name="start">Point in world space where the line should start.</param>
    /// <param name="end">Point in world space where the line should end.</param>
    /// <param name="color">Color of the line.</param>
    /// <param name="duration">How long the line should be visible for.</param>
    [ExcludeFromDocs]
    [Conditional(LOGGING)]
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawLine(start, end, color, duration);
        }
    }

    /// <summary>
    /// Draws a line between specified start and end points.
    /// </summary>
    /// <param name="start">Point in world space where the line should start.</param>
    /// <param name="end">Point in world space where the line should end.</param>
    /// <param name="color">Color of the line.</param>
    /// <param name="duration">How long the line should be visible for.</param>
    /// <param name="depthTest">Should the line be obscured by objects closer to the camera?</param>
    [Conditional(LOGGING)]
    public static void DrawLine(Vector3 start, Vector3 end, [DefaultValue("Color.white")] Color color, [DefaultValue("0.0f")] float duration, [DefaultValue("true")] bool depthTest, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest);
        }
    }

    /// <summary>
    /// Draws a line from start to start + dir in world coordinates.
    /// </summary>
    /// <param name="start">Point in world space where the ray should start.</param>
    /// <param name="dir">Direction and length of the ray.</param>
    [ExcludeFromDocs]
    [Conditional(LOGGING)]
    public static void DrawRay(Vector3 start, Vector3 dir, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawRay(start, dir);
        }
    }

    /// <summary>
    /// Draws a line from start to start + dir in world coordinates.
    /// </summary>
    /// <param name="start">Point in world space where the ray should start.</param>
    /// <param name="dir">Direction and length of the ray.</param>
    /// <param name="color">Color of the drawn line.</param>
    [ExcludeFromDocs]
    [Conditional(LOGGING)]
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawRay(start, dir, color);
        }
    }

    /// <summary>
    /// Draws a line from start to start + dir in world coordinates.
    /// </summary>
    /// <param name="start">Point in world space where the ray should start.</param>
    /// <param name="dir">Direction and length of the ray.</param>
    /// <param name="color">Color of the drawn line.</param>
    /// <param name="duration">How long the line will be visible for (in seconds).</param>
    [ExcludeFromDocs]
    [Conditional(LOGGING)]
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawRay(start, dir, color, duration);
        }
    }

    /// <summary>
    /// Draws a line from start to start + dir in world coordinates.
    /// </summary>
    /// <param name="start">Point in world space where the ray should start.</param>
    /// <param name="dir">Direction and length of the ray.</param>
    /// <param name="color">Color of the drawn line.</param>
    /// <param name="duration">How long the line will be visible for (in seconds).</param>
    /// <param name="depthTest">Should the line be obscured by other objects closer to the camera?</param>
    [Conditional(LOGGING)]
    public static void DrawRay(Vector3 start, Vector3 dir, [DefaultValue("Color.white")] Color color,
         [DefaultValue("0.0f")] float duration, [DefaultValue("true")] bool depthTest, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.DrawRay(start, dir, color, duration, depthTest);
        }
    }

    /// <summary>
    /// Logs message to the Unity Console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    [Conditional(LOGGING)]
    public static void Log(object message, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Log(message);
        }

    }

    /// <summary>
    /// Logs message to the Unity Console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING)]
    public static void Log(object message, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.Log(message, context);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs an assertion message to the console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void LogAssertion(object message, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogAssertion(message);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs an assertion message to the console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING), Conditional(UNITY_ASSERTIONS)]
    public static void LogAssertion(object message, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogAssertion(message, context);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs an error message to the console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    [Conditional(LOGGING)]
    public static void LogError(object message, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogError(message);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs an error message to the console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING)]
    public static void LogError(object message, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogError(message, context);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs an error message to the console.
    /// </summary>
    /// <param name="exception">Runtime Exception.</param>
    [Conditional(LOGGING)]
    public static void LogException(Exception exception, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogException(exception);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs an error message to the console.
    /// </summary>
    /// <param name="exception">Runtime Exception.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING)]
    public static void LogException(Exception exception, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogException(exception, context);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs a warning message to the console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    [Conditional(LOGGING)]
    public static void LogWarning(object message, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogWarning(message);
        }
    }

    /// <summary>
    /// A variant of Debug.Log that logs a warning message to the console.
    /// </summary>
    /// <param name="message">String or object to be converted to string representation for display.</param>
    /// <param name="context">Object to which the message applies.</param>
    [Conditional(LOGGING)]
    public static void LogWarning(object message, UnityEngine.Object context, [CallerFilePath] string memberName = "")
    {
        if (CanDebug(memberName))
        {
            UnityEngine.Debug.LogWarning(message, context);
        }
    }
}