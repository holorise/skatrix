﻿
using UnityEngine;

public class CameraObject : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    public static Camera Camera { get;  set; }

    private void Start()
    {
#if !UNITY_EDITOR
        Camera = _camera?? Camera.main;
#endif
    }

    private void OnDestroy()
    {
        Camera = null;
    }
}