﻿using UnityEngine;
using System.Collections;

namespace SKTRX.Tools
{
    public class MathTool
    {
        private static float marginOfError = 0.000001f;

        public static bool LineSegmentsIntersection2D(Vector3 lineOneStart, Vector3 lineOneEnd, Vector3 lineSecondStart, Vector3 lineSecondEnd, out Vector3 intersection)
        {
            intersection = Vector3.zero;

            float d = (lineOneEnd.x - lineOneStart.x) * (lineSecondEnd.z - lineSecondStart.z) - (lineOneEnd.z - lineOneStart.z) * (lineSecondEnd.x - lineSecondStart.x);

            if (d == 0.0f)
                return false;

            float u = ((lineSecondStart.x - lineOneStart.x) * (lineSecondEnd.z - lineSecondStart.z) - (lineSecondStart.z - lineOneStart.z) * (lineSecondEnd.x - lineSecondStart.x)) / d;
            float v = ((lineSecondStart.x - lineOneStart.x) * (lineOneEnd.z - lineOneStart.z) - (lineSecondStart.z - lineOneStart.z) * (lineOneEnd.x - lineOneStart.x)) / d;

            if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
                return false;

            intersection.x = lineOneStart.x + u * (lineOneEnd.x - lineOneStart.x);
            intersection.z = lineOneStart.z + u * (lineOneEnd.z - lineOneStart.z);

            return true;
        }

        public static void LineSegmentsIntersection3D(Vector3 lineOneStart, Vector3 lineOneEnd, Vector3 lineSecondStart, Vector3 lineSecondEnd, 
            out Vector3 p0, out Vector3 p1, out bool onSegment, out bool intersects)
        {
            Vector3 r = lineOneEnd - lineOneStart;
            Vector3 s = lineSecondEnd - lineSecondStart;
            Vector3 q = lineOneStart - lineSecondStart;

            float dotqr = Vector3.Dot(q, r);
            float dotqs = Vector3.Dot(q, s);
            float dotrs = Vector3.Dot(r, s);
            float dotrr = Vector3.Dot(r, r);
            float dotss = Vector3.Dot(s, s);

            float denom = dotrr * dotss - dotrs * dotrs;
            float numer = dotqs * dotrs - dotqr * dotss;

            float t = numer / denom;
            float u = (dotqs + t * dotrs) / dotss;
            p0 = lineOneStart + t * r;
            p1 = lineSecondStart + u * s;
            onSegment = false;
            intersects = false;
            if (0 <= t && t <= 1 && 0 <= u && u <= 1) onSegment = true;
            if ((p0 - p1).magnitude <= marginOfError) intersects = true;
        }
    }
}