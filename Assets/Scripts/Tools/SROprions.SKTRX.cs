﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SRF;
using System.ComponentModel;
using SKTRX.Enums;

public partial class SROptions
{
    private const string DEBUG_CATEGORY_NAME = "Debug";

    private void SetDebuggableActive(bool isActive, DebugType debugType)
    {
        var debuggables = CustomDebug.GetDebuggables();

        if(debuggables.ContainsKey(debugType))
        {
            debuggables[debugType] = isActive;
        }
        else
        {
            Debug.LogErrorFormat("There is no debuggable with type: {0}", debugType);
        }
    }

    private bool GetDebuggable(DebugType debugType)
    {
        var debuggables = CustomDebug.GetDebuggables();
        if (debuggables.ContainsKey(debugType))
        {
            return debuggables[debugType];
        }
        else
        {
            //Debug.LogErrorFormat("There is no debuggable with type: {0}", debugType);
            return false;
        }
    }

    private void OnValueChanged(string n, object newValue)
    {
        OnPropertyChanged(n);
    }

    [Category(DEBUG_CATEGORY_NAME)]
    public bool CanDebugMovement
    {
        get { return GetDebuggable(DebugType.Movement); }
        set
        {
            OnValueChanged("CanDebugMovement", value);
            SetDebuggableActive(value, DebugType.Movement);
        }
       
    }

    [Category(DEBUG_CATEGORY_NAME)]
    public bool CanDebugPath
    {
        get { return GetDebuggable(DebugType.Path);  }
        set
        {

            OnValueChanged("CanDebugPath", value);
            SetDebuggableActive(value, DebugType.Path);
        }
    }

    [Category(DEBUG_CATEGORY_NAME)]
    public bool CanDebugAR
    {
        get { return GetDebuggable(DebugType.AR); }
        set
        {
            OnValueChanged("CanDebugAR", value);
            SetDebuggableActive(value, DebugType.AR);
        }
    }

    [Category(DEBUG_CATEGORY_NAME)]
    public bool CanDebugGeneral
    {
        get { return GetDebuggable(DebugType.General); }
        set
        {
            OnValueChanged("CanDebugGeneral", value);
           SetDebuggableActive(value, DebugType.General);
        }
    }
}
