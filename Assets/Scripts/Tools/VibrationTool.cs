﻿

#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKTRX.Tools
{
    public class VibrationTool : MonoBehaviour
    {

        public static bool VibrationAllow = false;
        public static void HandheldVibrate()
        {
            Handheld.Vibrate();
        }

        public static void PeekVibration()
        {
            if (VibrationAllow)
            {
#if UNITY_EDITOR

#elif UNITY_IOS
        if (Vibration.HasVibrator())
            Vibration.VibratePeek();;
#elif UNITY_ANDROID
        if (Vibration.HasVibrator())
            Vibration.Vibrate(50);
#endif
            }
        }

        public static void PopVibration()
        {
            if (VibrationAllow)
            {
#if UNITY_EDITOR
                Debug.Log("PopVibration");
#elif UNITY_IOS
        if (Vibration.HasVibrator())
            Vibration.VibratePop();
#elif UNITY_ANDROID
        if (Vibration.HasVibrator())
            Vibration.Vibrate(200);
#endif
            }
        }

        public static void NopeVibration()
        {
            if (VibrationAllow)
            {
#if UNITY_EDITOR

#elif UNITY_IOS
        if (Vibration.HasVibrator())
            Vibration.VibrateNope();
#endif
            }
        }


        public static void LoadCatScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

    }
}
