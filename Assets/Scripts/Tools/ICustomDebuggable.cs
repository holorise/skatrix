﻿using SKTRX.Enums;

namespace SKTRX.Interfaces
{
    public interface ICustomDebuggable
    {
        DebugType DebugType { get; set; }
    }
}
