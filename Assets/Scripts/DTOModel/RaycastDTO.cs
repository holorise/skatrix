﻿using UnityEngine;

namespace SKTRX.DTOModel
{
    public struct RaycastDTO
    {
        public Vector3 CurrentPosition;
        public Vector3 Forward;
        public float Velocity;
        public float Height;
        public float SteeringInput;
        public float Delta;
        public int Count;

        public RaycastDTO(Vector3 currentPosition, Vector3 forward, float velocity, float height, float steeringInput, float delta, int count)
        {
            CurrentPosition = currentPosition;
            Forward = forward;
            Velocity = velocity;
            Height = height;
            SteeringInput = steeringInput;
            Delta = delta;
            Count = count;
        }
    }
}
