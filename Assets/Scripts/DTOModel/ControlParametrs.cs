﻿using UnityEngine;
using System;

namespace SKTRX.DTOModel
{
    [Serializable]
    public class ControlParametrs
    {
        [SerializeField] private float _min;
        [SerializeField] private float _max;

        public float Min { get => _min;}
        public float Max { get => _max;}
    }
}