﻿

#pragma warning disable 649
using UnityEngine;

namespace SKTRX.DTOModel
{

    public struct PlayerDTO
    {
        public float velocity;
        public float steering;
        public float skateboardSize;
        public float maxVelocity;
        public int preJumpPoints;

        public Vector3 position;
        public Vector3 movenentForward;

        public PlayerDTO(int jumpPoints, float vel, float maxVel, float steer, float skateSize, Vector3 pos, Vector3 moveForward)
        {
            preJumpPoints = jumpPoints;
            velocity = vel;
            maxVelocity = maxVel;
            steering = steer;
            skateboardSize = skateSize;
            position = pos;
            movenentForward = moveForward;
        }

        public void DebugData()
        {
            Debug.LogWarning("DTO: ");
            Debug.LogWarning("preJumpPoints: "+ preJumpPoints);
            Debug.LogWarning("velocity: "+ velocity);
            Debug.LogWarning("maxVelocity: " + maxVelocity);
            Debug.LogWarning("steering: " + steering);
            Debug.LogWarning("skateboardSize: "+ skateboardSize);
            Debug.LogWarning("position: " + position);
            Debug.LogWarning("movenentForward: " + movenentForward);
        }
    }
}