﻿using UnityEngine;
using SKTRX.Enums;
using System;

namespace SKTRX.DTOModel
{
    [Serializable]
    public struct PathPointDTO
    {
        [SerializeField] private Vector3 _point;
        [SerializeField] private PathPointType _pathPointType;
        [SerializeField] private PathPointJumpType _pathPointJumpType;

        public Vector3 Point { get => _point; }
        public PathPointType PathPointType { get => _pathPointType; }
        public PathPointJumpType PathPointJumpType { get => _pathPointJumpType; }

        public PathPointDTO(Vector3 position, PathPointType pathPointType)
        {
            _point = position;
            _pathPointType = pathPointType;
            _pathPointJumpType = PathPointJumpType.Point;
        }

        public PathPointDTO(Vector3 position, PathPointType pathPointType, PathPointJumpType pathPointJumpType)
        {
            _point = position;
            _pathPointType = pathPointType;
            _pathPointJumpType = pathPointJumpType;
        }
    }
}