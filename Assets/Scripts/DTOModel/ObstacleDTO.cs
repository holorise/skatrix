﻿using UnityEngine;
using System;
using SKTRX.Enums;
using System.Collections;

namespace SKTRX.DTOModel
{
    [Serializable]
    public class ObstacleDTO : IHashCodeProvider
    {
        [SerializeField] private string _title;
        [SerializeField] private string _description;
        [SerializeField] private ObstacleType _obstacleType;
        [SerializeField] private Sprite _image;
        [SerializeField] private ControlParametrs _rotateParametrs;
        [SerializeField] private ControlParametrs _scaleParametrs;

        public string Title { get => _title; }
        public string Description { get => _description; }
        public ObstacleType Type { get => _obstacleType; }
        public Sprite Image { get => _image; }
        public ControlParametrs RotateParametrs { get => _rotateParametrs; set => _rotateParametrs = value; }
        public ControlParametrs ScaleParametrs { get => _scaleParametrs; set => _scaleParametrs = value; }

        public int GetHashCode(object obj)
        {
            return _title.GetHashCode() ^ _description.GetHashCode();
        }
    }
}