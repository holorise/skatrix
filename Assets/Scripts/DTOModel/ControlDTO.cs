﻿
using UnityEngine;
using SKTRX.Enums;

namespace SKTRX.DTOModel
{
    public struct ControlDTO
    {
        public InputType inputType;
        public TrickType trickType;
        public Direction turnDirection;
        public int trickNumber;
        public Vector2 direction;
        public Vector3 direction3;
        public float velocity;
        public Vector3[] pointToMove;
        public float? speedNormalized;
        public bool isStaticTurn;
    }
}