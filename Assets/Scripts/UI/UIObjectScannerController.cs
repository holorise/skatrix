﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SKTRX.Managers;

public class UIObjectScannerController : MonoBehaviour
{
    public string menuSceneName = "SKTRXARKitMainMenu";
    public string editColliderSceneName = "SKTRXARKitMainMenu";
    public ScannerARCameraManager arManager;

    private void Start()
    {

    }

    public void MenuAction()
    {
        SceneManager.LoadScene(menuSceneName);
    }

    public void ColliderEditAction()
    {
        SceneManager.LoadScene(editColliderSceneName);
    }

    public void CreateAction()
    {
        ScannedObjectSerializationManager.instance.CreateRefObject(delegate ()
        {
            Debug.Log("@@@ CreateRefObject did finish");
            /*
            if (_arManager == null)
                Debug.Log("@@@ _arManager is null");
            else
            {
                _arManager.SetObjScaningActive(false);
                _arManager.SetObjTrackingActive(true);
            }
            */

            ScannedObjectSerializationManager.instance.SaveData();
        });
    }

    public void NextAction()
    {
        Debug.Log(@"@@@ Next Action");

        ScannedObjectSerializationManager.instance.TurnOffPoints();
        ScannedObjectSerializationManager.instance.TurnOffBoundingBox();

        if (arManager != null)
        {
            Debug.Log("@@@ Turn on tracking session");
            arManager.TurnOnTrackingSession();
        }
    }

    public void SaveAction()
    {

        Debug.Log(@"@@@ Save Action");

        ScannedObjectSerializationManager.instance.SaveData();
    }
}
