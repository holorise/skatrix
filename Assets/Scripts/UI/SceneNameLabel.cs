﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneNameLabel : MonoBehaviour
{
    private Text sceneNameLabel;

    // Start is called before the first frame update
    void Start()
    {
        sceneNameLabel = GetComponent<Text>();

        if(sceneNameLabel != null)
        {
            Scene scene = SceneManager.GetActiveScene();
            sceneNameLabel.text = scene.name;
        }
    }
}
