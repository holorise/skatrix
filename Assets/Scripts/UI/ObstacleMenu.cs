﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using SKTRX.UI.Item;
using SKTRX.Managers;

public class ObstacleMenu : MonoBehaviour
{
    [SerializeField] private RectTransform _menuRectTransform;
    [SerializeField] private Transform _obstacleParentItem;
    [SerializeField] private ObstacleItemUI _obstacleItemUIPrefab;
    [SerializeField] private ScrollRect _scrollRect;

    private List<ObstacleItemUI> obstacleItemUIs = new List<ObstacleItemUI>();
    private bool _show = false;
    private Action _onPointerExit;
    private ObstacleManager _obstacleManager { get => ObstacleManager.Instance; }

    private void OnEnable()
    {
        HideMenu();
    }

    private void Start()
    {
        for (int i = 0; i < _obstacleManager.Obstacles.Count; i++)
        {
            var obstaleUI = Instantiate(_obstacleItemUIPrefab, _obstacleParentItem);
            obstacleItemUIs.Add(obstaleUI.SetInformaiton(_obstacleManager.Obstacles[i].ObstacleDTO, ObjectDrag, _obstacleManager.GetObstacle, this));
        }   
    }

    public void OnBeginDrag(PointerEventData pointerData)
    {
        _scrollRect.OnBeginDrag(pointerData);
    }

    public void OnDraging(PointerEventData pointerData)
    {
        _scrollRect.OnDrag(pointerData);
    }

    public void OnEndDrag(PointerEventData pointerData)
    {
        _scrollRect.OnEndDrag(pointerData);
    }

    public void AddListener(Action onPointerExit)
    {
        _onPointerExit = onPointerExit;
    }

    public void RemoveListener()
    {
        _onPointerExit = null;
    }

    private void ObjectDrag(bool show)
    {
       if(show)
            ShowMenu();
       else
            HideMenu();
    }

    public void OnClickShow()
    {
        if (_show)
            HideMenu();
        else
            ShowMenu();
    }

    public void ShowMenu(bool show = true)
    {
        if (show)
            ShowMenu();
        else
            HideMenu();
    }

    private void ShowMenu()
    {
        _menuRectTransform.anchoredPosition = Vector2.zero;
        _show = true;
    }

    private void HideMenu()
    {
        _menuRectTransform.anchoredPosition = new Vector2(_menuRectTransform.sizeDelta.x, 0);
        _show = false;
    }

    public void OnPointerExit()
    {
        _onPointerExit?.Invoke();
    }
}