﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SKTRX.UI
{
    public class UIMainMenuController : MonoBehaviour
    {
        public string objectDetectionSceneName = "SKTRXARKitObjectAnchor";
        public string imageDetectionSceneName = "SKTRXARKitImageObjDetection";
        public string objectScanerSceneName = "SKTRXARKitObjectScanner";
        public string envProbeSceneName = "SKTRXARKitObjectScanner";


        public void LoadObjectDetectionScene()
        {
            SceneManager.LoadScene(objectDetectionSceneName);
        }

        public void LoadImageDetectionScene()
        {
            SceneManager.LoadScene(imageDetectionSceneName);
        }

        public void LoadObjectScannerScene()
        {
            SceneManager.LoadScene(objectScanerSceneName);
        }

        public void LoadEnvProbeScene()
        {
            SceneManager.LoadScene(envProbeSceneName);
        }
    }
}
