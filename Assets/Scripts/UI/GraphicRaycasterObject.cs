﻿using UnityEngine;
using UnityEngine.UI;
using Tools;
namespace SKTRX.UI
{
    public class GraphicRaycasterObject : Singelton<GraphicRaycasterObject>
    {
        [SerializeField] private GraphicRaycaster _graphicRaycaster;

        public GraphicRaycaster GraphicRaycaster
        {
            get
            {
                if (_graphicRaycaster)
                    _graphicRaycaster = FindObjectOfType<GraphicRaycaster>();
                return _graphicRaycaster;
            }
        }

    }
}