﻿
using UnityEngine;
using UnityEngine.UI;
using System;
using SKTRX.Obstacle;
using SKTRX.Enums;
using SKTRX.DTOModel;

namespace SKTRX.UI.Item
{
    public class SliderItemUI : MonoBehaviour
    {
        [SerializeField] private Slider _slider;
        [SerializeField] private ObstacleMenuType _obstacleMenuType;
        private Action<float> _onValueChangeAction;
        private Action _onCompleteControllAction;
        private bool _pointIn = false;
        private BaseObstacle _currentObstacle;

        public void Show(BaseObstacle obstacle, Action onCompleteControllAction)
        {
            _currentObstacle = obstacle;
            gameObject.SetActive(true);
            _onCompleteControllAction = onCompleteControllAction;
            SetParametrs(obstacle.ObstacleDTO);
        }
        public void OnSlideChange()
        {
            if (_obstacleMenuType.Equals(ObstacleMenuType.Rotate))
            {
                _currentObstacle.ChangeRotation(_slider.value);
            }
            else if (_obstacleMenuType.Equals(ObstacleMenuType.Scale))
            {
                _currentObstacle.ChangeScale(_slider.value);
            }
        }

        public void OnDragComplete()
        {
            CompleteUI();
        }

        public void OnPointerUp()
        {
            CompleteUI();
        }

        private void SetParametrs(ObstacleDTO obstacleDTO)
        {
            if(_obstacleMenuType.Equals(ObstacleMenuType.Rotate))
            {
                _slider.minValue = obstacleDTO.RotateParametrs.Min;
                _slider.maxValue = obstacleDTO.RotateParametrs.Max;
                _slider.value = _currentObstacle.GetRotate();
            }
            else if (_obstacleMenuType.Equals(ObstacleMenuType.Scale))
            {
                _slider.minValue = obstacleDTO.ScaleParametrs.Min;
                _slider.maxValue = obstacleDTO.ScaleParametrs.Max;
                _slider.value = _currentObstacle.GetScale();
            }
        }

        private void CompleteUI()
        {
            _onCompleteControllAction?.Invoke();
            gameObject.SetActive(false);
            _onCompleteControllAction = null;
        }
    }
}