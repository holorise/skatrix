﻿

#pragma warning disable 649
using UnityEngine;
using SKTRX.Obstacle;
using System;
using SKTRX.Managers;
using SKTRX.Screens;
using SKTRX.Obstacle.Rail;
using System.Collections.Generic;
using System.Linq;
using SKTRX.Statics;

namespace SKTRX.UI.Item
{
    public class RailBuilder : MonoBehaviour
    {

        private Action _onCompleteControllAction;
        private Action<bool> _onApplyButtonAction;
        private bool _pointIn = false;
        private BaseObstacle _currentObstacle;
        private bool _railWorks = false;
        private RailBuildPhase _currentStatePhase;

        private ProcedureRail _procedureRailObstacle;
        private GameObject _railObstacle;
        private RailPoint _firstRailPoint;
        private RailPoint _endRailPoint;
        private List<RailLine> _railLines = new List<RailLine>();

        private ObjectPlaceScreen ObjectPlaceScreen { get { return ScreenManager.Instance.Get<ObjectPlaceScreen>(Enums.ScreenType.ObjectPlace); } }
        
        private enum RailBuildPhase
        {
            tapOnObstacle,
            tapOnPlane,
            endBuild
        }

        public bool IsShow()
        {
            return _railWorks;
        }

        public void Show(BaseObstacle obstacle, Action onCompleteControllAction, Action<bool> applyButtonAction)
        {
            _railWorks = true;
            _firstRailPoint = null;
            _endRailPoint = null;
            _railObstacle = null;
            _railLines.Clear();
            _procedureRailObstacle = null;
            _currentObstacle = obstacle;
            gameObject.SetActive(true);
            _onCompleteControllAction = onCompleteControllAction;
            _onApplyButtonAction = applyButtonAction;
            ObjectPlaceScreen.ShowMessage("Tap an obstacle to start a rail.");
            ObstacleManager.Instance.SubscribeOnTaps(OnPlaneTap, OnObstacleTap);
            _currentStatePhase = RailBuildPhase.tapOnObstacle;
            _railObstacle = new GameObject("RailObstacleProcedure");
        }

        private void OnObstacleTap(BaseObstacle obstacle, Vector3 position)
        {
            if (_currentObstacle.Equals(obstacle))
            {
                if (_currentStatePhase.Equals(RailBuildPhase.tapOnObstacle))
                {
                    Vector3 newPosition = position;
                    if (GetPosition(_currentObstacle, position, ref newPosition))
                    {
                        Transform parentForObstacle = _railObstacle.transform;
                        if (_procedureRailObstacle == null)
                            _procedureRailObstacle = _railObstacle.AddComponent<ProcedureRail>();
                        parentForObstacle = obstacle.ParentForRail;
                        _firstRailPoint = Instantiate(ObstacleManager.Instance.RailPointPrefab, parentForObstacle);
                        _firstRailPoint.InstantiatePoint(_procedureRailObstacle, newPosition);
                        obstacle.AddPoint(_firstRailPoint);

                        ObjectPlaceScreen.ShowMessage("Tap the plane to end the rail.");
                        _currentStatePhase = RailBuildPhase.tapOnPlane;
                        BuildRailAndFinish();
                    }
                }
                else if (_currentStatePhase.Equals(RailBuildPhase.endBuild))
                {
                    Vector3 newPosition = position;
                    if (GetPosition(_currentObstacle, position, ref newPosition))
                    {
                        if (_firstRailPoint != null)
                            _firstRailPoint.InstantiatePoint(_procedureRailObstacle, newPosition);
                        BuildRailAndFinish();
                    }
                }
                else
                {
                    ObjectPlaceScreen.ShowMessage("Tap the plane to end the rail first.");
                }
            }
        }

        private bool GetPosition(BaseObstacle obstacleTarget, Vector3 position, ref Vector3 newPosition)
        {
            RaycastHit[] hits = Physics.RaycastAll(position+Vector3.up, Vector3.down);
            RaycastHit[] obstacle = hits.ToList().FindAll(p => p.collider.gameObject.tag.Equals(TagManager.OBSTACLE_TAG)).ToArray();
            for (int i = 0; i < obstacle.Length; i++)
            {
                if (obstacleTarget.Equals(obstacle[i].collider.GetComponent<BaseObstacle>()))
                {
                    newPosition = obstacle[i].point;
                    return true;
                }

                ColliderObstacle colliderObstacle = obstacle[0].transform.GetComponent<ColliderObstacle>();
                if (colliderObstacle != null && colliderObstacle.BaseObstacle != null)
                {
                    newPosition = obstacle[i].point;
                    return true;
                }
            }
            return false;
        }
        private void OnPlaneTap(Vector3 position)
        {
            if (_currentStatePhase.Equals(RailBuildPhase.tapOnPlane))
            {
                if (_procedureRailObstacle == null)
                    _procedureRailObstacle = _railObstacle.AddComponent<ProcedureRail>();
                _endRailPoint = Instantiate(ObstacleManager.Instance.RailPointPrefab, _railObstacle.transform);
                _endRailPoint.InstantiatePoint(_procedureRailObstacle, position);
                ObjectPlaceScreen.ShowMessage("Done? Tap apply");
                _currentStatePhase = RailBuildPhase.endBuild;
                BuildRailAndFinish();
            }
            else if (_currentStatePhase.Equals(RailBuildPhase.endBuild))
            {
                if (_endRailPoint != null)
                    _endRailPoint.InstantiatePoint(_procedureRailObstacle, position);
                BuildRailAndFinish();
            }
            else
            {
              
            }
        }

        private void BuildRailAndFinish()
        {
            if (_firstRailPoint != null &&  _endRailPoint != null)
            {
                Debug.Log("Build between succed");
                _onApplyButtonAction?.Invoke(BuildRailBetweenPoints());
            }
            else
            {
                Debug.Log("Build between fail");
                _onApplyButtonAction?.Invoke(false);
            }
        }
       
        private bool BuildRailBetweenPoints()
        {
            if (_firstRailPoint != null && _endRailPoint != null)
            {
                _procedureRailObstacle.InitializeRail(_firstRailPoint, _endRailPoint);
                return true;
            }
            return false;
        }
       

        public void OnApplyRailTap()
        {
            Debug.LogError("OnApplyRailTap");
            _railWorks = false;
            ObstacleManager.Instance.UnSubscribe();
            ObstacleManager.Instance.Obstacles.Add(_procedureRailObstacle);
            _procedureRailObstacle = null;
           _onCompleteControllAction?.Invoke();
        }

    }
}
