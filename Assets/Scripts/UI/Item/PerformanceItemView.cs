﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SKTRX.UI
{
    public class PerformanceItemView : MonoBehaviour
    {
        [SerializeField] private Text _labelNameText;
        [SerializeField] private Text _valueText;
        [SerializeField] private Text _valueUnitsText;

        public void SetValues(string labelName, string value, string valueUnits = "")
        {
            var hasUnits = !string.IsNullOrEmpty(valueUnits);
            _valueUnitsText.gameObject.SetActive(hasUnits);
            _labelNameText.text = labelName;
            _valueText.text = value;
            _valueUnitsText.text = valueUnits;
        }

        public void UpdateValue(string value)
        {
            _valueText.text = value; 
        }

    }
}
