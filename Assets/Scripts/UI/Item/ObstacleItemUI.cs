﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SKTRX.Obstacle;
using SKTRX.Statics;
using SKTRX.DTOModel;

namespace SKTRX.UI.Item
{
    public class ObstacleItemUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler,IBeginDragHandler, IEndDragHandler, IPointerExitHandler
    {
        [SerializeField] private Image _obstacleImage;
        [SerializeField] private Text _obstacleTitle;
        [SerializeField] private Text _obstacleDescription;

        private Action<bool> _showMenuAction;     
        private ObstacleMenu _obstacleMenu;
        private Func<int, BaseObstacle> _instantiateObstaceFunc;
        private BaseObstacle _targetInstantiateObstacle;
        private int _obstacleDTOHash;
        private bool _positionSet = false;
        private bool _leftLive = false;
        private bool _tap = false;

        public ObstacleItemUI SetInformaiton(ObstacleDTO obstacleDTO, Action<bool> showMenu, Func<int, BaseObstacle> instantiateObstaceFunc, ObstacleMenu obstacleMenu)
        {
            _obstacleMenu = obstacleMenu;
            _instantiateObstaceFunc = instantiateObstaceFunc;
            _showMenuAction = showMenu;
            _obstacleDTOHash = obstacleDTO.GetHashCode();
            _obstacleTitle.text = obstacleDTO.Title;
            _obstacleDescription.text = obstacleDTO.Description;
            _obstacleImage.sprite = obstacleDTO.Image;
            Input.multiTouchEnabled = true;
            return this;
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (_tap && _targetInstantiateObstacle)
            {
#if UNITY_EDITOR
                Ray raycast = CameraObject.Camera.ScreenPointToRay(eventData.position);
#else
                Ray raycast = CameraObject.Camera.ScreenPointToRay(Input.GetTouch(0).position);
#endif
                RaycastHit[] raycastHit = Physics.RaycastAll(raycast);
                List<RaycastHit> hits = raycastHit.ToList<RaycastHit>();
                RaycastHit plane = hits.Find(p => p.collider.gameObject.tag.Equals(TagManager.PLANE_TAG));
                RaycastHit[] obstacle = hits.FindAll(p => p.collider.gameObject.tag.Equals(TagManager.OBSTACLE_TAG)).ToArray();

                if (obstacle.Length > 0 && obstacle[0].collider != null)
                {
                    ColliderObstacle colliderObstacle = obstacle[0].transform.GetComponent<ColliderObstacle>();
                    if (colliderObstacle != null && colliderObstacle.BaseObstacle != null)
                    {
                        if (colliderObstacle.BaseObstacle.AllosSetObstacleUnder)
                        {
                            SetPosition(obstacle[0].point);
                        }
                        else if (_targetInstantiateObstacle != null && colliderObstacle.BaseObstacle.Equals(_targetInstantiateObstacle))
                        {
                            if (plane.collider != null)
                            {
                                SetPosition(plane.point);
                            }
                        }
                    }
                }
                else if(plane.collider != null)
                {
                    SetPosition(plane.point);
                    _leftLive = true;
                }
                else
                {
                    if (_targetInstantiateObstacle != null)
                    {
                        _targetInstantiateObstacle.gameObject.SetActive(false);
                        _leftLive = false;
                    }
                }              
            }
            _obstacleMenu.OnDraging(eventData);
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            _tap = true;
            _obstacleMenu.AddListener(InstantiatePlayer);
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            if (_tap)
            {
               
            }
        }

        private void InstantiatePlayer()
        {
            _obstacleMenu.RemoveListener();
            _targetInstantiateObstacle = _instantiateObstaceFunc?.Invoke(_obstacleDTOHash);
            _targetInstantiateObstacle.gameObject.SetActive(false);
            _positionSet = false;
            _showMenuAction?.Invoke(false);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            _tap = false;

            if (!_positionSet && _targetInstantiateObstacle != null)
            {
                Destroy(_targetInstantiateObstacle.gameObject);
            }
            else
            {
                if (_leftLive)
                    _targetInstantiateObstacle = null;
                else
                {
                    if (_targetInstantiateObstacle != null)
                        Destroy(_targetInstantiateObstacle.gameObject);
                }
            }

            _showMenuAction?.Invoke(true);
        }

        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            _obstacleMenu.OnBeginDrag(eventData);
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            _obstacleMenu.OnEndDrag(eventData);
            _obstacleMenu.RemoveListener();
        }

        private void SetPosition(Vector3 position)
        {
            if (_targetInstantiateObstacle != null)
            {
                _targetInstantiateObstacle.transform.position = position;
                _targetInstantiateObstacle.gameObject.SetActive(true);
                _positionSet = true;
            }
        }
    }
}