﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SKTRX.Managers;
using SKTRX.AR;

namespace SKTRX.UI
{
    public class UIEditColliderController : MonoBehaviour
    {
        enum MenuState
        {
            Default = 0,
            Edit = 1
        }

        public Slider scaleXSlider;
        public Slider scaleYSlider;
        public Slider scaleZSlider;
        public Slider rotXSlider;
        public Slider rotYSlider;
        public Slider rotZSlider;
        public Slider posXSlider;
        public Slider posYSlider;
        public Slider posZSlider;

        public GameObject defaultContent;
        public GameObject editContent;


        private MenuState menuState = MenuState.Default;



        // Start is called before the first frame update
        void Start()
        {

            if (scaleXSlider != null)
                scaleXSlider.onValueChanged.AddListener(delegate { ChangeScaleX(); });

            if (scaleYSlider != null)
                scaleYSlider.onValueChanged.AddListener(delegate { ChangeScaleY(); });

            if (scaleZSlider != null)
                scaleZSlider.onValueChanged.AddListener(delegate { ChangeScaleZ(); });

            if (rotXSlider != null)
                rotXSlider.onValueChanged.AddListener(delegate { ChangeRotX(); });

            if (rotYSlider != null)
                rotYSlider.onValueChanged.AddListener(delegate { ChangeRotY(); });

            if (rotZSlider != null)
                rotZSlider.onValueChanged.AddListener(delegate { ChangeRotZ(); });

            if (posXSlider != null)
                posXSlider.onValueChanged.AddListener(delegate { ChangePosX(); });

            if (posYSlider != null)
                posYSlider.onValueChanged.AddListener(delegate { ChangePosY(); });

            if (posZSlider != null)
                posZSlider.onValueChanged.AddListener(delegate { ChangePosZ(); });
        }

        private void EditMenuUpdate()
        {

        }

        // Update is called once per frame
        void Update()
        {
            switch (menuState)
            {
                case MenuState.Default:
                    defaultContent.SetActive(true);
                    editContent.SetActive(false);
                    break;

                case MenuState.Edit:
                    defaultContent.SetActive(false);
                    editContent.SetActive(true);
                    break;
            }

        }

        public void ChangeScaleX()
        {
            float value = scaleXSlider.value;
            Debug.Log("@@@ScaleX: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetScaleX(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangeScaleY()
        {
            float value = scaleYSlider.value;
            Debug.Log("@@@ScaleY: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetScaleY(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangeScaleZ()
        {
            float value = scaleZSlider.value;
            Debug.Log("@@@ScaleZ: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetScaleZ(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangeRotX()
        {
            float value = rotXSlider.value;
            Debug.Log("@@@RotX: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetRotationX(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangeRotY()
        {
            float value = rotYSlider.value;
            Debug.Log("@@@RotY: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetRotationY(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangeRotZ()
        {
            float value = rotZSlider.value;
            Debug.Log("@@@RotZ: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetRotationZ(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangePosX()
        {
            float value = posXSlider.value;
            Debug.Log("@@@PosX: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetPositionX(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangePosY()
        {
            float value = posYSlider.value;
            Debug.Log("@@@PosY: " + value);
            GameObject selectedObject = GameObject.FindGameObjectWithTag("EditCollider");

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetPositionY(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void ChangePosZ()
        {
            float value = posZSlider.value;
            Debug.Log("@@@PosZ: " + value);
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().SetPositionZ(value);
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void CylinderShapeAction()
        {
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();
            Debug.Log("@@@CylinderShapeAction0");
            if (selectedObject != null)
            {
                Debug.Log("@@@CylinderShapeAction1");
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                Debug.Log("@@@CylinderShapeAction2");
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    Debug.Log("@@@CylinderShapeAction3");
                    ColliderEditor script = colliderObject.GetComponent<ColliderEditor>();

                    if(script != null)
                    {
                        colliderObject.GetComponent<ColliderEditor>().CreateCylinder();
                    }
                    else
                    {
                        Debug.Log("@@@script is null");
                    }
                }
                else
                {
                    Debug.Log("@@@colliderObject is null");
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void CubeShapeAction()
        {
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().CreateCube();
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }

        public void SphereShapeAction()
        {
            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                ColliderScannedObjectAnchor colliderInitComp = selectedObject.GetComponent<ColliderScannedObjectAnchor>();
                GameObject colliderObject = colliderInitComp.CreatedObject;

                if (colliderObject != null)
                {
                    colliderObject.GetComponent<ColliderEditor>().CreateSphere();
                }
            }
            else
            {
                Debug.Log("@@@selectedObject is null");
            }
        }


        public void OpenEditMenu()
        {
            menuState = MenuState.Edit;
        }

        public void SaveCollider()
        {
            menuState = MenuState.Default;

            GameObject selectedObject = DetectionObjectManager.instance.GetLastObject();

            if (selectedObject != null)
            {
                selectedObject.GetComponent<ColliderEditor>().SaveCollider();
            }

            ScannedObjectSerializationManager.instance.SaveData();
        }

        public void MenuScene()
        {
            SceneManager.LoadScene("SKTRXARKitMainMenu");
        }
    }
}

