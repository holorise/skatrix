﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using SKTRX.InputControllers;
using SKTRX.Enums;

namespace SKTRX.UI
{
    public class ControlOptionUI : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Text _controlTitle;
        [SerializeField] private Text _controlDescription;
        [SerializeField] private Image _selectImage;

        private Action<ControlOptionUI> _onClickAction;
        private ControlOption _controlOption;

        public ControlType ControlType { get { return _controlOption.ControllType; } }

        public ControlOptionUI UpdateObject(ControlOption controlOption, Action<ControlOptionUI> onClickAction)
        {
            _controlOption = controlOption;
            _controlTitle.text = controlOption.Title;
            _controlDescription.text = controlOption.Descriprion;
            _selectImage.enabled = false;
            _onClickAction = onClickAction;
            return this;
        }

        public void Select()
        {
            _selectImage.enabled = true;
        }

        public void Deselect()
        {
            _selectImage.enabled = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _onClickAction?.Invoke(this);
        }
    }
}
