﻿using UnityEngine;
using UnityEngine.UI;
using System;
using SKTRX.Obstacle;
using SKTRX.UI.Item;

namespace SKTRX.UI
{
    public class ContentObstacleMenu : MonoBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private GameObject _buttonsPanel;
        [SerializeField] private GameObject _rotatePanel;
        [SerializeField] private GameObject _scalePanel;
        [SerializeField] private GameObject _obstaclePanel;
        [SerializeField] private Button _railBuildButton;
        [SerializeField] private Button _railApplydButton;
        [SerializeField] private Button _scaleButton;
        [SerializeField] private Button _rotateButton;
        [SerializeField] private Button _deleteButton;
        [SerializeField] private SliderItemUI _scaleSlider;
        [SerializeField] private SliderItemUI _rotateSlider;
        [SerializeField] private RailBuilder _railBuilder;

        private Action<BaseObstacle> _onDeleteAction;
        private BaseObstacle _targetObstacle;
        private bool _buildRailInProgress = false;

        public void Show(BaseObstacle obstacle, Vector2 positionTap, Action<BaseObstacle> onDeleteAction)
        {
            if (obstacle.Equals(_targetObstacle)) return;
            _onDeleteAction = onDeleteAction;
            _targetObstacle = obstacle;
            Show(true);
            _rectTransform.position = positionTap;
            _buttonsPanel.SetActive(true);
            _rotatePanel.SetActive(false);
            _scalePanel.SetActive(false);
            _obstaclePanel.SetActive(false);

            if (obstacle is RealObstacle)
            {
                _scaleButton.interactable = false;
                _rotateButton.interactable = false;
                _deleteButton.interactable = obstacle.AllosDestroy;
            }
            else
            {
                _scaleButton.interactable = obstacle.ObstacleDTO != null;
                _rotateButton.interactable = obstacle.ObstacleDTO != null;
                _deleteButton.interactable = obstacle.AllosDestroy;
            }

            _railBuildButton.interactable = obstacle.AllosSetRailpUnder;
        }

        public bool Hide()
        {
            if (_railBuilder.IsShow())
                return true;
            _targetObstacle = null;
            Show(false);
            return false;
        }

        public void OnScaleClick()
        {
            _buttonsPanel.SetActive(false);
            _rotatePanel.SetActive(false);
            _obstaclePanel.SetActive(false);
            _scalePanel.SetActive(true);
            _scaleSlider.Show(_targetObstacle, HideSliderPanel);
        }

        public bool IsBuildRail()
        {
            return _buildRailInProgress;
        }
        
        public void OnBuildRailClick()
        {
            _buttonsPanel.SetActive(false);
            _rotatePanel.SetActive(false);
            _scalePanel.SetActive(false);
            _obstaclePanel.SetActive(true);
            _railApplydButton.gameObject.SetActive(true);
            _railApplydButton.interactable = false;
            _buildRailInProgress = true;
            _railBuilder.Show(_targetObstacle, HideSliderPanel, RailButtonBuild);
        }

        private void RailButtonBuild(bool ineteractable)
        {
            _railApplydButton.interactable = ineteractable;
        }

        private void HideSliderPanel()
        {
            _buttonsPanel.SetActive(true);
            _rotatePanel.SetActive(false);
            _scalePanel.SetActive(false);
            _obstaclePanel.SetActive(false);
            _railApplydButton.gameObject.SetActive(false);
            _buildRailInProgress = false;
            _railApplydButton.interactable = false;
        }

        private void OnSlideChange(float value)
        {
            Debug.Log("OnSlideChange: " + value);
        }

        public void OnRotateClick()
        {
            _buttonsPanel.SetActive(false);
            _rotatePanel.SetActive(true);
            _scalePanel.SetActive(false);
            _obstaclePanel.SetActive(false);
            _rotateSlider.Show(_targetObstacle, HideSliderPanel);
        }

        public void OnDeleteClick()
        {            
            _onDeleteAction?.Invoke(_targetObstacle);
            _targetObstacle.Destroy();
            _targetObstacle = null;
            Show(false);
        }

        private void Show(bool show)
        {
            gameObject.SetActive(show);
        }

        public void OnClose()
        {
            _railApplydButton.gameObject.SetActive(false);
            Show(false);
        }
    }
}