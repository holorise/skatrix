﻿using UnityEngine;
using System.Collections;

namespace SKTRX.Statics
{
    public static class TagManager
    {
        public const string PLAYER_TAG = "Player";
        public const string PLANE_TAG = "Plane";
        public const string PLANE_DRAG_TAG = "PlaneDrag";
        public const string OBSTACLE_TAG = "Obstacle";
        public const string DRAG_PANEL_TAG = "DragPanel";
        public const string BOUNDARY_TAG = "Boundary";
    }
}