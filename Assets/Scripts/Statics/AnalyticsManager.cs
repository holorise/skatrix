﻿using System.Collections.Generic;
using KHD;
using Tools;
using UnityEngine;

namespace SKTRX.Statics
{
    public class AnalyticsManager : Singelton<AnalyticsManager>
    {
        [SerializeField] private string _IOSid;
        [SerializeField] private string _androidId;
        [SerializeField] private bool _canSendErrors = true;

        private FlurryAnalytics _analytics;

        private void Start()
        {
            _analytics = FlurryAnalytics.Instance;
            Init();
        }
       
        public void Init()
        {
            _analytics.StartSession(_IOSid, _androidId, _canSendErrors);
        }

        public void SendCustomEvent(string eventName)
        {
            _analytics.LogEvent(eventName);
        }

        public void SendCustomEventWithParameters(string eventName, Dictionary<string,string> parameters)
        {
            _analytics.LogEventWithParameters(eventName, parameters);
        }

        public void EndTimedEvent(string eventName, Dictionary<string, string> parameters = null)
        {
            _analytics.EndTimedEvent(eventName, parameters); 
        }
    }
}
