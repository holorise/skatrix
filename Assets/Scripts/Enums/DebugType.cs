﻿namespace SKTRX.Enums
{
    public enum DebugType
    {
        General = 0,
        Path = 1,
        AR = 2,
        Movement = 3 
    } 
}
