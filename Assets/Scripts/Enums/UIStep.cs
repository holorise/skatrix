﻿namespace SKTRX.Enums
{
    public enum GameScreenStep
    {
        TrackPlace,
        TapToInstantiatePlayer,
        Control
    }
}
