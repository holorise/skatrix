﻿namespace SKTRX.DTOModel
{
    public enum PathPointJumpType
    {
        Start,
        Point,
        Highest,
        End,
        Fail
    }
}