﻿

public enum MovementType
{
    Stop,
    MoveFroward,
    MoveByPath,
    SpeedUp,
    SpeedDown,
    TurnRight,
    TurnLeft,
    Jump
}