﻿
namespace SKTRX.Enums
{
    public enum ObstacleType
    {
        Ramp,
        Ramp_v_1,
        Bulge_out,
        Bowl_v_2,
        HipWedgeRound,
        HipWedgeSquare,
        Bulge_in,
        Bowl_v_3,
        BoxRail_v1,
        Hip_v_1,
        WedgeRamp_Rail,
        Hip_Wedge,
        Hal_pipe_and_vert,
        Bowl_45degree,
        Box,
        Rail_Horizontal,
        Ramp_v_2
    }
}