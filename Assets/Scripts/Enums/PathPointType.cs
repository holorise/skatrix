﻿namespace SKTRX.Enums
{
    public enum PathPointType
    {
        Plane,
        Jump,
        Fall,
        Obstacle,
        Rail,
    }
}