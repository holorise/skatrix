﻿namespace SKTRX.Enums
{
    public enum ControlType
    {
        None,
        Control_v1,
        Control_v2,
        Control_v3,
        Control_v4,
        Control_v5,
        Control_v6,
        Control_v7
    }
}