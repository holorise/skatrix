﻿using UnityEngine;
using System.Collections;

namespace SKTRX.Enums
{
    public enum InputType
    {
        None = 0,
        Jump,
        Left,
        Right,
        Hand,
        Rail,
        StartMove,
        MoveWithSpeed,
        SpeedUp,
        SpeedDown,
        Idle,
        Foot,
        Trick,
        SetDirection,
        SetOppositDirection,
        ChangeDirection,
        MoveTowards,
        Stop,
        Forward
    }
}