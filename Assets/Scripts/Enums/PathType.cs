﻿
namespace SKTRX.Enums
{
    public enum PathType
    {
        Forward,
        Jump,
        Turn,
    }
}