﻿namespace SKTRX.Enums
{
    public enum Direction 
    {
        none = -1,
        up = 0,
        left = 1,
        right = 2,
        down = 3,
        rightUp = 4,
        rightDown = 5,
        leftUp = 6,
        leftDown = 7,
    }
}