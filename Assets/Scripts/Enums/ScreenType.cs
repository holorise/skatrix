﻿using UnityEngine;
using System.Collections;

namespace SKTRX.Enums
{
    public enum ScreenType
    {
        None,
        Loading,
        ControlVariantSelect,
        GameScreen,
        PlaneDetect,
        ObjectPlace,
        RealObjectDetection
    }
}