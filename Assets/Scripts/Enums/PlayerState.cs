﻿
namespace SKTRX.Enums
{
    public enum PlayerState
    {
        Standing,
        Moving,
        Stopping,
    }
}