﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.DTOModel;
using SKTRX.Enums;
using DG.Tweening;

namespace SKTRX.PlayerBehaviour
{
    public class PlayerController : PlayerControllerBase
    {
        [SerializeField] private float _targetCastHeight;
        [SerializeField] private float _gravity;
        [SerializeField] private float _acceleration;
        [SerializeField] private float _speedUpValue;

        [Space]
        [SerializeField] private float _maxVelocity = 20f;
        [SerializeField] private float _minVelocity = 0.003f;
        [SerializeField] private float _turningMagnitude = 100;
        [SerializeField] private float _groudDampRate = 0.01f;
        [SerializeField] private float _groudedError = 0.01f;

        [Space]
        [SerializeField] private float _jumpSpeed;

        [Space]
        [SerializeField] private Transform _frontBridge;
        [SerializeField] private Transform _rareBridge;
        [SerializeField] private Transform _defaultBridge;
        [SerializeField] private Transform _board;

        [Space]
        [SerializeField] private float _frontLeftRayLength = 0;
        [SerializeField] private float _frontRightRayLength = 0;
        [SerializeField] private float _rareLeftRayLength = 0;
        [SerializeField] private float _rareRightRayLength = 0;

        [Space]
        [SerializeField] private Transform _frontLeftOrigin;
        [SerializeField] private Transform _frontRightOrigin;
        [SerializeField] private Transform _rareRightOrigin;
        [SerializeField] private Transform _rareLeftOrigin;

        [Space]
        [SerializeField] private GameObject _directionGameObject;

        private bool _isGrounded = false;
        private bool _isMoving = false;

        private float _rareRayLength = 0;
        private float _frontRayLength = 0;
        private float _velocity = 0;

        private float _verticalVelocity;
        private float _compessedLength;

        private Vector3 targetPoint = Vector3.zero;

        private Transform _rotator;
        private Transform _compressedSpring;


        #region Pivot Control
        public void SetRarePivot()
        {
            if (_rotator.Equals(_rareBridge)) return;
            ResetPivots();
            _board.SetParent(_rareBridge);
            _defaultBridge.SetParent(_board);
            _frontBridge.SetParent(_board);
            _rotator = _rareBridge;
            _verticalVelocity = 0;
            _compressedSpring = _frontBridge;
        }

        public void SetFrontPivot()
        {
            if (_rotator.Equals(_frontBridge)) return;
            ResetPivots();
            _board.SetParent(_frontBridge);
            _defaultBridge.SetParent(_board);
            _rareBridge.SetParent(_board);
            _rotator = _frontBridge;
            _verticalVelocity = 0;
            _compressedSpring = _rareBridge;
        }

        public void SetDefaultPivot()
        {
            if (_rotator.Equals(_defaultBridge)) return;
            ResetPivots();
            _board.SetParent(_defaultBridge);
            _rareBridge.SetParent(_board);
            _frontBridge.SetParent(_board);
            _rotator = _defaultBridge;
            _compressedSpring = _defaultBridge;
            _verticalVelocity = 0;
        }

        private void ResetPivots()
        {
            _board.SetParent(transform);
            _defaultBridge.SetParent(transform);
            _rareBridge.SetParent(transform);
            _frontBridge.SetParent(transform);
        }

        #endregion
        #region Physics
        private void CastRays()
        {
            RaycastHit rayHit = new RaycastHit();

            _frontLeftRayLength = _targetCastHeight;
            _frontRightRayLength = _targetCastHeight;
            _rareLeftRayLength = _targetCastHeight;
            _rareRightRayLength = _targetCastHeight;

            if (Physics.Raycast(_frontLeftOrigin.position, _frontLeftOrigin.up * -1, out rayHit, _targetCastHeight))
            {
                _frontLeftRayLength = rayHit.distance;
            }

            if (Physics.Raycast(_frontRightOrigin.position, _frontRightOrigin.up * -1, out rayHit, _targetCastHeight))
            {
                _frontRightRayLength = rayHit.distance;
            }

            if (Physics.Raycast(_rareLeftOrigin.position, _rareLeftOrigin.up * -1, out rayHit, _targetCastHeight))
            {
                _rareLeftRayLength = rayHit.distance;
            }

            if (Physics.Raycast(_rareRightOrigin.position, _rareRightOrigin.up * -1, out rayHit, _targetCastHeight))
            {
                _rareRightRayLength = rayHit.distance;
            }

            _frontRayLength = Mathf.Min(_frontLeftRayLength, _frontRightRayLength);
            _rareRayLength = Mathf.Min(_rareLeftRayLength, _rareRightRayLength);

            _compessedLength = Mathf.Min(_frontRayLength, _rareRayLength);

            _isGrounded = Mathf.Abs(_targetCastHeight - _compessedLength) > 0 &&
                    Mathf.Abs(_targetCastHeight - _compessedLength) < _groudedError;
            DebugRays();
        }

        private void DebugRays()
        {
            Debug.DrawRay(_frontLeftOrigin.position, _frontLeftOrigin.up * -_frontLeftRayLength, Color.red);
            Debug.DrawRay(_frontRightOrigin.position, _frontRightOrigin.up * -_frontRightRayLength, Color.red);
            Debug.DrawRay(_rareLeftOrigin.position, _rareLeftOrigin.up * -_rareLeftRayLength, Color.red);
            Debug.DrawRay(_rareRightOrigin.position, _rareRightOrigin.up * -_rareRightRayLength, Color.red);
        }

        private void UpdateVelocity()
        {

            _rotator.Translate(Vector3.forward * _velocity * Time.deltaTime + Vector3.down * _verticalVelocity);
            if (!_isGrounded)
            {
                _verticalVelocity += _gravity * Time.deltaTime;
            }
            else
            {
                if (_verticalVelocity > 0)
                {
                    _verticalVelocity = 0;
                }
            }
        }

        private void UpdatePivot()
        {
            if (_frontRayLength < _targetCastHeight)
            {
                if (_rareRayLength >= _targetCastHeight)
                {
                    SetRarePivot();
                    return;
                }
            }
            else if (_rareRayLength < _targetCastHeight)
            {
                if (_frontRayLength >= _rareRayLength)
                {
                    SetFrontPivot();
                    return;
                }
            }

            else if (_frontRayLength > _rareRayLength)
            {
                SetFrontPivot();
                return;
            }

            else if (_rareRayLength > _frontRayLength)
            {
                SetRarePivot();
                return;
            }

            SetDefaultPivot();
        }

        private void UpdateOrientation()
        {
            if (_rotator.Equals(_defaultBridge))
            {
                if (!_isGrounded && _compessedLength < _targetCastHeight)
                {
                    _rotator.position += (_targetCastHeight - _compessedLength) * Vector3.up * (1 - _groudDampRate);
                    if (_verticalVelocity > 0)
                    {
                        _verticalVelocity = 0;
                    }
                    UpdatePivot();
                }
            }
            else
            {
                targetPoint = _compressedSpring.position + (_targetCastHeight - _compessedLength) * Vector3.up;
                Vector3 targetPitch = targetPoint - _rotator.position;
                SetFroward(targetPitch);
            }
        }

        protected void SetFroward(Vector3 target)
        {
            if (_rotator.Equals(_rareBridge))
            {
                _rotator.forward = -target;
            }
            else
            {
                _rotator.forward = target;
            }
        }

        #endregion

        private Vector3[] SetPathY(Vector3[] path)
        {
            Vector3[] res = new Vector3[path.Length];
            for (int i = 0; i < res.Length; i++)
            {
                res[i] = new Vector3(path[i].x, _rotator.position.y + 0.003f, path[i].z);
            }
            return res;
        }

        //private IEnumerator JumpCoroutine()
        //{
        //    float jumpImpulse = _jumpSpeed;
        //    while (jumpImpulse>0)
        //    { 
        //        _verticalVelocity -= 
        //    }
        //}

        private IEnumerator StopByTime()
        {
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
            _velocity = Mathf.Clamp(_velocity - -Time.deltaTime, _minVelocity, _maxVelocity);

            while (_velocity > _minVelocity)
            {
                yield return new WaitForEndOfFrame();
                _velocity = Mathf.Clamp(_velocity - Time.deltaTime, _minVelocity, _maxVelocity);
            }
            StopMove();
        }

        protected void StopMove()
        {
            _currentController.StartControl();
            _currentController.ChangePlayerState(PlayerState.Stopping);
        }

        #region Monobehaviour
        private void Awake()
        {
            _frontLeftRayLength = _targetCastHeight;
            _frontRightRayLength = _targetCastHeight;
            _rareLeftRayLength = _targetCastHeight;
            _rareRightRayLength = _targetCastHeight;
            _rotator = _defaultBridge;
        }

        private void Update()
        {
            CastRays();
            UpdatePivot();
            UpdateOrientation();
            UpdateVelocity();

        }

        //protected override void ChangeDirection(ControlDTO data)
        //{
        //    if (_isGrounded || !_isMoving)
        //    {
        //        _rotator.eulerAngles += data.direction.x * Vector3.up;
        //        _directionGameObject.SetActive(true);
        //    }
        //}

        protected override void Jump()
        {
            if (_isGrounded)
            {
                _isGrounded = false;
                Debug.LogError("Jump");
                _directionGameObject.SetActive(false);
                _verticalVelocity = -_jumpSpeed;
                Debug.LogError("_verticalVelocity " + _verticalVelocity);
            }
        }

        public override void StartMove()
        {
            Debug.LogError("Star move");
            _velocity = Mathf.Clamp(_velocity + _acceleration, _minVelocity, _maxVelocity);
            _isMoving = true;
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void TurnRight(ControlDTO data)
        {
            if (_isGrounded)
            {
                _rotator.Rotate(Vector3.up * Time.deltaTime * _turningMagnitude);
            }
        }

        protected override void TurnLeft(ControlDTO data)
        {
            if (_isGrounded)
            {
                _rotator.Rotate(Vector3.down * Time.deltaTime * _turningMagnitude);
            }
        }

        protected override void Turn(ControlDTO data)
        {
            throw new System.NotImplementedException();
        }

        protected override void MoveTowards(ControlDTO data)
        {
            Debug.LogError("Star MoveTovards" + data.pointToMove.Length);

            Vector3 targetForwardPoint = new Vector3(data.pointToMove[0].x, _rotator.position.y, data.pointToMove[0].z);

            SetFroward(targetForwardPoint - _rotator.position);


            _rotator.DOPath(SetPathY(data.pointToMove), _acceleration * 2, DG.Tweening.PathType.CatmullRom, PathMode.Sidescroller2D)
                .SetSpeedBased()
                .SetEase(Ease.InOutCubic)
                .OnComplete(delegate
                {
                    StopMove();
                });

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void MoveWithSpeed(ControlDTO data)
        {
            _velocity = Mathf.Clamp(_velocity + _acceleration, _minVelocity, _maxVelocity);
            _isMoving = true;
            _velocity += _acceleration;// * onControlDTOAction.velocity;
            Debug.LogError("Star Move With Speed" + _velocity);
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void SetDirection(ControlDTO data)
        {
            if (_isGrounded || !_isMoving)
            {
                float angle = Mathf.Atan2(data.direction.x, data.direction.y) * Mathf.Rad2Deg;
                Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
                _rotator.rotation = rot;
                _directionGameObject.SetActive(true);
            }
        }

        protected override void SpeedDown()
        {
            Debug.LogError("SpeedDown");
            _velocity = Mathf.Clamp(_velocity - _speedUpValue, _minVelocity, _maxVelocity);

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void SpeedUp()
        {
            Debug.LogError("SpeedUp");
            _velocity = Mathf.Clamp(_velocity + _speedUpValue, _minVelocity, _maxVelocity);

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);

            /*  if (onControlDTOAction.speedNormalized.HasValue)
                _velocity += onControlDTOAction.speedNormalized.Value / 10f;
            else*/
            _velocity = Mathf.Clamp(_velocity, Mathf.Epsilon, _maxVelocity);
        }

        protected override void StopAction()
        {
            Debug.LogError("SpeedDown");
            StartCoroutine(StopByTime());
        }

        protected override void UseFoot()
        {
            throw new System.NotImplementedException();
        }

        protected override void UseHand()
        {
            throw new System.NotImplementedException();
        }

        protected override void UseRail()
        {
            throw new System.NotImplementedException();
        }

        protected override void PerformTrick(ControlDTO data)
        {
            throw new System.NotImplementedException();
        }

        protected override void SetOppositeDirection(ControlDTO data)
        {
            throw new System.NotImplementedException();
        }

        protected override void Forward()
        {
            throw new System.NotImplementedException();
        }

        protected void ResetBoard()
        {
            throw new System.NotImplementedException();
        }

        public override void ResetAll()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}