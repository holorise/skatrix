﻿

#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using SKTRX.DTOModel;
using SKTRX.Enums;
using UnityEngine;

namespace SKTRX.PlayerBehaviour
{
    public abstract class PlayerControllerBase : MonoBehaviour
    {
        protected IControler _currentController;
        protected const float _scalePoint = 0.01f;
        protected const float _scaleMin = 0.045f;
        protected const float _scaleMax = .3f;

        #region Control

        public virtual void Destroy()
        {
            _currentController = null;
            Destroy(gameObject);
        }

        public virtual void Stop()
        {
            StopAction();
        }

        public virtual void SetControler(IControler controler)
        {
            _currentController = controler;
            controler.Subscribe(Subscribe);
            controler.StartControl();
        }

        protected virtual void Subscribe(ControlDTO onControlDTOAction)
        {
            switch (onControlDTOAction.inputType)
            {
                case InputType.Foot:
                    UseFoot();
                    break;

                case InputType.Hand:
                    UseHand();
                    break;

                case InputType.Jump:
                    Jump();
                    break;

                case InputType.Left:
                    TurnLeft(onControlDTOAction);
                    break;

                case InputType.StartMove:
                    StartMove();
                    break;

                case InputType.MoveTowards:
                    MoveTowards(onControlDTOAction);
                    break;

                case InputType.MoveWithSpeed:
                    MoveWithSpeed(onControlDTOAction);
                    break;

                case InputType.None:
                    break;

                case InputType.Rail:
                    UseRail();
                    break;

                case InputType.Right:
                    TurnRight(onControlDTOAction);
                    break;

                case InputType.SetDirection:
                    SetDirection(onControlDTOAction);
                    break;

                case InputType.SetOppositDirection:
                    SetOppositeDirection(onControlDTOAction);
                    break;

                case InputType.SpeedDown:
                    SpeedDown();
                    break;

                case InputType.SpeedUp:
                    SpeedUp();
                    break;

                case InputType.Stop:
                    StopAction();
                    break;

                case InputType.Trick:
                    PerformTrick(onControlDTOAction);
                    break;

                case InputType.Forward:
                    Forward();
                    break;
            }
        }

        #endregion

        public virtual void ScaleUp()
        {
            transform.localScale = GetScaleVector(GetCurrentScaleValue() + _scalePoint);
        }

        public virtual void ScaleDown()
        {
            transform.localScale = GetScaleVector(GetCurrentScaleValue() - _scalePoint);
        }

        public Vector3 GetCurrentScale()
        {
            return transform.localScale;
        }

        private float GetCurrentScaleValue()
        {
            return transform.localScale.x;
        }

        private Vector3 GetScaleVector(float newScale)
        {
            return Vector3.one * Mathf.Clamp(newScale, _scaleMin, _scaleMax);
        }

        #region IControllerActions
        public abstract void StartMove();

        protected abstract void Jump();
        protected abstract void TurnRight(ControlDTO data);
        protected abstract void TurnLeft(ControlDTO data);
        protected abstract void Turn(ControlDTO data);
        protected abstract void MoveTowards(ControlDTO data);
        protected abstract void MoveWithSpeed(ControlDTO data);
        protected abstract void SetDirection(ControlDTO data);
        protected abstract void SetOppositeDirection(ControlDTO data);
        protected abstract void SpeedDown();
        protected abstract void SpeedUp();
        protected abstract void StopAction();
        protected abstract void UseFoot();
        protected abstract void UseHand();
        protected abstract void UseRail();
        protected abstract void Forward();
        public abstract void ResetAll();
        protected abstract void PerformTrick(ControlDTO data);
        #endregion
    }
}