﻿using System.Collections;
using System.Collections.Generic;
using SKTRX.DTOModel;
using SKTRX.Enums;
using UnityEngine;
using DG.Tweening;

namespace SKTRX.PlayerBehaviour
{
    public class WheeledBehaviour : PlayerControllerBase
    {
        [SerializeField] private Rigidbody _rigidBody;

        [SerializeField] private WheelCollider frontDriverW;
        [SerializeField] private WheelCollider frontPassengerW;
        [SerializeField] private WheelCollider rearDriverW;
        [SerializeField] private WheelCollider rearPassengerW;

        [SerializeField] private float motorForce = 50;
        [SerializeField] private float maxSteerAngle = 30;
        [SerializeField] private float _steeringForce = 1;
        [SerializeField] private float _jumpForce = 30;
        [SerializeField] private float _tweenSpeed = .1f;
        [SerializeField] private float _acceleration = 1f;
        [SerializeField] private float _minAceeleration = 0.01f;
        [SerializeField] private float _maxAceeleration = 20;
        [SerializeField] private float _steeringDamping = .3f;

        [SerializeField] private GameObject _directionGameObject;

        [SerializeField] private float _speedUpValue = .5f;

        private float _minTurnTime = 0.2f;
        private float _maxTurnTime = 0.5f;
        private float _minTurnAngle = 5;
         private float _maxTurnAngle = 60;

        private Coroutine _turningCoroutine;

        private float _steeringInput = 0;
        private float _accelerationInput = 0;
        private float steeringAngle = 0;
        //public Transform frontDriverT, frontPassengerT;
        //public Transform rearDriverT, rearPassengerT;

        private Tweener _pathTween = null;

        private void OnEnable()
        {
            StartCoroutine(StabiliseSteering());
        }

        public void GetInput()
        {
            _steeringInput = Input.GetAxis("Horizontal");
            _accelerationInput = Input.GetAxis("Vertical");
        }

        private void SteerUpdate()
        {
            steeringAngle = maxSteerAngle * _steeringInput;
            frontDriverW.steerAngle = steeringAngle;
            frontPassengerW.steerAngle = steeringAngle;
            //  _steeringInput = 0;
        }

        private void AccelerateUpdate()
        {
            frontDriverW.motorTorque = _accelerationInput * motorForce;
            frontPassengerW.motorTorque = _accelerationInput * motorForce;
            //  _accelerationInput = 0;
        }

        //private void UpdateWheelPoses()
        //{
        //    UpdateWheelPose(frontDriverW, frontDriverT);
        //    UpdateWheelPose(frontPassengerW, frontPassengerT);
        //    UpdateWheelPose(rearDriverW, rearDriverT);
        //    UpdateWheelPose(rearPassengerW, rearPassengerT);
        //}

        private void UpdateWheelPose(WheelCollider _collider, Transform _transform)
        {
            Vector3 _pos = _transform.position;
            Quaternion _quat = _transform.rotation;

            _collider.GetWorldPose(out _pos, out _quat);

            _transform.position = _pos;
            _transform.rotation = _quat;
        }

        private void FixedUpdate()
        {
            // GetInput();
            SteerUpdate();
            AccelerateUpdate();
            //UpdateWheelPoses();
        }

        //private IEnumerator AccelerationCoroutine()
        //{
        //    _accelerationInput = 1;
        //    float timer = 3;
        //    while(timer >0)
        //    {
        //        timer -= Time.deltaTime;
        //        yield return null;
        //    }
        //    _accelerationInput = 0;
        //}

        private IEnumerator StabiliseSteering()
        {
            while (true)
            {
                _steeringInput = Mathf.Lerp(_steeringInput, 0, _steeringDamping);
                yield return null;
            }
        }

        private IEnumerator TurnCoroutine(float value)
        {
            float turnTime = Remap(Mathf.Abs(value),_minTurnAngle, _maxTurnAngle, _minTurnTime, _maxTurnTime);

            Debug.LogError(Mathf.Abs(value)+ " "+ _minTurnAngle + " " + _maxTurnAngle + " " + _minTurnTime + " " + _maxTurnTime);
            Debug.LogError("turntime   " + turnTime);
            while (turnTime > 0)
            {
                turnTime -= Time.deltaTime;
                _steeringInput = _steeringForce * Mathf.Sign(value);
                yield return null;
            }
            _steeringInput = 0;
        }

        public float Remap(float value, float from1, float to1, float from2, float to2)
        {
            if(value > to1)
            {
                return to2;
            }
            if (value <= from1)
            {
                return from2;
            }
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        private IEnumerator StopByTime()
        {
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);

            while (_accelerationInput > _minAceeleration)
            {
                yield return new WaitForFixedUpdate();
                _accelerationInput = _accelerationInput - Time.deltaTime * _accelerationInput;
            }
        }

        private Vector3[] SetPathY(Vector3[] path)
        {
            Vector3[] res = new Vector3[path.Length];
            for (int i = 0; i < res.Length; i++)
            {
                res[i] = new Vector3(path[i].x, transform.position.y, path[i].z);
            }

            return res;
        }

        #region PlayerController

        protected override void Jump()
        {
            if (!frontDriverW.isGrounded && !frontPassengerW.isGrounded && !rearDriverW.isGrounded && !rearPassengerW.isGrounded)
            {
                return;
            }
            _rigidBody.AddForce(Vector3.up * _jumpForce);
        }

        public override void StartMove()
        {
            Debug.LogError("StartMove");
            _accelerationInput = _accelerationInput + _acceleration;
            _directionGameObject.SetActive(false);
        }

        protected override void TurnRight(ControlDTO data)
        {
            Debug.LogError("TurnRight");

            //if (data.angle.HasValue)
            //{
            //    StartTurning((float)data.angle);
            //}
            //else
            //{
            //    _steeringInput = _steeringForce;
            //}
        }

        private void StartTurning(float angle)
        {
            if (_turningCoroutine != null)
            {
                StopCoroutine(_turningCoroutine);
            }
            _turningCoroutine = StartCoroutine(TurnCoroutine(angle));
        }

        protected override void TurnLeft(ControlDTO data)
        {
            Debug.LogError("TurnLeft");

            //if (data.angle.HasValue)
            //{
            //    StartTurning(-(float)data.angle);
            //}
            //else
            //{
            //    _steeringInput = -_steeringForce;
            //}
        }

        protected override void MoveTowards(ControlDTO data)
        {
            _pathTween.Kill();
            Debug.LogError("Star MoveTovards" + data.pointToMove.Length);
            Vector3 prevPos = transform.position;
            Vector3[] path = SetPathY(data.pointToMove);
            transform.LookAt(prevPos);
            _pathTween = transform.DOPath(path, _tweenSpeed, DG.Tweening.PathType.CatmullRom, PathMode.Sidescroller2D)
                .SetSpeedBased()
                .OnUpdate(delegate
                {
                    transform.LookAt(prevPos);
                    prevPos = transform.position;
                })

                .OnComplete(delegate
                {
                    StopAction();
                });

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void MoveWithSpeed(ControlDTO data)
        {
            Debug.LogError("MoveWithSpeed");
            StartMove();
        }

        protected override void SetDirection(ControlDTO data)
        {
            Debug.LogError("SetDirection");
            //_accelerationInput = 0;
            float angle = Mathf.Atan2(data.direction.x, data.direction.y) * Mathf.Rad2Deg;
            Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
            transform.rotation = rot;
            _directionGameObject.SetActive(true);
        }

        protected override void SetOppositeDirection(ControlDTO data)
        {
            Debug.LogError("SetDirection");
            transform.forward = data.direction3;
            _directionGameObject.SetActive(true);
            _directionGameObject.SetActive(true);
        }

        protected override void SpeedDown()
        {
            Debug.LogError("SpeedDown");
            _accelerationInput = Mathf.Clamp(_accelerationInput - _acceleration, _minAceeleration, _maxAceeleration);
        }

        protected override void SpeedUp()
        {
            Debug.LogError("SpeedUp");
            _accelerationInput = _accelerationInput+_acceleration;
        }

        protected override void StopAction()
        {
            _pathTween.Kill();
            _accelerationInput = 0;
            _currentController.ChangePlayerState(PlayerState.Standing);
            _currentController.StartControl();
        }

        //protected override void StopMove()
        //{
        //    return;
        //}

        protected override void UseFoot()
        {
            //throw new System.NotImplementedException();
        }

        protected override void UseHand()
        {
            //throw new System.NotImplementedException();
        }

        protected override void UseRail()
        {
            //throw new System.NotImplementedException();
        }

        protected override void PerformTrick(ControlDTO data)
        {
            //throw new System.NotImplementedException();
        }

        protected override void Turn(ControlDTO data)
        {
            //throw new System.NotImplementedException();
        }

        protected override void Forward()
        {
            throw new System.NotImplementedException();
        }

        protected void ResetBoard()
        {
            throw new System.NotImplementedException();
        }

        public override void ResetAll()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}