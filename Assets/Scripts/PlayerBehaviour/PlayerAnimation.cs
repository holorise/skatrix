﻿//#define Debug
using System.Collections;
using UnityEngine;
using Tools;
using SKTRX.DTOModel;
using SKTRX.AnimatorUtilities;

namespace SKTRX.PlayerBehaviour
{
    public class PlayerAnimation : MonoBehaviour
    {
        private const string SPEEDANIMATIONFLOAT = "Speed";
        private const string DIRECTIONANIMATIONFLOAT = "Direction";
        private const string INPUTOLLIETRIGGER = "InputOllie";
        private const string LANDEDTRIGGER = "Landed";
        private const string RAILPROXIMITYBOOL = "RailProximity";
        private const string TRICKINDEXINT = "TrickIndex";
        private const string DISMOUNTTRIGGER = "Dismount";
        private const string INPUTFORWARDBOOL = "InputForward";
        private const string INPUTSLOWBOOL = "InputSlow";
        private const string MANUALTRICKTRIGGER = "Manual";
        private const string INPUTCROUCHTRIGGER = "InputCrouch";
        private const string DEFAULTSTATE = "OnBoard_BT";

        [SerializeField] private Animator _playerAnimator;

        private bool _playerStop = false;
        private bool _railStart = false;
        private bool _railing = false;
        private bool _executeLand = false;
        private bool _allowAnimation = true;
        private bool _jumpStart = false;
        private const float _minAngle = -.2f;
        private const float _maxAngle = 0.2f;
        private static int _trickRailIndex = 3;
        private static int _angleForExecuteNoseslide_FS = 20;
        private int _trickOllieIndex = 3;

        public PlayerStateMachineBehaviour GetAnimatorBehaviour()
        {
            return _playerAnimator.GetBehaviour<PlayerStateMachineBehaviour>();
        }
        #region Crouch

        public void Crouch()
        {
            _playerAnimator.SetTrigger(INPUTCROUCHTRIGGER);
        }

        #endregion

        #region Manual

        public void ManualTrick()
        {
            _playerAnimator.SetTrigger(MANUALTRICKTRIGGER);
        }

        #endregion

        #region Speed Up

        public void SpeedUp(PlayerDTO playerDto)
        {
            if (_playerStop && playerDto.velocity > 0)
                StartMove();
            else
            {
                SetSpeed(playerDto);
                if (_allowAnimation)
                {
                    SpeedUp();
                    StartCoroutine(BackToForward());
                }
            }
        }

        private IEnumerator BackToForward()
        {
            _allowAnimation = false;
            yield return new WaitForSeconds(1f);
            MoveForward();
            _allowAnimation = true;
        }

        public void MoveForvard(PlayerDTO playerDto)
        {
            SetDirection(playerDto.steering);
            SetSpeed(playerDto);

            if (_playerStop && playerDto.velocity > 0)
                StartMove();
            else
                MoveForward();
        }

        private void SpeedUp()
        {
            _playerAnimator.SetBool(INPUTFORWARDBOOL, true);
        }

        #endregion

        #region Speed down

        public void SpeedDown(PlayerDTO playerDto)
        {
            if (playerDto.velocity != 0)
            {
                SetSpeed(playerDto);

                if (_allowAnimation)
                {
                    SpeedDown();
                    StartCoroutine(BackToForward());
                }
            }
        }

        private void SpeedDown()
        {
            _playerAnimator.SetBool(INPUTSLOWBOOL, true);
        }

        #endregion

        #region Move Forward

        private void MoveForward()
        {
            _playerAnimator.SetBool(INPUTSLOWBOOL, false);
            _playerAnimator.SetBool(INPUTFORWARDBOOL, false);
        }

        #endregion

        #region StopMove

        private void StopMove()
        {
            _playerAnimator.SetFloat(SPEEDANIMATIONFLOAT, 0);
            _playerAnimator.SetTrigger(DISMOUNTTRIGGER);
            _playerStop = true;
        }

        #endregion

        #region StartMove

        private void StartMove()
        {
            _playerAnimator.SetFloat(SPEEDANIMATIONFLOAT, 0.1f);
            _playerStop = false;
            SpeedUp();
            StartCoroutine(BackToForward());
        }

        #endregion

        #region Jump

        public void SetupJumpAnimationParam(PlayerDTO playerDto, int trickIndex)
        {
            SetDirection(playerDto.steering);
            SetSpeed(playerDto);
            _playerAnimator.SetBool(RAILPROXIMITYBOOL, false);
            _playerAnimator.SetInteger(TRICKINDEXINT, trickIndex);
        }

        public void JumStart()
        {
#if Debug
            Debug.LogWarningFormat("JumStart - 1 {0}", _jumpStart);
#endif
            if (!_jumpStart)
            {
                _playerAnimator.SetTrigger(INPUTOLLIETRIGGER);
                _jumpStart = true;
            }
        }

        public void JumpEnd(PlayerDTO playerDto)
        {
#if Debug
            Debug.LogWarningFormat("JumpEnd - 2 {0}", _jumpStart);
#endif
            if (_jumpStart)
            {
                SetDirection(playerDto.steering);
                SetSpeed(playerDto);
                _playerAnimator.SetBool(RAILPROXIMITYBOOL, false);
                _playerAnimator.SetTrigger(LANDEDTRIGGER);
                _jumpStart = false;
            }
        }

        #endregion

        #region Rail part

        public void SetRailTrick(int trickIndex)
        {
            _trickRailIndex = trickIndex;
        }

        public static void UpdateAngle(float angle)
        {
            if (_trickRailIndex == 4 && angle < _angleForExecuteNoseslide_FS)
            {
                _trickRailIndex = 3;
            }
        }

        public void JumpOnRailStart()
        {
#if Debug
            Debug.LogFormat("JumpOnRailStart - 1 {0}", _railStart);
#endif
            if (!_railStart)
            {
                _railStart = true;
                _playerAnimator.SetBool(RAILPROXIMITYBOOL, true);
                _playerAnimator.SetInteger(TRICKINDEXINT, GetRailIndex());
                _playerAnimator.SetTrigger(INPUTOLLIETRIGGER);
            }
        }

        public void JumpOnRailEndLand()
        {
#if Debug
            Debug.LogFormat("JumpOnRailEndLand - 1 {0}", _railStart);
#endif
            if (_railStart)
            {
                _playerAnimator.SetTrigger(LANDEDTRIGGER);
                _railStart = false;
                _railing = true;
            }
        }

        public void JumpFromRailOnPlane()
        {
#if Debug
            Debug.LogFormat("JumpFromRailOnPlane - 3 {0}", _railing);
#endif
            if (_railing)
            {
                _railStart = false;
                _railing = false;
                _playerAnimator.SetBool(RAILPROXIMITYBOOL, false);
                _executeLand = true;
            }
        }


        public void JumpFromRailOnPlaneLand()
        {

#if Debug
            Debug.LogFormat("JumpFromRailOnPlaneLand - 4 {0}", _executeLand);
#endif
            if (_executeLand)
            {
                _executeLand = false;
                _playerAnimator.SetTrigger(LANDEDTRIGGER);
            }
        }

        private int GetRailIndex()
        {
            return _trickRailIndex;
        }

        #endregion

        #region Direction Left/Right
        public void UpdateTurn(PlayerDTO playerDto)
        {
            SetDirection(playerDto.steering);
            SetSpeed(playerDto);
        }

        private void SetDirection(float angle)
        {
            _playerAnimator.SetFloat(DIRECTIONANIMATIONFLOAT, GetNormalizedAngle(angle));
        }

        private float GetNormalizedAngle(float angle)
        {
            return Utils.Remap(angle, _minAngle, _maxAngle, -1, 1);
        }

        #endregion

        #region Speed animation

        /// <summary>
        /// Set speed value
        /// </summary>
        /// <param name="currentSpeed"></param>
        private void SetSpeed(PlayerDTO player)
        {
            _playerAnimator.SetFloat(SPEEDANIMATIONFLOAT, GetSpeed(player));
        }

        private float GetSpeed(PlayerDTO player)
        {
            return Utils.Remap(player.velocity, 0, player.maxVelocity, 0, 1);
        }

        #endregion

        #region Reset animation

        public void ResetAll()
        {
#if Debug
            Debug.Log("Reset All");
#endif
            _playerAnimator.ResetTrigger(INPUTOLLIETRIGGER);
            _playerAnimator.ResetTrigger(LANDEDTRIGGER);
            _playerAnimator.ResetTrigger(DISMOUNTTRIGGER);
            _playerAnimator.ResetTrigger(MANUALTRICKTRIGGER);
            _playerAnimator.ResetTrigger(INPUTCROUCHTRIGGER);
            _playerAnimator.SetBool(RAILPROXIMITYBOOL, false);
            _playerAnimator.SetFloat(DIRECTIONANIMATIONFLOAT, 0);
            _playerAnimator.SetFloat(SPEEDANIMATIONFLOAT, 0);
            _playerAnimator.Play(DEFAULTSTATE, -1, 0f);
            _jumpStart = false;
            _railStart = false;
            _railing = false;
            _executeLand = false;
            _allowAnimation = true;
        }

        #endregion
    }
}