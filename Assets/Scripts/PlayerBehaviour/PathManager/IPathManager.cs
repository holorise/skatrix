﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.DTOModel;
using SKTRX.Enums;

namespace SKTRX.PlayerBehaviour.Path
{
    /// <summary>
    /// Calculates the path for a specific Player.
    /// </summary>
    public interface IPathManager
    {
        void BuildPath(PlayerDTO data, PathType pathType);
    }
}