﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tools;
using SKTRX.Spline;

namespace SKTRX.DTOModel
{
    public struct PathDTO
    {
        public CatmullRom path;

        public PathDTO(CatmullRom spline)
        {
            path = spline;
        }
    }
}