﻿
#pragma warning disable 649
//#define Debug
using SKTRX.DTOModel;
using SKTRX.Enums;
using SKTRX.Obstacle;
using SKTRX.PlayerBehaviour.Test;
using SKTRX.Statics;
using System;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace SKTRX.PlayerBehaviour.Path
{
    public class PlayerPathManager : MonoBehaviour, IPathManager
    {
        [SerializeField] private float _deltaObstacleBuild = .008f; //0.2
        [SerializeField] private float _jumpForce = 5; //force by Y for jump
        [SerializeField] private float _jumpLegth = 1;
        [Range(0.3f, 10)] [SerializeField] private float _fallLegth = 1;
        [SerializeField] private float _obstacleHeighBuild = 5f;
        [SerializeField] private int _countObstaclePoints = 80;
        [SerializeField] private int _preJumpPointsCount = 9;
        [SerializeField] private float _radiusMin = 0.03f;
        [SerializeField] private float _radiusMax = 0.08f;
        [SerializeField] private int _railCount = 10;

        [SerializeField] private List<PathPointDTO> _pathPointDTO = new List<PathPointDTO>();

        #region default value
        private float _obstacleHight = 0.0001f; //use for send raycast for check obstacle
        private float _heighOfRaycastStart = 10;
        private float _playerScaleKoef = 1;
        private float _interpolationCurveProgress = 0;
        private int _turnCountKoef = 3;
        private float _turnAngle = 0;
        private float _obstacleDistance = 5f; //distance from player to forward for build raycast points
        #endregion

        #region Const values
        private const float GRAVITY = 9.8f;
        private const float ACCURACYCURVEJUMPFALLSTEP = 0.1f; //_accuracyCurveJumpFallStep
        private const int GRAVITYPOINTSOFCURVE = 200;
        private const float ANGLETURNMIN = 25;
        private const float ANGLETURNMAX = 100;
        private const int COUNTOFRAYCASTPOINTS = 20;
        private const float PLAYERSCALEMINIMAL = 0.045f;
        private const float ANGLEFORJUMP = 1.2f;
        private const float STRARTRIGHTANGLE = 1.638f;
        private const float STARTLEFTANGLE = 1.51f;
        #endregion

        #region cached values
        private Vector3 _gravityPosition;
        private Ray _obstacleRay;
        private List<Vector3> _pathRay = new List<Vector3>();
        private List<PathPointDTO> _curvePoints = new List<PathPointDTO>();
        private List<Vector3> _planePoints = new List<Vector3>();
        private List<BaseObstacle> _obstacleRaycastHit = new List<BaseObstacle>();
        private List<Vector3> _obstaclesPositionRayPoint = new List<Vector3>();
        private List<Vector3> _boundaryPositionRayPoint = new List<Vector3>();
        private BaseObstacle[] _obstaclesRayCast;
        private Vector3[] _obstaclesRayPosition;
        private Vector3[] _obstaclePoints;
        private Vector3[] _rotatePoints;
        #endregion

        private bool Turn { get; set; } = false;
        private float JumpLegth { get => Utils.Remap(_playerScaleKoef, 1, 4, _jumpLegth, 30); }
        private int CountObstaclePoints { get => _countObstaclePoints; }
        private int PreJumpPointsCount { get => _preJumpPointsCount; }
        private int RailCount { get => _railCount; }
        private float RadiusMin { get => _radiusMin * _playerScaleKoef; }//+
        private float RadiusMax { get => _radiusMax * _playerScaleKoef; }//+
        private float HeighOfRaycastStart { get => _heighOfRaycastStart * _playerScaleKoef; }//+
        private float JumpForce { get => Utils.Remap(_playerScaleKoef, 1, 4, _jumpForce, 11); }

        private float DeltaObstacleBuild { get => _deltaObstacleBuild * _playerScaleKoef; }
        private float ObstacleHeighBuild { get => _obstacleHeighBuild * _playerScaleKoef; }
        private float FallLegth { get => _fallLegth * _playerScaleKoef; }
        private float ObstacleHight => _obstacleHight * _playerScaleKoef;
        private float ObstacleDistance { get => _obstacleDistance * _playerScaleKoef; }

        public List<PathPointDTO> Path { get => _pathPointDTO; private set => _pathPointDTO = value; }

        public void BuildPath(PlayerDTO data, PathType pathType)
        {
            SetPlayerScaleKoef(data.skateboardSize);
            if (pathType.Equals(PathType.Forward) || pathType.Equals(PathType.Turn))
            {
                this.BuildForwardPath(data, COUNTOFRAYCASTPOINTS);
            }
            else if (pathType.Equals(PathType.Jump))
            {
                this.BuildJumpPath(data);
            }
        }

        public float GetBouncingAngle(Vector3 pathPoint, Vector3 forward)
        {
            var raycastHits = GetAllRayCasts(pathPoint, forward, 1f);
            for (int i = 0; i < raycastHits.Length; i++)
                if (raycastHits[i].collider.tag.Equals(TagManager.OBSTACLE_TAG))
                {
                    var colliderObstacle = raycastHits[i].transform.GetComponent<ColliderObstacle>();
                    if (colliderObstacle != null)
                    {
                        return Utils.GetIntersectionAngle(raycastHits[i].point, forward, raycastHits[i].collider);
                    }
                    else
                    {
                        var realObstacle = raycastHits[i].transform.GetComponent<RealObstacle>();
                        if (realObstacle != null)
                        {
                            return Utils.GetIntersectionAngle(raycastHits[i].point, forward, raycastHits[i].collider);
                        }
                    }
                }
#if Debug
            Debug.LogError("Angle not found");
#endif
            return 0;
        }

        private void SetPlayerScaleKoef(float currentScale)
        {
            _playerScaleKoef = currentScale / PLAYERSCALEMINIMAL;
        }

        private bool BuildForwardPath(PlayerDTO data, int count)
        {
            Path.Clear();
            _pathRay.Clear();
            Turn = data.steering != 0;
            UpdatePlayerPoints(data, count);
            Turn = false;
            _pathRebuild = true;

            List<PathPointDTO> pointsUpdated = new List<PathPointDTO>();
            if (CheckForwardPoints(Path, out pointsUpdated))
            {
                Path = new List<PathPointDTO>(pointsUpdated);
#if Debug
                Debug.LogWarning("CheckForwardPoints true");
                Debug.LogWarning("Path." + Path.Count);
#endif
                return true;
            }
            else
            {
#if Debug
                Debug.LogWarning("Path." + Path.Count);
#endif
            }
            return false;
        }

        private void BuildJumpPath(PlayerDTO data)
        {
            Turn = false;
            Path.Clear();
            _pathRay.Clear();

            List<PathPointDTO> pointsRail = new List<PathPointDTO>();
            List<PathPointDTO> jumpPointsUpdated = new List<PathPointDTO>();

            if (IsRailBelowRaycast(data, out pointsRail))
            {
#if Debug
                Debug.LogWarning("BuildJumpPath true");
#endif
                testRailPoints = new List<PathPointDTO>(pointsRail);
                Path.AddRange(pointsRail);

                Vector3 forwardNew = (pointsRail[pointsRail.Count - 1].Point - pointsRail[pointsRail.Count - 2].Point).normalized;
#if Debug
                Debug.LogWarning("BuildJumpPath true forward cant be 0,0,0" + forwardNew);
#endif
                RaycastDTO raycastDTO = new RaycastDTO(
                  currentPosition: pointsRail[pointsRail.Count - 1].Point,
                   forward: forwardNew,
                   velocity: data.velocity,
                   height: HeighOfRaycastStart,
                   steeringInput: data.steering,
                   delta: RemapDistanceBySpeed(data.velocity),
                   count: COUNTOFRAYCASTPOINTS
                  );

                UpdatePlayerPoints(GenerateRayCastPoints(raycastDTO), GetLastPathPoint(),
                    (pointsRail[pointsRail.Count - 1].Point - pointsRail[pointsRail.Count - 2].Point).normalized, data.steering,
                    data.velocity);

                //if (Path.Count > 0)
                //{
                //    if (Path[0].PathPointType == PathPointType.Obstacle)
                //        Path.Insert(0, new PathPointDTO(data.position, PathPointType.Obstacle));
                //    else
                //        Path.Insert(0, new PathPointDTO(data.position, PathPointType.Plane));
                //}
            }
            else
            {
                data.steering = 0;
                if (BuildForwardPath(data, data.preJumpPoints))
                {
#if Debug
                    Debug.LogError("BuildForwardPath fail true");
#endif
                    _pathRebuild = true;
                    return;
                }
                if (Path.Count > data.preJumpPoints)
                {
#if Debug
                    Debug.LogError("PreJumpPointsCount");
#endif
                    Path.RemoveRange(data.preJumpPoints, Path.Count - data.preJumpPoints);
                }

                Vector3 forward = data.movenentForward;
                forward.y = 0;

                var points = BuildCurveUsingGravity(data.velocity, ANGLEFORJUMP, forward, Path.Count == 0 ? data.position : GetLastPathPoint(), PathPointType.Jump);
                if (Path.Count > 0 && Path[Path.Count - 1].PathPointType.Equals(PathPointType.Plane))
                    points.Insert(0, new PathPointDTO(Path[Path.Count - 1].Point, PathPointType.Jump, PathPointJumpType.Start));

                if (CheckJumpPoints(points, out jumpPointsUpdated))
                {
#if Debug
                    Debug.LogWarning("CheckJumpPoints true");
#endif
                    points = new List<PathPointDTO>(jumpPointsUpdated);
                    Path.AddRange(points);
                    _pathRebuild = true;
                    return;
                }
                else
                {
#if Debug
                    Debug.LogWarning("CheckJumpPoints false");
#endif
                }
                Path.AddRange(points);
                RaycastDTO raycastDTO = new RaycastDTO(
                    currentPosition: Path.Count == 0 ? data.position : GetLastPathPoint() + (data.movenentForward * data.skateboardSize) / 2f,
                    forward: data.movenentForward,
                    velocity: data.velocity,
                    height: HeighOfRaycastStart,
                    steeringInput: data.steering,
                    delta: RemapDistanceBySpeed(data.velocity),
                    count: COUNTOFRAYCASTPOINTS
               );
               
                AddLastPointOnObstacleOrPlane(Path.Count == 0 ? data.position : GetLastPathPoint(), data, PathPointType.Jump);
                UpdatePlayerPoints(GenerateRayCastPoints(raycastDTO), GetLastPathPoint(), data.movenentForward, data.steering, data.velocity, true);

            }
            Turn = false;
            _pathRebuild = true;
        }

#region Build Path

        private void UpdatePlayerPoints(PlayerDTO data, int count)
        {
            UpdatePlayerPoints(GenerateRayCastPoints(data, count), data.position, data.movenentForward, data.steering, data.velocity);
        }

        private void UpdatePlayerPoints(Vector3[] rayCastPoint, Vector3 playerPosition, Vector3 forward, float steering, float velocity, bool startFromPoint = false, bool jump = false)
        {
            for (int i = 0; i < rayCastPoint.Length; i++)
            {
                BaseObstacle obstacleBellowReycast = null;
                Vector3 position = Vector3.zero;

                if (IsObstacleBelowRaycast(rayCastPoint[i], out position, out obstacleBellowReycast) && obstacleBellowReycast.AllowMoveUnder)
                {
                    if (!BuildObstaclePointsPath(obstacleBellowReycast, playerPosition, position, forward, steering, velocity, startFromPoint))
                        return;
                }
                else if (i > 0 && IsObstacleBetweenPoint(playerPosition, rayCastPoint[i], ref position, out obstacleBellowReycast) && obstacleBellowReycast.AllowMoveUnder)
                {
                    if (!BuildObstaclePointsPath(obstacleBellowReycast, playerPosition, position, forward, steering, velocity, startFromPoint))
                        return;
                }
                else
                    UpdatePointsOnPlane(rayCastPoint[i]);
            }
        }
#endregion

#region Build Obstacle Points

        private bool BuildObstaclePointsPath(BaseObstacle obstacle, Vector3 playerPosition, Vector3 startPosition, Vector3 forward, float steering, float velocity, bool startFromPoint = false)
        {
            Vector3 startPositionOfObstacle = startPosition;
            Vector3 startPositionOfPlane = startPosition;
            startPosition.y = playerPosition.y = playerPosition.y + ObstacleHight;
            float distance = Vector3.Distance(playerPosition, startPosition) + ObstacleDistance;
#if Debug
            Debug.Log("BuildObstaclePointsPath");
#endif
            int pathIndex = -1;
            PathPointDTO planePoint = new PathPointDTO();
            PathPointDTO firstObstaclePoint = new PathPointDTO();
            if (!startFromPoint && GetFirstWorldPostion(GetAllRayCasts(playerPosition, (startPosition - playerPosition).normalized, distance), TagManager.OBSTACLE_TAG, ref startPositionOfObstacle))
            {

#if Debug
                Debug.LogWarning("_startObstaclePoinSearch ok");
#endif
                RaycastDTO raycastDTO = new RaycastDTO(
                    currentPosition: startPositionOfObstacle,
                    forward: (startPositionOfObstacle - playerPosition).normalized,
                    velocity: velocity,
                    height: ObstacleHeighBuild,
                    steeringInput: steering,
                    delta: DeltaObstacleBuild,
                    count: CountObstaclePoints
               );
                _obstaclePoints = GenerateRayCastPoints(raycastDTO);
                Vector3 positionObstacle = Vector3.zero;
                bool allowAdd = false;
                RaycastDTO raycastDTO2 = new RaycastDTO(
                      currentPosition: startPositionOfObstacle + (((startPositionOfObstacle - playerPosition).normalized) * 0.002f),
                      forward: (playerPosition - startPositionOfObstacle).normalized,
                      velocity: velocity,
                      height: ObstacleHeighBuild,
                      steeringInput: steering,
                      delta: .001f * _playerScaleKoef,
                      count: 100
                 );
                Vector3[] planePoints = GenerateRayCastPoints(raycastDTO2);
                testfall2 = new List<Vector3>(planePoints);
                for (int i = 0; i < planePoints.Length; i++)
                {
                    Vector3 position = Vector3.zero;

                    if (GetFirstWorldPostion(GetAllRayCasts(planePoints[i], Vector3.down), TagManager.OBSTACLE_TAG, ref positionObstacle))
                    {
                        allowAdd = true;
                    }
                    else if (GetFirstWorldPostion(GetAllRayCasts(planePoints[i], Vector3.down), TagManager.PLANE_TAG, ref position))
                    {
#if Debug
                        Debug.LogError("_startObstaclePoinSearch ok plane found");
#endif
                        pathIndex = Path.Count;
                        planePoint = new PathPointDTO(position, PathPointType.Plane, PathPointJumpType.End);
                        firstObstaclePoint = new PathPointDTO(positionObstacle, PathPointType.Obstacle, PathPointJumpType.Start);
                        if (!Turn)
                        {
                            //  if (allowAdd) Path.Add(firstObstaclePoint);
                            if (Path.Count > 0)
                            {
                                if (Path[0].PathPointType.Equals(PathPointType.Obstacle))
                                {
#if Debug
                                    Debug.LogError("ObstacleFirst");
#endif

                                    Path.Add(planePoint);
                                }
                                else if (Path[0].PathPointType.Equals(PathPointType.Plane))
                                {
#if Debug
                                    Debug.LogError("PlaneFirst");
#endif
                                    Path.Add(planePoint);
                                    if (allowAdd) Path.Add(firstObstaclePoint);
                                }
                            }
                        }
                        break;
                    }
                }
                // return false;
            }
            else
            {
#if Debug
                Debug.LogWarning("_startObstaclePoinSearch not ok start from input point");
                Debug.LogWarning("forward" + forward);
#endif
                startPosition.y = playerPosition.y = playerPosition.y + ObstacleHight;
                RaycastDTO raycastDTO = new RaycastDTO(
                 currentPosition: startPositionOfObstacle,
                 forward: forward,
                 velocity: velocity,
                 height: ObstacleHeighBuild,
                 steeringInput: steering,
                 delta: DeltaObstacleBuild,
                 count: CountObstaclePoints
            );
                _obstaclePoints = GenerateRayCastPoints(raycastDTO);
            }

            int obstaclePoints = 0;
            for (int i = 1; i < _obstaclePoints.Length; i++)
            {
                Vector3 position = Vector3.zero;
                if (GetPointOnObstacle(GetAllRayCasts(_obstaclePoints[i], Vector3.down), ref position))
                {
                    //todo unigue situation where no need add point
                    if (i > 0)
                    {
                        if (Path.Count > 1)
                        {
                            if (Path[Path.Count - 1].PathPointType.Equals(PathPointType.Rail))
                                Path.Add(new PathPointDTO(position, PathPointType.Rail));
                            else
                                Path.Add(new PathPointDTO(position, PathPointType.Obstacle));
                        }
                        else
                            Path.Add(new PathPointDTO(position, PathPointType.Obstacle));
#if Debug
                        Debug.LogWarning("obstacle point on plane insert");
#endif
                    }
                    obstaclePoints++;
                    continue;
                }
                else
                {
                    if (Turn && pathIndex != -1 && obstaclePoints > 3)
                    {
#if Debug
                        Debug.LogWarning("obstacle point on plane insert");
#endif
                        Path.Insert(pathIndex, planePoint);
                    }
#if Debug
                    Debug.LogWarning("obstacle point not found");
#endif
                    if (i < 5)
                    {
                        //unique situation need change interpolation delta valuec
                        continue;
                    }
                    List<PathPointDTO> points = new List<PathPointDTO>();
                    if (Path.Count == 0)
                    {
                        points = BuildCurveUsingGravity(velocity, 0, forward, playerPosition, PathPointType.Fall);
                    }
                    else
                    {
                        if (Path.Count > 2)
                        {
                            if (Turn)
                                forward = GetLastPathForward();
                            else if (Vector3.Distance(Path[Path.Count - 1].Point, Path[Path.Count - 2].Point) > 0.2f)
                            {
                                forward = GetLastPathForward();
                            }
                            else
                            {
                                if (Path.Count > 3)
                                {
                                    forward = (Path[Path.Count - 1].Point - Path[Path.Count - 3].Point).normalized;
                                }
                            }
                            forward.y = 0;
                        }
                        points = BuildCurveUsingGravity(velocity, 0, forward, GetLastPathPoint(), PathPointType.Fall);
                    }

                    testfall3 = new List<PathPointDTO>(points);

                    Path.AddRange(points);
                    testfall = new List<PathPointDTO>(points);
                    if (Path.Count > 2)
                        forward = GetLastPathForward();
                    if (Path.Count == 0)
                    {
                        RaycastDTO raycastDTO = new RaycastDTO(
                            currentPosition: startPosition,
                            forward: forward,
                            velocity: velocity,
                            height: HeighOfRaycastStart,
                            steeringInput: steering,
                            delta: .001f * _playerScaleKoef,
                            count: CountObstaclePoints
                       );
                        BuildPathWhenUserOnObstacle(GenerateRayCastPoints(raycastDTO), startPosition, forward, velocity);
                    }
                    else if (points.Count == 0)
                    {
                        RaycastDTO raycastDTO = new RaycastDTO(
                            currentPosition: GetLastPathPoint(),
                            forward: forward,
                            velocity: velocity,
                            height: HeighOfRaycastStart,
                            steeringInput: steering,
                            delta: .001f * _playerScaleKoef,
                            count: CountObstaclePoints
                       );
                        AddedLastPoinOnObstacle(GenerateRayCastPoints(raycastDTO), GetLastPathPoint(), forward, velocity);
                        BuildPathWhenUserOnObstacle(GenerateRayCastPoints(raycastDTO), GetLastPathPoint(), forward, velocity, false, true);
                        RaycastDTO raycastDTO3 = new RaycastDTO(
                           currentPosition: GetLastPathPoint(),
                           forward: forward,
                           velocity: velocity,
                           height: HeighOfRaycastStart,
                           steeringInput: steering,
                           delta: RemapDistanceBySpeed(velocity),
                           count: CountObstaclePoints
                      );
                        BuildPathWhenUserOnObstacle(GenerateRayCastPoints(raycastDTO3), GetLastPathPoint(), forward, velocity);
                    }
                    else
                    {
                        steering = 0; //user can`t go on left or right
                        RaycastDTO raycastDTO = new RaycastDTO(
                          currentPosition: points[points.Count - 1].Point,
                          forward: forward,
                          velocity: velocity,
                          height: HeighOfRaycastStart,
                          steeringInput: steering,
                          delta: RemapDistanceBySpeed(velocity),
                          count: CountObstaclePoints
                     );
                        UpdatePlayerPoints(GenerateRayCastPoints(raycastDTO), points[points.Count - 1].Point, forward, steering, velocity);
                    }

                    break;
                }
            }
            return false;
        }

        private void AddedLastPoinOnObstacle(Vector3[] rayCastPoint, Vector3 playerPosition, Vector3 forward, float speed, bool startFromPoint = false)
        {
#if Debug
            Debug.Log("BuildPathWhenUserOnObstacle");
#endif

            Vector3 position = Vector3.zero;
            Vector3 positionObstacle = Vector3.zero;
            for (int i = 0; i < rayCastPoint.Length; i++)
            {

                if (GetFirstWorldPostion(GetAllRayCasts(rayCastPoint[i], Vector3.down), TagManager.OBSTACLE_TAG, ref positionObstacle))
                {
                }
                else if (GetFirstWorldPostion(GetAllRayCasts(rayCastPoint[i], Vector3.down), TagManager.PLANE_TAG, ref position))
                {
                    Path.Add(new PathPointDTO(positionObstacle, PathPointType.Obstacle, PathPointJumpType.End));
                    return;
                }
            }
        }

        private void BuildPathWhenUserOnObstacle(Vector3[] rayCastPoint, Vector3 playerPosition, Vector3 forward, float speed, bool startFromPoint = false, bool returnIfFound = false)
        {
            Vector3 position = Vector3.zero;
            Vector3 positionObstacle = Vector3.zero;
            for (int i = 0; i < rayCastPoint.Length; i++)
            {
                if (GetFirstWorldPostion(GetAllRayCasts(rayCastPoint[i], Vector3.down), TagManager.OBSTACLE_TAG, ref positionObstacle))
                {
                }
                else if (GetFirstWorldPostion(GetAllRayCasts(rayCastPoint[i], Vector3.down), TagManager.PLANE_TAG, ref position))
                {
                    if (returnIfFound)
                    {
                        Path.Add(new PathPointDTO(position, PathPointType.Plane, PathPointJumpType.Start));
                        return;
                    }
                    else
                    {
                        if (i == 0) continue;
                        Path.Add(new PathPointDTO(position, PathPointType.Plane, PathPointJumpType.Start));
                    }
                }
            }
        }

        private bool GetPointOnObstacle(RaycastHit[] raycastHits, ref Vector3 position)
        {
            if (GetFirstWorldPostion(raycastHits, TagManager.OBSTACLE_TAG, ref position))
                return true;
            else
                return false;
        }

        private bool GetPointOnPlane(RaycastHit[] raycastHits, ref Vector3 position)
        {
            if (GetFirstWorldPostion(raycastHits, TagManager.PLANE_TAG, ref position))
                return true;
            else
                return false;
        }

        private Vector3[] GenerateRayCastPoints(PlayerDTO data, int countNew)
        {
            RaycastDTO raycastDTO = new RaycastDTO(
                currentPosition: data.position,
                 forward: data.movenentForward,
                 velocity: data.velocity,
                 height: HeighOfRaycastStart,
                 steeringInput: data.steering,
                 delta: RemapDistanceBySpeed(data.velocity),
                 count: countNew
                );
            return GenerateRayCastPoints(raycastDTO);
        }

        private Vector3[] GenerateRayCastPoints(RaycastDTO raycastDTO)
        {
            if (Turn) raycastDTO.Count *= _turnCountKoef;
            _rotatePoints = new Vector3[raycastDTO.Count];
            raycastDTO.CurrentPosition.y = raycastDTO.Height;
            if(raycastDTO.Count >0)
            _rotatePoints[0] = raycastDTO.CurrentPosition;
            if (raycastDTO.SteeringInput > 0)
                _turnAngle = STRARTRIGHTANGLE;
            else
                _turnAngle = STARTLEFTANGLE;
            Turn = raycastDTO.SteeringInput != 0;

            for (int i = 1; i < raycastDTO.Count; i++)
            {
                if (Turn)
                {
                    _rotatePoints[i] = _rotatePoints[i - 1] + (Quaternion.LookRotation(raycastDTO.Forward, Vector3.up) *
                        (GetCurveRotateRight(raycastDTO.SteeringInput, raycastDTO.Velocity)));
                }
                else
                    _rotatePoints[i] = _rotatePoints[i - 1] + raycastDTO.Forward * raycastDTO.Delta;
            }
            return _rotatePoints;
        }

        private Vector3 GetCurveRotateRight(float steeringInput, float velocity)
        {
            _gravityPosition.y = 0;
            if (steeringInput < 0)
                _turnAngle += (2 * Mathf.PI) / RemapRadiusBySteering(Math.Abs(steeringInput));
            else
                _turnAngle -= (2 * Mathf.PI) / RemapRadiusBySteering(Math.Abs(steeringInput));
            _gravityPosition.x = Mathf.Cos(_turnAngle) * RemapRadiusByVelocity(velocity);
            _gravityPosition.z = Mathf.Sin(_turnAngle) * RemapRadiusByVelocity(velocity);
            return _gravityPosition;
        }

        private float RemapRadiusBySteering(float steeringInput)
        {
            return Utils.Remap(steeringInput, 0, 1, ANGLETURNMIN, ANGLETURNMAX);
        }

        private float RemapRadiusByVelocity(float velocity)
        {
            return Utils.Remap(velocity, 0, 2.5f, RadiusMin, RadiusMax);
        }
#endregion

#region Build Rail Obstacle Points

        private bool IsRailBelowRaycast(PlayerDTO data, out List<PathPointDTO> pointsRail)
        {
            Vector3 forward = data.movenentForward;
            forward.y = 0; // jump only up;
            Vector3 playerPosition = data.position;
            pointsRail = new List<PathPointDTO>();
            List<PathPointDTO> playerPositionBeforeStartJump = new List<PathPointDTO>();
            for (int i = 0; i < RailCount; i++)
            {
                var points = BuildCurveUsingGravity(data.velocity, ANGLEFORJUMP, forward, playerPosition, PathPointType.Jump);

                if (IsRailBelowRaycast(points, data, forward, out pointsRail))
                {
                    if (points.Count > 0)
                    {
                        if (playerPositionBeforeStartJump.Count == 0)
                        {
                            Vector3 firsPlanePosition = points[0].Point;
                            if (GetFirstWorldPostion(GetAllRayCasts(points[0].Point - data.movenentForward * data.skateboardSize, Vector3.down), TagManager.PLANE_TAG, ref firsPlanePosition))
                                playerPositionBeforeStartJump.Insert(0, new PathPointDTO(firsPlanePosition, PathPointType.Jump, PathPointJumpType.Start));
                        }
                        else
                        {
                            playerPositionBeforeStartJump.Add(new PathPointDTO(playerPositionBeforeStartJump[playerPositionBeforeStartJump.Count - 1].Point, PathPointType.Jump, PathPointJumpType.Start));
                        }
                    }
                    pointsRail.InsertRange(0, playerPositionBeforeStartJump);
#if Debug
                    Debug.LogFormat("Rail find, step: {0}", i);
#endif
                    return true;
                }
                playerPosition = playerPosition + forward * RemapDistanceBySpeed(data.velocity);
                Vector3 position = playerPosition;
                if (GetFirstWorldPostion(GetAllRayCasts(playerPosition + Vector3.up, Vector3.down), TagManager.PLANE_TAG, ref position))
                    playerPositionBeforeStartJump.Add(new PathPointDTO(position, PathPointType.Plane, PathPointJumpType.Point));
            }
#if Debug
            Debug.Log("Rail not found");
#endif
            return false;
        }

        private bool IsRailBelowRaycast(List<PathPointDTO> jumpPoints, PlayerDTO playerDTO,  Vector3 forward, out List<PathPointDTO> pointsRail)
        {
            pointsRail = new List<PathPointDTO>();
            List<PathPointDTO> railPoints = new List<PathPointDTO>();

            for (int i = 0; i < jumpPoints.Count; i++)
            {
                BaseObstacle obstacleBellowReycast = null;
                Vector3 position = Vector3.zero;
                if (IsObstacleBelowRaycast(jumpPoints[i].Point, out position, out obstacleBellowReycast)
                    && obstacleBellowReycast.GetRailPath(position, playerDTO, forward, ref railPoints))
                {
                    UpdateJumpRailPoints(forward, ref pointsRail);
                    pointsRail.AddRange(railPoints);
                    return true;
                }
                else
                {
                    pointsRail.Add(jumpPoints[i]);
                }
            }
            return false;
        }

        private void UpdateJumpRailPoints(Vector3 forward, ref List<PathPointDTO> points)
        {
            if (points.Count > 3)
            {
                float maxHeight = points[0].Point.y;
                int indexMax = -1;

                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i].Point.y < maxHeight)
                    {
                        break;
                    }
                    else
                    {
                        maxHeight = points[i].Point.y;
                        indexMax = i;
                    }
                }
                points[0] = new PathPointDTO(points[0].Point, points[0].PathPointType, PathPointJumpType.Start);
                if (indexMax != -1) points[indexMax] = new PathPointDTO(points[indexMax].Point, points[indexMax].PathPointType, PathPointJumpType.Highest);
                points[points.Count - 1] = new PathPointDTO(points[points.Count - 1].Point, points[points.Count - 1].PathPointType, PathPointJumpType.End);
            }
        }
#endregion

#region Build Plane Points

        private void UpdatePointsOnPlane(Vector3 rayCastPoint)
        {
            UpdatePlanePoints(GetAllRayCasts(rayCastPoint, Vector3.down));
            _pathRay.Add(rayCastPoint);
        }

#endregion

#region Get All Plane By RaycastHit[]

        private void UpdatePlanePoints(RaycastHit[] raycastHits)
        {
            Vector3 newPosition = Vector3.zero;
            if (GetFirstWorldPostion(raycastHits, TagManager.PLANE_TAG, ref newPosition))
                Path.Add(new PathPointDTO(newPosition, PathPointType.Plane));
        }

#endregion

#region Get Obstacle by rules

        private bool IsObstacleBelowRaycast(Vector3 targetPosition, out Vector3 position, out BaseObstacle obstacle)
        {
            return GetObstacleFromRaycast(GetAllRayCasts(targetPosition, Vector3.down), out position, out obstacle);
        }

        private bool IsObstacleBetweenPoint(Vector3 startPoint, Vector3 endPoint, ref Vector3 position, out BaseObstacle obstacle)
        {
            Vector3 positionSecond = Vector3.zero;
            obstacle = null;
            if (!GetFirstWorldPostion(GetAllRayCasts(endPoint, Vector3.down), TagManager.PLANE_TAG, ref positionSecond))
                return false;
            positionSecond.y += 0.01f * _playerScaleKoef;
            return GetObstacleFromRaycast(GetAllRayCasts(startPoint, (positionSecond - startPoint).normalized, Vector3.Distance(startPoint, positionSecond)), out position, out obstacle);
        }

        private bool IsObstacleBetweenPointOnly(Vector3 startPoint, Vector3 endPoint, ref Vector3 position, out BaseObstacle obstacle)
        {
            Vector3 positionSecond = Vector3.zero;
            obstacle = null;
            //if (!GetFirstWorldPostion(GetAllRayCasts(endPoint, Vector3.down), TagManager.PLANE_TAG, ref positionSecond))
            //    return false;
            positionSecond.y += 0.01f * _playerScaleKoef;
            return GetObstacleFromRaycast(GetAllRayCasts(startPoint, (endPoint - startPoint).normalized, Vector3.Distance(startPoint, positionSecond)), out position, out obstacle);
        }

        private bool GetObstacleFromRaycast(RaycastHit[] obstaclesRayCast, out Vector3 position, out BaseObstacle obstacle)
        {
            obstacle = null;
            position = Vector3.zero;
            if (GetObstacles(obstaclesRayCast, out _obstaclesRayPosition, out _obstaclesRayCast))
            {
                //todo improve this to get correct obstacle by distance maybe or tag or specific logic?
                if (_obstaclesRayCast.Length > 0)
                {
                    obstacle = _obstaclesRayCast[0];
                    position = _obstaclesRayPosition[0];
                }
                return _obstaclesRayCast.Length > 0;
            }
            return false;
        }

#endregion

#region Get RaycastHit[] By position and direction

        private RaycastHit[] GetAllRayCasts(Vector3 targetPosition, Vector3 direction, float maxDistance = Mathf.Infinity)
        {
            _obstacleRay.origin = targetPosition;
            _obstacleRay.direction = direction;
            return Physics.RaycastAll(_obstacleRay, maxDistance);
        }

#endregion

#region Get All Obstacles By RaycastHit[]

        private bool GetObstacles(RaycastHit[] raycastHits, out Vector3[] obstaclesPositionRayPoint, out BaseObstacle[] obstacles)
        {
            _obstacleRaycastHit.Clear();
            _obstaclesPositionRayPoint.Clear();
            for (int i = 0; i < raycastHits.Length; i++)
                if (raycastHits[i].collider.tag.Equals(TagManager.OBSTACLE_TAG))
                {
                    var colliderObstacle = raycastHits[i].transform.GetComponent<ColliderObstacle>();
                    if (colliderObstacle != null)
                    {
                        _obstaclesPositionRayPoint.Add(raycastHits[i].point);
                        _obstacleRaycastHit.Add(colliderObstacle.BaseObstacle);
                    }
                    else
                    {
                        var realObstacle = raycastHits[i].transform.GetComponent<RealObstacle>();
                        if (realObstacle != null)
                        {
                            _obstaclesPositionRayPoint.Add(raycastHits[i].point);
                            _obstacleRaycastHit.Add(realObstacle);
                        }
                    }
                }
            obstacles = _obstacleRaycastHit.ToArray();
            obstaclesPositionRayPoint = _obstaclesPositionRayPoint.ToArray();
            return obstacles.Length > 0;
        }
#endregion

#region Get All Boundary By RaycastHit[]

        private bool GetBoundarys(RaycastHit[] raycastHits, out Vector3[] boundaryPositionRayPoint)
        {
            _boundaryPositionRayPoint.Clear();
            for (int i = 0; i < raycastHits.Length; i++)
                if (raycastHits[i].collider.tag.Equals(TagManager.BOUNDARY_TAG))
                    _boundaryPositionRayPoint.Add(raycastHits[i].point);
            boundaryPositionRayPoint = _boundaryPositionRayPoint.ToArray();
            return boundaryPositionRayPoint.Length > 0;
        }
#endregion

#region Get All world positions

        private bool GetFirstWorldPostion(RaycastHit[] raycastHits, string tag, ref Vector3 firstPosition)
        {
            var positions = GetAllWordPostion(raycastHits, tag);
            if (positions.Length > 0)
                firstPosition = positions[0];
            return positions.Length > 0;
        }

        private Vector3[] GetAllWordPostion(RaycastHit[] raycastHits, string tag)
        {
            _planePoints.Clear();
            for (int i = 0; i < raycastHits.Length; i++)
                if (raycastHits[i].collider.tag.Equals(tag))
                    _planePoints.Add(raycastHits[i].point);
            return _planePoints.ToArray();
        }

#endregion

#region Jump path build

        /// <summary>
        /// Build path curve by angle 0 for fall and 1.3 for jump
        /// </summary>
        /// <param name="playerPosition"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        private List<PathPointDTO> BuildCurveUsingGravity(float speed, float angle, Vector3 forward, Vector3 playerPosition, PathPointType pathPointType)
        {
            Vector3 nextPosition;
            int step = GRAVITYPOINTSOFCURVE;
            bool insideObstacle = false;
            _curvePoints.Clear();
            _interpolationCurveProgress = 0f;
            Vector3 previousPosition = playerPosition;
            PathPointJumpType pathPointJump = PathPointJumpType.Point;
            while (step-- > 0)
            {
                _interpolationCurveProgress += ACCURACYCURVEJUMPFALLSTEP;
                Vector3 positionOnCurve = GetCurvePosition(angle == 0 ? speed : JumpForce, speed, _interpolationCurveProgress, angle);
                nextPosition = playerPosition + (Quaternion.LookRotation(forward, Vector3.up) * positionOnCurve);
               
                Vector3 collisionObstacle = nextPosition;
                BaseObstacle obstacle = null;

                Vector3 newPosition = Vector3.zero;
                if (GetFirstWorldPostion(GetAllRayCasts(nextPosition, Vector3.down), TagManager.OBSTACLE_TAG, ref newPosition))
                {
                    AddPoint(nextPosition, pathPointType, pathPointJump, ref _curvePoints);
                    insideObstacle = true;
                }
                else if (GetFirstWorldPostion(GetAllRayCasts(nextPosition, Vector3.down), TagManager.PLANE_TAG, ref newPosition))
                {
                    if (insideObstacle)
                    {
                        if (GetFirstWorldPostion(GetAllRayCasts(nextPosition, Vector3.down), TagManager.OBSTACLE_TAG, ref newPosition))
                        {
                            UpdateJumpPoints(_curvePoints);
                            return _curvePoints;
                        }
                        else
                            insideObstacle = false;
                    }
                    else if (_curvePoints.Count > 0)
                    {
                        AddPoint(nextPosition, pathPointType, pathPointJump, ref _curvePoints);
                    }
                    else if (_curvePoints.Count == 0)
                    {
                        AddPoint(nextPosition, pathPointType, pathPointJump, ref _curvePoints);
                    }
                }
                else
                {
                    UpdateJumpPoints(_curvePoints);
                    return _curvePoints;
                }
                previousPosition = nextPosition;
            }
            return _curvePoints;
        }

        private void AddPoint(Vector3 position, PathPointType pathType, PathPointJumpType pathPointJumpType, ref List<PathPointDTO> points)
        {
            points.Add(new PathPointDTO(position, pathType, pathPointJumpType));
        }

        private void UpdateJumpPoints(List<PathPointDTO> points)
        {
            if (points.Count > 3)
            {
                float maxHeight = points[0].Point.y;
                int indexMax = -1;

                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i].Point.y < maxHeight)
                    {
                        break;
                    }
                    else
                    {
                        maxHeight = points[i].Point.y;
                        indexMax = i;
                    }
                }

                points[0] = new PathPointDTO(points[0].Point, points[0].PathPointType, PathPointJumpType.Start);
                if (indexMax != -1) points[indexMax] = new PathPointDTO(points[indexMax].Point, points[indexMax].PathPointType, PathPointJumpType.Highest);
                points[points.Count - 1] = new PathPointDTO(points[points.Count - 1].Point, points[points.Count - 1].PathPointType, PathPointJumpType.End);
            }
        }

        private bool CheckJumpPoints(List<PathPointDTO> jumpPoints, out List<PathPointDTO> updateJumpPoints)
        {
            updateJumpPoints = new List<PathPointDTO>();
            if (jumpPoints.Count > 1)
            {
                updateJumpPoints.Add(jumpPoints[0]);
                for (int i = 1; i < jumpPoints.Count; i++)
                {
                    float distance = Vector3.Distance(jumpPoints[i].Point, jumpPoints[i - 1].Point);
                    Vector3 previousPoint = jumpPoints[i - 1].Point;
                    previousPoint.y = jumpPoints[i].Point.y;
                    Vector3 forward = (jumpPoints[i].Point - previousPoint).normalized;
                    Vector3[] obstaclesPoint;
                    Vector3[] obstaclesPoint2;
                    Vector3[] boundarysPoint2;
                    BaseObstacle[] obstacles;

                    if(GetBoundarys(GetAllRayCasts(jumpPoints[i - 1].Point, forward, distance*4f), out boundarysPoint2))
                    {
#if Debug
                        Debug.LogError("GetBoundarys");
#endif
                        Vector3 boundaryPlane = Vector3.zero;
                        updateJumpPoints.Add(new PathPointDTO(boundarysPoint2[0], PathPointType.Jump, PathPointJumpType.Fail));
                        if (GetFirstWorldPostion(GetAllRayCasts(boundarysPoint2[0], Vector3.down), TagManager.PLANE_TAG, ref boundaryPlane))
                            updateJumpPoints.Add(new PathPointDTO(boundaryPlane, PathPointType.Jump, PathPointJumpType.Fail));
                        else if (GetFirstWorldPostion(GetAllRayCasts(jumpPoints[i - 1].Point, Vector3.down), TagManager.PLANE_TAG, ref boundaryPlane))
                            updateJumpPoints.Add(new PathPointDTO(boundaryPlane, PathPointType.Jump, PathPointJumpType.Fail));
                        return true;
                    }
                    if (GetObstacles(GetAllRayCasts(jumpPoints[i-1].Point, forward, distance), out obstaclesPoint, out obstacles))
                    {
#if Debug
                        Debug.LogError("Fail");
#endif
                        if (GetObstacles(GetAllRayCasts(jumpPoints[i - 1].Point, Vector3.down, distance), out obstaclesPoint2, out obstacles))
                        {
#if Debug
                            Debug.LogError("GetObstacles");
#endif
                            if (GetObstacles(GetAllRayCasts(jumpPoints[i].Point+Vector3.up, Vector3.down, ObstacleDistance), out obstaclesPoint2, out obstacles))
                            {
#if Debug
                                Debug.LogError("FailDown2");
#endif
                                //test case when we have jump in obstacle;
                                updateJumpPoints.Add(new PathPointDTO(obstaclesPoint2[0], PathPointType.Jump, PathPointJumpType.Fail));
                                updateJumpPoints.Add(new PathPointDTO(obstaclesPoint2[0], PathPointType.Obstacle, PathPointJumpType.Fail));
                                Vector3 planePosition = Vector3.zero;
                            }
                            else
                            {
                                updateJumpPoints.Add(new PathPointDTO(obstaclesPoint[0], PathPointType.Jump, PathPointJumpType.Fail));
                                Vector3 planePosition = Vector3.zero;
                                if (GetFirstWorldPostion(GetAllRayCasts(obstaclesPoint[0], Vector3.down), TagManager.PLANE_TAG, ref planePosition))
                                    updateJumpPoints.Add(new PathPointDTO(planePosition, PathPointType.Jump, PathPointJumpType.Fail));
                            }
                        }
                        else
                        {
                            updateJumpPoints.Add(new PathPointDTO(obstaclesPoint[0], PathPointType.Jump, PathPointJumpType.Fail));
                            Vector3 planePosition = Vector3.zero;
                            if (GetFirstWorldPostion(GetAllRayCasts(obstaclesPoint[0], Vector3.down), TagManager.PLANE_TAG, ref planePosition))
                                updateJumpPoints.Add(new PathPointDTO(planePosition, PathPointType.Jump, PathPointJumpType.Fail));
                        }
                        return true;
                    }
                    else
                    {
                        updateJumpPoints.Add(jumpPoints[i]);
                    }
                }
            }
            return false;
        }

        private bool CheckForwardPoints(List<PathPointDTO> forwardPoints, out List<PathPointDTO> updateForwardPoints)
        {
            updateForwardPoints = new List<PathPointDTO>();
            if (forwardPoints.Count > 1)
            {
                updateForwardPoints.Add(forwardPoints[0]);
                for (int i = 1; i < forwardPoints.Count; i++)
                {
                    Vector3 previousPoint = forwardPoints[i - 1].Point;

                    float distance = Vector3.Distance(forwardPoints[i].Point, previousPoint);
                    previousPoint.y = forwardPoints[i].Point.y;
                    Vector3 forward = (forwardPoints[i].Point - previousPoint).normalized;
                    Vector3[] boundaryPoint;
                    if (GetBoundarys(GetAllRayCasts(forwardPoints[i].Point, forward, distance), out boundaryPoint))
                    {
                        updateForwardPoints.Add(new PathPointDTO(boundaryPoint[0], PathPointType.Plane, PathPointJumpType.Fail));
                        return true;
                    }
                    else
                    {
                        updateForwardPoints.Add(forwardPoints[i]);
                    }
                }
            }
            return false;
        }

#endregion

#region Build curve using gravity and speed

        private Vector3 GetCurvePosition(float speedY, float speedX, float interpolationKoef, float angle)
        {
            _gravityPosition.x = 0;
            _gravityPosition.y = (speedY * interpolationKoef * Mathf.Sin(angle) - GRAVITY * interpolationKoef * interpolationKoef / 2f) * .1f;
            if (angle == 0)
                _gravityPosition.z = FallLegth * speedX * interpolationKoef * Mathf.Cos(angle) * .1f;
            else
                _gravityPosition.z = JumpLegth * speedX * interpolationKoef * Mathf.Cos(angle) * .1f;
            return _gravityPosition;
        }
#endregion

#region Added point on planeOrObstacle

        private void AddLastPointOnObstacleOrPlane(Vector3 lastPoint, PlayerDTO playerDTO, PathPointType pathPointType)
        {
            Vector3 positionOnObstacle = lastPoint;
            Vector3 targetPoint = (lastPoint + Vector3.up) + (playerDTO.movenentForward * (RemapDistanceBySpeed(playerDTO.velocity)));
            if (GetPointOnObstacle(GetAllRayCasts(targetPoint, Vector3.down), ref positionOnObstacle))
            {
                Path.Add(new PathPointDTO(positionOnObstacle, pathPointType, PathPointJumpType.End));
                Path.Add(new PathPointDTO(positionOnObstacle, PathPointType.Obstacle, PathPointJumpType.End));
            }
            else if (GetPointOnPlane(GetAllRayCasts(targetPoint, Vector3.down), ref positionOnObstacle))
            {
                Path.Add(new PathPointDTO(positionOnObstacle, pathPointType, PathPointJumpType.End));
                Path.Add(new PathPointDTO(positionOnObstacle, PathPointType.Plane, PathPointJumpType.End));
            }
        }

#endregion

#region Tools

        private Vector3 GetLastPathForward()
        {
            if (Path.Count < 2)
                throw new Exception("GetLastPathForward Path is empty");
            return (Path[Path.Count - 1].Point - Path[Path.Count - 2].Point).normalized;
        }

        private Vector3 GetLastPathPoint()
        {
            if (Path.Count == 0)
                throw new Exception("GetLastPathPoint Path is empty");
            return Path[Path.Count - 1].Point;
        }

#endregion

        private float RemapDistanceBySpeed(float speed)
        {
            return Mathf.Clamp(speed / 33.33f * _playerScaleKoef,0.01f,100);
            //   return Utils.Remap(speed, 0.01f, 1, 0.001f, .03f);
        }


        List<PointView> testObjects = new List<PointView>();

        public GameObject _testObject;
        [SerializeField] private bool _pathRebuild = false;
        public void RebuildPath()
        {
            _pathRebuild = true;
            FixedUpdate();
        }


        private void InitFailPoint()
        {
            for (int i = 0; i < Path.Count; i++)
            {
                if (Path[i].PathPointJumpType.Equals(PathPointJumpType.Fail))
                {
                    try
                    {
                        var go = Instantiate(_testObject);
                        go.transform.position = Path[i].Point;
                        var point = go.AddComponent<PointView>();
                        Material material = new Material(Shader.Find("Standard"));
                        material.color = Color.red;
                        point.SetPathDTO(Path[i], _playerScaleKoef, material);
                        go.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f) * _playerScaleKoef; ;
                        go.name = "FAIL";
                    }
                    catch { }
                }
            }
        }

        private Material _material = null;
        private Material Material
        {
            get
            {
                if (_material == null)
                {
                    _material = new Material(Shader.Find("Standard"));
                    _material.color = Color.blue;
                }
                return _material;
            }
        }

        private void FixedUpdate()
        {

#if UNITY_EDITOR
            if (_pathRebuild)
#else
            if (_pathRebuild && Screens.GameScreen.ShowForward)
#endif
            {
                InitFailPoint();
                testObjects.ForEach(p => p.Hide());
                for (int i = 0; i < Path.Count; i++)
                {
                    PointView point;
                    if (i < testObjects.Count-1)
                    {
                        point = testObjects[i];
                    }
                    else
                    {
                        var go = Instantiate(_testObject);
                        go.transform.position = Path[i].Point;
                        go.transform.localScale = Vector3.one * Mathf.Clamp(_playerScaleKoef, 0.001f,100);
                        point = go.AddComponent<PointView>();
                        testObjects.Add(point);
                    }
                    point.SetPathDTO(Path[i], _playerScaleKoef, Material);
                }
                _pathRebuild = false;
            }

        }
        List<PathPointDTO> testfall = new List<PathPointDTO>();
        List<Vector3> testfall2 = new List<Vector3>();
        List<PathPointDTO> testfall3 = new List<PathPointDTO>();
        List<Vector3> testfall4 = new List<Vector3>();
        List<PathPointDTO> testRailPoints = new List<PathPointDTO>();

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            if (_rotatePoints != null)
                for (int i = 0; i < _rotatePoints.Length; i++)
                    Gizmos.DrawSphere(_rotatePoints[i], 0.08f);

            //for (int i = 0; i < testRailPoints.Count; i++)
            //    Gizmos.DrawSphere(testRailPoints[i], 0.05f);
            Gizmos.color = Color.black;
            for (int i = 0; i < _planePoints.Count; i++)
                Gizmos.DrawSphere(_planePoints[i], 0.013f);
            Gizmos.color = Color.black;
            if (_obstaclePoints != null)
                for (int i = 0; i < _obstaclePoints.Length; i++)
                    Gizmos.DrawSphere(_obstaclePoints[i], 0.013f);
            Gizmos.color = Color.grey;
            for (int i = 0; i < testfall2.Count; i++)
            {
                Vector3 test = testfall2[i];
                test.y = 0;
                Gizmos.DrawSphere(test, 0.07f);
            }
            //Gizmos.color = Color.cyan;
            //for (int i = 0; i < testfall3.Count; i++)
            //    Gizmos.DrawSphere(testfall3[i], 0.1f);
            Gizmos.color = Color.blue;
            for (int i = 0; i < testfall4.Count; i++)
                Gizmos.DrawSphere(testfall4[i], 0.04f);
        }
#endif
    }
}