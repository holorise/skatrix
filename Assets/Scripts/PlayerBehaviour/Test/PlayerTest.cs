﻿using UnityEngine;
using System.Collections;
using SKTRX.Spline;
using Tools;
using System.Collections.Generic;
using SKTRX.Statics;
using SKTRX.PlayerBehaviour.Path;
using SKTRX.DTOModel;
using SKTRX.Enums;

public class PlayerTest : MonoBehaviour
{

    private enum PlayerState
    {
        Idle,
        StartMove,
        Move,
        StopMove
    }
    Coroutine speedUpCoroutina;
    Coroutine speedDownCoroutina;

    string speedUp = "StartMove";
    private void OnGUI()
    {
        if (GUI.Button(new Rect(100, 100, 100, 100), speedUp))
        {
            speedUp = "Speed Up";
            if (speed == 0)
                _firstSpeed = true;
            if (speed == 1)
                _firstSecond = true;
            if (speed == 2)
                _firstThird = true;
            if (speedDownCoroutina != null) StopCoroutine(speedDownCoroutina);
            speedUpCoroutina = StartCoroutine(StartSpeedUp());
            StartMove();
        }
        if (GUI.Button(new Rect(100, 0, 100, 100), "Move"))
        {
            _pathBuilder.BuildPath(GetDTOFake, PathType.Forward);
            _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false);
            _progressIndex = 0.01f;
        }
        if (GUI.Button(new Rect(100, 200, 100, 100), "Speed Down"))
        {
            if (speed == 0)
                _firstSpeed = true;
            if (speed == 1)
                _firstSecond = true;
            if (speed == 2)
                _firstThird = true;
            if (speedUpCoroutina != null) StopCoroutine(speedUpCoroutina);
            speedDownCoroutina = StartCoroutine(StartSpeedDown());
        }
        if (GUI.Button(new Rect(0, 0, 100, 100), "Build Path turn left"))
        {
            PlayerDTO playerDTO = GetDTOFake;
            playerDTO.steering = -1;
            _pathBuilder.BuildPath(playerDTO, PathType.Forward);
            _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false);
            _progressIndex = 0.01f;
        }
        if (GUI.Button(new Rect(200, 0, 100, 100), "Build Path turn right"))
        {
            PlayerDTO playerDTO = GetDTOFake;
            playerDTO.steering = 1;
            _pathBuilder.BuildPath(playerDTO, PathType.Forward);
            _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false);
            _progressIndex = 0.01f;
        }
        //if (GUI.Button(new Rect(0, 300, 100, 100), "Right"))
        //{
        //    _pathBuilder.RebuildWithTurn(transform.position, transform.forward, 8, 20);
        //    _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false);
        //    _progressIndex = 0.01f;
        //}
        //if (GUI.Button(new Rect(0, 400, 100, 100), "Build Path forward"))
        //{
        //    _pathBuilder.BuildForwardPath(transform.position, transform.forward, 8);
        //    _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false);
        //    _progressIndex = 0.01f;
        //}
       
        if (GUI.Button(new Rect(0, 100, 100, 100), "Jump Test"))
        {
            _pathBuilder.BuildPath(GetDTOFake, PathType.Jump);
            _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false);
            _progressIndex = 0.01f;
        }
        GUI.Label(new Rect(100, 200, 200, 100), _currentPlayerState.ToString());
    }

    private float _speedFirst = 3;
    private float _speedSecond = 6;
    private float _speedThird = 9;
    private bool _firstSpeed = false;
    private bool _firstSecond = false;
    private bool _firstThird = false;
    [SerializeField]private int speed = 0;

    private IEnumerator StartSpeedUp()
    {
        while (ExecuteSpeedUp())
        {
            UpdateSpeeUp();
            yield return new WaitForEndOfFrame();
        }
        if (_firstSpeed) speed = 1;
        if (_firstSecond) speed = 2;
        _firstSpeed = false;
        _firstSecond = false;
        _firstThird = false;
    }
    private IEnumerator StartSpeedDown()
    {
        while (ExecuteSpeedDown())
        {
            UpdateSpeeDown();
            yield return new WaitForEndOfFrame();
        }
        if (_firstThird) speed = 1;
        if (_firstSecond) speed = 0;
        _firstSpeed = false;
        _firstSecond = false;
        _firstThird = false;
    }

    private bool ExecuteSpeedUp()
    {
        return (_firstSecond && _speed < _speedSecond) || (_firstSpeed && _speed < _speedFirst) || (_firstThird && _speed < _speedThird);
    }

    private bool ExecuteSpeedDown()
    {
        return (_firstSecond && _speed > _speedFirst) || (_firstSpeed && _speed > 0) || (_firstThird && _speed > _speedSecond);
    }

    private void UpdateSpeeUp()
    {
        _speed = Mathf.Clamp(_speed + (5f/60f), 0, GetMaxSpeed());
    }

    private void UpdateSpeeDown()
    {
        _speed = Mathf.Clamp(_speed - (5f/60f), GetMinSpeed(), 99 );
    }

    private float GetMaxSpeed()
    {
        if (_firstSecond)
            return _speedSecond;
        if (_firstSpeed)
            return _speedFirst;
        if (_firstThird)
            return _speedThird;
        return 0;
    }
    private float GetMinSpeed()
    {
        if (_firstSecond)
            return _speedFirst;
        if (_firstSpeed)
            return 0;
        if (_firstThird)
            return _speedSecond;
        return 0;
    }


    private void StartMove()
    {
        _currentPlayerState = PlayerState.StartMove;
    }

    private void Move()
    {
        _currentPlayerState = PlayerState.Move;
    }

    private void StopMove()
    {
        _currentPlayerState = PlayerState.StopMove;
    }

    private PlayerState _currentPlayerState = PlayerState.Idle;


    [SerializeField] private PlayerPathManager _pathBuilder;
    private CatmullRom _curve;
    [SerializeField] private float _progressIndex = 0;
    private Vector3 _targetPosition;
    [SerializeField] private float _speed = 0.002f;
    [SerializeField] private Transform _targetTransform;
    [SerializeField] private List<GameObject> _points = new List<GameObject>();

    [SerializeField] private GameObject _testObject;
    [SerializeField] private GameObject _testObjectGravity;
    public PlayerDTO GetDTO
    {
        get
        {
            return new PlayerDTO(
                jumpPoints: 0,
                vel: _speed,
                maxVel: 1,
                steer: 1,
                skateSize: 1,
                pos: transform.position,
                moveForward: transform.forward
                //skateForward: transform.forward
                );
        }
    }
    public PlayerDTO GetDTOFake
    {
        get
        {
            return new PlayerDTO(
                jumpPoints: 0,
                vel: 5,
                maxVel: 1,
                steer: 1,
                skateSize: 1,
                pos: transform.position,
                moveForward: transform.forward
                //skateForward: transform.forward
                );
        }
    }
    private void Update()
    {
        if (_currentPlayerState == PlayerState.Move || _currentPlayerState == PlayerState.StartMove)
        {
            if (_pathBuilder.Path.Count == 0)
                _pathBuilder.BuildPath (GetDTO, PathType.Forward);
            else
            {
                if (_progressIndex >= 0.98f) //0.7
                {
                    //Debug.LogError("test");
                    _pathBuilder.BuildPath(GetDTO, PathType.Forward);
                    _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 2, false, true);
                    _progressIndex = 0.04f;
                    _points.ForEach(p => Destroy(p));
                    _points.Clear();
                    for (int i=0;i< _pathBuilder.Path.Count; i++)
                    {
                        //var objectTest = Instantiate(_testObject);
                        //objectTest.transform.position = _pathBuilder.Path[i];
                       // _points.Add( objectTest);
                    }
                }
                else
                {
                    if (_curve == null)
                    {
                        _curve = new CatmullRom(_pathBuilder.Path.ToArray(), 10, false, true);
                        _progressIndex = 0.01f;
                    }
                    Vector3 targetPoint = _curve.GetPositionByProgress(_progressIndex+ 0.03f);
                    Vector3 t = transform.position;
                    transform.forward = Vector3.Lerp(transform.forward,(targetPoint - t).normalized,Time.deltaTime*9) ;
                    Vector3 newPosition = transform.forward * _speed * Time.deltaTime;
                    transform.position = transform.position+ newPosition;
                    if (Vector3.Distance(transform.position, targetPoint) < Utils.Remap(_speed, 0f, 10f, 0f, 2f))
                    {
                        _progressIndex += 0.04f;
                    }
                }
            }
        }
        if (_curve != null)
        {
            _curve.DrawSpline(Color.white);
        }
    }

    
}