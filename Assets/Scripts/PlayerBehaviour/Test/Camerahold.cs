﻿using UnityEngine;
using System.Collections;

public class Camerahold : MonoBehaviour
{
    [SerializeField]
    Quaternion rotation;
    [SerializeField] private float x;
    [SerializeField] private Transform _playerPosition;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private bool _applyRottation;
    private void Update()
    {

        transform.position = Vector3.Lerp(transform.position , _playerPosition.position + _offset, Time.deltaTime*2f) ;
           
        if(_applyRottation)
            rotation = _playerPosition.rotation;
        else
            rotation = transform.rotation;
        rotation.x = x;
        transform.rotation = rotation;
    }

    [ContextMenu  ("Test")]
    private void GetOffset()
    {
        _offset = _playerPosition.position - transform.position;
    }

}
