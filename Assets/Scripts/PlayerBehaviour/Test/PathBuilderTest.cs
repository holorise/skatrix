﻿using UnityEngine;
using System.Collections.Generic;
using SKTRX.Statics;
using Tools;
using SKTRX.Obstacle;
using System;

public class PathBuilderTest : MonoBehaviour
{
    [SerializeField] private List<Vector3> _path = new List<Vector3>();

    public List<Vector3> Path { get => _path; set => _path = value; }
   
    private const float _gravity = 9.8f;
    private const float _accuracyCurveJumpFallStep = 0.6f;
    private const int _curveGravityPoints = 200;
    private const float _tuneKoefRotation = 0.02f;
    private const float _tuneKoefRotationX = 16f;
    private const float _tuneKoefRotationZ = 9f;
    private const float _zRoateDialKoef = 0.6f;

    private float _angle = 20;
    private float _rotationInterpolation = 0.36f;
    private float _startRotateInterpolation = -0.2f;
    private float _interpolationCurveProgress = 0;
    private float _deltaObstacleBuild = .5f;
    private float _obstacleHeighBuild = 50f;
    private int _countObstaclePoints = 100;
    private bool _turn = false;
    private bool _startBuild = false;

    private Vector3 _gravityPosition;
    private Ray _obstacleRay;
    private List<Vector3> _curvePoints = new List<Vector3>();
    private List<Vector3> _planePoints = new List<Vector3>();
    private List<Vector3> _obstaclesPositionRayPoint = new List<Vector3>();
    private List<Vector3> _pathRay = new List<Vector3>();
    private List<BaseObstacle> _obstacleRaycastHit = new List<BaseObstacle>();
    private BaseObstacle[] _obstaclesRayCast;
    private Vector3[] _obstaclesRayPosition;
    private Vector3[] _rotatePoints;
    private Vector3[] _obstaclePoints;

    public void BuildForwardPath(Vector3 playerPosition, Vector3 forward, float speed)
    {
        if (_startBuild)
        {
            Debug.LogError("build not allow");
            return;
        }
        _startBuild = true;
        Path.Clear();
        _pathRay.Clear();
        UpdatePlayerPoints(GenerateNewPointsForObstacle(playerPosition, forward, RemapDistanceBySpeed(speed), 10), playerPosition, forward, speed);
        _startBuild = false;
        _turn = false;
    }

    public void BuildJumpPath(float speed, Vector3 playerPosition, Vector3 forward)
    {
        _turn = false;
        if (_startBuild)
        {
            Debug.LogError("build not allow");
            return;
        }
        _startBuild = true;
        Path.Clear();
        _pathRay.Clear();
        var points = BuildCurveUsingGravity(speed * 10, 1.2f, forward, playerPosition);
        Path.AddRange(points);
        UpdatePlayerPoints(GenerateNewPointsForObstacle(points[points.Count-1], forward, RemapDistanceBySpeed(speed), 10), Path[Path.Count - 1], forward, speed, true);
        _startBuild = false;
        _turn = false;
    }

    public void RebuildWithTurn(Vector3 playerPosition, Vector3 forward, float speed, float angle)
    {
        _angle = angle;
        _turn = true;
        BuildForwardPath(playerPosition, forward, speed);
        _turn = false;
    }

    #region Build Path
    private void UpdatePlayerPoints(Vector3[] rayCastPoint, Vector3 playerPosition, Vector3 forward, float speed, bool startFromPoint = false)
    {
        for (int i = 0; i < rayCastPoint.Length; i++)
        {
            BaseObstacle obstacleBellowReycast = null;
            Vector3 position = Vector3.zero;

            if (IsObstacleBelowRaycast(rayCastPoint[i], out position, out obstacleBellowReycast))
            {
                if (!BuildObstaclePointsPath(obstacleBellowReycast, playerPosition, position, forward, speed, startFromPoint))
                    return;
            }
            else if (i > 0 && IsObstacleBetweenPoint(playerPosition, rayCastPoint[i], ref position, out obstacleBellowReycast))
            {
                if (!BuildObstaclePointsPath(obstacleBellowReycast, playerPosition, position, forward, speed, startFromPoint))
                    return;
            }
            else
                UpdatePointsOnPlane(rayCastPoint[i]);
        }
    }
    #endregion

    #region Build Obstacle Points

    private bool BuildObstaclePointsPath(BaseObstacle obstacle, Vector3 playerPosition, Vector3 startPosition, Vector3 forward, float speed, bool startFromPoint = false)
    {
        Vector3 startPositionOfObstacle = startPosition;
        startPosition.y = playerPosition.y = playerPosition.y+0.1f;
        float distance = Vector3.Distance(playerPosition, startPosition) + 5;
        
        if (!startFromPoint && GetFirstWordPostion(GetAllRayCasts(playerPosition, (startPosition - playerPosition).normalized, distance), TagManager.OBSTACLE_TAG, ref startPositionOfObstacle))
        {
            Debug.LogWarning("_startObstaclePoinSearch ok");
           _obstaclePoints = GenerateNewPointsForObstacle(startPositionOfObstacle,
           (startPositionOfObstacle - playerPosition).normalized, _deltaObstacleBuild, _obstacleHeighBuild, _countObstaclePoints);
        }
        else
        {
            Debug.LogWarning("_startObstaclePoinSearch not ok start from input point");
            startPosition.y = playerPosition.y = playerPosition.y + 0.1f;
            _obstaclePoints = GenerateNewPointsForObstacle(startPositionOfObstacle, forward, _deltaObstacleBuild, _obstacleHeighBuild, _countObstaclePoints);
        }
        
        for (int i=0;i< _obstaclePoints.Length; i++)
        {
            if (AddePointOnObstacle(GetAllRayCasts(_obstaclePoints[i], Vector3.down)))
            {
                continue;
            }
            else
            {
                List<Vector3> points = new List<Vector3>();
                if (Path.Count == 0)
                {
                    points = BuildCurveUsingGravity(speed * 10, 0, forward, playerPosition);
                }
                else
                {
                    if (Path.Count > 2)
                        forward = (Path[Path.Count - 1] - Path[Path.Count - 2]).normalized;
                    points = BuildCurveUsingGravity(speed * 10, 0, forward, Path[Path.Count - 1]);
                }
                testfall = new List<Vector3>(points);
                Path.AddRange(points);

                if (Path.Count > 2)
                    forward = (Path[Path.Count - 1] - Path[Path.Count - 2]).normalized;

                if (points.Count == 0)
                    BuildPathWhenUserOnObstacle(GenerateNewPointsForObstacle(Path[Path.Count - 1], forward, RemapDistanceBySpeed(speed), 10), Path[Path.Count - 1], forward, speed);
                else
                    UpdatePlayerPoints(GenerateNewPointsForObstacle(points[points.Count - 1], forward, RemapDistanceBySpeed(speed), 10), points[points.Count - 1], forward, speed);

                break;
            }
        }
        return false;
    }

    private void BuildPathWhenUserOnObstacle(Vector3[] rayCastPoint, Vector3 playerPosition, Vector3 forward, float speed, bool startFromPoint = false)
    {
        Debug.LogError("Rebuild from here");
        for (int i = 0; i < rayCastPoint.Length; i++)
        {
            UpdatePointsOnPlane(rayCastPoint[i]);
        }
    }

    private bool AddePointOnObstacle(RaycastHit[] raycastHits)
    {
        Vector3 newPosition = Vector3.zero;
        if (GetFirstWordPostion(raycastHits, TagManager.OBSTACLE_TAG, ref newPosition))
        {
            Path.Add(newPosition);
            return true;
        }
        else
            return false;
    }

    private Vector3[] GenerateNewPointsForObstacle(Vector3 previousPosition, Vector3 forward, float delta, float height, int count = 10)
    {
        _rotatePoints = new Vector3[count];
        previousPosition.y = height;
        _rotatePoints[0] = previousPosition;

        float interpolationKoef = _startRotateInterpolation;

        for (int i = 1; i < count; i++)
        {
            if (_turn)
            {
                if (_angle < 0)
                    _rotatePoints[i] = _rotatePoints[i - 1] + (Quaternion.LookRotation(forward, Vector3.up) * (GetCurveRotateRight(8, interpolationKoef, 105)));
                else
                    _rotatePoints[i] = _rotatePoints[i - 1] + (Quaternion.LookRotation(forward, Vector3.up) * (GetCurveRotateRight(8, interpolationKoef, 75)));
                interpolationKoef += _rotationInterpolation;
            }
            else
                _rotatePoints[i] = _rotatePoints[i - 1] + forward * delta;
        }
        return _rotatePoints;
    }

    private Vector3 GetCurveRotateRight(float speed, float interpolationKoef, float angle)
    {
        interpolationKoef += _accuracyCurveJumpFallStep;
        _gravityPosition.y = 0;
        _gravityPosition.x = speed * interpolationKoef * Mathf.Cos(angle * Mathf.Deg2Rad) * _tuneKoefRotation * _tuneKoefRotationX;
        _gravityPosition.z = (speed * interpolationKoef * Mathf.Sin(angle * Mathf.Deg2Rad) - interpolationKoef * interpolationKoef / _zRoateDialKoef) * _tuneKoefRotation * _tuneKoefRotationZ;
        return _gravityPosition;
    }
    #endregion

    #region Build Plane Points

    private void UpdatePointsOnPlane(Vector3 rayCastPoint)
    {
        UpdatePlanePoints(GetAllRayCasts(rayCastPoint, Vector3.down));
        _pathRay.Add(rayCastPoint);
    }

    #endregion

    #region Get All Plane By RaycastHit[]

    private void UpdatePlanePoints(RaycastHit[] raycastHits)
    {
        Vector3 newPosition = Vector3.zero;
        if (GetFirstWordPostion(raycastHits, TagManager.PLANE_TAG, ref newPosition))
        {
            Path.Add(newPosition);
        }
    }

    #endregion

    #region Get Obstacle by rules

    private bool IsObstacleBelowRaycast(Vector3 targetPosition,out Vector3 position, out BaseObstacle obstacle)
    {
        return GetObstacleFromRaycast(GetAllRayCasts(targetPosition, Vector3.down),out position, out obstacle);
    }

    private bool IsObstacleBetweenPoint(Vector3 startPoint, Vector3 endPoint, ref Vector3 position, out BaseObstacle obstacle)
    {
        Vector3 positionSecond = Vector3.zero;
        obstacle = null;
        if (!GetFirstWordPostion(GetAllRayCasts(endPoint, Vector3.down), TagManager.PLANE_TAG, ref positionSecond))
            return false;
        positionSecond.y += 0.1f;
        return GetObstacleFromRaycast(GetAllRayCasts(startPoint, (positionSecond - startPoint).normalized, Vector3.Distance(startPoint, positionSecond)),out position, out obstacle);
    }

    private bool GetObstacleFromRaycast(RaycastHit[] obstaclesRayCast, out Vector3 position, out BaseObstacle obstacle)
    {
        obstacle = null;
        position = Vector3.zero;
        if (GetObstacles(obstaclesRayCast,out _obstaclesRayPosition, out _obstaclesRayCast)) 
        {
            //todo improve this to get correct obstacle by distance maybe or tag or specific logic?
            if (_obstaclesRayCast.Length > 0)
            {
                obstacle = _obstaclesRayCast[0];
                position = _obstaclesRayPosition[0];
            }
            return _obstaclesRayCast.Length > 0;
        }
        return false;
    }

    #endregion

    #region Get RaycastHit[] By position and direction

    private RaycastHit[] GetAllRayCasts(Vector3 targetPosition, Vector3 direction, float maxDistance = Mathf.Infinity)
    {
        _obstacleRay.origin = targetPosition;
        _obstacleRay.direction = direction;
        return Physics.RaycastAll(_obstacleRay, maxDistance);
    }

    #endregion

    #region Get All Obstacles By RaycastHit[]
  
    private bool GetObstacles(RaycastHit[] raycastHits, out Vector3[] obstaclesPositionRayPoint, out BaseObstacle[] obstacles)
    {
        _obstacleRaycastHit.Clear();
        _obstaclesPositionRayPoint.Clear();
        for (int i = 0; i < raycastHits.Length; i++)
            if (raycastHits[i].collider.tag.Equals(TagManager.OBSTACLE_TAG))
            {
                var colliderObstacle = raycastHits[i].transform.GetComponent<ColliderObstacle>();
                if (colliderObstacle != null)
                {
                    _obstaclesPositionRayPoint.Add(raycastHits[i].point);
                    _obstacleRaycastHit.Add(colliderObstacle.BaseObstacle);
                }
            }
        obstacles = _obstacleRaycastHit.ToArray();
        obstaclesPositionRayPoint = _obstaclesPositionRayPoint.ToArray();
        return obstacles.Length > 0;
    }
    #endregion

    #region Get All world positions

    private bool GetFirstWordPostion(RaycastHit[] raycastHits, string tag, ref Vector3 firstPosition)
    {
        var positions = GetAllWordPostion(raycastHits, tag);
        if (positions.Length > 0)
            firstPosition = positions[0];
        return positions.Length > 0;
    }

    private Vector3[] GetAllWordPostion(RaycastHit[] raycastHits, string tag)
    {
        _planePoints.Clear();
        for (int i = 0; i < raycastHits.Length; i++)
            if (raycastHits[i].collider.tag.Equals(tag))
                _planePoints.Add(raycastHits[i].point);
        return _planePoints.ToArray();
    }

    #endregion
    
    #region Jump path build

    /// <summary>
    /// Build path curve by angle 0 for fall and 1.3 for jump
    /// </summary>
    /// <param name="playerPosition"></param>
    /// <param name="angle"></param>
    /// <returns></returns>
    private List<Vector3> BuildCurveUsingGravity(float speed, float angle, Vector3 forward, Vector3 playerPosition)
    {
        Vector3 nextPosition;
        int step = _curveGravityPoints;
        bool insideObstacle = false;
        _curvePoints.Clear();
        _interpolationCurveProgress = 0;
        while (step-- > 0)
        {
            _interpolationCurveProgress += _accuracyCurveJumpFallStep;
            nextPosition = playerPosition + (Quaternion.LookRotation(forward, Vector3.up) * GetCurvePosition(speed, _interpolationCurveProgress, angle));

            Vector3 newPosition = Vector3.zero;
            if (GetFirstWordPostion(GetAllRayCasts(nextPosition, Vector3.down), TagManager.OBSTACLE_TAG, ref newPosition))
            {
                _curvePoints.Add(nextPosition);
                insideObstacle = true;
            }
            else if (GetFirstWordPostion(GetAllRayCasts(nextPosition, Vector3.down), TagManager.PLANE_TAG, ref newPosition))
            {
                if (insideObstacle)
                {
                    //if we still under obstacle
                    if (GetFirstWordPostion(GetAllRayCasts(nextPosition + Vector3.up * 5, Vector3.down), TagManager.OBSTACLE_TAG, ref newPosition))
                    {
                        return _curvePoints;
                    }
                    else
                        insideObstacle = false;
                }
                else
                    _curvePoints.Add(nextPosition);
            }
            else
            {
                return _curvePoints;
            }
        }
        throw new Exception("Cannot build correct path");
    }

    #endregion

    #region Build curve using gravity

    private Vector3 GetCurvePosition(float speed, float interpolationKoef, float angle)
    {
        _gravityPosition.x = 0;
        _gravityPosition.y = (speed * interpolationKoef * Mathf.Sin(angle) - _gravity * interpolationKoef * interpolationKoef / 2) * Time.deltaTime;
        _gravityPosition.z = speed * interpolationKoef * Mathf.Cos(angle) * Time.deltaTime;
        return _gravityPosition;
    }
    #endregion

    private float RemapDistanceBySpeed(float speed)
    {
        return Utils.Remap(speed, 0.01f, 10, 2f, 10f);
    }

    List<Vector3> testfall = new List<Vector3>();
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < testfall.Count; i++)
            Gizmos.DrawSphere(testfall[i], 0.2f);

        Gizmos.color = Color.green;
        for (int i = 0; i < Path.Count; i++)
            Gizmos.DrawSphere(Path[i], 0.1f);
      
        Gizmos.color = Color.black;
        for (int i = 0; i < _planePoints.Count; i++)
            Gizmos.DrawSphere(_planePoints[i], 0.13f);
        Gizmos.color = Color.black;
        if(_obstaclePoints != null)
        for (int i = 0; i < _obstaclePoints.Length; i++)
            Gizmos.DrawSphere(_obstaclePoints[i], 0.13f);
    }
}
