﻿

#pragma warning disable 649
using UnityEngine;
using System.Collections;
using SKTRX.DTOModel;

namespace SKTRX.PlayerBehaviour.Test
{

    public class PointView : MonoBehaviour
    {

        [SerializeField] private PathPointDTO _pathPointDTO;
        [SerializeField] private MeshRenderer _meshRenderer;

        public void SetPathDTO(PathPointDTO pathPointDTO, float scaleKoef, Material material)
        {
            transform.localScale = new Vector3(0.002f, 0.002f, 0.002f) * scaleKoef;
            _pathPointDTO = pathPointDTO;
            transform.position = pathPointDTO.Point;
            gameObject.SetActive(true);

            if(_meshRenderer == null)
                _meshRenderer = GetComponent<MeshRenderer>();
            if (_meshRenderer != null)
                _meshRenderer.material = material;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}