﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.DTOModel;
using SKTRX.Enums;
using DG.Tweening;

namespace SKTRX.PlayerBehaviour
{
    public class PhysicsPlayerController : PlayerControllerBase
    {
        [SerializeField] private float _acceleration;
        [SerializeField] private float _speedUpValue;

        [Space]
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Transform _player;

        [SerializeField] private float _maxVelocity = 20f;
        [SerializeField] private float _minVelocity = 0.003f;
        [SerializeField] private float _turningMagnitude = 100;

        [Space]
        [SerializeField] private float _jumpForce;

        [Space]
        [SerializeField] private GameObject _directionGameObject;

        private bool _isGrounded = false;
        private bool _isMoving = false;

        #region IControllerActions

        //protected override void ChangeDirection(ControlDTO data)
        //{
        //    if (_isGrounded || !_isMoving)
        //    {
        //        Quaternion deltaRotation = Quaternion.Euler(data.direction);
        //        _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotation);
        //        _directionGameObject.SetActive(true);
        //    }
        //}

        protected override void Jump()
        {
            Debug.LogError("Jump");

            _isGrounded = false;
            _rigidbody.AddForce(Vector3.up * _jumpForce);
            Debug.LogError("_verticalVelocity " + _rigidbody.velocity.y);
            _directionGameObject.SetActive(false);
        }

        public override void StartMove()
        {
            Debug.LogError("Star move");

            _rigidbody.AddRelativeForce(Vector3.forward * _acceleration);
            _isMoving = true;

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void TurnRight(ControlDTO data)
        {
            Quaternion deltaRotation = Quaternion.Euler(Vector3.up * Time.deltaTime * _turningMagnitude);

            Vector3 prevLocalVelocity = _player.InverseTransformDirection(_rigidbody.velocity);
            Debug.LogError("before " + _player.eulerAngles.y.ToString("G4"));

            _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotation);

            Debug.LogError("after  " + _player.eulerAngles.y.ToString("G4"));
            _rigidbody.velocity = _player.InverseTransformDirection(prevLocalVelocity);
        }

        protected override void TurnLeft(ControlDTO data)
        {
            Quaternion deltaRotation = Quaternion.Euler(Vector3.down * Time.deltaTime * _turningMagnitude);
            _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotation);
        }

        protected override void MoveTowards(ControlDTO data)
        {
            Debug.LogError("Star MoveTovards" + data.pointToMove.Length);

            Vector3 targetForwardPoint = new Vector3(data.pointToMove[0].x, _rigidbody.position.y, data.pointToMove[0].z);

            _rigidbody.DOPath(SetPathY(data.pointToMove), _acceleration * 2, DG.Tweening.PathType.CatmullRom, PathMode.Sidescroller2D)
                .SetSpeedBased()
                .SetEase(Ease.InOutCubic)
                .OnComplete(delegate
                {
                    StopMove();
                });

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void MoveWithSpeed(ControlDTO data)
        {
            StartMove();
            Debug.LogError("Star Move With Speed" + _acceleration);
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void SetDirection(ControlDTO data)
        {
            if (_isGrounded || !_isMoving)
            {
                float angle = Mathf.Atan2(data.direction.x, data.direction.y) * Mathf.Rad2Deg;
                Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
                _rigidbody.MoveRotation(rot);
                _directionGameObject.SetActive(true);
            }
        }

        protected override void SpeedDown()
        {
            Debug.LogError("SpeedDown");

            Vector3 forceVector = _player.TransformDirection(_player.forward * -1 * _acceleration * Time.deltaTime);
            _rigidbody.AddForce(forceVector);

            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void SpeedUp()
        {
            Debug.LogError("SpeedUp");

            Vector3 forceVector = _player.TransformDirection(_player.forward * -1 * _acceleration * Time.deltaTime);
            _rigidbody.AddForce(forceVector);

            _isMoving = true;
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);
        }

        protected override void StopAction()
        {
            Debug.LogError("SpeedDown");
            StartCoroutine(StopByTime());
        }

        private Vector3[] SetPathY(Vector3[] path)
        {
            Vector3[] res = new Vector3[path.Length];
            for (int i = 0; i < res.Length; i++)
            {
                res[i] = new Vector3(path[i].x, _rigidbody.position.y + 0.003f, path[i].z);
            }
            return res;
        }

        private IEnumerator StopByTime()
        {
            _currentController.ChangePlayerState(PlayerState.Moving);
            _directionGameObject.SetActive(false);

            Vector3 forceVector = _player.TransformDirection(_player.forward * -1 * _acceleration * Time.deltaTime);

            while (_player.InverseTransformDirection(_rigidbody.velocity).z > _minVelocity)
            {
                yield return new WaitForFixedUpdate();
                _rigidbody.velocity += forceVector;
            }

            StopMove();
        }

        protected override void UseFoot()
        {
            throw new System.NotImplementedException();
        }

        protected override void UseHand()
        {
            throw new System.NotImplementedException();
        }

        protected override void UseRail()
        {
            throw new System.NotImplementedException();
        }

        protected override void PerformTrick(ControlDTO data)
        {
            throw new System.NotImplementedException();
        }

        protected void StopMove()
        {
            _currentController.StartControl();
            _currentController.ChangePlayerState(PlayerState.Stopping);
            _isMoving = false;
            _rigidbody.velocity = Vector3.zero;
        }

        protected override void Turn(ControlDTO data)
        {
            throw new System.NotImplementedException();
        }

        protected override void SetOppositeDirection(ControlDTO data)
        {
            throw new System.NotImplementedException();
        }

        protected override void Forward()
        {
            throw new System.NotImplementedException();
        }

        protected void ResetBoard()
        {
            throw new System.NotImplementedException();
        }

        public override void ResetAll()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}