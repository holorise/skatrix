﻿
#pragma warning disable 649
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using SKTRX.DTOModel;
using SKTRX.Enums;
using SKTRX.PlayerBehaviour;
using SKTRX.PlayerBehaviour.Path;
using SKTRX.Spline;
using Tools;
using UnityEngine;

public class CustomPathPhysics : PlayerControllerBase
{
    [Header("PHYSICS_SETTINGS")]
    [SerializeField] private float UNITS = 1;
    [SerializeField] private float G = 1f;
    [SerializeField] private float FRICTION = .2f;
    [SerializeField] private float MAX_JUMP_Y_INCLINATION = .2f;

    private const int PATH_RESOLUTION = 2;
    private const float MIN_VELOCITY = 0.005f;
    private const float MAX_VELOCITY_MIN_SCALE = .35f;
    private const float MAX_VELOCITY_MAX_SCALE = 1.7f;
    private const float JUMP_DELAY = 0.3f;
    private const int PRE_JUMP_COUNT_KOEF = 3;
    private const float ACCELERATION_COEF = .25f;

    [Space(10)]
    [Header("CONTROL_SETTINGS")]
    [SerializeField] private float _fixedSteeringForce;
    [SerializeField] private float ACCELERATION_DELAY = .2f;
    [SerializeField] private float MAX_STEERING_INPUT = 0.2f;
    [SerializeField] private float REBUILD_PROGRESS_AMOUNT = .5f;
    [SerializeField] private float JUMP_LAND_PROGRESS = .4f;
    [SerializeField] private float LAND_ON_RAIL_PROGRESS = .8f;
    [SerializeField] private float RAIL_PROGRESS = .8f;
    [SerializeField] private float _verticalDrag = 1f;
    [SerializeField] private int _pathResolution = 3;


    [Space(10)]

    [SerializeField] private GameObject _directionGameObject;
    [SerializeField] private PlayerPathManager _pathManager = null;
    [SerializeField] private PlayerAnimation _playerAnimation = null;
    [SerializeField] private bool _drawPathSpline = true;

    private float _skateSize;
    private float _velocity = 0;
    private float _verticalVelocity;
    private float _verticalDirection;
    private float _accelertion;
    private float _maxVelocity;
    private float _segmentStartVelocity;
    private float _steeringInput;

    private bool _isGrounded = true;
    private RailState _railState;
    private JumpState _jumpState;

    private bool _isTurning = false;

    private PlayerState _playerState;
    private Vector3 _movementForward;

    private float _passedDistance;
    private float _curSegmentProgress;
    private int _preJumpPoints;

    private PlayerPath _path;
    private SplinePoint _targetMovementPoint;

    private Coroutine _jumpChangeStateCoroutine = null;
    private Coroutine _railChangeStateCoroutine = null;

    public PlayerDTO GetDTO
    {
        get
        {
            return new PlayerDTO(
                jumpPoints: _preJumpPoints,
                vel: Utils.Remap(_velocity, MIN_VELOCITY, _maxVelocity, MIN_VELOCITY, MAX_VELOCITY_MIN_SCALE),
                maxVel: _maxVelocity,
                steer: _steeringInput,
                skateSize: transform.localScale.x,
                pos: transform.position,
                moveForward: transform.forward
                );
        }
    }

    private void OnEnable()
    {
        //    _playerAnimation.GetAnimatorBehaviour().PlayerAccelerated += SpeedUp;
    }


    //private void OnGUI()
    //{
    //    if (GUI.Button(new Rect(0, 0, 100, 100), "Build forward"))
    //    {
    //        _pathManager.BuildPath(GetFakeDTO, PathType.Forward);
    //    }
    //    if (GUI.Button(new Rect(0, 100, 100, 100), "Build jump"))
    //    {
    //        _pathManager.BuildPath(GetFakeDTO, PathType.Jump);
    //    }
    //}

    [SerializeField] private float turnangle = 0;
    [SerializeField] private float velocityFake = 0;
    public PlayerDTO GetFakeDTO
    {
        get
        {
            return new PlayerDTO(
                jumpPoints: _preJumpPoints,
                vel: velocityFake,
                maxVel: _maxVelocity,
                steer: turnangle,
                skateSize: transform.localScale.x,
                pos: transform.position,
                moveForward: transform.forward
                );
        }
    }

    public override void ScaleUp()
    {
        base.ScaleUp();
        UpdateScaleCoefcients();
    }

    public override void ScaleDown()
    {
        base.ScaleDown();
        UpdateScaleCoefcients();
    }

    [ContextMenu("move")]
    public override void StartMove()
    {
        Debug.LogWarning("StartMove");
        SetGrounded(true);
        Accelerate();
        ChangePlayerState(PlayerState.Moving);
        RebuildPath(PathType.Forward);
        _playerAnimation.SpeedUp(GetDTO);
        _directionGameObject.SetActive(false);
    }

    protected override void Forward()
    {
        Debug.LogWarning("Forward");
        if (_isGrounded)
        {
            _steeringInput = 0;
            RebuildPath(PathType.Forward);
            _playerAnimation.UpdateTurn(GetDTO);
            _isTurning = false;
        }
    }

    protected override void PerformTrick(ControlDTO data)
    {
        if (_isGrounded && _playerState.Equals(PlayerState.Moving) && _jumpState.Equals(JumpState.None))
        {
            if (data.trickType.Equals(TrickType.JumpType))
            {
                _playerAnimation.SetRailTrick(2);
                JumpTrick(data.trickNumber);
            }
            else if (data.trickType.Equals(TrickType.Rail))
            {
                _playerAnimation.SetRailTrick(2/*data.trickNumber*/);
                JumpTrick(2);
            }
            else if (data.trickType.Equals(TrickType.Manual))
            {
                _playerAnimation.ManualTrick();
            }
        }
    }

    private void Crouch()
    {
        Debug.LogError("Crouch");
        _playerAnimation.Crouch();
    }

    private void JumpTrick(int trickIndex)
    {
        if (_isGrounded && Math.Abs(_movementForward.y) < MAX_JUMP_Y_INCLINATION)
        {
            Debug.LogError("Jump" + trickIndex);
            _playerAnimation.SetupJumpAnimationParam(GetDTO, trickIndex);
            RebuildPath(PathType.Jump);
        }
        //else
        //{
        //    _playerAnimation.UnCrouch();
        //}
    }

    protected override void MoveTowards(ControlDTO data)
    {
        Debug.LogWarning("MoveTowards");
        ChangePlayerState(PlayerState.Moving);
        RebuildPath(PathType.Forward);
    }

    protected override void MoveWithSpeed(ControlDTO data)
    {
        Debug.LogWarning("MoveWithSpeed");
        Accelerate();
        SetGrounded(true);
        _playerAnimation.ResetAll();
        ChangePlayerState(PlayerState.Moving);
        RebuildPath(PathType.Forward);
        _directionGameObject.SetActive(false);
        _playerAnimation.MoveForvard(GetDTO);
    }

    protected override void SetDirection(ControlDTO data)
    {
        Debug.LogWarning("SetDirection");
        float angle = Mathf.Atan2(data.direction.x, data.direction.y) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
        transform.rotation = rot;
        _directionGameObject.SetActive(true);
    }

    protected override void SetOppositeDirection(ControlDTO data)
    {
        Debug.LogWarning("SetDirection");
        SetForward(data.direction3);
        _directionGameObject.SetActive(true);
    }

    protected override void SpeedDown()
    {
        if (_isGrounded && _jumpState.Equals(JumpState.None) && _railState.Equals(RailState.None))
        {
            Debug.LogWarning("SpeedDown");
            Decelerate();
            ChangePlayerState(PlayerState.Moving);
            RebuildPath(PathType.Forward);
            _playerAnimation.SpeedDown(GetDTO);
        }
    }

    protected override void SpeedUp()
    {
        if (_isGrounded && _jumpState.Equals(JumpState.None) && _railState.Equals(RailState.None))
        {
            Debug.LogWarning("SpeedUp");
            Accelerate();
            ChangePlayerState(PlayerState.Moving);
            RebuildPath(PathType.Forward);
            _playerAnimation.SpeedUp(GetDTO);
            _directionGameObject.SetActive(false);
        }
    }

    protected override void StopAction()
    {
        Debug.LogWarning("StopAction");

        SetVelocity(0);
        _steeringInput = 0;
        SetGrounded(true);
        SetRailState(RailState.None);
        SetJumpState(JumpState.None);
        _isTurning = false;

        ChangePlayerState(PlayerState.Standing);
        _currentController.StartControl();
    }

    protected override void Turn(ControlDTO data)
    {
        //  throw new System.NotImplementedException();
    }

    protected override void TurnLeft(ControlDTO data)
    {
        if (_isGrounded && _playerState.Equals(PlayerState.Moving))
        {
            if (data.isStaticTurn && !_isTurning)
            {
                Debug.LogWarning("TurnLeft");
                _isTurning = true;
                _steeringInput = -_fixedSteeringForce;
                RebuildPath(PathType.Turn);
                _playerAnimation.UpdateTurn(GetDTO);
            }
        }
    }

    protected override void TurnRight(ControlDTO data)
    {
        if (_isGrounded && _playerState.Equals(PlayerState.Moving))
        {
            if (data.isStaticTurn && !_isTurning)
            {
                Debug.LogWarning("TurnRight");
                _isTurning = true;
                _steeringInput = +_fixedSteeringForce;
                RebuildPath(PathType.Turn);
                _playerAnimation.UpdateTurn(GetDTO);
            }
        }
    }

    protected override void UseFoot()
    {
        //
    }

    protected override void UseHand()
    {
        //
    }

    protected override void UseRail()
    {
        //
    }

    private void JumpOnTheRail()
    {
        Debug.LogError("JumpOnTheRailStart");
        _playerAnimation.JumpOnRailStart();
    }

    private void LandOnTheRail()
    {
        Debug.LogError("LandOnTheRail");
        _playerAnimation.JumpOnRailEndLand();
    }

    private void JumpFromRail()
    {
        Debug.LogError("JumpFromRail");
        _playerAnimation.JumpFromRailOnPlane();
    }

    private void LandFromRail()
    {
        _playerAnimation.JumpFromRailOnPlaneLand();
    }

    private IEnumerator JumpCoroutine()
    {
        Crouch();
        float delay = JUMP_DELAY;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }
        SetGrounded(false);
        RebuildPath(PathType.Jump);
        SetGrounded(false);

        yield return null;
    }

    #region MovementLogics

    private void SetVelocity(float newVelocity)
    {
        _velocity = newVelocity;
    }

    private void SetGrounded(bool grounded)
    {
        Debug.LogWarning("set gorunded: " + grounded);
        _isGrounded = grounded;
    }

    private void SetForward(Vector3 newForward, bool debug = false)
    {
        if (debug)
        {
            Debug.LogWarning(newForward.ToString("F10"));
        }

        _movementForward = newForward;
        transform.forward = _movementForward;
    }

    private void Accelerate()
    {
        //if(_isGrounded)
        //{
        //StartCoroutine(AccelerationCoroutine());
        //}
        SetVelocity(Mathf.Clamp(_velocity + _accelertion, MIN_VELOCITY, _maxVelocity));
    }

    private void Decelerate()
    {
        SetVelocity(Mathf.Clamp(_velocity - _accelertion, MIN_VELOCITY, _maxVelocity));
    }

    private IEnumerator AccelerationCoroutine()
    {
        float delay = ACCELERATION_DELAY;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }
        SetVelocity(_velocity + _accelertion);
    }

    private void UpdateScaleCoefcients()
    {
        float scaleProgressNormalized = Utils.Remap(transform.localScale.x, _scaleMin, _scaleMax, 0, 1);
        //SetVelocity(Utils.Remap(_oldVelocityNormalized, 0, 1, MAX_VELOCITY_MIN_SCALE, MAX_VELOCITY_MAX_SCALE));
        _maxVelocity = Utils.Remap(scaleProgressNormalized, 0, 1, MAX_VELOCITY_MIN_SCALE, MAX_VELOCITY_MAX_SCALE);
        _accelertion = ACCELERATION_COEF * _maxVelocity * (1 + scaleProgressNormalized);
        SetVelocity(0);
    }

    private void MovePlayer()
    {
        _passedDistance += _velocity * Time.deltaTime;
        _targetMovementPoint = new SplinePoint();

        if (_path.IsPathLongEnoghForDistance(_passedDistance, _isGrounded, 1))
        {
            bool isGroundedNew;
            _targetMovementPoint = _path.GetPointAndApply(ref _passedDistance, _isGrounded, out isGroundedNew);
            SetGrounded(isGroundedNew);
        }

        else
        {
            Debug.LogWarning("prev _targetMovementPoint progress: " + _targetMovementPoint.progress);

            float pathleftover = _path.CurSegment.full3DPath.Length * (1 - _targetMovementPoint.progress);
            float curTheoreticalProgres = _targetMovementPoint.progress + (pathleftover / _path.CurSegment.full3DPath.Length);

            Debug.LogWarning("cur theoretical _targetMovementPoint progress: " + curTheoreticalProgres);

            Debug.LogWarning("Path is too short for last deltaTime");
            Debug.LogWarning("_passedDistance: " + _passedDistance);

            Debug.LogWarning("Path length 3d: " + _path.Length3D);
            Debug.LogWarning("Path length 2d: " + _path.Length2D);

            Debug.LogWarning("Path points count: " + _path.controlPoints.Count);


            _pathManager.RebuildPath();

            ApplyPathCollision();
            _playerAnimation.ResetAll();
            //StopAction();
            return;
        }
        _curSegmentProgress = _targetMovementPoint.progress;

        CallRailAnimationEvents();

        if (_isGrounded)
        {
            //TODO think about forward lerp
            SetForwardByPath();
            CalculateGroundedPhysicsValues();
            if (IsStopedOrTurnAroundIfSpeedIsTooLow())
            {
                return;
            }
        }

        if (_railState.Equals(RailState.None))
        {
            CallJumpAnimationEvents();
        }

        transform.position = _targetMovementPoint.position;

        CheckPathProgressForRebuild();
    }

    private void CheckForSimpleLanding()
    {
        //TODO CALCULATE LANDING TIME
        float curLandingProgress = .9f;
        if (_curSegmentProgress > curLandingProgress)
        {
            _playerAnimation.JumpEnd(GetDTO);
        }
    }

    private void CheckPathProgressForRebuild()
    {
        if (_path.IsPathPassible
            && _curSegmentProgress > REBUILD_PROGRESS_AMOUNT
            && _path.IsMovingOnLastSegment()
        )
        {
            RebuildPath(PathType.Forward);
        }
    }

    private void SetForwardByPath()
    {
        SetForward(_targetMovementPoint.tangent);
    }

    public override void ResetAll()
    {
        _playerAnimation.ResetAll();
    }

    private void ApplyPathCollision()
    {
        Debug.LogWarning("Collision");
        SetForward(transform.forward *= -1);

        //_velocity *= .5f;// * _path.controlPoints.GetLast().HitAngle;
        //_movementForward = Quaternion.AngleAxis(90 + _path.PathEndBouncingAngle, Vector3.up) * _movementForward;
        SetGrounded(true);
        RebuildPath(PathType.Forward);
    }

    private void CallRailAnimationEvents()
    {
        switch (_railState)
        {
            case RailState.None:
            case RailState.InTransition:
                return;
            case RailState.ApproachingRail:
                //TODO CALCULATE LANDING TIME
                float railApproachingProgress = .9f;
                if (_curSegmentProgress > railApproachingProgress || _path.CurSegment.segmentType.Equals(PathPointType.Jump))
                {
                    SetRailState(RailState.None);
                    JumpOnTheRail();
                    MoveToRailStateOnSegmentChange(RailState.JumpOnRail);
                }
                break;

            case RailState.JumpOnRail:
                //TODO CALCULATE LANDING TIME
                float curLandingProgress = .4f;
                if (_curSegmentProgress > curLandingProgress 
                    || _path.CurSegment.segmentType.Equals(PathPointType.Rail))
                {
                    SetRailState(RailState.InTransition);
                    LandOnTheRail();
                    MoveToRailStateOnSegmentChange(RailState.LandOnRail);
                }
                break;
            case RailState.LandOnRail:
                if (_isGrounded || _path.CurSegment.segmentType.Equals(PathPointType.Rail))
                {
                    SetRailState(RailState.Rail);
                }
                break;
            case RailState.Rail:
                //TODO CALCULATE LANDING TIME
                float curRailEscapeProgress = .5f;
                if (_curSegmentProgress > curRailEscapeProgress 
                    || _path.CurSegment.segmentType.Equals(PathPointType.Fall) 
                    || _path.CurSegment.segmentType.Equals(PathPointType.Plane))
                {
                    JumpFromRail();
                    SetRailState(RailState.JumpFromRail);
                }
                break;
            case RailState.JumpFromRail:
                //TODO CALCULATE LANDING TIME
                float curEscapeLandingProgress = .9f;
                if (_curSegmentProgress > curEscapeLandingProgress 
                    || _path.CurSegment.segmentType.Equals(PathPointType.Fall)
                    || _path.CurSegment.segmentType.Equals(PathPointType.Plane)
                    )
                {
                    SetRailState(RailState.None);
                    //SetJumpState(JumpState.InJump);
                    LandFromRail();
                }
                break;
        }
    }

    private void MoveToRailStateOnSegmentChange(RailState state)
    {
        if (_railChangeStateCoroutine != null)
        {
            StopCoroutine(_railChangeStateCoroutine);
        }
        _railChangeStateCoroutine = StartCoroutine(ChangeRailsState(state));
    }

    private void MoveToJumpStateOnSegmentChange(JumpState state)
    {
        if (_jumpChangeStateCoroutine != null)
        {
            StopCoroutine(_jumpChangeStateCoroutine);
        }
        _jumpChangeStateCoroutine = StartCoroutine(ChangeJumpState(state));
    }

    private IEnumerator ChangeJumpState(JumpState state)
    {
        PathSegment curSegment = _path.CurSegment;
        while (_path.CurSegment.Equals(curSegment))
        {
            yield return new WaitForEndOfFrame();
        }
        SetJumpState(state);
    }

    private IEnumerator ChangeRailsState(RailState state)
    {
        PathSegment curSegment = _path.CurSegment;
        while (_path.CurSegment.Equals(curSegment))
        {
            yield return new WaitForEndOfFrame();
        }
        SetRailState(state);
    }

    private void CallJumpAnimationEvents()
    {
        switch (_jumpState)
        {
            case JumpState.None:
                break;
            case JumpState.Approaching:
                //TODO CALCULATE JumpTime TIME
                float approachingProgress = GetProgressKoef();
                var currentSegmentState = _path.CurSegment.segmentType;
                if (currentSegmentState == PathPointType.Jump)
                {
                    _playerAnimation.JumStart();
                    SetJumpState(JumpState.InJump);
                }
                if (_curSegmentProgress > approachingProgress)
                {
                    _playerAnimation.JumStart();
                    SetJumpState(JumpState.None);
                    MoveToJumpStateOnSegmentChange(JumpState.InJump);
                }
                break;
            case JumpState.InJump:
                //TODO CALCULATE Land TIME
                float jumpProgress = GetProgressKoef();
                if (_curSegmentProgress > jumpProgress)
                {
                    _playerAnimation.JumpEnd(GetDTO);
                    SetJumpState(JumpState.None);
                }
                break;
            default:
                break;
        }
    }

    private int GetPreJumpPointsCount()
    {
        //Debug.LogError(_maxVelocity + " - maxVelocity");
        var oldRange = _maxVelocity - MIN_VELOCITY;
        var maxDots = PRE_JUMP_COUNT_KOEF / _maxVelocity;
        var newRange = maxDots - 0;
        //Debug.LogError(newRange + " - newRange");

        var count = 1;
        var delta = _velocity;
        var newValue = (((delta - MIN_VELOCITY) * newRange) / oldRange) + 0;

        //Debug.LogError("newValue - " + newValue);
        //var clampedDelta = Mathf.Clamp(delta, 1, 5);
        //Debug.LogError("delta - " + delta);

        return count + (int)newValue;
    }

    private float GetProgressKoef()
    {
        var koef = 1f;
        var delta = _velocity - MIN_VELOCITY;
        var clampedDelta = Mathf.Clamp(delta, MIN_VELOCITY, 0.5f);
        koef = 1 - clampedDelta;
        return koef;
    }

    private bool IsStopedOrTurnAroundIfSpeedIsTooLow()
    {
        if (_verticalDirection > 0)
        {
            if (_velocity <= 0)
            {
                SetForward(-_movementForward);
                RebuildPath(PathType.Forward);
                Debug.LogWarning("Going up too slow. Turning around (vertical direction: " + _verticalDirection + ") (_velocity: " + _velocity + ")");
                return true;
            }
        }

        else if (_velocity < MIN_VELOCITY && _path.CurSegment.segmentType.Equals(PathPointType.Plane))
        {
            SetVelocity(0);
            ChangePlayerState(PlayerState.Standing);
            return true;
        }
        return false;
    }

    private void CalculateGroundedPhysicsValues()
    {
        _verticalDirection = _movementForward.y;
        float deltaDistance = Vector3.Distance(transform.position, _targetMovementPoint.position);

        float deltaVelocity = UNITS * G * Time.deltaTime * -_verticalDirection;

        float deltaFriction = 0;

#if !UNITY_EDITOR
        if (_railState.Equals(RailState.None))
        {
            deltaFriction = UNITS * FRICTION * Time.deltaTime * deltaDistance;
        }
#endif
        if (_railState.Equals(RailState.None))
        {
            SetVelocity(_velocity + deltaVelocity - deltaFriction);
            SetVelocity(Mathf.Clamp(_velocity, 0, _maxVelocity));
        }
    }

    private void SetRailState(RailState state)
    {
        _railState = state;
    }

    private void SetJumpState(JumpState state)
    {
        _jumpState = state;
    }

    private void ChangePlayerState(PlayerState state)
    {
        _playerState = state;
        _currentController.ChangePlayerState(state);
    }

    private void RebuildPath(PathType type)
    {
        _preJumpPoints = GetPreJumpPointsCount();
        Debug.LogWarning("Rebuild Path " + type);

        GetDTO.DebugData();
        _pathManager.BuildPath(GetDTO, type);

        if (_pathManager.Path.Count == 0)
        {
            throw new Exception("Recieved Empty Path");
        }

        _path = new PlayerPath();
        _path.UpdatePath(_pathManager.Path, GetDTO);

        bool isPathUpdated = _path.UpdatePath(_pathManager.Path, GetDTO);
        Debug.LogWarning("_path length : " + _path.Length3D);
        if (!isPathUpdated)
        {
            ApplyPathCollision();
            return;
        }

        _passedDistance = 0;
        _curSegmentProgress = 0;
        SetRailState(RailState.None);
        SetJumpState(JumpState.None);
        _segmentStartVelocity = _velocity;

        if (type.Equals(PathType.Jump))
        {
            //if (_path.IsPathPassible)
            //{
            Crouch();
            SetGrounded(false);
            if (!IsRailDetected())
            {
                SetJumpState(JumpState.Approaching);
            }
            //}
        }
    }

    #endregion

    #region MonoBehaviour
    private bool IsRailNextSegment()
    {
        return _path.GetSegment(1) != null && _path.GetSegment(1).segmentType.Equals(PathPointType.Rail);
    }

    private bool IsRailWithPlane()
    {
        return _path.GetSegment(2) != null && _path.GetSegment(2).segmentType.Equals(PathPointType.Rail);
    }

    private bool IsRailDetected()
    {
        bool result = false;

        if (IsRailNextSegment())
        {
            SetGrounded(false);
            SetRailState(RailState.JumpOnRail);
            JumpOnTheRail();
            result = true;
            Debug.LogWarning("Rail detected: IsRailNextSegment");
        }

        else if (IsRailWithPlane())
        {
            SetRailState(RailState.ApproachingRail);
            result = true;
            Debug.LogWarning("Rail detected: IsRailWithPlane");
        }
        return result;
    }

    private void Awake()
    {
        Application.targetFrameRate = 25;
        UpdateScaleCoefcients();
    }

    private void Update()
    {
        if (_playerState == PlayerState.Moving)
        {
#if UNITY_EDITOR
            if (_drawPathSpline)
            {
                if (_path != null)
                {
                    _path.DrawPath();
                    _path.DrawTangents();
                }
            }
#endif
            MovePlayer();
        }
    }

    protected override void Jump()
    {
    }

    [Serializable]
    private class PlayerPath
    {
        private const float PASSABLE_HEIGHT_COEF = 0.3f;

        public List<PathPointDTO> controlPoints = new List<PathPointDTO>();
        public List<PathSegment> allFull3DSegments = new List<PathSegment>();

        private bool _isPathPasiible;
        private PathSegment _curSegment;
        private float _passableHeight;

        private Vector3 _pathEndPosition;
        private Vector3 _pathEndDirection;
        private float _pathBouncingAngle;

        public PathSegment GetSegment(int segmentIndex)
        {
            if (allFull3DSegments.Count > segmentIndex)
            {
                return allFull3DSegments[segmentIndex];
            }
            return null;
        }

        public PathSegment CurSegment { get => _curSegment; }
        public bool IsPathPassible { get => _isPathPasiible; }
        public Vector3 PathEndPosition { get => _pathEndPosition; }
        public Vector3 PathEndDirection { get => _pathEndDirection; }
        public float PathEndBouncingAngle { get => _pathBouncingAngle; }

        public float Length2D
        {
            get
            {
                float length = 0;
                foreach (PathSegment segment in allFull3DSegments)
                {
                    length += segment.horizontalPath.Length;
                }
                return length;
            }
        }

        public float Length3D
        {
            get
            {
                float length = 0;
                foreach (PathSegment segment in allFull3DSegments)
                {
                    length += segment.full3DPath.Length;
                }
                return length;
            }
        }
        /// <summary>
        /// The get next segment.
        /// </summary>
        public PathSegment GetNextSegment()
        {
            if (_curSegment == null) return null;

            int nextIndex = allFull3DSegments.IndexOf(_curSegment) + 1;
            if (allFull3DSegments.Count > nextIndex)
            {
                return allFull3DSegments[nextIndex];
            }
            return null;
        }

        /// <summary>
        /// Cuts the path if it is impassable.
        /// </summary>
        private void CutPathIfImpassable()
        {
            PathPointType prevPointType;
            Vector3 prevPointPosition;
            int passablePathIndex = 0;
            _isPathPasiible = false;

            for (int i = 1; i < controlPoints.Count; i++)
            {
                prevPointType = controlPoints[i - 1].PathPointType;
                prevPointPosition = controlPoints[i - 1].Point;

                if (!controlPoints[i].PathPointType.Equals(prevPointType))
                {
                    if
                    (prevPointType.Equals(PathPointType.Plane)
                        && controlPoints[i].PathPointType.Equals(PathPointType.Obstacle)
                        && Mathf.Abs(controlPoints[i].Point.y - prevPointPosition.y) > _passableHeight
                        || (controlPoints[i].PathPointType.Equals(PathPointType.Jump) && i == controlPoints.Count - 1)
                    )
                    {
                        break;
                    }
                }
                passablePathIndex = i;
            }

            if (passablePathIndex < controlPoints.Count - 1)
            {
                Debug.LogWarning("Path has an impassable obstacle on its way");
                Debug.LogWarning("path count:" + controlPoints.Count);

                //cut the redundant path points
                controlPoints = controlPoints.ClearFrom(passablePathIndex);

                controlPoints[controlPoints.Count - 1] = new PathPointDTO
                    (
                        HotFixShiftLastPointBack(controlPoints[controlPoints.Count - 1].Point, controlPoints[controlPoints.Count - 2].Point),
                        controlPoints[controlPoints.Count - 1].PathPointType
                    );
                //defines path end params for bouncing angle
                //CalculateBouncingAngle();
            }

            else if (controlPoints[controlPoints.Count - 1].PathPointType.Equals(PathPointType.Jump))
            {
                //TODO MOVE this hot fix into path manager
                Debug.LogError("removing second from end jump point");
                controlPoints.RemoveAt(controlPoints.Count - 2);
                //
                Debug.LogError("Path has an impassable obstacle on its way. Bouncing in the air");
                Debug.LogWarning("path count:" + controlPoints.Count);

            }

            else if (controlPoints[controlPoints.Count - 1].PathPointType.Equals(PathPointType.Plane)
                && controlPoints[controlPoints.Count - 1].PathPointJumpType.Equals(PathPointType.Fall)
                )
            {
                Debug.LogError("Path has an impassable boudarie on its way");
            }

            else
            {
                _isPathPasiible = true;
            }
        }

        private Vector3 HotFixShiftLastPointBack(Vector3 lastPoint, Vector3 prevPoint)
        {
            Vector3 direction = prevPoint - lastPoint;
            return lastPoint + (direction.normalized * Vector3.Distance(lastPoint, prevPoint) * .8f);
        }

        /// <summary>
        /// Calculates the bouncing angle.
        /// </summary>
        private void CalculateBouncingAngle()
        {
            _pathEndPosition = controlPoints.GetLast().Point;
            _pathEndDirection = controlPoints.GetLast().Point - controlPoints[controlPoints.Count - 2].Point;

            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(_pathEndPosition, _pathEndDirection, out hit))
            {
                if (hit.collider != null)
                {
                    _pathBouncingAngle = Utils.GetIntersectionAngle(_pathEndPosition, _pathEndDirection, hit.collider);
                }
            }
        }

        /// <summary>
        /// Devides the control points into typed segments.
        /// </summary>
        private void DevidePointsIntoSegments()
        {
            //cur iteration values
            PathPointType curSegmType = controlPoints[0].PathPointType;
            List<Vector3> curSegm = new List<Vector3>();
            Dictionary<List<Vector3>, PathPointType> segmentsPointsDict = new Dictionary<List<Vector3>, PathPointType>();
            segmentsPointsDict.Clear();
            allFull3DSegments.Clear();

            foreach (PathPointDTO point in controlPoints)
            {
                if (!point.PathPointType.Equals(curSegmType))
                {
                    if (point.PathPointType.Equals(PathPointType.Rail))
                    {
                        curSegm.Add(point.Point);
                    }
                    segmentsPointsDict.Add(curSegm.CopyList(), curSegmType);
                    curSegmType = point.PathPointType;
                    curSegm.Clear();
                }
                curSegm.Add(point.Point);
            }

            if (!segmentsPointsDict.ContainsKey(curSegm) && curSegm.Count > 0)
            {
                segmentsPointsDict.Add(curSegm, curSegmType);
            }

            ClearStraightPlainPoints(segmentsPointsDict);

            bool canAddSegments = PrepareSegmentsToBuildSpline(segmentsPointsDict.Select(p => p.Key).ToList());

            if (!canAddSegments)
            {
                Debug.LogError("NOT ADDING EMPTY SEGMENTS");
                return;
            }

            foreach (KeyValuePair<List<Vector3>, PathPointType> segment in segmentsPointsDict)
            {
                allFull3DSegments.Add(new PathSegment(segment.Key.ToArray(), segment.Value));
            }
        }

        /// <summary>
        /// Clears the straight plain points.
        /// </summary>
        private void ClearStraightPlainPoints(Dictionary<List<Vector3>, PathPointType> dict)
        {
            foreach (KeyValuePair<List<Vector3>, PathPointType> segment in dict)
            {
                if (
                       segment.Value.Equals(PathPointType.Plane)
                    && segment.Key.Count > 2
                    && IsSegmentStraight(segment.Key)
                    )
                {
                    for (int i = 1; i < segment.Key.Count - 1;)
                    {
                        segment.Key.RemoveAt(i);
                    }
                }
            }
        }

        private bool IsSegmentStraight(List<Vector3> segment)
        {
            return Vector3.Angle((segment[1] - segment[0]).normalized, (segment.GetLast() - segment[segment.Count - 2]).normalized) < 4;
        }

        /// <summary>
        /// Prepares the segments to build spline on each. Fullfills the segment to a minimum of 4 points
        /// </summary>
        /// <param name="segments">Segments.</param>
        private bool PrepareSegmentsToBuildSpline(List<List<Vector3>> segments)
        {
            var canAddSegment = true;
            for (int i = 0; i < segments.Count; i++)
            {
                var currentSegment = segments[i];
                if (currentSegment.Count == 1)
                {
                    //var isLast = i == segments.Count - 1;
                    bool isFirst = i == 0;
                    int index = isFirst ? i + 1 : i - 1;
                    if (segments.Count == 1)
                    {
                        Debug.LogError("1 in 1");
                        segments.RemoveAt(i);
                        i--;
                        canAddSegment = false;
                        continue;
                    }
                    List<Vector3> segmentToGet = segments[index];
                    Vector3 nextPoint = new Vector3();
                    if (isFirst)
                    {
                        nextPoint = segmentToGet[0];
                        currentSegment.Add(nextPoint);
                    }
                    else
                    {
                        nextPoint = segmentToGet.Last();
                        currentSegment.Insert(0, nextPoint);
                    }
                }

                AddSegmentHeadAndTail(segments[i]);
            }
            return canAddSegment;
        }
        /// <summary>
        /// Expands 1 the points case.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="allSegments">All segments.</param>
        private void Expand1PointsCase(int index, List<List<Vector3>> allSegments)
        {
            List<Vector3> segment = allSegments[index];
            if (allSegments.Count > 1)
            {
                if (index > 0)
                {
                    Vector3 helperPoint = allSegments[index - 1][allSegments[index - 1].Count - 1];
                    segment.Insert(0, new Vector3(helperPoint.x, segment[0].y, helperPoint.z));
                }
                else
                {
                    Vector3 helperPoint = allSegments[index + 1][allSegments[index + 1].Count - 1];
                    segment.Add(new Vector3(helperPoint.x, segment[0].y, helperPoint.z));
                }
            }
            else throw new Exception("Invalid path points from Path Manager");
        }

        /// <summary>
        /// Adds segments head and tail.
        /// </summary>
        /// <param name="segment">Segment.</param>
        private void AddSegmentHeadAndTail(List<Vector3> segment)
        {
            segment.Add(GetNextPointInRow(segment[segment.Count - 2], segment[segment.Count - 1]));
            segment.Insert(0, GetNextPointInRow(segment[1], segment[0]));
        }

        private Vector3 GetNextPointInRow(Vector3 StartPoint, Vector3 EndPoint)
        {
            Vector3 direction = EndPoint - StartPoint;
            float dist = Vector3.Distance(StartPoint, EndPoint);
            Vector3 finalDirection = direction + direction.normalized * dist;

            return EndPoint + finalDirection;
        }

        /// <summary>
        /// Says if the path is long enogh to pass a cpecific distance.
        /// </summary>
        /// <returns><c>true</c>, if path long enogh for distance was ised, <c>false</c> otherwise.</returns>
        /// <param name="distance">Distance.</param>
        /// <param name="isGrounded">If set to <c>true</c> is grounded.</param>
        public bool IsPathLongEnoghForDistance(float distance, bool isGrounded, float curProgress)
        {
            float leftPathDist = isGrounded
                ? _curSegment.full3DPath.Length * curProgress
                : _curSegment.horizontalPath.Length * curProgress;

            for (int i = allFull3DSegments.IndexOf(_curSegment) + 1; i < allFull3DSegments.Count; i++)
            {
                leftPathDist += allFull3DSegments[i].full3DPath.Length;
            }

            return leftPathDist > distance;
        }

        private void MoveToNextSegment()
        {
            _curSegment = allFull3DSegments[allFull3DSegments.IndexOf(_curSegment) + 1];
        }

        private void SetCurSegmentByDistAndSetDistance(ref float distance, bool isGrounded, out bool isGroundedres)
        {
            float curSegmentLength = isGrounded
                ? CurSegment.full3DPath.Length
                : CurSegment.horizontalPath.Length;

            while (curSegmentLength < distance)
            {
                Debug.LogWarning("change segment");
                distance -= curSegmentLength;

                MoveToNextSegment();

                isGrounded = CurSegment.IsSegmentGrounded;

                curSegmentLength = isGrounded
                ? CurSegment.full3DPath.Length
                : CurSegment.horizontalPath.Length;
            }
            isGroundedres = isGrounded;
        }

        public void DrawPath()
        {
            foreach (PathSegment segment in allFull3DSegments)
            {
                segment.full3DPath.DrawSpline(Color.red);
            }
        }

        public void DrawTangents()
        {
            foreach (PathSegment segment in allFull3DSegments)
            {
                segment.full3DPath.DrawTangent(Color.blue);
            }
        }

        public bool UpdatePath(List<PathPointDTO> points, PlayerDTO playerDTO)
        {
            var isUpdated = true;
            _passableHeight = playerDTO.skateboardSize * PASSABLE_HEIGHT_COEF;

            controlPoints = points.CopyList();

            CutPathIfImpassable();
            DevidePointsIntoSegments();

            if (allFull3DSegments.Count > 0)
            {
                _curSegment = allFull3DSegments[0];
            }
            else
            {
                Debug.LogError("Can't rebuild pass");
                isUpdated = false;
            }

            return isUpdated;
        }

        public SplinePoint GetPointAndApply(ref float distance, bool isGrounded, out bool isGroundedres)
        {
            SetCurSegmentByDistAndSetDistance(ref distance, isGrounded, out isGroundedres);
            if (isGrounded)
            {
                return CurSegment.full3DPath.GetSplinePointByDist(distance);
            }
            else
            {
                SplinePoint horPoint = CurSegment.horizontalPath.GetSplinePointByDist(distance);
                return CurSegment.full3DPath.GetSplinePointByProgress(horPoint.progress);
            }
        }

        public bool IsMovingOnLastSegment()
        {
            return allFull3DSegments.IndexOf(CurSegment).Equals(allFull3DSegments.Count - 1);
        }
    }
    [Serializable]
    private class PathSegment
    {
        private bool _isSegmentGrounded;

        public PathPointType segmentType;

        public CatmullRomVersion2 full3DPath;
        public CatmullRomVersion2 horizontalPath;

        public bool IsSegmentGrounded { get => _isSegmentGrounded; }

        public PathSegment(Vector3[] points, PathPointType type)
        {
            segmentType = type;
            full3DPath = new CatmullRomVersion2(points, PATH_RESOLUTION);

            Vector3[] horisontalPoints = points.Select(p => new Vector3(p.x, points[0].y, p.z)).ToArray();
            horizontalPath = new CatmullRomVersion2(horisontalPoints, PATH_RESOLUTION);

            _isSegmentGrounded = IsCurSegmentOnTheGround();
        }

        private bool IsCurSegmentOnTheGround()
        {
            return segmentType.Equals(PathPointType.Plane) ||
                   segmentType.Equals(PathPointType.Obstacle) ||
                   segmentType.Equals(PathPointType.Rail);
        }


    }
    #endregion

    private enum RailState
    {
        None = 0,
        JumpOnRail = 1,
        LandOnRail = 2,
        Rail = 3,
        JumpFromRail = 4,
        ApproachingRail = 5,
        InTransition = 6,
    }

    private enum JumpState
    {
        None = 0,
        Approaching = 1,
        InJump = 2,
        InTransition = 3,
    }
}