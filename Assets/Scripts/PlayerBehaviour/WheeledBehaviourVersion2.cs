﻿
#pragma warning disable 649
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SKTRX.PlayerBehaviour;
using SKTRX.DTOModel;
using DG.Tweening;
using SKTRX.Enums;
using System;
using SKTRX.Managers;

public class WheeledBehaviourVersion2 : PlayerControllerBase
{
    private const float BOUNDARIES_SHOWING_DISTANCE = 2f;
    private const string BOUNDARIES_TAG = "Boundary";


    private const float MAX_TILT_ROLL_ANGLE = 120;

    [SerializeField] private GameObject _directionGameObject;

    [SerializeField] private Rigidbody _rigidBody;

    [SerializeField] private WheelCollider rearDriverW;
    [SerializeField] private WheelCollider rearPassengerW;
    [SerializeField] private WheelCollider frontDriverW;
    [SerializeField] private WheelCollider frontPassengerW;

    [SerializeField] private Transform frontDriverT, frontPassengerT;
    [SerializeField] private Transform rearDriverT, rearPassengerT;
    [SerializeField] private Transform _sphereRaycastTR;

    [SerializeField] private float _accelerationTime = .5f;
    [SerializeField] private float _jumpForce = 30;
    [SerializeField] private float maxSteerAngle = 30;
    [SerializeField] private float _fixedSteeringForce = 1;
    [SerializeField] private float _steeringAdditiveForce = .1f;
    [SerializeField] private float _steeringInput;
    [SerializeField] private float _maxSteeringInput = 1;
    [SerializeField] private float _steeringAngle;
    [SerializeField] private float _tweenSpeed = .1f;
    [SerializeField] private float _accelerationForce = 0.2f;

    [SerializeField] private float motorForce = 50;

    private Coroutine _accelerationCoroutine = null;
    private Coroutine _turningCoroutine = null;

    private float _accelerationInput;
    private Tween _pathTween = null;

    private void Steer()
    {
        _steeringAngle = maxSteerAngle * _steeringInput;
        frontDriverW.steerAngle = _steeringAngle;
        frontPassengerW.steerAngle = _steeringAngle;
    }

    private void ApplyAcceleration()
    {
        frontDriverW.motorTorque = _accelerationInput * motorForce;
        frontPassengerW.motorTorque = _accelerationInput * motorForce;
    }

    private void FixedUpdate()
    {
        RaycastHit rayHit = new RaycastHit();

        if (Physics.SphereCast(_sphereRaycastTR.position, 5, _sphereRaycastTR.forward, out rayHit, 5))
        {
            if (rayHit.transform.tag == BOUNDARIES_TAG)
            {
                if (rayHit.distance <= BOUNDARIES_SHOWING_DISTANCE)
                {
                    PlaneDetectionManager.Instance.SetActiveBoundaries(true);
                    Debug.LogError("Should show boundaries");
                }
                else
                {
                    PlaneDetectionManager.Instance.SetActiveBoundaries(false);
                }
            }
        }


        Steer();
        ApplyAcceleration();
        if (IsGrounded)
        {
            _rigidBody.angularVelocity = Vector3.zero;
        }
        if (IsUpsideDown)
        {
            ResetBoard();
        }
    }

    #region Tools

    private Vector3 LocalVelocity
    {
        get
        {
            return transform.InverseTransformDirection(_rigidBody.velocity);
        }
    }

    private bool IsGrounded
    {
        get
        {
            return (frontDriverW.isGrounded || frontPassengerW.isGrounded || rearDriverW.isGrounded || rearPassengerW.isGrounded);
        }
    }

    private Vector3[] SetPathY(Vector3[] path)
    {
        Vector3[] res = new Vector3[path.Length];
        for (int i = 0; i < res.Length; i++)
        {
            res[i] = new Vector3(path[i].x, transform.position.y, path[i].z);
        }

        return res;
    }

    private bool IsUpsideDown
    {
        get
        {
            return 
            (transform.eulerAngles.x > MAX_TILT_ROLL_ANGLE && transform.eulerAngles.x < MAX_TILT_ROLL_ANGLE + MAX_TILT_ROLL_ANGLE)
            || (transform.eulerAngles.z > MAX_TILT_ROLL_ANGLE && transform.eulerAngles.z < MAX_TILT_ROLL_ANGLE + MAX_TILT_ROLL_ANGLE);
        }
    }

    #endregion

    #region IEnumerators

    private IEnumerator AccelerateIEnumerator(float time)
    {
        float timer = time;
        _accelerationInput = 1;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        _accelerationInput = 0.0000000001f;
    }

    private IEnumerator DecelerateIEnumerator(float time)
    {
        float timer = time;
        _accelerationInput = -1;
        while (timer > 0 && LocalVelocity.z > 0)
        {
            yield return null;
        }
        _accelerationInput = 0;
    }

    #endregion

    #region EnumeratorStarters

    private void Accelerate()
    {
        _accelerationInput += _accelerationForce;
    }

    private void Decelerate()
    {
        _accelerationInput = Mathf.Clamp(_accelerationInput - _accelerationForce, 0, _accelerationInput);

    }

    #endregion

    #region PlayerBehaviourBase

    public override void StartMove()
    {
        _currentController.ChangePlayerState(PlayerState.Moving);
        Debug.LogWarning("StartMove");
        Accelerate();
        Accelerate();
        Accelerate();
    }

    protected override void Jump()
    {
        Debug.LogWarning("Jump");
        Debug.LogWarning(IsGrounded);
        if (IsGrounded)
        {
            _rigidBody.AddForce(Vector3.up * _jumpForce);
        }
    }

    protected override void TurnRight(ControlDTO data)
    {
        Debug.LogWarning("TurnRight" + "  fixed " + data.isStaticTurn);

        if (data.isStaticTurn)
        {
            _steeringInput = _fixedSteeringForce;
        }
        else
        {
            _steeringInput = Mathf.Clamp(_steeringInput + _steeringAdditiveForce, -_maxSteeringInput, _maxSteeringInput);
        }
    }

    protected override void TurnLeft(ControlDTO data)
    {
        Debug.LogWarning("TurnLeft" + "  fixed " + data.isStaticTurn);

        if (data.isStaticTurn)
        {
            _steeringInput = -_fixedSteeringForce;
        }
        else
        {
            _steeringInput = Mathf.Clamp(_steeringInput - _steeringAdditiveForce, -_maxSteeringInput, _maxSteeringInput);
        }
    }

    protected override void Turn(ControlDTO data)
    {
        // throw new System.NotImplementedException();
    }

    protected override void MoveTowards(ControlDTO data)
    {
        _currentController.ChangePlayerState(PlayerState.Moving);
        _pathTween.Kill();
        Debug.LogWarning("Star MoveTovards" + data.pointToMove.Length);
        Vector3 prevPos = transform.position;
        Vector3[] path = SetPathY(data.pointToMove);
        transform.LookAt(prevPos);
        _pathTween = transform.DOPath(path, _tweenSpeed, DG.Tweening.PathType.CatmullRom, PathMode.Sidescroller2D)
            .SetSpeedBased()
            .OnUpdate(delegate
            {
                transform.LookAt(prevPos);
                prevPos = transform.position;
            })

            .OnComplete(delegate
            {
                StopAction();
            });

        _currentController.ChangePlayerState(PlayerState.Moving);
        _directionGameObject.SetActive(false);
    }

    protected override void MoveWithSpeed(ControlDTO data)
    {
        Debug.LogWarning("MoveWithSpeed");
        Accelerate();
        Accelerate();
        Accelerate();
    }

    protected override void SetDirection(ControlDTO data)
    {
        Debug.LogWarning("SetDirection");
        float angle = Mathf.Atan2(data.direction.x, data.direction.y) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
        transform.rotation = rot;
        _directionGameObject.SetActive(true);
    }

    protected override void SetOppositeDirection(ControlDTO data)
    {
        Debug.LogWarning("SetDirection");
        transform.forward = data.direction3;
        _directionGameObject.SetActive(true);
        _directionGameObject.SetActive(true);
    }

    protected override void SpeedDown()
    {
        Debug.LogWarning("SpeedDown");
        if (IsGrounded)
        {
            Decelerate();
        }
    }

    protected override void SpeedUp()
    {
        Debug.LogWarning("SpeedUp");
        if (IsGrounded)
        {
            Accelerate();
        }
    }

    protected override void StopAction()
    {
        Debug.LogWarning("StopAction");
        if (_accelerationCoroutine != null)
        {
            StopCoroutine(_accelerationCoroutine);
        }
        _accelerationInput = 0;
        _currentController.ChangePlayerState(PlayerState.Standing);

        _currentController.StartControl();
    }

    protected override void UseFoot()
    {
        //throw new System.NotImplementedException();
    }

    protected override void UseHand()
    {
        // throw new System.NotImplementedException();
    }

    protected override void UseRail()
    {
        // throw new System.NotImplementedException();
    }

    protected override void PerformTrick(ControlDTO data)
    {
        // throw new System.NotImplementedException();
    }

    protected override void Forward()
    {
        _steeringInput = 0;
    }

    protected void ResetBoard()
    {
        transform.eulerAngles = Vector3.zero;
        Debug.LogWarning("AutoResetPlayer");
    }

    public override void ResetAll()
    {
        throw new NotImplementedException();
    }

    #endregion
}