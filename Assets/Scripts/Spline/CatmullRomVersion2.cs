﻿using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace SKTRX.Spline
{
    [System.Serializable]
    public class CatmullRomVersion2
    {
        #region Variables
        private const int MIN_SPLINE_POINTS_COUNT = 4;
        private const int MIN_RESOLUTION = 2;

        private bool _isLooped;

        private float _splineLength;
        private float _resolution;
        private float _splineStep;
        private float _lengthAccuracy = 1 / 5f;
        private float _nodeValue;

        private Vector3[] _controlPoints;
        private float[] _controlNodesLength;
        private List<Vector3> _splinePoints;
        private List<Vector3> _splineTangents;

        #endregion

        #region Properties

        public float Length { get => _splineLength; }
        public int ControlPointsCount { get => _controlPoints.Length; }
        public float NodeValue { get => _nodeValue; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CatmullRomVersion2"/> class.
        /// </summary>
        /// <param name="controlPoints">Control points.</param>
        /// <param name="resolution">Resolution.</param>
        /// <param name="isLooped">If set to <c>true</c> is looped.</param>
        public CatmullRomVersion2(Vector3[] controlPoints, int resolution, bool isLooped = false)
        {
            _splinePoints = new List<Vector3>();
            _splineTangents = new List<Vector3>();
            UpdateSpline(controlPoints, resolution, isLooped);
            _nodeValue = 1 / (_controlPoints.Length - 2);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Updates the spline with new data.
        /// </summary>
        /// <param name="controlPoints">Control points.</param>
        /// <param name="resolution">Resolution.</param>
        /// <param name="looped">If set to <c>true</c> looped.</param>
        public void UpdateSpline(Vector3[] controlPoints, int resolution, bool looped = false)
        {
            if (IsDataValid(ref controlPoints, resolution, looped))
            {
                CreateSpline();
                CalculateSplineLength();
            }
        }

        /// <summary>
        /// Gets the spline point by dist.
        /// </summary>
        /// <returns>The spline point by dist.</returns>
        /// <param name="dist">Dist.</param>
        public SplinePoint GetSplinePointByDist(float dist)
        {
            //what control point node it is
            int node = 0;

            while (dist > _controlNodesLength[node])
            {
                dist -= _controlNodesLength[node];
                node++;
                if (node > _controlNodesLength.Length - 1 || node < 0 || _controlNodesLength.Length == 0)
                {
                    Debug.LogError("ERROR");
                    // Debug.Break();
                }
            }

            //transforming distance to spline normalized value
            float splineProgress = node + (dist / _controlNodesLength[node]);

            float splineNormalizedProgress = (Utils.Remap(splineProgress, 0, (_controlPoints.Length - 3), 0, 1));

            // returning common spline point
            Vector3 pointPosition = GetSplinePosition(splineProgress);
            Vector3 pointTangent = GetSplineTangent(splineProgress);

            return new SplinePoint(pointPosition, pointTangent, splineNormalizedProgress, dist, node);
        }

        public SplinePoint GetSplinePointByProgress(float splineNormalizedProgress)
        {
            //remapping progress fron (0,1) to the spline progress
            float splineProgress = (Utils.Remap(splineNormalizedProgress, 0, 1, 0, (_controlPoints.Length - 3)));

            //getting influence node index
            int node = Mathf.FloorToInt(splineProgress);

            //calculating progress at segment the poin is
           // float segmentProgress = splineProgress - node;

            //calculating distance of the spline at current progress
            //getting distace of all integer segments
            float dist = 0;
            for (int i = 0; i < node - 1; i++)
            {
                dist += _controlNodesLength[node];
            }

            //adding current segment progress length
            dist += (splineProgress - node) * _controlNodesLength[node];

            // returning common spline point
            Vector3 pointPosition = GetSplinePosition(splineProgress);
            Vector3 pointTangent = GetSplineTangent(splineProgress);

            return new SplinePoint(pointPosition, pointTangent, splineNormalizedProgress, dist, node);
        }

        public void DrawSpline(Color color)
        {
            for (int i = 0; i < _splinePoints.Count; i++)
            {
                if (i < _splinePoints.Count - 1)
                {
                    Debug.DrawLine(_splinePoints[i], _splinePoints[i + 1], color);
                }
            }
        }

        public void DrawTangent(Color color)
        {
            for (int i = 0; i < _splineTangents.Count; i++)
            {
                if (i < _splineTangents.Count - 1)
                {
                    Debug.DrawRay(_splinePoints[i],Quaternion.Euler(Vector3.up * 47f)*_splineTangents[i],color,.2f);
                }
            }
        }
        #endregion

        #region Private Methods

        private Vector3 GetSplineTangent(float t)
        {
            int p0, p1, p2, p3;

            //defining points of influence (p0,p1,p2,p3)
            if (!_isLooped)
            {
                p1 = (int)t + 1;
                p2 = p1 + 1;
                p3 = p2 + 1;
                p0 = p1 - 1;
            }
            else
            {
                p1 = (int)t;
                p2 = (p1 + 1) % _controlPoints.Length;
                p3 = (p2 + 1) % _controlPoints.Length;
                p0 = p1 >= 1 ? p1 - 1 : _controlPoints.Length - 1;
            }

            //subtracting int value from t to get the normalized value of the point pos
            t = t - (int)t;

            //cache tˆ2 and tˆ3 in advance;
            float tt = t * t;
            float ttt = tt * t;

            //calculating the value of influence by formulas
            float q1 = -3.0f * tt + 4.0f * t - 1;
            float q2 = 9.0f * tt - 10.0f * t;
            float q3 = -9.0f * tt + 8.0f * t + 1.0f;
            float q4 = 3.0f * tt - 2.0f * t;

            float influenceCoefitient = .5f;

            //applying the points of influence for the points
            float x = influenceCoefitient * (_controlPoints[p0].x * q1 + _controlPoints[p1].x * q2 + _controlPoints[p2].x * q3 + _controlPoints[p3].x * q4);
            float y = influenceCoefitient * (_controlPoints[p0].y * q1 + _controlPoints[p1].y * q2 + _controlPoints[p2].y * q3 + _controlPoints[p3].y * q4);
            float z = influenceCoefitient * (_controlPoints[p0].z * q1 + _controlPoints[p1].z * q2 + _controlPoints[p2].z * q3 + _controlPoints[p3].z * q4);

            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Calculates the length of the specific node.
        /// </summary>
        /// <returns>The segment length.</returns>
        /// <param name="node">Node.</param>
        private float CalculateNodeLength(int node)
        {
            float segmentLength = 0.0f;

            Vector3 prevPoint;
            Vector3 nextPoint;

            prevPoint = GetSplinePosition(node);

            //iterates between two points with a steep of _lengthAccuracy
            for (float step = _lengthAccuracy; step <= 1; step += .05f)
            {
                nextPoint = GetSplinePosition(node + step);
                segmentLength += Vector3.Distance(nextPoint, prevPoint);
                prevPoint = nextPoint;
            }

            return segmentLength;
        }

        /// <summary>
        /// Gets the spline point.
        /// </summary>
        /// <returns>The spline point.</returns>
        /// <param name="t">T.</param>
        private Vector3 GetSplinePosition(float t)
        {
            int p0, p1, p2, p3;

            //defining points of influence (p0,p1,p2,p3)
            if (!_isLooped)
            {
                p1 = (int)t + 1;
                p2 = p1 + 1;
                p3 = p2 + 1;
                p0 = p1 - 1;
            }

            else
            {
                p1 = (int)t;
                p2 = (p1 + 1) % _controlPoints.Length;
                p3 = (p2 + 1) % _controlPoints.Length;
                p0 = p1 >= 1 ? p1 - 1 : _controlPoints.Length - 1;
            }

            //subtracting int value from t to get the normalized value of the point pos
            t = t - (int)t;

            //cache tˆ2 and tˆ3 in advance;
            float tt = t * t;
            float ttt = tt * t;

            //calculating the value of influence by formulas
            float q1 = -ttt + 2.0f * tt - t;
            float q2 = 3.0f * ttt - 5.0f * tt + 2.0f;
            float q3 = -3.0f * ttt + 4.0f * tt + t;
            float q4 = ttt - tt;

            if (p3 == _controlPoints.Length)
            {
                Debug.LogError(1);
                Debug.Break();
            }

            float influenceCoefitient = .5f;

            //applying the points of influence for the points
            float x = influenceCoefitient * (_controlPoints[p0].x * q1 + _controlPoints[p1].x * q2 + _controlPoints[p2].x * q3 + _controlPoints[p3].x * q4);
            float y = influenceCoefitient * (_controlPoints[p0].y * q1 + _controlPoints[p1].y * q2 + _controlPoints[p2].y * q3 + _controlPoints[p3].y * q4);
            float z = influenceCoefitient * (_controlPoints[p0].z * q1 + _controlPoints[p1].z * q2 + _controlPoints[p2].z * q3 + _controlPoints[p3].z * q4);

            return new Vector3(x, y, z);
        }
        /// <summary>
        /// Tells if the data is valid. If it is applies the values
        /// </summary>
        /// <returns><c>true</c>, if data valid was ised, <c>false</c> otherwise.</returns>
        /// <param name="controlPoints">Control points.</param>
        /// <param name="resolution">Resolution.</param>
        /// <param name="looped">If set to <c>true</c> looped.</param>
        private bool IsDataValid(ref Vector3[] controlPoints, int resolution, bool looped = false)
        {
            if (controlPoints == null || controlPoints.Length < MIN_SPLINE_POINTS_COUNT || resolution < MIN_RESOLUTION)
            {
                if (controlPoints == null)
                    throw new System.Exception("CutmullRomVersion2: Invalid spline input data. controlPoints == null");
                if (resolution < MIN_RESOLUTION)
                    throw new System.Exception("CutmullRomVersion2: Invalid spline input data. resolution < MIN_RESOLUTION");
            }

            _controlPoints = controlPoints.CopyArray();
            _resolution = resolution;
            _isLooped = looped;
            _splineStep = 1 / (_resolution);

            return true;
        }

        /// <summary>
        /// Calculates the length of the spline.
        /// </summary>
        private void CalculateSplineLength()
        {
            //clear current length;
            _splineLength = 0;
            _controlNodesLength = new float[_controlPoints.Length];

            //iterates through each points 
            for (int i = 0; i < _controlPoints.Length - 3; i++)
            {
                _controlNodesLength[i] = CalculateNodeLength(i);
                _splineLength += _controlNodesLength[i];
            }
        }

        /// <summary>
        /// Creates the spline.
        /// </summary>
        private void CreateSpline()
        {
            _splinePoints.Clear();
            for (float t = 0; t < _controlPoints.Length - 3; t += _splineStep)
            {
                _splinePoints.Add(GetSplinePosition(t));
                _splineTangents.Add(GetSplineTangent(t));
            }
        }
        #endregion
    }

    /// <summary>
    /// Spline point struct.
    /// </summary>
    public struct SplinePoint
    {
        public Vector3 position;
        public Vector3 tangent;
        public readonly float progress;
        public readonly float length;
        public readonly int pointNode;

        public SplinePoint(Vector3 pos, Vector3 tan, float prog, float leng, int controlPointOfInfluence)
        {
            position = pos;
            tangent = tan;
            progress = prog;
            length = leng;
            pointNode = controlPointOfInfluence;
        }

        public string GetInfo()
        {
            return "pos: " + position.ToString() + "   forward: " + tangent + "  length: " + length + "  node: " + pointNode;
        }
    }
}