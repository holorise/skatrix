﻿using System;
using Tools;
using UnityEngine;
using System.Linq;
using SKTRX.DTOModel;

namespace SKTRX.Spline
{
    /*  
        Hermite curve formula:
        (2t^3 - 3t^2 + 1) * p0 + (t^3 - 2t^2 + t) * m0 + (-2t^3 + 3t^2) * p1 + (t^3 - t^2) * m1
        For points p0 and p1 passing through points m0 and m1 interpolated over t = [0, 1]
        Tangent M[k] = (P[k+1] - P[k-1]) / 2
    */

    [Serializable]
    public class CatmullRom
    {
        [System.Serializable]
        public struct CatmullRomPoint
        {
            public Vector3 position;
            public Vector3 tangent;
            public Vector3 normal;

            public CatmullRomPoint(Vector3 position, Vector3 tangent, Vector3 normal)
            {
                this.position = position;
                this.tangent = tangent;
                this.normal = normal;
            }
        }

        public int Resolution
        {
            get { return _resolution; }
            set { _resolution = value; }
        }

        private int _resolution; //Amount of points between control points. Tesselation factor
        private bool _closedLoop;
        private bool _drawSpline = false;
        private int _length = 0;

        [SerializeField] private CatmullRomPoint[] _splinePoints;

        private Vector3[] _controlPoints;

        public int Length
        {
            get { return _length; }
        }

        /// <summary>
        /// Returns spline points. Count is contorolPoints * resolution + [resolution] points if closed loop.
        /// </summary>
        /// <returns>The points.</returns>
        public CatmullRomPoint[] GetPoints()
        {
            if(_splinePoints == null)
            {
                throw new System.NullReferenceException("Spline is not Initialized!");
            }

            return _splinePoints;
        }

        public Vector3[] GetPointsV3()
        {
            if (_splinePoints == null)
            {
                throw new System.NullReferenceException("Spline is not Initialized!");
            }

            return _splinePoints.Select(point=>point.position).ToArray();
        }

        public CatmullRom(PathPointDTO[] controlPoints, int resolution, bool closedLoop, bool drawSpline = true)
        {
            _controlPoints = new Vector3[controlPoints.Length];
            for(int i=0;i< controlPoints.Length; i++)
            {
                _controlPoints[i] = controlPoints[i].Point;
            }
            _resolution = resolution;
            _closedLoop = closedLoop;

            GenerateSplinePoints();
            _drawSpline = drawSpline;
        }

        public CatmullRom(Vector3[] controlPoints, int resolution, bool closedLoop, bool drawSpline = true)
        {
            if (controlPoints == null || controlPoints.Length <= 2 || resolution < 2)
            {
                throw new ArgumentException("Catmull Rom Error: Too few control points or resolution too small");
            }
            _controlPoints = new Vector3[controlPoints.Length];
            for (int i = 0; i < controlPoints.Length; i++)
            {
                _controlPoints[i] = controlPoints[i];
            }

            _resolution = resolution;
            _closedLoop = closedLoop;

            GenerateSplinePoints();
            _drawSpline = drawSpline;
        }
        /// <summary>
        /// Initializes a new instance of the CatmullRom class.
        /// </summary>
        /// <param name="controlPoints">Control points.</param>
        /// <param name="resolution">Resolution.</param>
        /// <param name="closedLoop">If set to <c>true</c> closed loop.</param>
        public CatmullRom(Transform[] controlPoints, int resolution, bool closedLoop)
        {
            if(controlPoints == null || controlPoints.Length <= 2 || resolution < 2)
            {
                throw new ArgumentException("Catmull Rom Error: Too few control points or resolution too small");
            }

            _controlPoints = new Vector3[controlPoints.Length];
            for(int i = 0; i < controlPoints.Length; i++)
            {
                _controlPoints[i] = controlPoints[i].position;             
            }

            _resolution = resolution;
            _closedLoop = closedLoop;

            GenerateSplinePoints();
        }

        /// <summary>
        /// Updates control points.
        /// </summary>
        /// <param name="controlPoints">Control points.</param>
        public void Update(Vector3[] controlPoints)
        {
            if(controlPoints.Length <= 0 || controlPoints == null)
            {
                throw new ArgumentException("Invalid control points");
            }

            _controlPoints = new Vector3[controlPoints.Length];
            for(int i = 0; i < controlPoints.Length; i++)
            {
                _controlPoints[i] = controlPoints[i];             
            }

            GenerateSplinePoints();
        }

        public void Update(Transform[] controlPoints)
        {
            Update(controlPoints.Select(tr => tr.position).ToArray());
        }

        /// <summary>
        /// Updates resolution and closed loop values.
        /// </summary>
        /// <param name="resolution">Resolution.</param>
        /// <param name="closedLoop">If set to <c>true</c> closed loop.</param>
        public void Update(int resolution, bool closedLoop)
        {
            if(resolution < 2)
            {
                throw new ArgumentException("Invalid Resolution. Make sure it's >= 1");
            }
            _resolution = resolution;
            _closedLoop = closedLoop;

            GenerateSplinePoints();
        }

        /// <summary>
        /// Draws a line between every point and the next one.
        /// </summary>
        /// <param name="color">Color.</param>
        public void DrawSpline(Color color)
        {
            if(_drawSpline && IsPointsValid())
            {
                for(int i = 0; i < _splinePoints.Length; i++)
                {
                    if(i == _splinePoints.Length - 1 && _closedLoop)
                    {
                        Debug.DrawLine(_splinePoints[i].position, _splinePoints[0].position, color);
                    }                
                    else if(i < _splinePoints.Length - 1)
                    {
                        Debug.DrawLine(_splinePoints[i].position, _splinePoints[i+1].position, color);
                    }
                }
            }
        }

        /// <summary>
        /// Draws the normals.
        /// </summary>
        /// <param name="extrusion">Extrusion.</param>
        /// <param name="color">Color.</param>
        public void DrawNormals(float extrusion, Color color)
        {
            if(IsPointsValid())
            {
                for(int i = 0; i < _splinePoints.Length; i++)
                {
                    Debug.DrawLine(_splinePoints[i].position, _splinePoints[i].position + _splinePoints[i].normal * extrusion, color);
                }
            }
        }

        /// <summary>
        /// Draws the tangents.
        /// </summary>
        /// <param name="extrusion">Extrusion.</param>
        /// <param name="color">Color.</param>
        public void DrawTangents(float extrusion, Color color)
        {
            if(IsPointsValid())
            {
                for(int i = 0; i < _splinePoints.Length; i++)
                {
                    Debug.DrawLine(_splinePoints[i].position, _splinePoints[i].position + _splinePoints[i].tangent * extrusion, color);
                }
            }
        }

        /// <summary>
        /// Validates if splinePoints have been set already. Throws nullref exception.
        /// </summary>
        /// <returns><c>true</c>, if points was validated, <c>false</c> otherwise.</returns>
        private bool IsPointsValid()
        {
            if(_splinePoints == null)
            {
                throw new NullReferenceException("Spline not initialized!");
            }
            return true;
        }

        /// <summary>
        /// Sets the length of the point array based on resolution/closed loop.
        /// </summary>
        private void InitializeProperties()
        {
            int pointsToCreate;
            if (_closedLoop)
            {
                //Loops back to the beggining, so no need to adjust arrays starting at 0
                pointsToCreate = _resolution * _controlPoints.Length;
            }
            else
            {
                pointsToCreate = _resolution * (_controlPoints.Length - 1);
            }

            _splinePoints = new CatmullRomPoint[pointsToCreate];
            _length = _splinePoints.Length;
        }

        /// <summary>
        /// Math stuff to generate the spline points
        /// </summary>
        private void GenerateSplinePoints()
        {
            InitializeProperties();

            Vector3 p0; //Start point
            Vector3 p1; //end point

            //Tangents
            Vector3 m0;
            Vector3 m1; 

            // First loop goes through each individual control point and connects it to the next one: 0-1, 1-2, 2-3...
            int closedAdjustment = _closedLoop ? 0 : 1;
            for (int currentPoint = 0; currentPoint < _controlPoints.Length - closedAdjustment; currentPoint++)
            {
                bool closedLoopFinalPoint = (_closedLoop && currentPoint == _controlPoints.Length - 1);

                p0 = _controlPoints[currentPoint];
                
                if(closedLoopFinalPoint)
                {
                    p1 = _controlPoints[0];
                }
                else
                {
                    p1 = _controlPoints[currentPoint + 1];
                }

                // m0
                if (currentPoint == 0) // Tangent M[k] = (P[k+1] - P[k-1]) / 2
                {
                    if(_closedLoop)
                    {
                        m0 = p1 - _controlPoints[_controlPoints.Length - 1];
                    }
                    else
                    {
                        m0 = p1 - p0;
                    }
                }
                else
                {
                    m0 = p1 - _controlPoints[currentPoint - 1];
                }

                // m1
                if (_closedLoop)
                {
                    if (currentPoint == _controlPoints.Length - 1) //Last point case
                    {
                        m1 = _controlPoints[(currentPoint + 2) % _controlPoints.Length] - p0;
                    }
                    else if (currentPoint == 0) //First point case
                    {
                        m1 = _controlPoints[currentPoint + 2] - p0;
                    }
                    else
                    {
                        m1 = _controlPoints[(currentPoint + 2) % _controlPoints.Length] - p0;
                    }
                }
                else
                {
                    if (currentPoint < _controlPoints.Length - 2)
                    {
                        m1 = _controlPoints[(currentPoint + 2) % _controlPoints.Length] - p0;
                    }
                    else
                    {
                        m1 = p1 - p0;
                    }
                }

                m0 *= 0.01f; //Doing this here instead of  in every single above statement
                m1 *= 0.01f;

                float pointStep = 1.0f / _resolution;

                if ((currentPoint == _controlPoints.Length - 2 && !_closedLoop) || closedLoopFinalPoint) //Final point
                {
                    pointStep = 1.0f / (_resolution - 1);  // last point of last segment should reach p1
                }

                // Creates [resolution] points between this control point and the next
                for (int tesselatedPoint = 0; tesselatedPoint < _resolution; tesselatedPoint++)
                {
                    float t = tesselatedPoint * pointStep;

                    CatmullRomPoint point = Evaluate(p0, p1, m0, m1, t);

                    _splinePoints[currentPoint * _resolution + tesselatedPoint] = point;
                }
            }
        }

        /// <summary>
        /// Evaluates curve at t[0, 1]. Returns point/normal/tan struct. [0, 1] means clamped between 0 and 1.
        /// </summary>
        /// <returns>The evaluate.</returns>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        /// <param name="tanPoint1">Tan point1.</param>
        /// <param name="tanPoint2">Tan point2.</param>
        /// <param name="t">T.</param>
        public static CatmullRomPoint Evaluate(Vector3 start, Vector3 end, Vector3 tanPoint1, Vector3 tanPoint2, float t)
        {
            Vector3 position = CalculatePosition(start, end, tanPoint1, tanPoint2, t);
            Vector3 tangent = CalculateTangent(start, end, tanPoint1, tanPoint2, t);            
            Vector3 normal = NormalFromTangent(tangent);

            return new CatmullRomPoint(position, tangent, normal);
        }

        /// <summary>
        /// Calculates curve position at t[0, 1]
        /// </summary>
        /// <returns>The position.</returns>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        /// <param name="tanPoint1">Tan point1.</param>
        /// <param name="tanPoint2">Tan point2.</param>
        /// <param name="t">T.</param>
        public static Vector3 CalculatePosition(Vector3 start, Vector3 end, Vector3 tanPoint1, Vector3 tanPoint2, float t)
        {
            // (2t^3 - 3t^2 + 1) * p0 + (t^3 - 2t^2 + t) * m0 + (-2t^3 + 3t^2) * p1 + (t^3 - t^2) * m1
            Vector3 position = (2.0f * t * t * t - 3.0f * t * t + 1.0f) * start
                + (t * t * t - 2.0f * t * t + t) * tanPoint1
                + (-2.0f * t * t * t + 3.0f * t * t) * end
                + (t * t * t - t * t) * tanPoint2;

            return position;
        }

        /// <summary>
        /// Calculates tangent at t[0, 1]
        /// </summary>
        /// <returns>The tangent.</returns>
        /// <param name="start">Start.</param>
        /// <param name="end">End.</param>
        /// <param name="tanPoint1">Tan point1.</param>
        /// <param name="tanPoint2">Tan point2.</param>
        /// <param name="t">T.</param>
        public static Vector3 CalculateTangent(Vector3 start, Vector3 end, Vector3 tanPoint1, Vector3 tanPoint2, float t)
        {
            // p'(t) = (6t² - 6t)p0 + (3t² - 4t + 1)m0 + (-6t² + 6t)p1 + (3t² - 2t)m1
            Vector3 tangent = (6 * t * t - 6 * t) * start
                + (3 * t * t - 4 * t + 1) * tanPoint1
                + (-6 * t * t + 6 * t) * end
                + (3 * t * t - 2 * t) * tanPoint2;

            return tangent.normalized;
        }

        /// <summary>
        /// Calculates normal vector from tangent
        /// </summary>
        /// <returns>The from tangent.</returns>
        /// <param name="tangent">Tangent.</param>
        public static Vector3 NormalFromTangent(Vector3 tangent)
        {
            return Vector3.Cross(tangent, Vector3.up).normalized * 0.5f;
        }        

        /// <summary>
        /// Gets the position by progress.
        /// </summary>
        /// <returns>The position by progress.</returns>
        /// <param name="progress">Progress.</param>
        public Vector3 GetPositionByProgress(float progress)
        {
            int _carretIndexPosition = (int)Utils.Remap(progress, 0, 1, 0, GetPoints().Length - 1);
            return GetPoints()[_carretIndexPosition].position;
        }

        /// <summary>
        /// Gets the forward by progress.
        /// </summary>
        /// <returns>The forward by progress.</returns>
        /// <param name="progress">Progress.</param>
        public Vector3 GetForwardByProgress(float progress)
        {
            int _carretIndexPosition = (int)Utils.Remap(progress, 0, 1, 0, GetPoints().Length - 1);
           return GetPoints()[_carretIndexPosition].tangent;
        }
    }
}