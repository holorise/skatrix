using System.Collections.Generic;
using UnityEngine;
using Tools;
using SKTRX.Spline;

[ExecuteInEditMode]
public class SplineTester : MonoBehaviour
{
    [SerializeField] private CatmullRom _spline;

    [SerializeField] private Transform[] _controlPoints;
    [SerializeField] private Transform _pathCarret;
    [SerializeField] private Vector3 _pathCarretTang;

    [Range(0.0f, 1.0f)]
    [SerializeField] private float _carretPathValue;

    [Range(2, 25)]
    [SerializeField] private int _resolution;

    [Range(0, 20)]
    [SerializeField] private float _normalExtrusion;

    [Range(0, 20)]
    [SerializeField] private float _tangentExtrusion;

    [SerializeField] private bool _isClosedLoop;
    [SerializeField] private bool drawNormals;
    [SerializeField] private bool drawTangents;
    [SerializeField] private bool drawSpline;

    void Start()
    {
        if (_spline == null)
        {
            _spline = new CatmullRom(_controlPoints, _resolution, _isClosedLoop);
        }
    }

    void Update()
    {
        if (_spline != null)
        {
            _spline.Update(_controlPoints);
            _spline.Update(_resolution, _isClosedLoop);
            if(drawSpline)
                _spline.DrawSpline(Color.white);

            if (drawNormals)
                _spline.DrawNormals(_normalExtrusion, Color.red);

            if (drawTangents)
                _spline.DrawTangents(_tangentExtrusion, Color.cyan);
        }

        else
        {
            _spline = new CatmullRom(_controlPoints, _resolution, _isClosedLoop);
        }


        if (_pathCarret == null)
        {
            _pathCarret = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            _pathCarret.SetParent(transform);
            _pathCarret.localScale = Vector3.one * 5;
        }

        int _carretIndexPosition = (int)Utils.Remap(_carretPathValue, 0, 1, 0, _spline.GetPoints().Length - 1);
        _pathCarret.position = _spline.GetPoints()[_carretIndexPosition].position;
        _pathCarret.forward = _spline.GetPoints()[_carretIndexPosition].tangent;
    }
}
